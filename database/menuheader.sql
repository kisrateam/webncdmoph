-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2020 at 11:50 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ncdstrategy`
--

--
-- Dumping data for table `menu_header`
--

INSERT INTO `menu_header` (`id`, `level`, `parent_id`, `parent_index`, `name`, `link`, `type`, `active`, `css`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 'down', NULL, NULL, 'รู้ทันโรค', '', 'menu', 1, NULL, '2020-10-29 11:49:49', '2020-10-29 11:49:49', NULL),
(2, 'down', 1, 1, 'รายชื่อโรค', 'https://ddc.moph.go.th/disease.php', 'link', 1, NULL, '2020-10-29 11:50:07', '2020-10-30 06:54:35', NULL),
(3, 'up', NULL, NULL, 'งานบริการต่างๆ', 'https://ddc.moph.go.th/ddcportal/', 'menulink', 1, NULL, '2020-11-30 08:19:46', '2020-11-30 08:19:46', NULL),
(26, 'down', NULL, NULL, 'เกี่ยวกับกรม', '', 'menu', 1, NULL, '2020-10-08 07:45:33', '2020-10-08 07:45:33', NULL),
(28, 'down', NULL, NULL, 'กฏหมายที่เกี่ยวข้อง', '', 'menu', 1, NULL, '2020-10-08 07:46:54', '2020-10-08 07:46:54', NULL),
(29, 'down', NULL, NULL, 'ข่าวจัดซื้อจัดจ้าง', '', 'menu', 1, NULL, '2020-10-08 07:47:13', '2020-10-08 07:47:13', NULL),
(30, 'down', NULL, NULL, 'ข่าวสาร', '', 'menu', 1, NULL, '2020-10-08 07:47:33', '2020-10-08 07:47:33', NULL),
(31, 'down', NULL, NULL, 'คลังข้อมูล', '', 'menu', 1, NULL, '2020-10-08 07:50:00', '2020-10-08 07:50:00', NULL),
(32, 'down', NULL, NULL, 'หน่วยงานในสังกัด', '', 'menu', 1, NULL, '2020-10-08 07:50:15', '2020-10-08 07:50:15', NULL),
(33, 'down', NULL, NULL, 'รายชื่อโรค', 'https://ddc.moph.go.th/disease.php', 'menu', 1, 'background-color:#179c9b;\r\ncolor:white;\r\nborder-radius: 10px 10px 0 0;\r\nheight:100%;', '2020-10-08 07:53:35', '2020-10-08 07:53:35', NULL),
(35, 'up', NULL, NULL, 'รับเรื่องร้องเรียน', 'https://ddc.moph.go.th/complaint.php', 'menulink', 1, NULL, '2020-10-08 08:13:20', '2020-10-08 08:13:20', NULL),
(36, 'up', NULL, NULL, 'ถาม-ตอบ', '/home/topic', 'menulink', 1, NULL, '2020-10-08 08:14:05', '2020-10-08 08:14:05', NULL),
(37, 'up', NULL, NULL, 'ผังเว็บไซต์', '/', 'menulink', 1, NULL, '2020-10-08 08:14:25', '2020-10-08 08:14:25', NULL),
(43, 'down', 1, 2, 'สื่อเผยแพร่', NULL, 'havelink', 1, NULL, '2020-10-29 11:47:04', '2020-10-30 08:51:21', NULL),
(48, 'up', 26, 1, 'ประวัติกรม', 'https://ddc.moph.go.th/history.php', 'link', 1, NULL, '2020-10-30 07:30:49', '2020-10-30 07:30:49', NULL),
(49, 'down', 26, 2, 'โครงสร้างกรม', 'https://ddc.moph.go.th/organization.php', 'link', 1, NULL, '2020-10-30 07:34:04', '2020-10-30 07:34:04', NULL),
(50, 'down', 26, 3, 'ผู้บริหารกรมควบคุมโรค', 'https://ddc.moph.go.th/executive.php', 'link', 1, NULL, '2020-10-30 07:34:44', '2020-10-30 07:34:44', NULL),
(51, 'down', 26, 4, 'กฏกระทรวงการแบ่งส่วนราชการ', 'https://ddc.moph.go.th/uploads/ckeditor2/ethg/file/ministerial_ddc.pdf', 'link', 1, NULL, '2020-10-30 07:35:38', '2020-10-30 07:35:38', NULL),
(52, 'down', 26, 5, 'คำรับรอง', NULL, 'havelink', 1, NULL, '2020-10-30 07:35:59', '2020-10-30 07:36:44', NULL),
(53, 'down', 26, 6, 'ติดต่อกรม', 'https://ddc.moph.go.th/contact.php', 'link', 1, NULL, '2020-10-30 07:36:19', '2020-10-30 07:36:19', NULL),
(54, 'down', 52, 1, 'ระดับกรมควบคุมโรค', 'https://ddc.moph.go.th/agreement.php?1', 'linkinlink', 1, NULL, '2020-10-30 07:36:44', '2020-10-30 07:36:44', NULL),
(55, 'down', 52, 2, 'ผลคะแนนระดับกรมควบคุมโรค', 'https://ddc.moph.go.th/agreement.php?2', 'linkinlink', 1, NULL, '2020-10-30 07:47:48', '2020-10-30 07:47:48', NULL),
(56, 'down', 52, 3, 'ระดับหน่วยงาน', 'https://ddc.moph.go.th/agreement.php?3', 'linkinlink', 1, NULL, '2020-10-30 07:48:22', '2020-10-30 07:48:22', NULL),
(57, 'down', 28, 1, 'พ.ร.บ. โรคติดต่อ 2558', 'https://ddc.moph.go.th/law.php?law=1', 'link', 1, NULL, '2020-10-30 07:49:10', '2020-10-30 07:49:10', NULL),
(58, 'down', 28, 2, 'พ.ร.บ. ควบคุมพลิตภัณฑ์ยาสูบ 2560', 'https://ddc.moph.go.th/law.php?law=2', 'link', 1, NULL, '2020-10-30 07:50:18', '2020-10-30 07:50:18', NULL),
(59, 'down', 28, 3, 'พ.ร.บ. ควบคุมเครื่องดื่มแอลกอฮอล์ 2551', 'https://ddc.moph.go.th/law.php?law=3', 'link', 1, NULL, '2020-10-30 07:50:59', '2020-10-30 07:50:59', NULL),
(60, 'down', 28, 3, 'พ.ร.บ. ควบคุมโรคจากการประกอบอาชีพและโรคจากสิ่งแวดล้อม พ.ศ. 2562', 'https://ddc.moph.go.th/law.php?law=5', 'link', 1, NULL, '2020-10-30 07:52:01', '2020-10-30 07:52:01', NULL),
(61, 'down', 52, 4, 'ผลคะแนนระดับหน่วยงาน', 'https://ddc.moph.go.th/agreement.php?4', 'linkinlink', 1, NULL, '2020-10-30 07:53:46', '2020-10-30 07:53:46', NULL),
(62, 'down', 29, 1, 'แผนการจัดซื้อจัดจ้าง', 'https://ddc.moph.go.th/auctions.php?auctions_plan', 'link', 1, NULL, '2020-10-30 07:54:17', '2020-10-30 07:54:17', NULL),
(63, 'down', 29, 2, 'ประกาศการจัดซื้อ-จัดจ้าง', 'https://ddc.moph.go.th/auctions.php?auctions_announ', 'link', 1, NULL, '2020-10-30 07:54:46', '2020-10-30 07:54:46', NULL),
(64, 'down', 29, 3, 'ประกาศร่าง TOR', 'https://ddc.moph.go.th/auctions.php?auctions_draft', 'link', 1, NULL, '2020-10-30 08:33:17', '2020-10-30 08:33:17', NULL),
(65, 'down', 29, 4, 'การเผยแพร่ราคากลางตามแบบของสำนัก ป.ป.ช.', 'https://ddc.moph.go.th/auctions.php?auctions_public', 'link', 1, NULL, '2020-10-30 08:33:48', '2020-10-30 08:33:48', NULL),
(66, 'down', 29, 5, 'สรุปผลการจัดซื้อจ้าง', 'https://ddc.moph.go.th/auctions.php?auctions_result', 'link', 1, NULL, '2020-10-30 08:34:16', '2020-10-30 08:34:16', NULL),
(67, 'down', 29, 6, 'ศูนย์ราคากลาง กรมควบคุมโรค', 'https://ddc.moph.go.th/auctions.php?auctions_center', 'link', 1, NULL, '2020-10-30 08:34:43', '2020-10-30 08:34:43', NULL),
(68, 'down', 29, 7, 'ข้อมูลสาระสำคัญในสัญญา', 'https://ddc.moph.go.th/auctions.php?auctions_contract', 'link', 1, NULL, '2020-10-30 08:35:11', '2020-10-30 08:35:11', NULL),
(69, 'down', 29, 8, 'รายงานผลการจัดซื้อจัดจ้าง', 'https://ddc.moph.go.th/auctionssum.php', 'link', 1, NULL, '2020-10-30 08:35:39', '2020-10-30 08:35:39', NULL),
(70, 'down', 30, 1, 'ข่าวเพิ่มสื่อมวลชน', 'https://ddc.moph.go.th/newsmass.php', 'link', 1, NULL, '2020-10-30 08:36:04', '2020-10-30 08:36:04', NULL),
(71, 'down', 30, 2, 'ข่าวรับสมัครบุคลากร', 'https://ddc.moph.go.th/newshr.php', 'link', 1, NULL, '2020-10-30 08:36:24', '2020-10-30 08:36:24', NULL),
(72, 'down', 30, 2, 'ข่าวประชาสัมพันธ์', 'https://ddc.moph.go.th/newspic.php', 'link', 1, NULL, '2020-10-30 08:36:45', '2020-10-30 08:36:45', NULL),
(73, 'down', 30, 4, 'วิดีโอข่าวประชาสัมพันธ์', 'https://ddc.moph.go.th/newsvdo.php', 'link', 1, NULL, '2020-10-30 08:37:10', '2020-10-30 08:37:10', NULL),
(74, 'down', 30, 5, 'ข่าวกิจกรรม', 'https://ddc.moph.go.th/newsactivity.php', 'link', 1, NULL, '2020-10-30 08:37:30', '2020-10-30 08:37:30', NULL),
(75, 'down', 31, 1, 'ฐานข้อมูลงานวิจัย', 'http://klb.ddc.moph.go.th/dataentry/research', 'link', 1, NULL, '2020-10-30 08:37:54', '2020-10-30 08:37:54', NULL),
(76, 'down', 31, 2, 'ฐานข้อมูล KM หน่วยงาน', 'http://klb.ddc.moph.go.th/dataentry/knowledge', 'link', 1, NULL, '2020-10-30 08:38:14', '2020-10-30 08:38:14', NULL),
(77, 'down', 31, 3, 'ฐานข้อมูลนวัตกรรม', 'http://klb.ddc.moph.go.th/dataentry/inovation', 'link', 1, NULL, '2020-10-30 08:38:55', '2020-10-30 08:38:55', NULL),
(78, 'down', 31, 4, 'ฐานข้อมูลวิชาการ', 'http://klb.ddc.moph.go.th/dataentry/handbook', 'link', 1, NULL, '2020-10-30 08:39:09', '2020-10-30 08:39:09', NULL),
(79, 'down', 31, 5, 'ฐานข้อมูลทำเนียบนักวิจัย', 'http://klb.ddc.moph.go.th/dataentry/researcher', 'link', 1, NULL, '2020-10-30 08:39:36', '2020-10-30 08:39:36', NULL),
(80, 'down', 31, 6, 'ฐานข้อมูลทำเนียบนักวิจัย', 'http://klb.ddc.moph.go.th/dataentry/consultant', 'link', 1, NULL, '2020-10-30 08:40:02', '2020-10-30 08:40:02', NULL),
(81, 'down', 31, 7, 'อำเภอควบคุมโรคเข้มแข็ง', 'http://irem2.ddc.moph.go.th/page/269', 'link', 1, NULL, '2020-10-30 08:40:26', '2020-10-30 08:40:26', NULL),
(82, 'down', 32, 1, 'หน่วยงานส่วนกลาง', 'https://ddc.moph.go.th/office.php?central', 'link', 1, NULL, '2020-10-30 08:40:48', '2020-10-30 08:40:48', NULL),
(83, 'down', 32, 2, 'หน่วยงานส่วนภูมิภาค', 'https://ddc.moph.go.th/office.php?provincial', 'link', 1, NULL, '2020-10-30 08:41:08', '2020-10-30 08:41:08', NULL),
(84, 'down', 43, 1, 'สื่อมัลติมีเดีย', 'https://ddc.moph.go.th/publishvdo.php?vdo', 'linkinlink', 1, NULL, '2020-10-30 08:51:21', '2020-10-30 08:51:21', NULL),
(85, 'down', 43, 2, 'สื่อโทรทัศน์', 'https://ddc.moph.go.th/publishvdo.php?television', 'linkinlink', 1, NULL, '2020-10-30 08:51:44', '2020-10-30 08:51:44', NULL),
(86, 'down', 43, 3, 'สื่อวิทยุ', 'https://ddc.moph.go.th/publishvdo.php?radio', 'linkinlink', 1, NULL, '2020-10-30 08:52:00', '2020-10-30 08:52:00', NULL),
(87, 'down', 43, 4, 'วารสารออนไลน์', 'https://ddc.moph.go.th/publishbook.php?journal', 'linkinlink', 1, NULL, '2020-10-30 08:52:24', '2020-10-30 08:52:24', NULL),
(88, 'down', 43, 5, 'อินโฟกราฟฟิก', 'https://ddc.moph.go.th/publishinfo.php?info', 'linkinlink', 1, NULL, '2020-10-30 08:52:50', '2020-10-30 08:52:50', NULL),
(89, 'down', 43, 6, 'พยากรณ์โรคและภัยสุขภาพ', 'https://ddc.moph.go.th', 'linkinlink', 1, NULL, '2020-10-30 08:53:19', '2020-10-30 08:53:19', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
