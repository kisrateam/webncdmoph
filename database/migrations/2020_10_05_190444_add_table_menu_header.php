<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableMenuHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_header', function (Blueprint $table) {
            $table->id();
            $table->integer('level');
            $table->integer('parent_id');
            $table->integer('index');
            $table->string('name');
		        $table->string('link');
		        $table->boolean('type');
		        $table->boolean('active');
		        $table->string('css');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_header');
    }
}
