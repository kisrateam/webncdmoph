-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2020 at 07:01 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ncdstrategy`
--

-- --------------------------------------------------------

--
-- Table structure for table `badwords`
--

CREATE TABLE `badwords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `badword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `badwords`
--

INSERT INTO `badwords` (`id`, `badword`, `created_at`, `updated_at`, `edited_by`) VALUES
(5, 'kdkfl;skglsdk;', '2020-10-06 09:06:17', '2020-10-06 09:06:17', NULL),
(8, 'บบ5', '2020-10-06 09:26:09', '2020-10-06 09:26:09', NULL),
(12, 're', '2020-10-26 07:30:52', '2020-10-26 07:30:52', NULL),
(14, 'asd', '2020-10-26 07:39:47', '2020-10-26 07:39:47', NULL),
(16, 'kkk', '2020-11-11 11:48:14', '2020-11-11 11:48:14', 'Administat0r'),
(17, 'asdf', '2020-11-11 11:50:02', '2020-11-11 11:50:02', '6');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `topicid` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `userid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `topicid`, `message`, `userid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, '<p>asdasdsadasdsadsadsadas</p>', 1, '2020-08-23 22:47:48', '2020-08-23 22:47:48', NULL),
(2, 4, '<p>asdsadsadasdasdsads</p>', 1, '2020-08-23 22:48:03', '2020-11-05 07:40:19', '2020-11-05 07:40:19'),
(3, 5, '<p>test</p>', 1, '2020-08-23 23:43:17', '2020-08-23 23:43:17', NULL),
(4, 5, '<p>asd</p>', 1, '2020-08-24 00:09:20', '2020-08-24 00:09:20', NULL),
(5, 5, '<p>1122</p>', 1, '2020-08-24 07:10:48', '2020-08-24 07:10:48', NULL),
(6, 4, '<p>gg</p>', 1, '2020-08-24 07:58:07', '2020-11-05 07:40:19', '2020-11-05 07:40:19'),
(7, 5, '<p>mflvp</p>', 1, '2020-08-24 08:04:44', '2020-08-24 08:04:44', NULL),
(8, 4, '<p>fghf</p>', 1, '2020-08-24 08:06:18', '2020-11-05 07:40:19', '2020-11-05 07:40:19'),
(9, 5, '<p>เทส เวลา</p>', 1, '2020-08-24 08:48:40', '2020-08-24 08:48:40', NULL),
(10, 5, '<p>;,asjdasdasdasfasf &nbsp;<span style=\"color:hsl(60, 75%, 60%);\">asdasdasdsad</span></p>', 1, '2020-08-24 08:52:06', '2020-08-24 08:52:06', NULL),
(11, 5, '<p>ควย</p>', 1, '2020-08-25 09:16:56', '2020-08-25 09:16:56', NULL),
(12, 5, '<p>asdf</p>', 1, '2020-08-25 10:08:17', '2020-08-25 10:08:17', NULL),
(13, 5, '<p><a href=\"http://127.0.0.1:8000/storage/files/1/sample.pdf\">ทดสอบ</a></p>', 1, '2020-08-25 11:24:28', '2020-08-25 11:24:28', NULL),
(14, 5, '<p><img alt=\"\" src=\"http://127.0.0.1:8000/storage/photos/1/test.png\" style=\"height:76px; width:58px\" /></p>', 1, '2020-08-25 11:24:58', '2020-08-25 11:24:58', NULL),
(15, 5, '<p>sad<span style=\"font-size:11px\">asdasdasdasd</span><span style=\"font-size:16px\">&nbsp;asdsad</span><span style=\"font-size:24px\">&nbsp;asdasd</span><span style=\"font-size:72px\">&nbsp;asdasdsadas&nbsp;<span style=\"background-color:#f1c40f\">asd</span><span style=\"color:#1abc9c\">asd</span></span></p>\r\n\r\n<h2 style=\"font-style:italic\"><span style=\"color:#1abc9c\"><span style=\"font-size:12px\"><span style=\"font-family:Arial,Helvetica,sans-serif\">asd&nbsp;</span><span style=\"font-family:Comic Sans MS,cursive\">asdas&nbsp;</span><span style=\"font-family:Courier New,Courier,monospace\">asdsad&nbsp;</span><span style=\"font-family:Georgia,serif\">asdsa&nbsp;<code>asdsad<del>a asdasd&nbsp; asd<tt>asdsa&nbsp; &nbsp;asdad<var>&nbsp;asd</var></tt></del></code></span></span></span>&nbsp;as<tt>asdsddd</tt><tt>&nbsp;&nbsp;<strong>asd</strong><em>asd</em><em>&nbsp;asd<u>&nbsp;asd<s>asd</s></u></em><sub>as</sub><sup>aa</sup></tt></h2>\r\n\r\n<ol>\r\n	<li><tt>a</tt></li>\r\n	<li><tt>2</tt></li>\r\n</ol>\r\n\r\n<p><tt>​​​​​​​​​​​​​​1334<img alt=\"\" src=\"http://127.0.0.1:8000/storage/photos/1/test.png\" style=\"height:76px; width:58px\" />&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/sample.pdf\" target=\"_blank\">ทดสอบส่งไฟล์</a></tt></p>', 1, '2020-08-26 05:02:06', '2020-08-26 05:02:06', NULL),
(16, 6, '<p><a href=\"http://127.0.0.1:8000/storage/files/1/file-sample_100kB.doc\">word</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/file_example_PPT_250kB.ppt\">power point</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/file_example_XLS_10.xls\">excel</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/sample.pdf\">pdf</a>&nbsp;ทดสอบตอบกลับโดยไฟล์</p>', 1, '2020-08-26 05:27:03', '2020-08-26 05:27:03', NULL),
(17, 6, '<p>test new icon</p>', 1, '2020-08-27 03:56:31', '2020-08-27 03:56:31', NULL),
(18, 8, '<p>123123</p>', 1, '2020-08-31 07:06:24', '2020-08-31 07:06:24', NULL),
(19, 8, '<p>123</p>', 1, '2020-08-31 07:06:36', '2020-08-31 07:06:36', NULL),
(20, 5, '<p>ทดสอบวัน ตอบเม้น</p>', 4, '2020-09-29 07:03:32', '2020-09-29 07:03:32', NULL),
(21, 65, '<p><a href=\"http://storage/files/ที่เก็บไฟล์/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20201106.pdf\">เอกสาร</a></p>', 6, '2020-11-10 09:27:15', '2020-11-10 09:27:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(1) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `best` int(1) NOT NULL DEFAULT 0,
  `fileuniq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `name`, `category`, `active`, `best`, `fileuniq`, `created_at`, `updated_at`, `edited_by`) VALUES
(71, 'retest redirect retest redirect retest redirect retest redirect retest redirect retest redirect', 0, 1, 1, '091520201129165f60431c91628', '2020-09-15 04:29:16', '2020-09-15 04:50:37', NULL),
(73, '123123213', 0, 1, 1, '091720201646525f63308cef55b', '2020-09-17 09:46:52', '2020-09-17 09:46:52', NULL),
(74, 'asdasd', 0, 0, 1, '102020201551225f8ea50a1f428', '2020-10-20 08:51:22', '2020-10-30 11:10:05', NULL),
(75, '123', NULL, 0, 0, '102020201657555f8eb4a3605c5', '2020-10-20 09:57:55', '2020-10-20 09:57:55', NULL),
(76, 'ฉบับปรับปรุง', 1, 0, 0, '102020201753295f8ec1a977ccc', '2020-10-20 10:53:29', '2020-10-20 10:53:29', NULL),
(77, 'นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูงและเบาหวาน ปี 2563 - 2564', 1, 1, 0, '102120201709385f9008e22ec96', '2020-10-21 10:09:38', '2020-11-09 06:21:44', NULL),
(79, 're test link', 1, 0, 0, 'http://localhost/phpmyadmin/sql.php?server=1&db=ncdstrategy&table=content&pos=0', '2020-10-26 05:32:43', '2020-10-26 05:40:32', NULL),
(80, 'ปัญหาเลือดออก', 2, 1, 1, 'http://localhost:8000/home/content', '2020-11-09 06:21:21', '2020-11-09 06:21:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `content_activity`
--

CREATE TABLE `content_activity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imguniq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namefile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileuniq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `hitcount` int(11) NOT NULL,
  `downloaded` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_activity`
--

INSERT INTO `content_activity` (`id`, `name`, `details`, `imguniq`, `namefile`, `fileuniq`, `category`, `subcategory`, `active`, `hitcount`, `downloaded`, `created_at`, `updated_at`, `edited_by`) VALUES
(81, '1234', '<p>3214</p>', '092320201504505f6b01a275b93', NULL, '092320201504505f6b01a275b96', 'ผลการดำเนินงาน ผลการติดตาม และประเมินผลแผน/ยุทธศาสตร์', '', 0, 20, 0, '2020-09-23 08:04:50', '2020-09-24 05:25:42', NULL),
(82, 'test', '<p>test</p>', '092320201511475f6b03437eb37', NULL, '092320201511475f6b03437eb3a', 'คู่มือ แนวทาง สื่อ', '', 0, 6, 0, '2020-09-23 08:11:47', '2020-09-28 10:32:46', NULL),
(86, 'ประชุมคณะทำงานหลักสูตรการเพิ่มสมรรถนะการจัดการโรคเบาหวานประชุมคณะทำงานหลักสูตรการเพิ่มสมรรถนะการจัดการโรคเบาหวาน', '<p>กองโรคไม่ติดต่อ กรมควบคุมโรค<br />\r\nจัดประชุมคณะทำงานหลักสูตรการเพิ่มสมรรถนะการจัดการโรคเบาหวานและความดันวันที่ 24 กุมภาพันธ์ 2563 กองโรคไม่ติดต่อ กรมควบคุมโรคจัดประชุมคณะทำงานหลักสูตรการเพิ่มสมรรถนะการจัดการโรคเบาหวานและความดัวันที่ 24 กุมภาพันธ์ 2563 กองโรคไม่ติดต่อ กรมควบคุมโรคจัดประชุมคณะทำงานหลักสูตรการเพิ่มสมรรถนะการจัดการโรคเบาหวานและความดัง</p>', '092420201203165f6c2894f3f63', NULL, '', 'ข่าวกิจกรรม', NULL, 0, 50, 0, '2020-09-24 05:03:16', '2020-11-02 08:02:37', NULL),
(87, 'RSS Feed re updated', '<p>เตือนพ่อแม่ ผู้ปกครอง ช่วงนี้ต้องดูแลเด็กอย่างใกล้ชิด เพราะยังเป็นช่วงปิดเทอมยาวอย่าปล่อยให้เด็กชวนกันไป เล่นน้ำกัน เอง ตามลำพังเสี่ยงตกน้ำหรือจมน้ำเสียชีเตือนพ่อแม่ ผู้ปกครอง ช่วงนี้ต้องดูแลเด็กอย่างใกล้ชิด เพราะยังเป็นช่วงปิดเทอมยาวอย่าปล่อยให้เด็กชวนกันไป เล่นน้ำกัน เอง ตามลำพังเสี่ยงตกน้ำหรือจมน้ำเสียชี</p>', '092420201229505f6c2ece18c82', NULL, '092420201229505f6c2ece18c8a', 'ข่าวกิจกรรม', '', 0, 27, 0, '2020-09-24 05:29:50', '2020-10-19 05:56:52', NULL),
(88, '111', '<p>1111</p>', '092420201252065f6c34060bb64', NULL, '092420201252065f6c34060bb67', 'ข่าวกิจกรรม', '', 0, 4, 0, '2020-09-24 05:52:06', '2020-09-24 05:52:06', NULL),
(89, '!important !important !important !important !important !important!important !important !important !important !important !important!important !important !important !important !important !important', '<p>asdsadjaskdsad</p>\r\n\r\n<p>asdasdsakjdoiasdkqwd</p>\r\n\r\n<p>wqloiudfoipgkfdgdfg</p>\r\n\r\n<p>aogiaopkgawekl;gadsfg</p>\r\n\r\n<p>alsgkawpoekgwegawegaegawegladlfgaopdfgdasfg</p>\r\n\r\n<p>ASpdlfkopsdifopgksdflg,klfdkglzk;lfgkl;fkopgkp;gkretrgr</p>', '092420201720105f6c72dab9e2c', NULL, '092420201720105f6c72dab9e30', 'ข่าวประชาสัมพันธ์', '', 0, 2, 0, '2020-09-24 10:20:10', '2020-09-28 09:52:31', NULL),
(90, '!important', '<div>สถานการณ์โรคไม่ติดต่อ และปัจจัยเสี่ยง ตาม 9 เป้าหมายระดับโลก ตัวชี้วัดสถาน การณ์โรคไม่ติดต่อในประเทศไทยที่กำหนด โดยคณะผู้เชี่ยวชาญทางด้านนโยบาย ผู้สถานการณ์โรคไม่ติดต่อ และปัจจัยเสี่ยง ตาม 9 เป้าหมายระดับโลก ตัวชี้วัดสถาน การณ์โรคไม่ติดต่อในประเทศไทยที่กำหนด โดยคณะผู้เชี่ยวชาญทางด้านนโยบาย ผู้เสถานการณ์โรคไม่ติดต่อ และปัจจัยเสี่ยง ตาม 9 เป้าหมายระดับโลก ตัวชี้วัดสถาน การณ์โรคไม่ติดต่อในประเทศไทยที่กำหนด โดยคณะผู้เชี่ยวชาญทางด้านนโยบาย ผู้เสถานการณ์โรคไม่ติดต่อ และปัจจัยเสี่ยง ตาม 9 เป้าหมายระดับโลก ตัวชี้วัดสถาน การณ์โรคไม่ติดต่อในประเทศไทยที่กำหนด โดยคณะผู้เชี่ยวชาญทางด้านนโยบาย ผู้เ</div>', '092420201720285f6c72ec2d7a7', NULL, '092420201720285f6c72ec2d7aa', 'คู่มือ แนวทาง สื่อ', '', 0, 3, 0, '2020-09-24 10:20:28', '2020-09-28 10:27:35', NULL),
(91, 'allsdifjiosjifohwjeoifjewfwef', '<p>srhgdkbopiseportawet</p>', '100120201721125f75ad9877074', NULL, '100120201721125f75ad9877077', 'สถานการณ์ / ข้อมูล', '', 0, 8, 0, '2020-10-01 10:21:12', '2020-10-01 10:21:12', NULL),
(92, 'dfh1234th6drhlpfdkhosfhs', '<p>er4yd5f64g65cvlopikypsrty</p>', '100120201721345f75adae78f78', NULL, '100120201721345f75adae78f7c', 'แผนการป้องกันควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)', '', 0, 5, 0, '2020-10-01 10:21:34', '2020-10-01 10:21:34', NULL),
(93, 'allsdifjiosjifohwjeoifjewfwef2', '<p>srhgdkbopiseportawet2</p>', '100120201721125f75ad9877074', NULL, '100120201721125f75ad9877077', 'สถานการณ์ / ข้อมูล', '', 0, 8, 0, '2020-10-01 10:21:12', '2020-10-01 10:21:12', NULL),
(94, 'dfh1234th6drhlpfdkhosfhs554654654', '<p>er4yd5f64g65cvlopikypsrty</p>', '100120201721345f75adae78f78', NULL, '100120201721345f75adae78f7c', 'แผนการป้องกันควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)', '', 0, 3, 0, '2020-10-01 10:21:34', '2020-10-01 10:21:34', NULL),
(95, '4567867', '<p>3214</p>', '092320201504505f6b01a275b93', NULL, '092320201504505f6b01a275b96', 'ผลการดำเนินงาน ผลการติดตาม และประเมินผลแผน/ยุทธศาสตร์', '', 0, 21, 0, '2020-09-23 08:04:50', '2020-09-24 05:25:42', NULL),
(96, 'test RSS feed', '<p>ทดสอบระบบ RSS</p>', '101920201252395f8d29a73bd5c', NULL, '101920201252395f8d29a73bd5f', 'ข่าวกิจกรรม', '', 0, 2, 0, '2020-10-19 05:52:39', '2020-10-19 05:52:39', NULL),
(97, 'delete file from news', '<p>delete file from news</p>', '102020201145215f8e6b6156591', NULL, '102020201145215f8e6b6156595', 'ข่าวกิจกรรม', '', 0, 1, 0, '2020-10-20 04:45:21', '2020-10-20 04:45:21', NULL),
(98, 'updated_at order', '<p>updated_at order</p>', '102020201204175f8e6fd1987f8', NULL, '102020201204175f8e6fd1987fa', 'ผลการดำเนินงาน ผลการติดตาม และประเมินผลแผน/ยุทธศาสตร์', '', 0, 14, 0, '2020-10-20 05:04:17', '2020-10-20 05:04:17', NULL),
(99, 'RSS new feed', '<p>RSS new feed</p>', '102020201724135f8ebacd4d953', NULL, '102020201724135f8ebacd4d95c', 'ข่าวกิจกรรม', '', 0, 6, 0, '2020-10-20 10:24:13', '2020-10-20 10:24:13', NULL),
(101, '5555', '<p>5555</p>', '110220201510105f9fbee25c7f2', 'ชื่อไฟล์', 'https://ddc.moph.go.th/dncd/ncdstrategy/public/index.php/home/media', 'สถานการณ์ / ข้อมูล', NULL, 1, 13, 3, '2020-11-02 08:10:10', '2020-11-12 04:45:30', '6'),
(102, '11', '<p>11</p>', '110320201354405fa0feb0ab224', NULL, 'news', 'ข่าวกิจกรรม', 'ประชุมคณะกรรมการ', 1, 0, 0, '2020-11-03 06:54:40', '2020-11-03 06:54:40', NULL),
(103, '12', '<p>12</p>', '110320201354495fa0feb9a6008', NULL, '', 'ข่าวกิจกรรม', 'อบรม/สัมมา', 1, 0, 0, '2020-11-03 06:54:49', '2020-11-09 03:52:55', NULL),
(104, '13', '<p>13</p>', '110320201354595fa0fec30a9e0', NULL, 'news', 'ข่าวกิจกรรม', 'รณรงค์', 0, 0, 0, '2020-11-03 06:54:59', '2020-11-03 06:54:59', NULL),
(105, '21', '<p>21</p>', '110320201356215fa0ff153554d', NULL, 'news', 'ข่าวประชาสัมพันธ์', 'ประชุมคณะกรรมการ', 0, 3, 0, '2020-11-03 06:56:21', '2020-11-03 06:56:21', NULL),
(106, '22', '<p>22</p>', '110320201356335fa0ff21070f9', NULL, 'news', 'ข่าวประชาสัมพันธ์', 'อบรม/สัมนา', 0, 1, 0, '2020-11-03 06:56:33', '2020-11-03 06:56:33', NULL),
(107, '23', '<p>23</p>', '110320201356435fa0ff2b50e6f', NULL, 'news', 'ข่าวประชาสัมพันธ์', 'รณรงค์', 0, 0, 0, '2020-11-03 06:56:43', '2020-11-03 06:56:43', NULL),
(108, '24', '<p>24</p>', '110320201356525fa0ff344d86f', NULL, 'news', 'ข่าวประชาสัมพันธ์', 'ข่าวประกวด/ประกาศ', 0, 8, 0, '2020-11-03 06:56:52', '2020-11-03 06:56:52', NULL),
(109, '12', '<p>12</p>', '110320201354495fa0feb9a6008', NULL, 'news', 'ข่าวกิจกรรม', 'อบรม/สัมนา', 1, 0, 0, '2020-11-03 06:54:49', '2020-11-03 06:54:49', NULL),
(110, 'show on main page', '<p>show on main page</p>', '110920201053275fa8bd37506cd', NULL, 'news', 'ข่าวประชาสัมพันธ์', 'อบรม/สัมนา', 1, 0, 0, '2020-11-09 03:53:27', '2020-11-09 03:53:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eng`
--

CREATE TABLE `eng` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileuniq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `highlights` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eng`
--

INSERT INTO `eng` (`id`, `name`, `category`, `fileuniq`, `active`, `highlights`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, '5-year NCDs Prevention and Control Plan (2017-2021)', 'นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูง', 'https://mdbootstrap.com/docs/jquery/content/icons-list/', 1, 1, '2020-10-15 08:00:24', '2020-10-15 08:25:17', NULL),
(3, 'Policy to expedite the management of hypertension and diabetes', 'แผนการป้องกันควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ.2560-2564)', 'https://stackoverflow.com/questions/42894137/displaying-check-mark-and-cross-instead-of-boolean-true-and-false/42894215', 1, 1, '2020-10-15 08:03:29', '2020-10-15 08:25:41', NULL),
(5, 'Policy to expedite the management of hypertension and diabetes', 'แผนการป้องกันควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ.2560-2564)', 'Policy to expedite the management of hypertension and diabetes', 1, 1, '2020-10-16 05:06:33', '2020-10-16 05:06:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idfk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `files` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `downloaded` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `idfk`, `name`, `files`, `downloaded`, `created_at`, `updated_at`) VALUES
(1, '081120201007575f326dfd7b299', '[\"081120201007575f326dfd807f8.pdf\"]', '[\"081120201007575f326dfd807f8.pdf\"]', 5, '2020-08-11 03:07:57', '2020-08-11 03:07:57'),
(2, '082620201455245f46156c135c9', '[\"082620201455245f46156c17995.xls\",\"082620201455245f46156c1837b.ppt\",\"082620201455245f46156c189a1.doc\"]', '[\"082620201455245f46156c17995.xls\",\"082620201455245f46156c1837b.ppt\",\"082620201455245f46156c189a1.doc\"]', 0, '2020-08-26 07:55:24', '2020-08-26 07:55:24'),
(3, '082620201500005f46168051c2b', '[\"082620201500005f46168052a83.xls\",\"082620201500005f46168053c3d.ppt\",\"082620201500005f4616805439f.doc\"]', '[\"082620201500005f46168052a83.xls\",\"082620201500005f46168053c3d.ppt\",\"082620201500005f4616805439f.doc\"]', 0, '2020-08-26 08:00:00', '2020-08-26 08:00:00'),
(4, '082620201505305f4617ca740f2', '[\"082620201505305f4617ca754d5.xls\",\"082620201505305f4617ca75bee.ppt\",\"082620201505305f4617ca76246.doc\"]', '[\"082620201505305f4617ca754d5.xls\",\"082620201505305f4617ca75bee.ppt\",\"082620201505305f4617ca76246.doc\"]', 0, '2020-08-26 08:05:30', '2020-08-26 08:05:30'),
(5, '082620201508265f46187a586ba', '[\"082620201508265f46187a59949.xls\",\"082620201508265f46187a5a13e.ppt\",\"082620201508265f46187a5ac46.doc\"]', '[\"082620201508265f46187a59949.xls\",\"082620201508265f46187a5a13e.ppt\",\"082620201508265f46187a5ac46.doc\"]', 0, '2020-08-26 08:08:26', '2020-08-26 08:08:26'),
(6, '082720201604265f47771a1cc32', '[null,null,null,null]', '[\"xls\",\"ppt\",\"doc\",\"pdf\"]', 0, '2020-08-27 09:04:26', '2020-08-27 09:04:26'),
(7, '082720201605375f4777613ca49', '[\"x\",\"p\",\"d\",\"pdf\"]', '[\"x.xls\",\"p.ppt\",\"d.doc\",\"pdf.pdf\"]', 0, '2020-08-27 09:05:37', '2020-08-27 09:05:37'),
(8, '082720201607585f4777eec869d', '[\"1\",\"2\",\"3\",\"4\"]', '[\"1.xls\",\"2.ppt\",\"3.doc\",\"4.pdf\"]', 0, '2020-08-27 09:07:58', '2020-08-27 09:07:58'),
(9, '082720201611485f4778d4325a3', '[\"1\",\"2\",\"3\",\"4\"]', '[\"082720201611485f4778d433f01.xls\",\"082720201611485f4778d434a2e.ppt\",\"082720201611485f4778d43586d.doc\",\"082720201611485f4778d4362bb.pdf\"]', 0, '2020-08-27 09:11:48', '2020-08-27 09:11:48'),
(10, '082720201753525f4790c0e7ab2', '[\"12\",\"23\"]', '[\"082720201611485f4778d433f01.xls\",\"082720201611485f4778d434a2e.ppt\",\"082720201611485f4778d43586d.doc\",\"082720201611485f4778d4362bb.pdf\"]', 0, '2020-08-27 10:53:52', '2020-08-27 10:53:52'),
(11, '082720201754485f4790f8559bc', '[\"xls\",\"ppt\"]', '[\"082720201611485f4778d433f01.xls\",\"082720201611485f4778d434a2e.ppt\",\"082720201611485f4778d43586d.doc\",\"082720201611485f4778d4362bb.pdf\"]', 0, '2020-08-27 10:54:48', '2020-08-27 10:54:48'),
(12, '082720201756545f4791760c41c', '[\"xxxx\",\"aaaa\"]', '[\"082720201611485f4778d433f01.xls\",\"082720201611485f4778d434a2e.ppt\",\"082720201611485f4778d43586d.doc\",\"082720201611485f4778d4362bb.pdf\"]', 0, '2020-08-27 10:56:54', '2020-08-27 10:56:54'),
(13, '082720201800545f47926624270', '[\"123\",\"123\"]', '[\"082720201611485f4778d433f01.xls\",\"082720201611485f4778d434a2e.ppt\",\"082720201611485f4778d43586d.doc\",\"082720201611485f4778d4362bb.pdf\"]', 0, '2020-08-27 11:00:54', '2020-08-27 11:00:54'),
(14, '082820201037425f487c06afefe', '[\"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e021\",\"dfg22222\",\"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e023\"]', '[\"082820201035465f487b92bf043.xls\",\"082820201037425f487c06b22ae.xls\",\"082820201037425f487c06b3ad0.xls\"]', 0, '2020-08-27 11:02:03', '2020-08-28 03:37:42'),
(15, '082820201123435f4886cfe8760', '[]', '[]', 0, '2020-08-28 04:23:43', '2020-08-28 10:49:25'),
(16, '082820201845435f48ee67d6426', '[]', '[]', 0, '2020-08-28 11:45:43', '2020-08-28 12:10:31'),
(17, '082820201912105f48f49a62449', '[]', '[]', 0, '2020-08-28 12:12:10', '2020-08-28 12:18:16'),
(18, '082820201919215f48f64900ae7', '[\"11\",\"22\",\"333\"]', '[\"082820201919215f48f64901de7.xls\",\"082820201927405f48f83c9d543.pdf\",\"082820201919215f48f64903255.doc\"]', 11, '2020-08-28 12:19:21', '2020-09-03 09:02:27'),
(19, '090820201418145f573036e32f2', '[\"\\u0e1f\\u0e2b\\u0e01\"]', '[\"090820201418145f573036e42d1.ppt\"]', 0, '2020-09-08 07:18:14', '2020-09-08 07:18:14'),
(20, '091520201125495f60424de9571', '\"091520201142595f604653c18b2.doc\"', '\"091520201142595f604653c18b2.doc\"', 4, '2020-09-15 04:25:49', '2020-09-15 04:42:59'),
(21, '091520201129165f60431c91628', '\"091520201129165f60431c925ce.doc\"', '\"091520201129165f60431c925ce.doc\"', 3, '2020-09-15 04:29:16', '2020-09-15 04:29:16'),
(22, '091720201645075f633023cc804', '\"091720201645075f633023cdb70.doc\"', '\"091720201645075f633023cdb70.doc\"', 0, '2020-09-17 09:45:09', '2020-09-17 09:45:09'),
(23, '091720201646525f63308cef55b', '\"091720201646525f63308cf0b9d.xls\"', '\"091720201646525f63308cf0b9d.xls\"', 3, '2020-09-17 09:46:52', '2020-09-17 09:46:52'),
(24, '092320201502245f6b01104bc6c', '[\"1.pdf\",\"2.xls\"]', '[\"092320201502245f6b01104cc61.pdf\",\"092320201502245f6b01104dca9.xls\"]', 0, '2020-09-23 08:02:24', '2020-09-23 08:02:24'),
(25, '092320201504505f6b01a275b96', '[\"1\"]', '[\"092320201504505f6b01a276b38.xls\"]', 1, '2020-09-23 08:04:50', '2020-09-24 05:25:25'),
(26, '092420201203165f6c2894f3f65', '[]', '[]', 2, '2020-09-24 05:03:17', '2020-10-20 05:06:17'),
(27, '092420201229505f6c2ece18c8a', '[\"wewqe\"]', '[\"092420201229505f6c2ece1a5ad.doc\"]', 1, '2020-09-24 05:29:50', '2020-09-24 05:29:50'),
(29, '092520201652505f6dbdf283fdd', '\"file-sample_100kB.doc\"', '\"092520201721065f6dc49252ee3.doc\"', 1, '2020-09-25 09:54:52', '2020-09-25 10:21:06'),
(31, '092520201726225f6dc5ce8f045', '\"file_example_XLS_10.xls\"', '\"092520201726305f6dc5d6ad99d.xls\"', 4, '2020-09-25 10:26:22', '2020-09-25 10:26:30'),
(32, '092820201220595f7172bb943c3', '\"file-sample_100kB.doc\"', '\"092820201220595f7172bb95c1a.doc\"', 1, '2020-09-28 05:20:59', '2020-09-28 05:20:59'),
(33, '092920201308395f72cf6746a1c', '\"file_example_PPT_250kB.ppt\"', '\"092920201308395f72cf6748203.ppt\"', 0, '2020-09-29 06:08:39', '2020-09-29 06:08:39'),
(34, '092920201318145f72d1a6cce71', '\"sample.pdf\"', '\"092920201318145f72d1a6ce718.pdf\"', 1, '2020-09-29 06:18:14', '2020-09-29 06:18:14'),
(35, '102020201204175f8e6fd1987fa', '[\"updated_at order\"]', '[\"102020201204175f8e6fd19913f.pdf\"]', 2, '2020-10-20 05:04:17', '2020-10-20 05:04:17'),
(36, '102020201551225f8ea50a1f428', '\"102020201551225f8ea50a2052b.pdf\"', '\"102020201551225f8ea50a2052b.pdf\"', 0, '2020-10-20 08:51:22', '2020-10-20 08:51:22'),
(37, '102020201657555f8eb4a3605c5', '\"102020201657555f8eb4a361ea2.pdf\"', '\"102020201657555f8eb4a361ea2.pdf\"', 0, '2020-10-20 09:57:55', '2020-10-20 09:57:55'),
(38, '102020201753295f8ec1a977ccc', '\"102020201753295f8ec1a978b3b.pdf\"', '\"102020201753295f8ec1a978b3b.pdf\"', 0, '2020-10-20 10:53:29', '2020-10-20 10:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `homebanner`
--

CREATE TABLE `homebanner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homebanner`
--

INSERT INTO `homebanner` (`id`, `url`, `path`, `created_at`, `updated_at`, `edited_by`) VALUES
(27, '123', '111220201212545facc45606baf.png', NULL, NULL, '6');

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

CREATE TABLE `img` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idfk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `img`
--

INSERT INTO `img` (`id`, `idfk`, `name`, `path`, `created_at`, `updated_at`, `edited_by`) VALUES
(54, '081120201112275f327d1b27e9b', '[\"081120201112275f327d1b2d6c8.jpg\"]', '[\"081120201112275f327d1b2d6c8.jpg\"]', '2020-08-11 04:12:27', '2020-08-11 04:12:27', NULL),
(56, '081320200645265f34e18600388', '[\"081320200645265f34e186097f2.jpg\"]', '[\"081320200645265f34e186097f2.jpg\"]', '2020-08-12 23:45:26', '2020-08-12 23:45:26', NULL),
(57, '081320200645335f34e18de12d7', '[\"081320200645335f34e18de5b9b.jpg\"]', '[\"081320200645335f34e18de5b9b.jpg\"]', '2020-08-12 23:45:33', '2020-08-12 23:45:33', NULL),
(59, '081320200738325f34edf894f19', '[\"081320200738325f34edf89ac4c.jpg\"]', '[\"081320200738325f34edf89ac4c.jpg\"]', '2020-08-13 00:38:32', '2020-08-13 00:38:32', NULL),
(60, '081320200740165f34ee6018ef9', '[\"081320200740165f34ee601c190.jpg\"]', '[\"081320200740165f34ee601c190.jpg\"]', '2020-08-13 00:40:16', '2020-08-13 00:40:16', NULL),
(61, '081320200741035f34ee8f9ba50', '[\"081320200741035f34ee8f9e0f0.jpg\"]', '[\"081320200741035f34ee8f9e0f0.jpg\"]', '2020-08-13 00:41:03', '2020-08-13 00:41:03', NULL),
(75, '082820201919215f48f64900ae5', '[\"082820201919215f48f64905018.png\"]', '[\"090320201602275f50b123453ea.png\",\"090320201602275f50b12345ca4.png\",\"090320201602275f50b12346445.png\"]', '2020-08-28 12:19:21', '2020-09-03 09:02:27', NULL),
(81, '083120201641595f4cc5e7b9afb', '[\"083120201754045f4cd6cc43c84.jpg\"]', '[\"083120201754045f4cd6cc43c84.jpg\"]', '2020-08-31 10:50:17', '2020-08-31 10:54:04', NULL),
(82, '090320201128145f5070dea53c1', '[\"090320201128145f5070deafc8e.jpg\"]', '[\"090320201128145f5070deafc8e.jpg\"]', '2020-09-03 04:28:14', '2020-09-03 04:28:14', NULL),
(83, '090320201136535f5072e550d59', '[\"090320201136535f5072e552630.png\"]', '[\"090320201136535f5072e552630.png\"]', '2020-09-03 04:36:53', '2020-09-03 04:36:53', NULL),
(84, '090820201417425f573016d7211', '[\"090820201417425f573016d840c.png\"]', '[\"090820201417425f573016d840c.png\"]', '2020-09-08 07:17:42', '2020-09-08 07:17:42', NULL),
(85, '090820201418145f573036e32ef', '[\"090820201418145f573036e59ac.png\"]', '[\"090820201418145f573036e59ac.png\"]', '2020-09-08 07:18:14', '2020-09-08 07:18:14', NULL),
(86, '090820201418355f57304b1df9b', '[\"090820201418355f57304b1f0e7.jpg\"]', '[\"090820201418355f57304b1f0e7.jpg\"]', '2020-09-08 07:18:35', '2020-09-08 07:18:35', NULL),
(93, '092220201232335f698c7161bff', '[\"092220201232335f698c71629b0.svg\"]', '[\"092220201232335f698c71629b0.svg\"]', '2020-09-22 05:32:33', '2020-09-22 05:32:33', NULL),
(94, '092220201235135f698d117c117', '[\"092220201235135f698d117d2a1.svg\"]', '[\"092220201235135f698d117d2a1.svg\"]', '2020-09-22 05:35:13', '2020-09-22 05:35:13', NULL),
(95, '092220201454115f69ada30732f', '\"092220201454115f69ada307cd1.svg\"', '\"092220201454115f69ada307cd1.svg\"', '2020-09-22 07:54:11', '2020-09-22 07:54:11', NULL),
(96, '092220201454565f69add005e37', '\"092220201558465f69bcc680af0.svg\"', '\"092220201558465f69bcc680af0.svg\"', '2020-09-22 07:54:56', '2020-09-22 08:58:46', NULL),
(98, '092320201504505f6b01a275b93', '\"092320201504505f6b01a2789c4.svg\"', '\"092320201504505f6b01a2789c4.svg\"', '2020-09-23 08:04:50', '2020-09-23 08:04:50', NULL),
(99, '092320201511475f6b03437eb37', '\"092320201511475f6b03437fa2f.svg\"', '\"092320201511475f6b03437fa2f.svg\"', '2020-09-23 08:11:47', '2020-09-23 08:11:47', NULL),
(100, '092420201133235f6c21933d770', '\"092420201133235f6c21933e8b3.svg\"', '\"092420201133235f6c21933e8b3.svg\"', '2020-09-24 04:33:23', '2020-09-24 04:33:23', NULL),
(101, '092420201203165f6c2894f3f63', '\"092820201654135f71b2c57fe3b.png\"', '\"092820201654135f71b2c57fe3b.png\"', '2020-09-24 05:03:17', '2020-09-28 09:54:13', NULL),
(102, '092420201229505f6c2ece18c82', '\"092420201229505f6c2ece1c98e.svg\"', '\"092420201229505f6c2ece1c98e.svg\"', '2020-09-24 05:29:50', '2020-09-24 05:29:50', NULL),
(103, '092420201252065f6c34060bb64', '\"092420201252065f6c34060ce3d.svg\"', '\"092420201252065f6c34060ce3d.svg\"', '2020-09-24 05:52:06', '2020-09-24 05:52:06', NULL),
(104, '092420201720105f6c72dab9e2c', '\"092420201720105f6c72dabaf30.svg\"', '\"092420201720105f6c72dabaf30.svg\"', '2020-09-24 10:20:10', '2020-09-24 10:20:10', NULL),
(105, '092420201720285f6c72ec2d7a7', '\"092420201720285f6c72ec2f3db.svg\"', '\"092420201720285f6c72ec2f3db.svg\"', '2020-09-24 10:20:28', '2020-09-24 10:20:28', NULL),
(106, '092520201103085f6d6bfc01a04', '\"092520201235565f6d81bc139d6.svg\"', '\"092520201235565f6d81bc139d6.svg\"', '2020-09-25 04:03:08', '2020-09-25 05:35:56', NULL),
(107, '092520201103515f6d6c2789a9e', '\"092520201103515f6d6c278ab5a.svg\"', '\"092520201103515f6d6c278ab5a.svg\"', '2020-09-25 04:03:51', '2020-09-25 04:03:51', NULL),
(108, '092520201104035f6d6c3355f04', '\"092520201104035f6d6c3357032.svg\"', '\"092520201104035f6d6c3357032.svg\"', '2020-09-25 04:04:03', '2020-09-25 04:04:03', NULL),
(109, '092520201104165f6d6c401d941', '\"092520201104165f6d6c401ea3e.svg\"', '\"092520201104165f6d6c401ea3e.svg\"', '2020-09-25 04:04:16', '2020-09-25 04:04:16', NULL),
(111, '100120201721125f75ad9877074', '\"100120201721125f75ad9877ba0.svg\"', '\"100120201721125f75ad9877ba0.svg\"', '2020-10-01 10:21:12', '2020-10-01 10:21:12', NULL),
(112, '101220201030385f83cdde0ea37', '\"101220201030385f83cdde0fb8d.png\"', '\"101220201030385f83cdde0fb8d.png\"', '2020-10-12 03:30:38', '2020-10-12 03:30:38', NULL),
(113, '101220201132505f83dc720b78f', '\"101220201132505f83dc720ca76.png\"', '\"101220201132505f83dc720ca76.png\"', '2020-10-12 04:32:50', '2020-10-12 04:32:50', NULL),
(114, '101420201504135f86b0fd0bad2', '\"101420201504135f86b0fd180e2.png\"', '\"101420201504135f86b0fd180e2.png\"', '2020-10-14 08:04:13', '2020-10-14 08:04:13', NULL),
(115, '101420201505555f86b163e1946', '\"101420201505555f86b163e292a.jpg\"', '\"101420201505555f86b163e292a.jpg\"', '2020-10-14 08:05:55', '2020-10-14 08:05:55', NULL),
(116, '101420201507325f86b1c49d6f4', '\"101420201507325f86b1c49e7b8.png\"', '\"101420201507325f86b1c49e7b8.png\"', '2020-10-14 08:07:34', '2020-10-14 08:07:34', NULL),
(117, '101420201508245f86b1f8c9804', '\"101420201508245f86b1f8ca7fd.png\"', '\"101420201508245f86b1f8ca7fd.png\"', '2020-10-14 08:08:24', '2020-10-14 08:08:24', NULL),
(118, '101420201509085f86b2241acda', '\"101420201509085f86b2241b873.png\"', '\"101420201509085f86b2241b873.png\"', '2020-10-14 08:09:08', '2020-10-14 08:09:08', NULL),
(119, '101420201509435f86b24726920', '\"101420201509435f86b24727c0f.png\"', '\"101420201509435f86b24727c0f.png\"', '2020-10-14 08:09:43', '2020-10-14 08:09:43', NULL),
(120, '101420201510475f86b28763754', '\"101420201510475f86b287648d7.jpg\"', '\"101420201510475f86b287648d7.jpg\"', '2020-10-14 08:10:47', '2020-10-14 08:10:47', NULL),
(121, '101420201511575f86b2cdbc5d6', '\"101420201511575f86b2cdbd5cc.png\"', '\"101420201511575f86b2cdbd5cc.png\"', '2020-10-14 08:11:57', '2020-10-14 08:11:57', NULL),
(122, '101420201513065f86b312cb211', '\"101420201513065f86b312cca27.png\"', '\"101420201513065f86b312cca27.png\"', '2020-10-14 08:13:08', '2020-10-14 08:13:08', NULL),
(123, '101420201513425f86b336c3462', '\"101420201513425f86b336c42bd.png\"', '\"101420201513425f86b336c42bd.png\"', '2020-10-14 08:13:42', '2020-10-14 08:13:42', NULL),
(124, '101420201515045f86b388690be', '\"101420201515045f86b3886a1c1.jpg\"', '\"101420201515045f86b3886a1c1.jpg\"', '2020-10-14 08:15:04', '2020-10-14 08:15:04', NULL),
(125, '101420201516415f86b3e9af363', '\"101420201516415f86b3e9b0345.jpg\"', '\"101420201516415f86b3e9b0345.jpg\"', '2020-10-14 08:16:41', '2020-10-14 08:16:41', NULL),
(126, '101420201519045f86b478ad816', '\"101420201519045f86b478aeb84.png\"', '\"101420201519045f86b478aeb84.png\"', '2020-10-14 08:19:06', '2020-10-14 08:19:06', NULL),
(127, '101420201519495f86b4a56d3fb', '\"101420201519495f86b4a56dfed.png\"', '\"101420201519495f86b4a56dfed.png\"', '2020-10-14 08:19:49', '2020-10-14 08:19:49', NULL),
(128, '101420201520235f86b4c713a1a', '\"101420201520235f86b4c714863.png\"', '\"101420201520235f86b4c714863.png\"', '2020-10-14 08:20:23', '2020-10-14 08:20:23', NULL),
(129, '101420201521185f86b4fe98ec9', '\"101420201521185f86b4fe99f15.jpg\"', '\"101420201521185f86b4fe99f15.jpg\"', '2020-10-14 08:21:18', '2020-10-14 08:21:18', NULL),
(130, '101420201521445f86b518424cc', '\"101420201521445f86b518434ae.jpg\"', '\"101420201521445f86b518434ae.jpg\"', '2020-10-14 08:21:44', '2020-10-14 08:21:44', NULL),
(131, '101420201522445f86b5540c682', '\"101420201522445f86b5540d139.jpg\"', '\"101420201522445f86b5540d139.jpg\"', '2020-10-14 08:22:44', '2020-10-14 08:22:44', NULL),
(134, 'eng3', '\"eng3\"', '\"101620201225565f892ee4dd0e4.jpg\"', '2020-10-16 05:25:56', '2020-10-16 05:25:56', NULL),
(135, 'eng4', '\"eng4\"', '\"101620201226175f892ef92f50f.jpg\"', '2020-10-16 05:26:17', '2020-10-16 05:26:17', NULL),
(138, '101920201252395f8d29a73bd5c', '\"101920201252395f8d29a73d500.jpg\"', '\"101920201252395f8d29a73d500.jpg\"', '2020-10-19 05:52:39', '2020-10-19 05:52:39', NULL),
(139, '102020201145215f8e6b6156591', '\"102020201145215f8e6b61578e5.jpg\"', '\"102020201145215f8e6b61578e5.jpg\"', '2020-10-20 04:45:21', '2020-10-20 04:45:21', NULL),
(140, '102020201204175f8e6fd1987f8', '\"102020201204175f8e6fd199c18.jpg\"', '\"102020201204175f8e6fd199c18.jpg\"', '2020-10-20 05:04:17', '2020-10-20 05:04:17', NULL),
(145, '102020201724135f8ebacd4d953', '\"102020201724135f8ebacd4eaec.svg\"', '\"102020201724135f8ebacd4eaec.svg\"', '2020-10-20 10:24:13', '2020-10-20 10:24:13', NULL),
(147, 'team1', '\"team1\"', '\"102620201235535f9660395872f.jpg\"', '2020-10-22 05:28:55', '2020-10-26 05:35:53', NULL),
(148, 'team2', '\"team2\"', '\"102620201235535f9660395a201.jpg\"', '2020-10-26 05:35:53', '2020-10-26 05:35:53', NULL),
(153, '110920201053275fa8bd37506cd', '\"red.png\"', '\"110920201053275fa8bd375159a.png\"', '2020-11-09 03:53:27', '2020-11-09 03:53:27', NULL),
(154, '110920201716525fa9171430082', '\"110920201716525fa91714319b9.png\"', '\"110920201716525fa91714319b9.png\"', '2020-11-09 10:16:52', '2020-11-09 10:16:52', NULL),
(155, '110920201717425fa91746af0c6', '\"110920201717425fa91746afe3c.png\"', '\"110920201717425fa91746afe3c.png\"', '2020-11-09 10:17:42', '2020-11-09 10:17:42', NULL),
(159, '110920201820495fa92611cd0be', '\"110920201820495fa92611ce158.png\"', '\"110920201820495fa92611ce158.png\"', '2020-11-09 11:20:49', '2020-11-09 11:20:49', NULL),
(160, '110920201820495fa92611cf99b', '[\"\\u0e25\\u0e14\\u0e15\\u0e32\\u0e22\\u0e01\\u0e48\\u0e2d\\u0e19\\u0e27\\u0e31\\u0e22\\u0e2d\\u0e31\\u0e19\\u0e04\\u0e27\\u0e23\\u0e08\\u0e32\\u0e01\\u0e42\\u0e23\\u0e04 NCDs (20%) 25%\"]', '[\"111120201734385fabbe3e86a24.svg\"]', '2020-11-09 11:20:49', '2020-11-11 10:34:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `imgslide`
--

CREATE TABLE `imgslide` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `details` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imgslide`
--

INSERT INTO `imgslide` (`id`, `name`, `details`, `url`, `path`, `active`, `created_at`, `updated_at`, `edited_by`) VALUES
(41, 'สถานการณ์โรคไม่ติดต่อ', 'ในช่วงทศวรรษที่ผ่านมา กลุ่มโรคไม่ติด ต่อได้คร่าชีวิตประชากรไทยถึงร้อยละ7...', 'https://www.youtube.com/watch?v=Kj1hinqoljA&list=PLEEX8g2XtzsGv61U11HpoWt5JP18NHeve&index=34&ab_channel=FrogLeapStudios', '110920201345525fa8e5a0d04c7.png', 1, '2020-10-05 10:43:07', '2020-11-09 06:45:52', NULL),
(42, 'สถานการณ์โรคไม่ติดต่อ', 'ในช่วงทศวรรษที่ผ่านมา กลุ่มโรคไม่ติด ต่อได้คร่าชีวิตประชากรไทยถึงร้อยละ7...', 'https://www.youtube.com/watch?v=Kj1hinqoljA&list=PLEEX8g2XtzsGv61U11HpoWt5JP18NHeve&index=34&ab_channel=FrogLeapStudios', '110920201345595fa8e5a7ccace.png', 1, '2020-10-05 10:43:07', '2020-11-09 06:45:59', NULL),
(43, 'สถานการณ์โรคไม่ติดต่อ', 'ในช่วงทศวรรษที่ผ่านมา กลุ่มโรคไม่ติด ต่อได้คร่าชีวิตประชากรไทยถึงร้อยละ7...', 'https://www.youtube.com/watch?v=Kj1hinqoljA&list=PLEEX8g2XtzsGv61U11HpoWt5JP18NHeve&index=34&ab_channel=FrogLeapStudios', '110920201346085fa8e5b0501c1.png', 1, '2020-10-05 10:43:07', '2020-11-09 06:46:08', NULL),
(44, 'สถานการณ์โรคไม่ติดต่อ', 'ในช่วงทศวรรษที่ผ่านมา กลุ่มโรคไม่ติด ต่อได้คร่าชีวิตประชากรไทยถึงร้อยละ7...', 'https://www.youtube.com/watch?v=Kj1hinqoljA&list=PLEEX8g2XtzsGv61U11HpoWt5JP18NHeve&index=34&ab_channel=FrogLeapStudios', '110920201346215fa8e5bd9f0fd.jpg', 1, '2020-10-05 10:43:07', '2020-11-09 06:46:21', NULL),
(46, 'NONCOMMUNICABLE DISEASES (NCDs)', 'AND MENTAL HEALTH', 'http://localhost:8000/', '110920201101015fa8befd2cc95.png', 1, '2020-11-09 04:01:01', '2020-11-09 04:01:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE `link` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imguniq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `name`, `url`, `imguniq`, `created_at`, `updated_at`, `edited_by`) VALUES
(15, 'กรมการแพทย์', 'https://www.dms.go.th', '092520201103085f6d6bfc01a04', '2020-09-25 04:03:08', '2020-10-14 07:52:40', NULL),
(16, 'สำนักงานคณะกรรมการอาหารและยา', 'http://www.fda.moph.go.th/', '092520201103515f6d6c2789a9e', '2020-09-25 04:03:51', '2020-10-14 07:53:05', NULL),
(17, 'สำนักงานหลักประกันสุขภาพแห่งชาติ', 'https://www.nhso.go.th/', '092520201104035f6d6c3355f04', '2020-09-25 04:04:03', '2020-10-14 07:53:26', NULL),
(18, 'สำนักงานกองทุนสนับสนุนการสร้างเสริมสุขภาพ', 'https://www.thaihealth.or.th/', '092520201104165f6d6c401d941', '2020-09-25 04:04:16', '2020-10-14 07:53:41', NULL),
(20, 'กองงานคณะกรรมการควบคุมผลิตภัณฑ์ยาสูบ', 'https://ddc.moph.go.th/otpc/', '101420201504135f86b0fd0bad2', '2020-10-14 08:04:13', '2020-10-14 08:04:13', NULL),
(21, 'สำนักงานคณะกรรมการควบคุมเครื่องดื่มแอลกอฮอล์', 'https://ddc.moph.go.th/oabc/index.php', '101420201505555f86b163e1946', '2020-10-14 08:05:55', '2020-10-14 08:05:55', NULL),
(22, 'กรมสุขภาพจิต', 'https://www.dmh.go.th/main.asp', '101420201507325f86b1c49d6f4', '2020-10-14 08:07:32', '2020-10-14 08:07:32', NULL),
(23, 'กรมสนับสนุนบริการสุขภาพ', 'https://hss.moph.go.th/', '101420201508245f86b1f8c9804', '2020-10-14 08:08:24', '2020-10-14 08:08:24', NULL),
(24, 'กรมอนามัย', 'http://www.anamai.moph.go.th/main.php?filename=index', '101420201509085f86b2241acda', '2020-10-14 08:09:08', '2020-10-14 08:09:08', NULL),
(25, 'กระทรวงสาธารณสุข', 'https://www.moph.go.th/', '101420201509435f86b24726920', '2020-10-14 08:09:43', '2020-10-14 08:09:43', NULL),
(26, 'สถาบันวิจัยระบบสาธารณสุข', 'https://www.hsri.or.th/researcher', '101420201510475f86b28763754', '2020-10-14 08:10:47', '2020-10-14 08:10:47', NULL),
(27, 'สำนักงานพัฒนานโยบายสุขภาพระหว่างประเทศ', 'http://www.ihppthaigov.net/', '101420201511575f86b2cdbc5d6', '2020-10-14 08:11:57', '2020-10-14 08:11:57', NULL),
(28, 'สำนักงานสภาพัฒนาการเศรษฐกิจและสังคมแห่งชาติ', 'https://www.nesdc.go.th/main.php?filename=index', '101420201513065f86b312cb211', '2020-10-14 08:13:06', '2020-10-14 08:13:06', NULL),
(29, 'สำนักงานสถิติแห่งชาติ', 'http://www.nso.go.th/sites/2014/', '101420201513425f86b336c3462', '2020-10-14 08:13:42', '2020-10-14 08:13:42', NULL),
(30, 'สำนักงานพัฒนาระบบข้อมูลข่าวสารสุขภาพ', 'https://www.hiso.or.th/hiso/center.php/', '101420201515045f86b388690be', '2020-10-14 08:15:04', '2020-10-14 08:15:04', NULL),
(31, 'HDC', 'https://hdcservice.moph.go.th/hdc/main/index.php', '101420201516415f86b3e9af363', '2020-10-14 08:16:41', '2020-10-14 08:16:41', NULL),
(32, 'กยผ สป', 'http://bps.moph.go.th/new_bps/', '101420201519045f86b478ad816', '2020-10-14 08:19:04', '2020-10-14 08:19:04', NULL),
(33, 'เครือข่ายคนไทยไร้พุง', 'http://www.raipoong.com/', '101420201519495f86b4a56d3fb', '2020-10-14 08:19:49', '2020-10-14 08:19:49', NULL),
(34, 'เครือข่ายลดบริโภคเค็ม', 'https://www.lowsaltthai.com/', '101420201520235f86b4c713a1a', '2020-10-14 08:20:23', '2020-10-14 08:20:23', NULL),
(35, 'มูลนิธิเพื่อผู้บริโภค', 'https://www.consumerthai.org/', '101420201521185f86b4fe98ec9', '2020-10-14 08:21:18', '2020-10-14 08:21:18', NULL),
(36, 'มูลนิธิสถาบันวิจัยและพัฒนาระบบสุขภาพชุมชน', 'http://thaiichr.org/', '101420201521445f86b518424cc', '2020-10-14 08:21:44', '2020-10-14 08:21:44', NULL),
(37, 'สำนักงานเครือข่ายองค์กรงดเหล้า', 'https://www.stopdrink.com/', '101420201522445f86b5540c682', '2020-10-14 08:22:44', '2020-10-14 08:22:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_header`
--

CREATE TABLE `menu_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_index` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `css` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_header`
--

INSERT INTO `menu_header` (`id`, `level`, `parent_id`, `parent_index`, `name`, `link`, `type`, `active`, `css`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 'down', NULL, NULL, 'รู้ทันโรค', '', 'menu', 1, NULL, '2020-10-29 11:49:49', '2020-10-29 11:49:49', NULL),
(2, 'down', 1, 1, 'รายชื่อโรค', '/', 'link', 1, NULL, '2020-10-29 11:50:07', '2020-10-30 06:54:35', NULL),
(26, 'down', NULL, NULL, 'เกี่ยวกับกรม', '', 'menu', 1, NULL, '2020-10-08 07:45:33', '2020-10-08 07:45:33', NULL),
(28, 'down', NULL, NULL, 'กฏหมายที่เกี่ยวข้อง', '', 'menu', 1, NULL, '2020-10-08 07:46:54', '2020-10-08 07:46:54', NULL),
(29, 'down', NULL, NULL, 'ข่าวจัดซื้อจัดจ้าง', '', 'menu', 1, NULL, '2020-10-08 07:47:13', '2020-10-08 07:47:13', NULL),
(30, 'down', NULL, NULL, 'ข่าวสาร', '', 'menu', 1, NULL, '2020-10-08 07:47:33', '2020-10-08 07:47:33', NULL),
(31, 'down', NULL, NULL, 'คลังข้อมูล', '', 'menu', 1, NULL, '2020-10-08 07:50:00', '2020-10-08 07:50:00', NULL),
(32, 'down', NULL, NULL, 'หน่วยงานในสังกัด', '', 'menu', 1, NULL, '2020-10-08 07:50:15', '2020-10-08 07:50:15', NULL),
(33, 'down', NULL, NULL, 'รายชื่อโรค', '', 'menu', 1, 'background-color:green;\r\ncolor:white;\r\nborder-radius: 10px 10px 0 0;\r\nheight:100%;', '2020-10-08 07:53:35', '2020-10-08 07:53:35', NULL),
(34, 'up', NULL, NULL, 'งานบริการต่างๆ', '/', 'menulink', 1, NULL, '2020-10-08 07:56:39', '2020-10-08 07:56:39', NULL),
(35, 'up', NULL, NULL, 'รับเรื่องร้องเรียน', '/', 'menulink', 1, NULL, '2020-10-08 08:13:20', '2020-10-08 08:13:20', NULL),
(36, 'up', NULL, NULL, 'ถาม-ตอบ', '/home/topic', 'menulink', 1, NULL, '2020-10-08 08:14:05', '2020-10-08 08:14:05', NULL),
(37, 'up', NULL, NULL, 'ผังเว็บไซต์', '/', 'menulink', 1, NULL, '2020-10-08 08:14:25', '2020-10-08 08:14:25', NULL),
(43, 'down', 1, 2, 'สื่อเผยแพร่', '/', 'havelink', 1, NULL, '2020-10-29 11:47:04', '2020-10-30 08:51:21', NULL),
(48, 'up', 26, 1, 'ประวัติกรม', '/', 'link', 1, NULL, '2020-10-30 07:30:49', '2020-10-30 07:30:49', NULL),
(49, 'down', 26, 2, 'โครงสร้างกรม', NULL, 'link', 1, NULL, '2020-10-30 07:34:04', '2020-10-30 07:34:04', NULL),
(50, 'down', 26, 3, 'ผู้บริหารกรมควบคุมโรค', NULL, 'link', 1, NULL, '2020-10-30 07:34:44', '2020-10-30 07:34:44', NULL),
(51, 'down', 26, 4, 'กฏกระทรวงการแบ่งส่วนราชการ', NULL, 'link', 1, NULL, '2020-10-30 07:35:38', '2020-10-30 07:35:38', NULL),
(52, 'down', 26, 5, 'คำรับรอง', NULL, 'havelink', 1, NULL, '2020-10-30 07:35:59', '2020-10-30 07:36:44', NULL),
(53, 'down', 26, 6, 'ติดต่อกรม', NULL, 'link', 1, NULL, '2020-10-30 07:36:19', '2020-10-30 07:36:19', NULL),
(54, 'down', 52, 1, 'ระดับกรมควบคุมโรค', NULL, 'linkinlink', 1, NULL, '2020-10-30 07:36:44', '2020-10-30 07:36:44', NULL),
(55, 'down', 52, 2, 'ผลคะแนนระดับกรมควบคุมโรค', NULL, 'linkinlink', 1, NULL, '2020-10-30 07:47:48', '2020-10-30 07:47:48', NULL),
(56, 'down', 52, 3, 'ระดับหน่วยงาน', NULL, 'linkinlink', 1, NULL, '2020-10-30 07:48:22', '2020-10-30 07:48:22', NULL),
(57, 'down', 28, 1, 'พ.ร.บ. โรคติดต่อ 2558', NULL, 'link', 1, NULL, '2020-10-30 07:49:10', '2020-10-30 07:49:10', NULL),
(58, 'down', 28, 2, 'พ.ร.บ. ควบคุมพลิตภัณฑ์ยาสูบ 2560', NULL, 'link', 1, NULL, '2020-10-30 07:50:18', '2020-10-30 07:50:18', NULL),
(59, 'down', 28, 3, 'พ.ร.บ. ควบคุมเครื่องดื่มแอลกอฮอล์ 2551', NULL, 'link', 1, NULL, '2020-10-30 07:50:59', '2020-10-30 07:50:59', NULL),
(60, 'down', 28, 3, 'พ.ร.บ. ควบคุมโรคจากการประกอบอาชีพและโรคจากสิ่งแวดล้อม พ.ศ. 2562', NULL, 'link', 1, NULL, '2020-10-30 07:52:01', '2020-10-30 07:52:01', NULL),
(61, 'down', 52, 4, 'ผลคะแนนระดับหน่วยงาน', NULL, 'linkinlink', 1, NULL, '2020-10-30 07:53:46', '2020-10-30 07:53:46', NULL),
(62, 'down', 29, 1, 'แผนการจัดซื้อจัดจ้าง', NULL, 'link', 1, NULL, '2020-10-30 07:54:17', '2020-10-30 07:54:17', NULL),
(63, 'down', 29, 2, 'ประกาศการจัดซื้อ-จัดจ้าง', NULL, 'link', 1, NULL, '2020-10-30 07:54:46', '2020-10-30 07:54:46', NULL),
(64, 'down', 29, 3, 'ประกาศร่าง TOR', NULL, 'link', 1, NULL, '2020-10-30 08:33:17', '2020-10-30 08:33:17', NULL),
(65, 'down', 29, 4, 'การเผยแพร่ราคากลางตามแบบของสำนัก ป.ป.ช.', NULL, 'link', 1, NULL, '2020-10-30 08:33:48', '2020-10-30 08:33:48', NULL),
(66, 'down', 29, 5, 'สรุปผลการจัดซื้อจ้าง', NULL, 'link', 1, NULL, '2020-10-30 08:34:16', '2020-10-30 08:34:16', NULL),
(67, 'down', 29, 6, 'ศูนย์ราคากลาง กรมควบคุมโรค', NULL, 'link', 1, NULL, '2020-10-30 08:34:43', '2020-10-30 08:34:43', NULL),
(68, 'down', 29, 7, 'ข้อมูลสาระสำคัญในสัญญา', NULL, 'link', 1, NULL, '2020-10-30 08:35:11', '2020-10-30 08:35:11', NULL),
(69, 'down', 29, 8, 'รายงานผลการจัดซื้อจัดจ้าง', NULL, 'link', 1, NULL, '2020-10-30 08:35:39', '2020-10-30 08:35:39', NULL),
(70, 'down', 30, 1, 'ข่าวเพิ่มสื่อมวลชน', NULL, 'link', 1, NULL, '2020-10-30 08:36:04', '2020-10-30 08:36:04', NULL),
(71, 'down', 30, 2, 'ข่าวรับสมัครบุคลากร', NULL, 'link', 1, NULL, '2020-10-30 08:36:24', '2020-10-30 08:36:24', NULL),
(72, 'down', 30, 2, 'ข่าวประชาสัมพันธ์', NULL, 'link', 1, NULL, '2020-10-30 08:36:45', '2020-10-30 08:36:45', NULL),
(73, 'down', 30, 4, 'วิดีโอข่าวประชาสัมพันธ์', NULL, 'link', 1, NULL, '2020-10-30 08:37:10', '2020-10-30 08:37:10', NULL),
(74, 'down', 30, 5, 'ข่าวกิจกรรม', NULL, 'link', 1, NULL, '2020-10-30 08:37:30', '2020-10-30 08:37:30', NULL),
(75, 'down', 31, 1, 'ฐานข้อมูลงานวิจัย', NULL, 'link', 1, NULL, '2020-10-30 08:37:54', '2020-10-30 08:37:54', NULL),
(76, 'down', 31, 2, 'ฐานข้อมูล KM หน่วยงาน', NULL, 'link', 1, NULL, '2020-10-30 08:38:14', '2020-10-30 08:38:14', NULL),
(77, 'down', 31, 3, 'ฐานข้อมูลนวัตกรรม', NULL, 'link', 1, NULL, '2020-10-30 08:38:55', '2020-10-30 08:38:55', NULL),
(78, 'down', 31, 4, 'ฐานข้อมูลวิชาการ', NULL, 'link', 1, NULL, '2020-10-30 08:39:09', '2020-10-30 08:39:09', NULL),
(79, 'down', 31, 5, 'ฐานข้อมูลทำเนียบนักวิจัย', NULL, 'link', 1, NULL, '2020-10-30 08:39:36', '2020-10-30 08:39:36', NULL),
(80, 'down', 31, 6, 'ฐานข้อมูลทำเนียบนักวิจัย', NULL, 'link', 1, NULL, '2020-10-30 08:40:02', '2020-10-30 08:40:02', NULL),
(81, 'down', 31, 7, 'อำเภอควบคุมโรคเข้มแข็ง', NULL, 'link', 1, NULL, '2020-10-30 08:40:26', '2020-10-30 08:40:26', NULL),
(82, 'down', 32, 1, 'หน่วยงานส่วนกลาง', NULL, 'link', 1, NULL, '2020-10-30 08:40:48', '2020-10-30 08:40:48', NULL),
(83, 'down', 32, 2, 'หน่วยงานส่วนภูมิภาค', NULL, 'link', 1, NULL, '2020-10-30 08:41:08', '2020-10-30 08:41:08', NULL),
(84, 'down', 43, 1, 'สื่อมัลติมีเดีย', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:51:21', '2020-10-30 08:51:21', NULL),
(85, 'down', 43, 2, 'สื่อโทรทัศน์', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:51:44', '2020-10-30 08:51:44', NULL),
(86, 'down', 43, 3, 'สื่อวิทยุ', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:52:00', '2020-10-30 08:52:00', NULL),
(87, 'down', 43, 4, 'วารสารออนไลน์', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:52:24', '2020-10-30 08:52:24', NULL),
(88, 'down', 43, 5, 'อินโฟกราฟฟิก', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:52:50', '2020-10-30 08:52:50', NULL),
(89, 'down', 43, 6, 'พยากรณ์โรคและภัยสุขภาพ', NULL, 'linkinlink', 1, NULL, '2020-10-30 08:53:19', '2020-10-30 08:53:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_06_082006_add_column_for_users', 2),
(5, '2020_08_06_082527_add_column_for_users', 3),
(6, '2020_08_06_082646_add_table_content', 4),
(7, '2020_08_06_082704_add_table_link', 4),
(8, '2020_08_06_082717_add_table_team', 4),
(9, '2020_08_06_082733_add_table_title', 4),
(10, '2020_08_06_082752_add_table_content_activity', 4),
(11, '2020_08_06_082943_add_table_files', 4),
(12, '2020_08_06_082956_add_table_img', 4),
(13, '2020_08_06_083009_add_table_userlogs', 4),
(14, '2020_08_06_083102_add_table_topics', 4),
(15, '2020_08_06_083126_add_table_comments', 4),
(16, '2020_08_07_094600_change_type_text', 5),
(17, '2020_08_10_041722_add_array', 6),
(19, '2020_08_20_064024_softdeletes_topics_table', 7),
(20, '2020_08_24_053544_column_comment', 8),
(21, '2020_09_14_105224_column_homebanner', 9),
(22, '2020_09_22_120603_add_table_result', 10),
(23, '2020_09_23_144302_add_table_content_new', 11),
(24, '2020_10_05_190444_add_table_menu_header', 12),
(25, '2020_10_06_144607_badwords', 13),
(26, '2020_10_15_114629_add_table_eng', 14),
(27, '2020_10_19_121324_news_items', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('tmp@tmp.com', '$2y$10$aYy7R05Qw5S8Xpxqz8Mbu.orATMgX3xpKhwmAWlbAvPnOA.uXb1pS', '2020-08-31 08:17:42'),
('tunmypro@hotmail.co.th', '$2y$10$H6rG9GXSsAve2/LyCwgKWu9FlDQiZ6hr0oxPaV2k6XX2puctk5rNK', '2020-09-01 03:26:59');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imguniq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imguniqicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `best` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `name`, `details`, `category`, `imguniq`, `imguniqicon`, `active`, `best`, `created_at`, `updated_at`, `edited_by`) VALUES
(10, '1', '2', 'ผลการประเมิน 9 global targets', '092220201454565f69add005e37', NULL, 0, 1, '2020-09-22 07:54:56', '2020-11-09 11:21:02', NULL),
(12, 'test', 'test', 'ผลการประเมิน NCD progress monitor', '101220201030385f83cdde0ea37', NULL, 1, 1, '2020-10-12 03:30:38', '2020-10-12 03:31:42', NULL),
(13, 'สถานการณ์โรคไม่ติดต่อตาม 9 เป้าหมายระดับโลกของประเทศไทย', 'จำแนกตามตัวชี้วัดเบื้องต้น 9 ตัวชี้วัด', 'ผลการ ประเมินแผน', '101220201132505f83dc720b78f', NULL, 1, 0, '2020-10-12 04:32:50', '2020-10-12 04:32:57', NULL),
(17, 'สถานการณ์โรคไม่ติดต่อตาม 9 เป้าหมายระดับโลกของประเทศไทย', 'จำแนกตามตัวชี้วัดเบื้องต้น 9 ตัวชี้วัด', 'ผลการประเมิน 9 global targets', '110920201820495fa92611cd0be', '110920201820495fa92611cf99b', 1, 0, '2020-11-09 11:20:49', '2020-11-09 11:20:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank` decimal(5,1) NOT NULL,
  `fileuniq` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `rank`, `fileuniq`, `created_at`, `updated_at`, `edited_by`) VALUES
(17, '2.2', '3.2', '092820201220595f7172bb943c3', '2020-09-28 05:20:59', '2020-11-09 07:12:12', NULL),
(24, '2', '2.0', 'http://localhost:8000/admin/team', '2020-11-09 07:12:04', '2020-11-09 07:12:04', NULL),
(25, '1', '1.0', 'http://localhost:8000/', '2020-11-09 07:13:26', '2020-11-09 07:13:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topicreport`
--

CREATE TABLE `topicreport` (
  `id` int(11) NOT NULL,
  `idreport` int(11) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topicreport`
--

INSERT INTO `topicreport` (`id`, `idreport`, `type`, `details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 20, 'comments', 'มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด', '2020-11-04 10:32:10', '2020-11-04 10:32:10', '2020-11-05 06:57:11'),
(2, 12, 'comments', 'มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ', '2020-11-04 10:33:07', '2020-11-04 10:33:07', '2020-11-05 06:57:11'),
(3, 12, 'comments', 'เป็นสแปม', '2020-11-04 10:33:14', '2020-11-04 10:33:14', '2020-11-05 06:57:11'),
(4, 4, 'topics', 'เป็นสแปม', '2020-11-04 10:33:27', '2020-11-04 10:33:27', '2020-11-05 07:41:10'),
(5, 20, 'comments', 'มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด', '2020-11-05 07:05:26', '2020-11-05 07:05:26', NULL),
(6, 20, 'comments', 'มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ', '2020-11-05 07:20:54', '2020-11-05 07:20:54', NULL),
(7, 64, 'topics', 'เป็นสแปม', '2020-11-05 09:54:22', '2020-11-05 09:54:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `userid` int(20) NOT NULL,
  `privacy` int(1) NOT NULL COMMENT '1 = guest\r\n2 = user\r\n3 = member',
  `category` int(1) NOT NULL COMMENT '1 = ยุทธศาสตร์ที่ 1;\r\n2 = 2;\r\n3 = 3;\r\n4 = 4;\r\n5 = 5;\r\n6 = 6;',
  `subcategory` int(1) DEFAULT 0 COMMENT '1 = แผนและการดำเนินงาน\r\n2 = รายงานผลดำเนินงาน\r\n3 = การประสานงาน',
  `hitcount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `name`, `details`, `userid`, `privacy`, `category`, `subcategory`, `hitcount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'zx', '<p>zx</p>', 2, 1, 0, 1, 71, 1, '2020-08-19 00:09:13', '2020-11-05 07:40:19', '2020-11-05 07:40:19'),
(5, 'ทดสอบระบบ5', '<p>ทดสอบ <strong>ทดสอบ </strong><i><strong>ทดสอบ </strong>ทดสอบ&nbsp;</i></p><hr><p><span style=\"color:hsl(150,75%,60%);\">ทดสอบ </span><span style=\"color:hsl(240,75%,60%);\">ทดสอบ </span><span class=\"text-huge\" style=\"color:hsl(90,75%,60%);\">ทดทอบ</span></p>', 1, 1, 1, 2, 476, 1, '2020-08-19 03:49:20', '2020-11-04 07:56:41', NULL),
(6, 'upload muti FileType', '<p><a href=\"http://127.0.0.1:8000/storage/files/1/ตัวอย่าง.doc\">word</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/ตัวอย่าง.ppt\">powerpoint</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/ตัวอย่าง.xls\">excel</a>&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/1/ตัวอย่าง.pdf\">pdf</a>&nbsp; &nbsp;&nbsp;<img alt=\"รูป\" src=\"http://127.0.0.1:8000/storage/photos/1/test.png\" style=\"height:76px; width:58px\" /></p>', 1, 1, 0, 3, 18, 1, '2020-08-26 05:22:35', '2020-08-27 03:56:31', NULL),
(7, 'test icon', '<p>test icon new&nbsp;&nbsp;<img alt=\"devil\" src=\"http://127.0.0.1:8000/CKEditor/plugins/smiley/images/devil_smile.png\" style=\"height:23px; width:23px\" title=\"devil\" /></p>', 1, 1, 0, 1, 5, 1, '2020-08-27 03:57:39', '2020-08-27 03:57:39', NULL),
(8, 'asdasdsa', '', 1, 1, 0, 2, 18, 1, '2020-08-31 07:05:28', '2020-08-31 07:07:07', NULL),
(9, 'retetstsetestsetsetse', '<p>สวัสดีท่านผู้อ่านทุกท่าน ท่านกำลังมองหาวิธีที่จะ<strong><em>จ้างเขียนบทความ</em></strong>ให้ได้ผลมากที่สุด เพราะบางคนก็ยังไม่เข้าใจอย่างถ่องแท้เกี่ยวกับเรื่องของการ<em>จ้างเขียนบทความ</em>&nbsp;บางคนก็ยังไม่รู้เลยว่า<strong>จ้างเขียนบทความ</strong>ไปทำไม ร้ายกว่านั้น บางคนไม่ทราบด้วยซ้ำว่าโลกนี้มีการ<strong>จ้างเขียนบทความ</strong>อยู่ด้วยเหรอ แล้วเขา<em>จ้างเขียนบทความ</em>ทำไม&nbsp;<a href=\"https://www.3e3x.com/\">จ้างเขียนบทความ</a>ไปเพื่ออะไร ฉะนั้นบทความนี้ก็จะยาวหน่อยนะครับ เพราะผมจะเริ่มอธิบายตั้งแต่เริ่มต้นเลย คือเริ่มจาก 0 ไปจนถึง 10 เลยครับ จากที่ยังไม่รู้ว่าโลกนี้มีงานแบบนี้ด้วยเหรอ จนถึงสามารถนำบทความที่จ้างเขียนไปใช้ให้เกิดประโยชน์ต่อการ<a href=\"http://www.promotewebsite.in.th/\">โปรโมทเว็บไซต์</a>&nbsp;การโปรโมทสินค้า เพิ่มยอดขาย เพิ่มความสำเร็จให้ได้มากที่สุด</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>ความสำคัญของ &ldquo;บทความ&rdquo;</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>ก่อนอื่นเราต้องเข้าใจก่อนว่า บทความ มีประโยชน์อะไรบ้าง สามารถนำมาใช้ทำอะไรได้บ้าง แล้วการได้มาซึ่งบทความนั้นได้มายังไงได้บ้าง ต้องจ้างเขียนอย่างเดียวเลยหรือสามารถเขียนเองได้ไหม มาเริ่มกันเลย &ldquo;บทความ&rdquo; ถือว่ามีความสำคัญอย่างมาก ตั้งแต่อดีตจนถึงปัจจุบัน บทความก็ยังคงมีความสำคัญอย่างมากเช่นเคย สำคัญกับอะไร สำคัญกับคนที่ต้องการ<a href=\"http://www.xn--c3cuemd0bn8a6qpac3a1b0e.xn--o3cw4h/\">โปรโมทเว็บไซต์</a>&nbsp;<a href=\"http://www.promotewebsite.in.th/\">โปรโมทเพจ</a>&nbsp;สำคัญสำหรับกับคนที่ต้องการโปรโมทสินค้า หรือบริการเพื่อเพิ่มยอดขายให้กับตัวเอง สำหรับคนที่ต้องการประสบผลสำเร็จ เพราะอะไร..?? เพราะบทความนั้นถือว่าเป็นการอธิบายถึงอารมณ์ ความรู้สึก ความรู้ สรรพคุณ หรืออะไรก็แล้วแต่ที่ผู้ส่งสารต้องการส่งสารให้กับผู้รับสาร บทความถือว่าเป็นการสื่อสารเบื้องต้นระหว่างผู้ส่งสาร(ผู้ขาย)กับผู้รับสาร(ลูกค้า) ถึงแม้ว่าปัจจุบัน การนำเสนอ สื่อสารกับลูกค้านั้น รูปแบบของวีดีโอ(VDO) เข้ามามีบทบาทอย่างมาก แต่ถึงกระนั้น จากประสบการณ์ไม่น้อยกว่า 10ปีของผู้เขียนเอง บทความที่เขียนด้วยตัวหนังสือยังมีความสำคัญอย่างมากต่อการ<strong><em>โปรโมทเว็บไซต์</em></strong>&nbsp;สินค้าและบริการต่างๆที่มีอยู่บนโลกนี้ เคล็ดลับของความสำเร็จของการทำงานออนไลน์ก็คือ &ldquo;บทความ&rdquo; นี่แหละครับ</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>บทความมีกี่ประเภท</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>มีหลายแหล่งและหลายบุคคลที่ให้คำจำกัดความของคำว่า&rdquo;บทความ&rdquo;ไว้ แต่โดยสรุปแล้ว ก็เป็นดังที่ผมเข้าใจนั่นก็คือ บทความมีอยู่ 2 ประเภทคือ บทความทั่วไป กับ บทความวิชาการ บทความทั่วไปก็คือบทความอะไรก็ได้ที่เราเขียนขึ้นมา ไม่ค่อยมีหลักการใดๆ และบทความวิชาการก็คือ บทความที่เค้าโครง มีองค์ประกอบที่แน่นอน มีหลักการ มีความน่าเชื่อถือนักวิจัยสามารถนำไปอ้างอิงหรือนำไปใช้งานได้<br />\r\n(อ้างอิง:www.trueplookpanya.com/learning/detail/1034, www.president.su.ac.th/personnel/images/Project/Project60/AcademicPosition/2017-12-19/2-A.Chaiyong-19122560(2).pdf )</p>\r\n\r\n<p>&nbsp;</p>', 4, 1, 0, 3, 63, 0, '2020-09-28 11:02:51', '2020-11-02 10:34:51', '2020-11-02 10:34:51'),
(10, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 1, 49, 1, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(11, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 2, 50, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(12, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 3, 46, 1, '2020-09-28 11:02:51', '2020-11-05 06:36:25', '2020-11-05 06:36:25'),
(13, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 1, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(14, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 2, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(15, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 3, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(16, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 3, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(17, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 48, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(18, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 48, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(19, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(20, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(21, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(22, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(23, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(24, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(25, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(26, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(27, 'retetstsetestsetsetse', '<p>asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;asd asdasfksdfogujopsek gsjkol sdrg dfg&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls\">http://127.0.0.1:8000/storage/files/4/file_example_XLS_10.xls</a></p>', 4, 1, 0, 0, 45, 0, '2020-09-28 11:02:51', '2020-09-29 04:40:46', NULL),
(28, 'กดเดกเด', '<p>กดเดกเ</p>', 6, 1, 0, 0, 0, 1, '2020-10-12 09:53:07', '2020-10-12 09:53:07', NULL),
(29, 'เอกสารการประชุม  ทดสอบ อัพโหลดไฟล์', '<p><a href=\"http://127.0.0.1:8000/storage/files/6/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\" target=\"_blank\">เอกสารการประชุม</a></p>\r\n\r\n<p>ทดสอบ อัพโหลดไฟล์</p>', 6, 1, 0, 0, 3, 1, '2020-10-12 09:58:29', '2020-10-12 10:00:08', NULL),
(30, '1234', '<p><a href=\"http://127.0.0.1:8000/storage/files/6/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">asdsadasd</a></p>', 6, 1, 0, 0, 4, 1, '2020-10-12 10:02:59', '2020-10-12 10:11:31', NULL),
(31, 'test new player', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 0, 0, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(32, 'post form mobile', '<p>asdasd123213</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://127.0.0.1:8000/storage/files/6/file-sample_100kB.doc\" target=\"_blank\">mobile</a></p>', 6, 1, 0, 0, 22, 0, '2020-10-12 10:40:41', '2020-10-20 05:23:50', NULL),
(33, 'new post', '<p>asdasdasdasds</p>', 6, 1, 0, 0, 33, 1, '2020-10-21 04:40:16', '2020-10-26 08:25:34', NULL),
(35, 'ทดสอบ เวลา', '<p>ทดสอบ เวลา</p>', 6, 1, 0, 0, 0, 1, '2020-10-28 10:16:51', '2020-10-28 10:16:51', NULL),
(36, 'ทดสอบ แคตกอรี่', '<p>ทดสอบ แคตกอรี่</p>', 6, 1, 0, 0, 1, 1, '2020-10-28 10:18:32', '2020-10-28 10:18:32', NULL),
(37, 'category 1', '<p>category 1</p>', 6, 1, 0, 0, 0, 1, '2020-10-28 10:20:10', '2020-10-28 10:20:10', NULL),
(38, 'category1', '<pre>\r\ncategory1</pre>', 6, 1, 0, 0, 1, 1, '2020-10-28 10:23:12', '2020-10-28 10:23:12', NULL),
(39, 'cat1', '<p>cat1</p>', 6, 1, 0, 0, 1, 1, '2020-10-28 10:24:34', '2020-10-28 10:24:34', NULL),
(41, 'cat1', '<p>cat1</p>', 6, 1, 0, 0, 0, 1, '2020-10-28 10:24:34', '2020-10-28 10:24:34', NULL),
(46, 'cat1', '<p>cat1</p>', 6, 1, 0, 0, 0, 1, '2020-10-28 10:24:34', '2020-10-28 10:24:34', NULL),
(47, 'cat1', '<p>cat1</p>', 6, 1, 0, 0, 0, 1, '2020-10-28 10:24:34', '2020-10-28 10:24:34', NULL),
(48, '1', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 1, 2, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(49, '2', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 2, 3, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(50, '3', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 3, 1, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(51, '4', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 4, 2, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(52, '5', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 5, 3, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(53, '6', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 6, 1, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(54, '6', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 6, 2, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(55, '5', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 5, 1, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(56, '4', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 4, 3, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(57, '3', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 3, 2, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(58, '2', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 2, 1, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(59, '1', '<p>new player file&nbsp;<a href=\"http://127.0.0.1:8000/storage/files/7/FAMIK_เอกสารสรุปการประชุมเว็บไซต์ NCD_20200928.pdf\">new file</a></p>', 7, 1, 1, 3, 4, 1, '2020-10-12 10:21:32', '2020-10-12 10:21:32', NULL),
(60, 'รายงานผลดำเนินงาน ใน จังหวัด กาญ', '<p>รายงานผลดำเนินงาน ใน จังหวัด กาญ</p>', 6, 3, 2, 2, 0, 1, '2020-10-29 07:42:01', '2020-10-29 07:42:01', NULL),
(61, '5-user', '<p>5-user</p>', 6, 1, 5, NULL, 0, 1, '2020-10-29 07:48:55', '2020-10-29 07:48:55', NULL),
(62, '4-admin', '<p>4-admin</p>', 6, 1, 4, 2, 1, 1, '2020-10-29 08:01:02', '2020-10-29 08:01:02', NULL),
(63, 'new 1', '<p>new 1</p>', 6, 1, 1, NULL, 4, 1, '2020-10-30 08:03:58', '2020-10-30 08:03:58', NULL),
(64, 'Tester ?', '<p>Tester ?</p>', 6, 2, 1, 1, 7, 1, '2020-11-04 07:13:53', '2020-11-04 07:13:53', NULL),
(65, 'Only Member ?', '<p>Only Member ?</p>', 6, 3, 1, 2, 20, 1, '2020-11-04 07:26:28', '2020-11-10 09:27:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userlogs`
--

CREATE TABLE `userlogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `userlogs`
--

INSERT INTO `userlogs` (`id`, `log`, `created_at`, `updated_at`) VALUES
(1, '1 เข้าสู่ระบบ เวลา2020-08-20 09:39:35', NULL, NULL),
(2, '1 เข้าสู่ระบบ เวลา 2020-08-21 03:09:43', NULL, NULL),
(3, '1 เข้าสู่ระบบ เวลา 2020-08-21 03:27:06', NULL, NULL),
(4, '1 เข้าสู่ระบบ เวลา 2020-08-21 03:38:08', NULL, NULL),
(5, '1 เข้าสู่ระบบ เวลา 2020-08-23 12:24:05', NULL, NULL),
(6, '1 เข้าสู่ระบบ เวลา 2020-08-24 03:36:44', NULL, NULL),
(7, '1 เข้าสู่ระบบ เวลา 2020-08-24 14:10:32', NULL, NULL),
(8, '1 เข้าสู่ระบบ เวลา 2020-08-25 14:50:23', NULL, NULL),
(9, '1 เข้าสู่ระบบ เวลา 2020-08-25 14:56:12', NULL, NULL),
(10, '1 เข้าสู่ระบบ เวลา 2020-08-25 15:02:26', NULL, NULL),
(11, '1 เข้าสู่ระบบ เวลา 2020-08-25 16:15:39', NULL, NULL),
(12, '1 เข้าสู่ระบบ เวลา 2020-08-25 16:45:09', NULL, NULL),
(13, '1 เข้าสู่ระบบ เวลา 2020-08-26 10:58:20', NULL, NULL),
(14, '1 เข้าสู่ระบบ เวลา 2020-08-26 17:25:13', NULL, NULL),
(15, '1 เข้าสู่ระบบ เวลา 2020-08-27 09:59:30', NULL, NULL),
(16, '1 เข้าสู่ระบบ เวลา 2020-08-28 10:34:37', NULL, NULL),
(17, '1 เข้าสู่ระบบ เวลา 2020-08-30 13:38:19', NULL, NULL),
(18, '1 เข้าสู่ระบบ เวลา 2020-08-31 14:00:37', NULL, NULL),
(19, '4 เข้าสู่ระบบ เวลา 2020-09-02 11:20:45', NULL, NULL),
(20, '4 เข้าสู่ระบบ เวลา 2020-09-02 11:22:24', NULL, NULL),
(21, '4 เข้าสู่ระบบ เวลา 2020-09-02 11:41:53', NULL, NULL),
(22, '4 เข้าสู่ระบบ เวลา 2020-09-02 12:18:37', NULL, NULL),
(23, '4 เข้าสู่ระบบ เวลา 2020-09-03 11:25:11', NULL, NULL),
(24, '4 เข้าสู่ระบบ เวลา 2020-09-08 14:16:57', NULL, NULL),
(25, '4 เข้าสู่ระบบ เวลา 2020-09-08 14:23:34', NULL, NULL),
(26, '4 เข้าสู่ระบบ เวลา 2020-09-10 15:11:39', NULL, NULL),
(27, '4 เข้าสู่ระบบ เวลา 2020-09-10 15:11:56', NULL, NULL),
(28, '4 เข้าสู่ระบบ เวลา 2020-09-10 16:48:18', NULL, NULL),
(29, '4 เข้าสู่ระบบ เวลา 2020-09-11 10:54:54', NULL, NULL),
(30, '4 เข้าสู่ระบบ เวลา 2020-09-11 14:45:04', NULL, NULL),
(31, '4 เข้าสู่ระบบ เวลา 2020-09-14 10:34:51', NULL, NULL),
(32, '4 เข้าสู่ระบบ เวลา 2020-09-15 10:34:10', NULL, NULL),
(33, '4 เข้าสู่ระบบ เวลา 2020-09-16 10:55:35', NULL, NULL),
(34, '4 เข้าสู่ระบบ เวลา 2020-09-17 10:48:42', NULL, NULL),
(35, '4 เข้าสู่ระบบ เวลา 2020-09-18 10:56:33', NULL, NULL),
(36, '4 เข้าสู่ระบบ เวลา 2020-09-18 14:31:38', NULL, NULL),
(37, '4 เข้าสู่ระบบ เวลา 2020-09-21 12:39:26', NULL, NULL),
(38, '4 เข้าสู่ระบบ เวลา 2020-09-21 16:19:47', NULL, NULL),
(39, '4 เข้าสู่ระบบ เวลา 2020-09-21 16:27:11', NULL, NULL),
(40, '4 เข้าสู่ระบบ เวลา 2020-09-21 17:17:43', NULL, NULL),
(41, '4 เข้าสู่ระบบ เวลา 2020-09-22 10:42:47', NULL, NULL),
(42, '4 เข้าสู่ระบบ เวลา 2020-09-23 13:56:05', NULL, NULL),
(43, '4 เข้าสู่ระบบ เวลา 2020-09-24 11:05:38', NULL, NULL),
(44, '4 เข้าสู่ระบบ เวลา 2020-09-24 17:19:50', NULL, NULL),
(45, '4 เข้าสู่ระบบ เวลา 2020-09-25 10:34:52', NULL, NULL),
(46, '4 เข้าสู่ระบบ เวลา 2020-09-28 10:23:48', NULL, NULL),
(47, '4 เข้าสู่ระบบ เวลา 2020-09-28 14:39:33', NULL, NULL),
(48, '4 เข้าสู่ระบบ เวลา 2020-09-29 10:14:02', NULL, NULL),
(49, '4 เข้าสู่ระบบ เวลา 2020-09-29 12:07:44', NULL, NULL),
(50, '4 เข้าสู่ระบบ เวลา 2020-09-29 18:57:43', NULL, NULL),
(51, '4 เข้าสู่ระบบ เวลา 2020-09-30 10:58:48', NULL, NULL),
(52, '4 เข้าสู่ระบบ เวลา 2020-10-01 17:20:33', NULL, NULL),
(53, '4 เข้าสู่ระบบ เวลา 2020-10-01 18:23:26', NULL, NULL),
(54, '4 เข้าสู่ระบบ เวลา 2020-10-05 15:46:35', NULL, NULL),
(55, '4 เข้าสู่ระบบ เวลา 2020-10-05 17:40:37', NULL, NULL),
(56, '4 เข้าสู่ระบบ เวลา 2020-10-06 14:43:14', NULL, NULL),
(57, '6 เข้าสู่ระบบ เวลา 2020-10-07 11:10:03', NULL, NULL),
(58, '6 เข้าสู่ระบบ เวลา 2020-10-08 11:12:29', NULL, NULL),
(59, '6 เข้าสู่ระบบ เวลา 2020-10-08 15:22:40', NULL, NULL),
(60, '6 เข้าสู่ระบบ เวลา 2020-10-09 11:42:28', NULL, NULL),
(61, '6 เข้าสู่ระบบ เวลา 2020-10-09 11:47:45', NULL, NULL),
(62, '6 เข้าสู่ระบบ เวลา 2020-10-09 12:05:35', NULL, NULL),
(63, '6 เข้าสู่ระบบ เวลา 2020-10-09 12:13:21', NULL, NULL),
(64, '6 เข้าสู่ระบบ เวลา 2020-10-09 13:15:14', NULL, NULL),
(65, '6 เข้าสู่ระบบ เวลา 2020-10-09 15:08:13', NULL, NULL),
(66, '6 เข้าสู่ระบบ เวลา 2020-10-12 09:48:51', NULL, NULL),
(67, '6 เข้าสู่ระบบ เวลา 2020-10-12 17:23:17', NULL, NULL),
(68, '6 เข้าสู่ระบบ เวลา 2020-10-14 11:24:27', NULL, NULL),
(69, '6 เข้าสู่ระบบ เวลา 2020-10-14 11:29:21', NULL, NULL),
(70, '6 เข้าสู่ระบบ เวลา 2020-10-14 12:04:01', NULL, NULL),
(71, '6 เข้าสู่ระบบ เวลา 2020-10-14 14:51:05', NULL, NULL),
(72, '6 เข้าสู่ระบบ เวลา 2020-10-14 16:35:17', NULL, NULL),
(73, '6 เข้าสู่ระบบ เวลา 2020-10-14 17:48:13', NULL, NULL),
(74, '6 เข้าสู่ระบบ เวลา 2020-10-15 11:44:48', NULL, NULL),
(75, '6 เข้าสู่ระบบ เวลา 2020-10-16 11:44:06', NULL, NULL),
(76, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:47:48', NULL, NULL),
(77, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:50:37', NULL, NULL),
(78, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:51:12', NULL, NULL),
(79, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:51:45', NULL, NULL),
(80, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:52:30', NULL, NULL),
(81, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:52:44', NULL, NULL),
(82, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:54:19', NULL, NULL),
(83, '6 เข้าสู่ระบบ เวลา 2020-10-16 12:54:46', NULL, NULL),
(84, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:40:48', NULL, NULL),
(85, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:43:12', NULL, NULL),
(86, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:44:08', NULL, NULL),
(87, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:45:29', NULL, NULL),
(88, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:45:49', NULL, NULL),
(89, '6 เข้าสู่ระบบ เวลา 2020-10-16 13:52:59', NULL, NULL),
(90, '6 เข้าสู่ระบบ เวลา 2020-10-19 12:51:58', NULL, NULL),
(91, '6 เข้าสู่ระบบ เวลา 2020-10-20 11:43:57', NULL, NULL),
(92, '6 เข้าสู่ระบบ เวลา 2020-10-20 14:50:01', NULL, NULL),
(93, '6 เข้าสู่ระบบ เวลา 2020-10-21 10:23:37', NULL, NULL),
(94, '6 เข้าสู่ระบบ เวลา 2020-10-21 11:18:23', NULL, NULL),
(95, '6 เข้าสู่ระบบ เวลา 2020-10-21 11:40:01', NULL, NULL),
(96, '6 เข้าสู่ระบบ เวลา 2020-10-21 16:55:00', NULL, NULL),
(97, '6 เข้าสู่ระบบ เวลา 2020-10-22 11:00:48', NULL, NULL),
(98, '6 เข้าสู่ระบบ เวลา 2020-10-26 11:03:25', NULL, NULL),
(99, '6 เข้าสู่ระบบ เวลา 2020-10-26 15:23:44', NULL, NULL),
(100, '6 เข้าสู่ระบบ เวลา 2020-10-26 17:09:44', NULL, NULL),
(101, '6 เข้าสู่ระบบ เวลา 2020-10-27 11:11:21', NULL, NULL),
(102, '6 เข้าสู่ระบบ เวลา 2020-10-27 14:31:19', NULL, NULL),
(103, '6 เข้าสู่ระบบ เวลา 2020-10-28 15:17:25', NULL, NULL),
(104, '6 เข้าสู่ระบบ เวลา 2020-10-29 14:23:15', NULL, NULL),
(105, '6 เข้าสู่ระบบ เวลา 2020-10-29 15:24:32', NULL, NULL),
(106, '6 เข้าสู่ระบบ เวลา 2020-10-29 18:22:08', NULL, NULL),
(107, '6 เข้าสู่ระบบ เวลา 2020-10-30 13:18:52', NULL, NULL),
(108, '6 เข้าสู่ระบบ เวลา 2020-10-30 15:04:50', NULL, NULL),
(109, '6 เข้าสู่ระบบ เวลา 2020-11-02 10:40:18', NULL, NULL),
(110, '6 เข้าสู่ระบบ เวลา 2020-11-03 11:24:33', NULL, NULL),
(111, '6 เข้าสู่ระบบ เวลา 2020-11-03 11:37:14', NULL, NULL),
(112, '6 เข้าสู่ระบบ เวลา 2020-11-04 11:05:58', NULL, NULL),
(113, '6 เข้าสู่ระบบ เวลา 2020-11-04 14:12:51', NULL, NULL),
(114, '8 เข้าสู่ระบบ เวลา 2020-11-04 14:13:16', NULL, NULL),
(115, '8 เข้าสู่ระบบ เวลา 2020-11-04 14:17:02', NULL, NULL),
(116, '8 เข้าสู่ระบบ เวลา 2020-11-04 14:23:01', NULL, NULL),
(117, '6 เข้าสู่ระบบ เวลา 2020-11-05 11:14:53', NULL, NULL),
(118, '6 เข้าสู่ระบบ เวลา 2020-11-05 16:46:55', '2020-11-05 09:46:55', NULL),
(119, '6 เข้าสู่ระบบ เวลา 2020-11-06 15:05:47', '2020-11-06 08:05:47', NULL),
(120, '6 เข้าสู่ระบบ เวลา 2020-11-09 10:38:18', '2020-11-09 03:38:18', NULL),
(121, '6 เข้าสู่ระบบ เวลา 2020-11-10 11:26:50', '2020-11-10 04:26:50', NULL),
(122, '6 เข้าสู่ระบบ เวลา 2020-11-11 11:21:35', '2020-11-11 04:21:35', NULL),
(123, '6 เข้าสู่ระบบ เวลา 2020-11-12 11:38:05', '2020-11-12 04:38:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `img`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 'ดดดดดดดดดด', 'tunmypro@hotmail.co.th', 'member', '', NULL, '$2y$10$Wvk.8sHo7ff1sNSy/w/m0OXaE3bOZHTzI2Wl308.YnU2jVpIe.HEe', NULL, '2020-08-13 02:00:33', '2020-11-11 08:40:41', NULL),
(2, 'ticetmp', 'ticetmp@ticetmp.com', 'member', '', NULL, '$2y$10$YLHA/XMYShOVOGAxAeCJu.FZ.CYu0toYNQOH3YfQSzxebSwUb68Di', NULL, '2020-08-13 03:49:32', '2020-09-10 08:14:17', NULL),
(3, 'TiceTmP', 'tunmypro@gmail.com', 'member', '', NULL, '$2y$10$bkcZANuUQTvfrHhm.nXNx.tCG2GVsu5d3yjjMR/RCpnREKSw/tsi2', NULL, '2020-08-17 02:50:45', '2020-08-21 00:29:01', NULL),
(4, '1111', '1@1.com', 'admin', '', NULL, '$2y$10$uT2mDui4Aq3L6c/qzvNs7.UBBd3gheBZ6Xt8dNCbu5RHLFBWavRUK', NULL, '2020-08-31 09:27:03', '2020-10-06 08:19:44', NULL),
(5, 'admin', 'admin@admin.com', 'admin', '', NULL, '$2y$10$vLiEK4SsyUsFuh0P3jDK0.9PsPvphyAW/YOUwXz4vGxChwO8hxy26', NULL, '2020-09-01 03:30:03', '2020-09-01 03:30:03', NULL),
(6, 'Administat0r', 'a@a.com', 'admin', '', NULL, '$2y$10$316nj6kP0Dpb1q4oeBvyGe2jqMKrs46LKmDqFA5QptNyDPCNB9h5e', NULL, '2020-10-06 08:21:37', '2020-10-09 05:13:34', NULL),
(7, 'Tester', '123@123.com', 'user', '', NULL, '$2y$10$dy27Lva2TFu9W780JBvNCeTo8fir2J.n.n0W8MzkQrghXExCTmDeG', NULL, '2020-10-12 10:20:50', '2020-10-12 10:20:50', NULL),
(8, 'Tester ?', 't@t.com', 'member', '', NULL, '$2y$10$swT77.iDfXudrgHfudqmDOYEs0vPHWiAMEYl7AR7Lo.G1d8q/1jHa', NULL, '2020-11-04 07:12:00', '2020-11-05 11:39:52', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `badwords`
--
ALTER TABLE `badwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_activity`
--
ALTER TABLE `content_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eng`
--
ALTER TABLE `eng`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homebanner`
--
ALTER TABLE `homebanner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `img`
--
ALTER TABLE `img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imgslide`
--
ALTER TABLE `imgslide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_header`
--
ALTER TABLE `menu_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topicreport`
--
ALTER TABLE `topicreport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlogs`
--
ALTER TABLE `userlogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `badwords`
--
ALTER TABLE `badwords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `content_activity`
--
ALTER TABLE `content_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `eng`
--
ALTER TABLE `eng`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `homebanner`
--
ALTER TABLE `homebanner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `img`
--
ALTER TABLE `img`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `imgslide`
--
ALTER TABLE `imgslide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `menu_header`
--
ALTER TABLE `menu_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `topicreport`
--
ALTER TABLE `topicreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `userlogs`
--
ALTER TABLE `userlogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
