<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'รหัสผ่านได้ถูกรีเซ็ตแล้ว!',
    'sent' => 'ระบบได้ส่งการรีเซ็ตเข้าไปที่อีเมลแล้ว!',
    'throttled' => 'กรุณารอและลองอีกครั้ง',
    'token' => 'โทเค็นได้หมดอายุแล้วกรุณาทำรายการใหม่',
    'user' => "ไม่พบข้อมูลอีเมลนี้ในระบบ",

];
