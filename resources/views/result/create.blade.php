@extends('layouts.appadmin')
<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
@section('content')
<form method="POST" action="{{ route('result.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="container py-1">
		<!--Section: Content-->
		<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
			<!--Grid row-->
			<div class="row d-flex justify-content-center">
				<!--Grid column-->
				<div class="col-md-12">
					<!-- Default form contact -->
					<form class="text-center" action="#!">
						<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลผลการดำเนินงาน</h3>
						<div class="row">
							<div class="col-12">
								<span>นำรูปไปแสดงที่หน้าหลัก(เลือกเพียงรูปเดียว)</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
									   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
							</div>
						</div>
						<!-- Name -->
						<div class="md-form">
							<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">ชื่อหัวข้อ</label>
						</div>
						<div class="form-group row">
							<label for="text" class="col-12">รายละเอียด</label>
							<div class="col-12">
							<textarea placeholder="รายละเอียด" class="details" name="details">{{ old('details') }}
							</textarea>
								<script>
                                    let CSRFToken = $('meta[name="csrf-token"]').attr('content');
                                    let options = {
                                        filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
                                        filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
                                        filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
                                        filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
                                    };
								</script>
								<script>
                                    CKEDITOR.replace('details', options);
								</script>
							</div>
						</div>
						<div class="md-form">
							<div align="center">
								ตัวอย่างรูป
								<div id="previewImg"></div>
							</div>
							<div class="custom-file">
								<i role="presentation"  class="far fa-images prefix"></i>
								<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
								<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
							</div>
						</div>
						<script type="text/javascript">
							  window.preview = function (input) {
								if (input.files && input.files[0]) {
								  $("#previewImg").html("");
								  $(input.files).each(function () {
									var reader = new FileReader();
									reader.readAsDataURL(this);
									reader.onload = function (e) {
									  $("#previewImg").append("<img alt=\"\" class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
									}
								  });
								}
							  }
						</script>
						<div class="md-form">
							<select type="text" id="category" name="category" class="form-control col-md-12" autocomplete="off">
								<option disabled selected hidden>เลือกประเภท</option>
								<option value="ผลการประเมิน NCD progress monitor">ผลการประเมิน NCD progress monitor</option>
								<option value="ผลการ ประเมินแผน">ผลการ ประเมินแผน</option>
								<option value="ผลการดำเนินงานภายใต้แผน">ผลการดำเนินงานภายใต้แผน</option>
							</select>
						</div>
						{{--<div class="md-form" style="display: none" id="forsubcategory">--}}
							{{--<select type="text" name="subcategory" class="form-control col-md-12" autocomplete="off">--}}
								{{--<option disabled selected hidden>เลือกประเภท</option>--}}
								{{--<option value="1">ยุทธศาสตร์ที่ 1</option>--}}
								{{--<option value="2">ยุทธศาสตร์ที่ 2</option>--}}
								{{--<option value="3">ยุทธศาสตร์ที่ 3</option>--}}
								{{--<option value="4">ยุทธศาสตร์ที่ 4</option>--}}
								{{--<option value="5">ยุทธศาสตร์ที่ 5</option>--}}
								{{--<option value="6">ยุทธศาสตร์ที่ 6</option>--}}
							{{--</select>--}}
						{{--</div>--}}
						{{--<div class="md-form">--}}
							{{--<input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control col-md-12" autocomplete="off">--}}
							{{--<label for="url">ลิ้งดาวน์โหลด</label>--}}
						{{--</div>--}}
						<div class="row">
							<div class="md-form col-12">
								<a style="margin-top: 0" class="btn btn-block btn-sm btn-outline-primary" name="addDom">เพิ่มดาวน์โหลด</a>
								<div class="row" id="addDom">
								</div>
							</div>
						</div>
						<!-- Send button -->
						<button class="btn btn-info btn-block" type="submit" style="margin-top: 50px">บันทึก</button>
						<!-- Basic dropdown -->
					</form>
					<!-- Default form contact -->
				</div>
				<!--Grid column-->
			</div>
			<!--Grid row-->
		</section>
		<!--Section: Content-->
	</div>

</form>
@endsection
@include('layouts.onlyDatatable')
<script>
    // function getsubcategory(select) {
	 //    if(select.value == 'ผลการดำเนินงานภายใต้แผน'){
    //         $('#forsubcategory').show();
    //     }else{
    //         $('#forsubcategory').removeAttr("style").hide();
		// }
    // }


	$(document).ready(function() {
        let i = 1;
        $("a[name='addDom']").click(function() {
        $('<div class="col-5" id="file'+i+'">')
            .append($("<input />", { class:'form-control',name:"namefile[]", id:"img"+i , autoComplete:"off", placeholder:"ชื่อไฟล์", required:'true'}))
            .appendTo('#addDom');
        $('<div class="col-6" id="namefile'+i+'">')
            .append($("<input />", { class:'form-control',name:"url[]", id:"namefile"+i , autoComplete:"off",placeholder:"ลิ้งดาวน์โหลด", required:'true' }))
        .appendTo('#addDom');
        $('<p class="col-1" style="cursor: pointer" id="'+i+'">'+'-'+'</p>')
            .appendTo('#addDom');
        i++;
    });
        //
        // window.preview1 = function (input) {
		// 	if (input.getElementsByTagName("input")[0].files && input.getElementsByTagName("input")[0].files[0]) {
		// 		$(input.getElementsByTagName("span")[0]).html("");
		// 		$(input.getElementsByTagName("input")[0].files).each(function () {
		// 			var reader = new FileReader();
		// 			reader.readAsDataURL(this);
		// 			reader.onload = function (e) {
		// 				$(input.getElementsByTagName("span")[0]).append("<img class='thumb' style=\"max-height: 100px;max-width: 100%;\" src='" + e.target.result + "'>");
		// 			}
		// 		});
		// 		input.getElementsByTagName("label")[0].style.backgroundImage = 'none';
		// 	}
		// };

        $("#addDom").on('click', 'p', function() {
            let id = $(this).attr('id');
            const namefile = 'div[id='+'\'namefile'+id+'\']';
            const file = 'div[id='+'\'file'+id+'\']';
            $($(this)).remove();
            $(namefile).remove();
            $(file).remove();
        });
    });
</script>
