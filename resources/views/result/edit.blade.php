@extends('layouts.appadmin')
<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
@section('content')
	<form method="POST" action="{{ route('result.update',$data->id) }}" enctype="multipart/form-data">
		@csrf
		<div class="container py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-12">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">แก้ไขข้อมูลผลการดำเนินงาน</h3>
							<div class="row">
								<div class="col-12">
									<span>นำรูปไปแสดงที่หน้าหลัก(เลือกเพียงรูปเดียว)</span>
									<input id="active" name="active" type="checkbox" data-toggle="toggle"
										   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow" {{$data->active == 1 ? 'checked':''}}/>
								</div>
							</div>
							<div class="md-form">
								<input type="text" id="name" name="name" value="{{ $data->name }}" class="form-control col-md-12" autocomplete="off" required>
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<div class="form-group row">
								<label for="text" class="col-12">รายละเอียด</label>
								<div class="col-12">
								<textarea placeholder="รายละเอียด" class="details" name="details">{{ $data->details }}</textarea>
									<script>
                                        let CSRFToken = $('meta[name="csrf-token"]').attr('content');
                                        let options = {
                                            filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
                                            filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
                                            filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
                                            filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
                                        };
									</script>
									<script>
                                        CKEDITOR.replace('details', options);
									</script>
								</div>
							</div>
							{{--<div class="md-form">--}}
								{{--<input type="text" id="details" name="details" value="{{ $data->details }}" class="form-control col-md-12" autocomplete="off" required>--}}
								{{--<label for="details">รายละเอียด</label>--}}
							{{--</div>--}}
							<div class="md-form">
								@if($data->img !== null)
									<div align="center">
										ตัวอย่างรูปเก่า
									</div>
									<div align="center">
										<img alt="thumb" class='thumb' style="max-height: 100px;max-width: 100px;" src="{{$site}}/uploads/{{$data->img->path}}">
									</div>
								@endif
								<div align="center">
									ตัวอย่างรูปที่เปลี่ยน
								</div>
								<div align="center">
									<div id="previewImg"></div>
								</div>
								<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
								</div>
							</div>
							<script type="text/javascript">
								window.preview = function (input) {
                                    if (input.files && input.files[0]) {
                                        $("#previewImg").html("");
                                        $(input.files).each(function () {
                                            var reader = new FileReader();
                                            reader.readAsDataURL(this);
                                            reader.onload = function (e) {
                                                $("#previewImg").append("<img alt=\"\" class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
                                            }
                                        });
                                    }
                                }
							</script>
							<div class="md-form">
								<select type="text" id="category" name="category" class="form-control col-md-12" autocomplete="off">
									<option disabled selected hidden>เลือกประเภท</option>
									<option  {{$data->category == 'ผลการประเมิน NCD progress monitor' ? 'selected' : ''}}>ผลการประเมิน NCD progress monitor</option>
									<option  {{$data->category == 'ผลการ ประเมินแผน' ? 'selected' : ''}}>ผลการ ประเมินแผน</option>
									<option  {{$data->category == 'ผลการดำเนินงานภายใต้แผน' ? 'selected' : ''}}>ผลการดำเนินงานภายใต้แผน</option>
								</select>
							</div>
							<div class="row">
								<div class="md-form col-12">
									<a style="margin-top: 0" class="btn btn-block btn-sm btn-outline-primary" name="addDom">เพิ่มดาวน์โหลด</a>
									<div class="row">
										<div class="col-5">
											<span>ชื่อไฟล์</span>
										</div>
										<div class="col-6">
											<span>ลิ้งดาวน์โหลด</span>
										</div>
									</div>
									<div class="col-1"></div>
									<div class="row" id="addDom">
										@if($data->files != null)
											@foreach($data->files->name as $key => $row)
												<div class="col-5" id="namefile{{$key+1}}">
													<input class='form-control' name="namefile[{{$key+1}}]" id="img{{$key+1}}" value="{{$row}}" autoComplete="off" placeholder="รายละเอียด" required>
												</div>
												<div class="col-6" id="file{{$key+1}}">
													<input class='form-control' name="url[{{$key+1}}]" id="namefile{{$key+1}}" value="{{$data->files->files[$key]}}" autoComplete="off" required>
												</div>
												<p class="col-1" style="cursor: pointer" id="{{$key+1}}">-</p>
											@endforeach
										@endif
									</div>
								</div>
							</div>
							<!-- Send button -->
							{{method_field('PUT')}}
							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
							<!-- Basic dropdown -->
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>

	</form>
	<?php
		$count = ($data->files != null ? count($data->files->name):0);
	?>
@endsection
@include('layouts.onlyDatatable')
<script>
    $(document).ready(function() {
        let i = {{ $count }}+1;

        $("a[name='addDom']").click(function() {
            $('<div class="col-5" id="file'+i+'">')
                .append($("<input />", { class:'form-control', name:"namefile["+i+"]" , id:"img"+i ,autoComplete:"off", placeholder:"ชื่อไฟล์", required:'true' }))
                .appendTo('#addDom');
            $('<div class="col-6" id="namefile'+i+'">')
                .append($("<input />", { class:'form-control',name:"url["+i+"]", id:"namefile"+i,autoComplete:"off",placeholder:"ลิ้งดาวน์โหลด",required:'true' }))
                .appendTo('#addDom');
            $('<p class="col-1" style="cursor: pointer" id="'+i+'">'+'-'+'</p>')
                .appendTo('#addDom');
            i++;
        });

        $("#addDom").on('click', 'p', function() {
            let id = $(this).attr('id');
            const namefile = 'div[id='+'\'namefile'+id+'\']';
            const file = 'div[id='+'\'file'+id+'\']';
            $($(this)).remove();
            $(namefile).remove();
            $(file).remove();
        });
    });
</script>
