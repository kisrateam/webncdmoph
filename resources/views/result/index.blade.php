@extends('layouts.appadmin')
<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
<style>
	#btn-custom{
		border: 1px solid #508EBF;
		box-sizing: border-box;
		border-radius: 20px;
	}
	a#btn-custom.active{
		border: 1px solid #508EBF;
		color: white;
		background-color: #508EBF;
	}
    .ck-editor {
        min-height: 700px;
    }
    .green-border-focus .form-control:focus {
        border: 1px solid #8bc34a;
        box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
    }
</style>
@section('content')
    <script>
        let CSRF = $('meta[name="csrf-token"]').attr('content');
        let options = {
            filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRF,
            filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRF,
        };
        let optionforglobal = {
            filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRF,
            filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRF,
            customConfig: 'http://localhost:8000/CKEditor/configforglobal.js'
        }
    </script>
	<div class="container-fluid">
		<div class="col-12">
			<img alt="การจัดการผลการดำเนินการ" style="width: 44px;
    height: 44px;
    padding-bottom: 5px;" src="{{$site}}/Defaultimg/4.svg"/>
			<span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการผลการดำเนินการ</span>
			<hr style="border: 1px solid #3D3D3D;">
		</div>
		<ul class="nav nav-pills">
			<li class="nav-item">
				<a id="btn-custom" class="nav-link btn active" data-toggle="pill" href="#menu1">ผลการประเมิน 9 global targets</a>
			</li>
			<li class="nav-item">
				<a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu2">ผลการประเมิน NCD progress monitor</a>
			</li>
			<li class="nav-item">
				<a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu3">ผลการ ประเมินแผน</a>
			</li>
			<li class="nav-item">
				<a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu4">ผลการดำเนินงานภายใต้แผน</a>
			</li>
		</ul>

		<div class="tab-content">
			<div id="menu1" class="container-fluid tab-pane active"><br>
                <form method="POST" action="{{ route('result.store') }}" enctype="multipart/form-data">
                @csrf
				<div class="row">
                    <?php $j = 0 ?>
                        <div class="col-2 text-center">
                            <span>รูป</span>
                        </div>
                        <div class="col-6 text-center">
                            <span>รายละเอียด</span>
                        </div>
                        <div class="col-4 text-center">
                            <span>ลิ้ง</span>
                        </div>
                        <div class="col-12">
                            <br/>
                        </div>
                        @for($i = 1;$i <= 9;$i++)
                        @foreach($dataicon as $row)
                            @if($row->name == '9global'.$i)
                                <div class="row" style="margin: 10px 0">
                                    <div class="col-2 text-center" style="height: 100px;cursor: pointer" onchange="preview1(this);" id="file{{$i}}">
                                        <label for="img{{$i}}" id="uploadimg{{$i}}" style="cursor: pointer;width: 100px;height: 100px;background-image:url('{{ $row->img!=null?asset('uploads/'.$row->img->path):asset('Defaultimg/imgupload.svg') }}');background-size:100% 100%;"></label>
                                        <input type="file" name="imgicon[{{$i}}]" id="img{{$i}}" style="display:none" accept="image/*">
                                    </div>
                                    <div class="col-6" id="namefile{{$i}}">
                                        <textarea id="ck{{$i}}" placeholder="รายละเอียด" class="details" name="9global[{{$i}}]">{!! $row->details !!}</textarea>
                                        <script>
                                            {{--CKEDITOR.replace('ck{{$i}}', optionforglobal);--}}
                                            CKEDITOR.replace( 'ck{{$i}}',optionforglobal);
                                        </script>

                                        {{--<div class="form-group green-border-focus">--}}
                                        {{--<textarea class='form-control' name="9global[{{$i}}]" id="namefile{{$i}}" autoComplete="off" placeholder="รายละเอียด" rows="4">{{$row->details}}</textarea>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-4">
                                        <div class="md-form">
                                            <input type="text" id="input{{$i}}" name="input[{{$i}}]" class="form-control" value="{{$row->imguniqicon}}">
                                            <label for="input{{$i}}">ลิ้ง URL</label>
                                        </div>
                                    </div>
                                </div>
                                <?php $j++ ?>
                            @endif
                        @endforeach
                            @if($j == 0)
                                <div class="col-2 text-center" style="height: 100px;margin: 10px 0;cursor: pointer" onchange="preview1(this);" id="file{{$i}}">
                                    <label for="img{{$i}}" id="uploadimg{{$i}}" style="cursor: pointer;width: 100px;height: 100px;background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');background-size:100% 100%;"></label>
                                    <input type="file" name="imgicon[{{$i}}]" id="img{{$i}}" style="display:none" accept="image/*">
                                </div>
                                <div class="col-6" id="namefile{{$i}}">
                                    <div class="form-group green-border-focus">
                                        <textarea class='form-control' name="9global[{{$i}}]" id="namefile{{$i}}" autoComplete="off" placeholder="รายละเอียด" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="md-form">
                                        <input type="text" id="input{{$i}}" name="input[{{$i}}]" class="form-control">
                                        <label for="input{{$i}}">ลิ้ง URL</label>
                                    </div>
                                </div>
                            @else
                                <?php $j = 0 ?>
                            @endif
                    @endfor
                        <script>
                            window.preview1 = function (input) {
                                if (input.getElementsByTagName("input")[0].files && input.getElementsByTagName("input")[0].files[0]) {
                                    $(input.getElementsByTagName("span")[0]).html("");
                                    $(input.getElementsByTagName("input")[0].files).each(function () {
                                        let reader = new FileReader();
                                        reader.readAsDataURL(this);
                                        reader.onload = function (e) {
                                            input.getElementsByTagName("label")[0].style.backgroundImage = "url('" + e.target.result + "')";
                                        }
                                    });
                                }
                            };
                        </script>
                </div>
                <br/>
                <button type="submit" class="btn btn-outline-primary btn-block">ยืนยัน</button>
                </form>
                <br/>
                <img alt="สถานการณ์โรคไม่ติดต่อ" style="width: 44px;
    height: 44px;
    padding-bottom: 5px;" src="{{$site}}/Defaultimg/4.svg"/>
                <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการตาราง สถานการณ์โรคไม่ติดต่อตาม 9 เป้าหมายระดับโลกของประเทศไทย</span>
                <hr style="border: 1px solid #3D3D3D;">
                <br/>
                <?php
                    $resulttable = \App\result::where('name','resulttable')->first();
                ?>
                @if($resulttable)
                    <form method="POST" action="{{ url($site.'/admin/result/resulttable') }}" enctype="multipart/form-data">
                        @csrf
                        <textarea placeholder="ตาราง" name="resulttable">
                            {{ $resulttable->details }}
                        </textarea>
                        <script>
                            CKEDITOR.replace('resulttable', options);
                        </script>
                        <br/>
                        <button type="submit" class="btn btn-outline-primary btn-block">ยืนยัน</button>
                    </form>
                @endif

			</div>
			<div id="menu2" class="container-fluid tab-pane fade"><br>
				<table id="result2" class="table table-striped table-bordered" style="width:100%">
					<thead>
					<tr>
						<th>ไอดี</th>
						<th>ชื่อหัวข้อ</th>
						<th>แสดงหน้าหลัก</th>
					</tr>
					</thead>
					<tbody>
					@foreach($data as $row)
						@if($row->category == 'ผลการประเมิน NCD progress monitor')
							<tr>
								<td>{{$row->id}}</td>
								<td>{{$row->name}}</td>
								<td>{{($row->active == 1 ? 'แสดงหน้าหลัก':'ไม่แสดง')}}</td>
							</tr>
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
			<div id="menu3" class="container-fluid tab-pane fade"><br>
				<table id="result3" class="table table-striped table-bordered" style="width:100%">
					<thead>
					<tr>
						<th>ไอดี</th>
						<th>ชื่อหัวข้อ</th>
						<th>แสดงหน้าหลัก</th>
					</tr>
					</thead>
					<tbody>
					@foreach($data as $row)
						@if($row->category == 'ผลการ ประเมินแผน')
							<tr>
								<td>{{$row->id}}</td>
								<td>{{$row->name}}</td>
								<td>{{($row->active == 1 ? 'แสดงหน้าหลัก':'ไม่แสดง')}}</td>
							</tr>
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
			<div id="menu4" class="container-fluid tab-pane fade"><br>
				<table id="result4" class="table table-striped table-bordered" style="width:100%">
					<thead>
					<tr>
						<th>ไอดี</th>
						<th>ชื่อหัวข้อ</th>
						<th>แสดงหน้าหลัก</th>
					</tr>
					</thead>
					<tbody>
					@foreach($data as $row)
						@if($row->category == 'ผลการดำเนินงานภายใต้แผน')
							<tr>
								<td>{{$row->id}}</td>
								<td>{{$row->name}}</td>
								<td>{{($row->active == 1 ? 'แสดงหน้าหลัก':'ไม่แสดง')}}</td>
							</tr>
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@include('layouts.onlyDatatable')

<script type="text/javascript">
    let id = 0;

    $(document).ready(function() {
        let table = $('#result1').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 5,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = '{{route('result.create')}}';
                    }
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result1 .selected').each(function() {
                            window.location.href = '{{url('/admin/result')}}'+'/'+id[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result1 .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });

        table.buttons().container().appendTo( '#result1_wrapper .col-md-6:eq(0)' );

        $('#result1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#result1').on( 'click', 'tr', function () {
            id = table.row( this ).data();
        } );

        let table2 = $('#result2').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 5,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = '{{url('/admin/result/create')}}';
                    }
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result2 .selected').each(function() {
                            window.location.href = '{{url('/admin/result')}}'+'/'+id[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result2 .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table2.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });

        table2.buttons().container().appendTo( '#result2_wrapper .col-md-6:eq(0)' );

        $('#result2 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table2.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#result2').on( 'click', 'tr', function () {
            id = table2.row( this ).data();
        } );

        let table3 = $('#result3').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 5,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = '{{url('/admin/result/create')}}';
                    }
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result3 .selected').each(function() {
                            window.location.href = '{{url('/admin/result')}}'+'/'+id[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result3 .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table3.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });

        table3.buttons().container().appendTo( '#result3_wrapper .col-md-6:eq(0)' );

        $('#result3 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table3.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#result3').on( 'click', 'tr', function () {
            id = table3.row( this ).data();
        } );

        let table4 = $('#result4').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 5,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = '{{url('/admin/result/create')}}';
                    }
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result4 .selected').each(function() {
                            window.location.href = '{{url('/admin/result')}}'+'/'+id[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#result4 .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table4.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });

        table4.buttons().container().appendTo( '#result4_wrapper .col-md-6:eq(0)' );

        $('#result4 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table4.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#result4').on( 'click', 'tr', function () {
            id = table4.row( this ).data();
        } );

        function goDelete() {
            let _token = $('input[name="_token"]').val();
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{ route('result.store') }}"+'/'+id[0],
                    data: { _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    },
                    error: function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'อุ๊บบ...',
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    } );
</script>
