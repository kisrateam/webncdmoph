<html lang="th">
@extends('layouts.app')
<title>หน้าแรก</title>
@section('content')
    <h1 role="heading" hidden>หน้าแรก</h1>
    @include('leftmenu-carouselpc')
  <br/>
@endsection
@section('content2')
  <div class="container">
      <h1 role="heading" hidden>หน้าแรก</h1>
    @include('leftmenu-carouselmobile')
    <br/>
    @include('team-new')
    <br/>
    @include('content-home')
    <br/>
    @include('result-home')
    <br/>
    @include('new-carousel')
    <br/>
    @include('link-home')
    <br/>
  </div>
@endsection
</html>
