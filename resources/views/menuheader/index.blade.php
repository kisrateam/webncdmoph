@extends('layouts.appadmin')
@section('content')
    <div class="container-fluid">
    <form method="get" action="{{ route('link.create') }}">
        @csrf
    </form>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ไอดี</th>
            <th>ชื่อ</th>
            {{--<th>ตำแหน่ง</th>--}}
            <th>เมนูย่อยของ</th>
            <th>ลำดับ</th>
            <th>link</th>
            <th>ประเภท</th>
            <th>สถานะ</th>
            {{--<th>css</th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->name}}</td>
                {{--<td>{{$row->level}}</td>--}}
                @if($row->getName != null)
                    <td>{{$row->getName->name}}</td>
                @else
                    <td>เมนูหลัก</td>
                @endif
                @if($row->parent_index != null)
                    <td>{{$row->parent_index}}</td>
                @else
                    <td>-</td>
                @endif
                @if($row->link != null)
                    <td>{{$row->link}}</td>
                @else
                    <td>-</td>
                @endif
                <td>{{$row->type}}</td>
                @if($row->active)
                    <td style="color: green;">แสดง</td>
                @else
                    <td style="color: red;">ซ่อน</td>
                @endif
                {{--@if($row->css != null)--}}
                    {{--<td>{{$row->css}}</td>--}}
                {{--@else--}}
                    {{--<td>-</td>--}}
                {{--@endif--}}
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection

@include('layouts.onlyDatatable')

<script type="text/javascript">
    var id = 0;

    $(document).ready(function() {
        var table = $('#example').DataTable({
            lengthChange: false,
            rowGroup: {
                dataSrc: [ 2 ]
            },
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    targets: [ 2 ],
                    visible: false
                }
            ],
            "pageLength": 20,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = '{{ route('admin') }}/menuheader/create';
                    }
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#example .selected').each(function() {
                            window.location.href =  '{{ route('admin') }}/menuheader/'+id[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#example .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "หากลบข้อมูล เมนูหลัก เมนูย่อยที่อยู่ด้วยกันจะถูกลบด้วย !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });

        table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );

        $('#example tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') || $(this).hasClass('dtrg-group dtrg-start dtrg-level-0')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#example').on( 'click', 'tr', function () {
            id = table.row( this ).data();
        } );

    } );

    function goDelete() {
        var _token = $('input[name="_token"]').val();
        $.ajax(
            {
                type: "DELETE",
                url: "{{ route('menuheader.store') }}"+'/'+id[0],
                data:{ _token:_token  },
                success: function (data) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'ข้อมูลถูกลบแล้ว',
                        showConfirmButton: false,
                        timer: 3000
                    });
                },
                error: function (data) {
                    console.log(data);
                    Swal.fire({
                        icon: 'error',
                        title: 'อุ๊บบ...',
                        text: 'มีบางอย่างผิดพลาด !'
                    })
                }
            });
    }
</script>