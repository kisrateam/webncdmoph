@extends('layouts.appadmin')
@section('content')
	<form method="POST" action="{{ route('menuheader.store') }}">
		@csrf
		<div class="container py-1">
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<div class="row d-flex justify-content-center">
					<div class="col-md-6">
						<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลเมนู</h3>
						<label for="type">ประเภท</label>
						<select class="browser-default custom-select" id="type" name="type" onchange="menu(this);">
							<option disabled selected hidden>เลือกประเภท</option>
							<option {{$data->type == "menu" ? "selected":''}} value="menu">เมนู</option>
							<option {{$data->type == "menulink" ? "selected":''}} value="menulink">เมนู(แบบลิ้ง)</option>
							<option {{$data->type == "link" ? "selected":''}} value="link">ลิ้ง</option>
							<option {{$data->type == "linkinlink" ? "selected":''}} value="linkinlink">Link In Link In Drop-down</option>
						</select>
						<div id="level1">
							<label for="level1">ระดับ</label>
							<select class="form-control" name="level">
								<option disabled selected hidden>เลือกตำแหน่งแสดง</option>
								<option {{$data->level == "up" ? "selected":''}} value="up">ด้านบน</option>
								<option {{$data->level == "down" ? "selected":''}} value="down">ด้านล่าง</option>
							</select>
						</div>
						<div id="level2">
							<label for="level2">ระดับ</label>
							<select class="form-control" name="level" onchange="child(this.value,false);">
								<option disabled selected hidden>เลือกตำแหน่งแสดง</option>
								<option	{{$data->level == "up" ? "selected":''}} value="up">ด้านบน</option>
								<option {{$data->level == "down" ? "selected":''}} value="down">ด้านล่าง</option>
							</select>
						</div>
						<div id="level3" style="display: none">
							<label for="level3">ระดับ</label>
							<select class="form-control" name="level" onchange="childinchild(this.value);">
								<option disabled selected hidden>เลือกตำแหน่งแสดง</option>
								<option value="up">ด้านบน</option>
								<option value="down">ด้านล่าง</option>
							</select>
						</div>
						<input id="parent" style="display: none;" value="{{$data->parent_id != null ? $data->parent_id:''}}">
						<div id="parent_id2">
							<label for="parent_id">ย่อยเมนูของ</label>
							<select class="form-control" id="parent_id" name="parent_id">
								<option></option>
							</select>
						</div>
						<div class="md-form" id="name">
							<input type="text" name="name" class="form-control col-12" autocomplete="off" value="{{ $data->name }}" required>
							<label for="name">ชื่อเมนู</label>
						</div>
						<div class="md-form" id="link">
							<input type="text" name="link" class="form-control col-12" autocomplete="off" value="{{ $data->link != null ? $data->link:'' }}">
							<label for="link">ลิ้ง</label>
						</div>
						<style>
							.green-border-focus .form-control:focus {
								border: 1px solid #8bc34a;
								box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
							}
						</style>
						<div class="form-group green-border-focus" id="css" >
							<textarea class="form-control" name="css" placeholder="ใส่ CSS (ไม่มีปล่อยว่าง)" rows="3"></textarea>
						</div>
						<div class="md-form">
							<button id="sub" class="btn btn-info btn-block" type="submit">บันทึก</button>
						</div>
					</div>
				</div>
			</section>
		</div>
	</form>
@endsection
@include('layouts.onlyDatatable')
<script type="text/javascript">

    $( "#css" ).ready(function () {
		if(this.getElementById("type").value == "menu"){
			document.getElementById('level1').style.display = "block";
			document.getElementById('level2').style.display = "none";
			document.getElementById('parent_id2').style.display = "none";
			document.getElementById('link').style.display = "none";
			document.getElementById('name').style.display = "block";
			document.getElementById('css').style.display = "block";
		}else if(this.getElementById("type").value == "menulink"){
			document.getElementById('level1').style.display = "block";
			document.getElementById('level2').style.display = "none";
			document.getElementById('parent_id2').style.display = "none";
			document.getElementById('link').style.display = "block";
			document.getElementById('name').style.display = "block";
			document.getElementById('css').style.display = "block";
		}else if(this.getElementById("type").value == "link"){
			document.getElementById('level1').style.display = "none";
			document.getElementById('level2').style.display = "block";
			child(this.getElementsByName("level")[1].value,
				this.getElementById("parent").value);
			document.getElementById('parent_id2').style.display = "block";
			document.getElementById('link').style.display = "block";
			document.getElementById('name').style.display = "block";
			document.getElementById('css').style.display = "block";
		}
    });

    function menu(value){
        if(value.value == "menu"){
            document.getElementById('level1').style.display = "block";
            document.getElementById('level2').style.display = "none";
            document.getElementById('parent_id2').style.display = "none";
            document.getElementById('link').style.display = "none";
            document.getElementById('name').style.display = "block";
            document.getElementById('css').style.display = "block";
        }else if(value.value == "menulink"){
            document.getElementById('level1').style.display = "block";
            document.getElementById('level2').style.display = "none";
            document.getElementById('parent_id2').style.display = "none";
            document.getElementById('link').style.display = "block";
            document.getElementById('name').style.display = "block";
            document.getElementById('css').style.display = "block";
        }else if(value.value == "link"){
            document.getElementById('level1').style.display = "none";
            document.getElementById('level2').style.display = "block";
            document.getElementById('parent_id2').style.display = "block";
            document.getElementById('link').style.display = "block";
            document.getElementById('name').style.display = "block";
            document.getElementById('css').style.display = "block";
        }
    }

    function child(data,chk){
        let name=data;
        $.ajax({
            url: '/admin/getchild/'+name,
            type: 'get',
            dataType: 'json',
            success: function(response){
                $("#parent_id").empty();
                let len = 0;
                if(response['data'] != null){
                    len = response['data'].length;
                }
                if (!chk){
					if(len != 0){
						for(let i=0; i<len; i++){
							let id = response['data'][i].id;
							let name = response['data'][i].name;
							let option = "<option value='"+id+"'>"+name+"</option>";
							$("#parent_id").append(option);
							}
					}else{
						let id = null;
						let name = "ไม่พบค่า";
						let option = "<option value='"+id+"'>"+name+"</option>";
						$("#parent_id").append(option);
					}
				}else{
                    for(let i=0; i<len; i++){
                        let id = response['data'][i].id;
                        let name = response['data'][i].name;
                        let option = "";
                        if (id == chk){
                            option = "<option value='"+id+"' selected>"+name+"</option>";
                        } else{
                            option = "<option value='"+id+"'>"+name+"</option>";
                        }
                        $("#parent_id").append(option);
                    }
				}
			}
        });
    }


</script>
