@extends('layouts.app')
<style>
    html { height: 100%; }
    body { height: 100%; }
</style>
@section('content')
    <div class="container-lg" id="forpc">
        <div class="row">
            <div class="col-12">
                <a style="background: #FAFAFA;">
                    <span>
                        <img src="{{$site}}/Defaultimg/rssicon.png" width="80" height="33">
                    </span>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <h2 style="color: #508EBF;">RSS feeds</h2>
                <br/>
            </div>
            <div class="col-12">
                <h5 style="font-weight: bold">วิธีการรับ RSS</h5>
                <h6>คุณสามารถคัดลอก ให้คัดลอก (copy) URLs ไปเพิ่มในฟังก์ชั่นรับ RSS ที่เว็บเบราวเซอร์, โปรแกรมอ่าน RSS, บล๊อคหรือเว็บ Social Network เพียงเท่านี้ คุณก็สามารถติดตามข่าวและข้อมูลจากแผนการป้องกันและควบคุมโรค ไม่ติดต่อ 5 ปี (พ.ศ. 2560-2564) ได้ตลอดเวลา</h6>
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                <br/>
                <br/>
                <h5 style="font-weight: bold">หัวข้อ</h5>
            </div>
            <div class="col-7">
                <br/>
                <br/>
                <h5 style="font-weight: bold">Copy URLs ไปยัง RSS Reader</h5>
            </div>
            <div class="col-12">
                <hr style="border: 1px solid rgba(0,0,0,.3);">
            </div>
            <?php
                $arrName = ['ข่าวสารทั้งหมด',
                    'ข่าวกิจกรรม',
                    'ข่าวประชาสัมพันธ์',
                    'สื่อเผยแพร่แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560-2564)',
                    'สถานการณ์ / ข้อมูล',
                    'ผลการดำเนินงาน ผลการติดตาม และประเมินผลแผน/ยุทธศาสตร์',
                    'คู่มือแนวทาง สื่อ'];
                $arrLinks = [$site.'/feed/News',
                    $site.'/feed/News_activity',
                    $site.'/feed/News_release',
                    $site.'/feed/News_plan',
                    $site.'/feed/News_status',
                    $site.'/feed/News_result',
                    $site.'/feed/News_media',];
            ?>
            @foreach($arrName as $keys => $name)
                <div class="col-5">
                    <h6>{{$name}}</h6>
                </div>
                <div class="col-7" style="width: 100%;word-wrap: break-word">
                        <a href="{{$arrLinks[$keys]}}" target="_blank">{{$arrLinks[$keys]}}</a>
                </div>
                <div class="col-12">
                    <hr style="border: 1px solid rgba(0,0,0,.1)">
                </div>
            @endforeach
        </div>
    </div>
@endsection
