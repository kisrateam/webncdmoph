@extends('layouts.app')
<style>
	html { height: 100%; }
	body { height: 100%; }

	#img-custom{
		position: relative;
		z-index: -1;
		width:100%;
		overflow: hidden;
	}
</style>
@section('contentteam')
<div class="container" id="forpc">
	<div class="col-12">
		@if($data)
			<img class="img-fluid" src="{{$site}}/uploads/{{$data->path}}" width="100%" alt="{{$data->name}}">
		@else
			<img class="img-fluid" src="{{$site}}/Defaultimg/noimg.jpg" width="100%">
		@endif
	</div>
</div>
@endsection
@section('content')
	<div class="container" id="forpc">
		<div class="row">
			<div class="col-12 font-size-change">
				<img style="width: 44px;
  				height: 44px;"
					 src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
				<span role="heading" class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> คณะกรรมการ</span>
			</div>
		</div>
	</div>
@endsection
@section('content2')
{{--data for download--}}
<div class="container-fluid" id="formobile">
	<div class="col-12">
		<img style="width: 44px;
  				height: 44px;" src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
		<span role="heading" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> คณะกรรมการ</span>
	</div>
	<div class="col-12">
		@if($data)
			<img class="img-fluid" src="{{$site}}/uploads/{{$data->path}}" width="100%" alt="{{$data->name}}">
		@else
			<img class="img-fluid" src="{{$site}}/Defaultimg/noimg.jpg" width="100%">
		@endif
	</div>
	<br/>
	<br/>
	<div class="row">
		<div class="col-12" style="padding: 0;margin-bottom: 20px">
			<img style="width: 44px;
		  	height: 44px;" src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
			<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> ไฟล์แนบ</span>
		</div>
		<br/>
		<br/>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 10px;border-radius: 10px">
			@foreach($download as $item)
				@if($item->rank == 1)
					<div class="row" style="margin: 20px 0">
						<div class="col-9">
							<span class="font-size-change">{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
					</div>
				@endif
			@endforeach
		</div>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 10px;border-radius: 10px;padding-top: 10px;padding-bottom: 10px">
			@foreach($download as $item)
				@if($item->rank == 2 || $item->rank > 3 && $item->rank < 4)
					<div class="row" style="margin: 20px 0">
						<div class="col-9">
							@if($item->rank > 3 && $item->rank < 4)
								<span class="font-size-change" style="padding-left: 5%">{{$item->name}}</span>
							@else
								<span class="font-size-change">{{$item->name}}</span>
							@endif
						</div>

						<div class="col-3">
							<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
					</div>
					@if($loop->index < 6)
						<div class="row">
							<div class="col-12" style="padding: 0 3%;height: 0">
								<hr style="border: 1px solid #B6C3C6;margin: 0">
							</div>
						</div>
					@endif
				@endif
			@endforeach
		</div>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 50px;border-radius: 10px">
			@foreach($download as $item)
				@if($item->rank == 4)
					<div class="row" style="margin: 20px 0">
						<div class="col-9">
							<span class="font-size-change">{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	</div>
</div>
<div class="container" id="forpc">
		<div class="col-12" style="position: relative;visibility: hidden;height: auto;width: calc(100% - 110px);">
			@if($data)
				<img id="img-custom" class="img-fluid" src="{{$site}}/uploads/{{$data->path}}" alt="{{$data->name}}">
			@else
				<img id="img-custom" class="img-fluid" src="{{$site}}/Defaultimg/noimg.jpg">
			@endif
		</div>
	<div class="row">
		<div class="col-12" style="padding: 0;margin-bottom: 20px">
			<img style="width: 30px;
		  	height: 30px;" src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
			<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> ไฟล์แนบ</span>
		</div>
		<br/>
		<br/>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 15px;border-radius: 10px">
			@foreach($download as $item)
				@if($item->rank == 1)
				<div class="row" style="margin: 20px 0">
					<div class="col-9">
						<span class="font-size-change">{{$item->name}}</span>
					</div>

					<div class="col-3">
						<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
					</div>
				</div>
				@endif
			@endforeach
		</div>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 15px;border-radius: 10px;">
			@foreach($download as $item)
				@if($item->rank == 2 || $item->rank > 3 && $item->rank < 4)
				<div class="row" style="margin: 20px 0">
					<div class="col-9">
						@if($item->rank > 3 && $item->rank < 4)
							<span class="font-size-change" style="padding-left: 5%">{{$item->name}}</span>
						@else
							<span class="font-size-change">{{$item->name}}</span>
						@endif
					</div>

					<div class="col-3">
						<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
					</div>
				</div>
				@if($loop->index < 6)
					<div class="row">
						<div class="col-12" style="padding: 0 3%;height: 0">
							<hr style="border: 1px solid #B6C3C6;margin: 0">
						</div>
					</div>
				@endif
			@endif
			@endforeach
		</div>
		<div class="col-12" style="box-shadow:0 0 10px 2px #dadada;margin-bottom: 50px;border-radius: 10px">
			@foreach($download as $item)
				@if($item->rank == 4)
					<div class="row" style="margin: 20px 0">
						<div class="col-9">
							<span class="font-size-change">{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	</div>
</div>
@endsection
