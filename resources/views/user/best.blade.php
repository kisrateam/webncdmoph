@extends('layouts.app')
<style>
    li.nav-item a.nav-link{
        font-size: 85%;
        border: none;
        box-shadow: none;
        margin: 0;
        color: #B6C3C6;
    }
    li.nav-item a.nav-link.active{
        border-radius: 10px;
        border: 1px solid #508EBF;
        color: white;
    }
    .content .linebreak{
        max-width: 180px;
        display: -webkit-box;
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 5;
        box-orient: vertical;
        line-clamp: 3;
    }
    div.no-gutters .row{
        flex: unset !important;
    }
</style>
<?php
$databest = new \App\best();
$listofgroup = ["ทั้งหมด","ยุทธศาสตร์ที่ 1","ยุทธศาสตร์ที่ 2","ยุทธศาสตร์ที่ 3","ยุทธศาสตร์ที่ 4","ยุทธศาสตร์ที่ 5","ยุทธศาสตร์ที่ 6"];
$listofsubgroup = ["ทั้งหมด","ตัวชี้วัดที่ 1","ตัวชี้วัดที่ 2","ตัวชี้วัดที่ 3","ตัวชี้วัดที่ 4","ตัวชี้วัดที่ 5","ตัวชี้วัดที่ 6","ตัวชี้วัดที่ 7","ตัวชี้วัดที่ 8","ตัวชี้วัดที่ 9"];
$listofnamesubgroup = ["ทั้งหมด","ลดตายก่อนวัยอันควรจากโรค NCDs","ลดการบริโภค เครื่องดื่มแอลกอฮอล","ลดการขาดกิจกรรม"
    ,"ลดการบริโภคเกลือ","ลดการบริโภคยาสูบ","ลดภาวะความดันโลหิตสูง"
    ,"ภาวะเบาหวาน และอ้วนไม่ให้เพิ่มขึ้น","ผู้ที่เสี่ยงสูงต่อ CVD ได้รับยาและคำปรึกษา","ยาและเทคโนโลยีจำเป็นครอบคลุม"];
?>
@section('content')
    <div class="container-lg" id="forpc">
        <div class="row">
            <div class="col-12">
                <img style="width: 44px;
							height: 44px;
							padding-bottom: 5px;" class="imgcolorblue" src="{{$site}}/Defaultimg/best.svg"/>
                <span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  Best Practice</span>
            </div>
        </div>
        <br/>
        <ul class="nav nav-pills row" style="border: none;">
            @for($i = 1;$i <= 7;$i++)
                <li class="nav-item" tabindex="0" onclick="resetcategory(this);" style="padding: 0;text-align: center;width: 14.2%;">
                    <a class="nav-link {{$i == 1 ? 'active':''}} font-size-change" data-target="#tab{{$i}}" data-toggle="tab" role="tab" aria-selected="false" >{{$listofgroup[$i-1]}}</a>
                </li>
            @endfor
        </ul>
        <hr style="border: 1px solid #cbcbcb">
        <div class="tab-content">
            @for($i = 1;$i <= 7;$i++)
                <div class="tab-pane fade {{$i == 1 ? 'active in show':''}}" id="tab{{$i}}">
                    <ul class="nav nav-pills row">
                        @for($j = 1;$j <= 10;$j++)
                            <li style="width:20%;text-align: center" class="nav-item" data-toggle="tooltip" title="{{$listofnamesubgroup[$j-1]}}">
                                <a id="1-1" class="nav-link {{$j == 1 ? 'active':''}} font-size-change {{$i == 1 ? 'resetcategory':''}}" data-target="#subtab{{$i.'-'.$j}}" data-toggle="tab">{{$listofsubgroup[$j-1]}}</a>
                            </li>
                        @endfor
                    </ul>

                    <div class="tab-content" style="margin-bottom:50px">
                        @for($j = 1;$j <= 10;$j++)
                            <?php
                                if($i == 1){
                                    $best = $databest->orderBy('updated_at','Desc')->get();
                                }else{
                                    $best = $databest->where('group',$i-1)->orderBy('updated_at','Desc')->get();
                                }
                            ?>
                            <div class="tab-pane fade {{$j == 1 ? 'active in show':''}}" id="subtab{{$i.'-'.$j}}">
                                <div class="row">
                                @if($j == 1)
                                    @foreach($best as $item)
                                    <div class="col-4" style="padding: 10px;">
                                        <span tabindex="0" onclick="window.open('{{$item->url}}');" style="cursor: pointer">
                                            <div class="card" style="box-shadow: none;border: none;background-color: transparent;">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                @if($item->img === null)
                                                            <div data-content="" class="image">
                                                        <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto" alt="noimg.jpg">
                                                        </div>
                                                            @else
                                                                <div data-content="" class="image">
                                                            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
                                                        </div>
                                                            @endif
                                                            <div class="col-12 changefontcolor">
                                                        <div class="card-block content">
                                                            <p class="card-title font-size-change" style="font-weight: bold">
                                                                {{ str_limit($item->name,50) }}
                                                            </p>
                                                            <span class="font-size-change linebreak">
                                                                {{ $item->details }}
                                                            </span>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    @endforeach
                                @else
                                    @foreach($best as $item)
                                        @if(strpos($item->subgroup,strval($j-1)) !== false)
                                            <div class="col-4" style="padding: 10px;">
                                    <span tabindex="0" onclick="window.open('{{$item->url}}');" style="cursor: pointer">
                                        <div class="card" style="box-shadow: none;border: none;background-color: transparent;">
                                            <div class="row no-gutters">
                                                <div class="col-12">
                                            @if($item->img === null)
                                                <div data-content="" class="image">
                                                    <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto" alt="noimg.jpg">
                                                </div>
                                            @else
                                                <div data-content="" class="image">
                                                    <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
                                                </div>
                                            @endif
                                            <div class="col-12 changefontcolor">
                                                <div class="card-block content">
                                                    <span class="card-title font-size-change" style="font-weight: bold">
                                                        {{ str_limit($item->name,50) }}
                                                    </span>
                                                    <span class="font-size-change linebreak">
                                                                {{ $item->details }}
                                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </span>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif

                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            @endfor
        </div>

    </div>
@endsection
@include('layouts.onlyDatatable')
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $("ul.nav-tabs a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    function resetcategory(cate) {
        if(cate.getElementsByTagName('a')[0].innerHTML == 'ทั้งหมด'){
            Array.from(document.getElementsByClassName('resetcategory')).forEach( v => {
                v.classList.remove('active');
            });
            document.getElementById('1-1').click();
        }
    }
    function mresetcategory(cate) {
        if(cate.getElementsByTagName('a')[0].innerHTML == 'ทั้งหมด'){
            Array.from(document.getElementsByClassName('mresetcategory')).forEach( v => {
                v.classList.remove('active');
            });
            document.getElementById('m1-1').click();
        }
    }
</script>
@section('content2')
    <div class="container-fluid" id="formobile">
        <div class="row">
            <div class="col-12">
                <img style="width: 44px;
							height: 44px;
							padding-bottom: 5px;" class="imgcolorblue" src="{{$site}}/Defaultimg/best.svg"/>
                <span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  Best Practice</span>
            </div>
        </div>
        <br/>
        <ul class="nav nav-pills row" style="border: none;">
            @for($i = 1;$i <= 7;$i++)
                <li class="nav-item col-sm-4 col-6" tabindex="0" onclick="mresetcategory(this);" style="padding: 0;text-align: center;">
                    <a class="nav-link {{$i == 1 ? 'active':''}} font-size-change" data-target="#tabm{{$i}}" data-toggle="tab" role="tab" aria-selected="false" >{{$listofgroup[$i-1]}}</a>
                </li>
            @endfor
        </ul>
        <hr style="border: 1px solid #cbcbcb">
        <div class="tab-content">
            @for($i = 1;$i <= 7;$i++)
                <div class="tab-pane fade {{$i == 1 ? 'active in show':''}}" id="tabm{{$i}}">
                    <ul class="nav nav-pills row">
                        @for($j = 1;$j <= 10;$j++)
                            <li style="width:20%;text-align: center" class="nav-item" data-toggle="tooltip" title="{{$listofnamesubgroup[$j-1]}}">
                                <a id="m1-1" class="nav-link {{$j == 1 ? 'active':''}} font-size-change {{$i == 1 ? 'mresetcategory':''}}" data-target="#subtabm{{$i.'-'.$j}}" data-toggle="tab">{{$listofsubgroup[$j-1]}}</a>
                            </li>
                        @endfor
                    </ul>

                    <div class="tab-content" style="margin-bottom:50px">
                        @for($j = 1;$j <= 10;$j++)
                            <?php
                            if($i == 1){
                                $bestm = $databest->orderBy('updated_at','Desc')->get();
                            }else{
                                $bestm = $databest->where('group',$i-1)->orderBy('updated_at','Desc')->get();
                            }
                            ?>
                            <div class="tab-pane fade {{$j == 1 ? 'active in show':''}}" id="subtabm{{$i.'-'.$j}}">
                                <div class="row">
                                    @if($j == 1)
                                        @foreach($bestm as $item)
                                            <div class="col-4" style="padding: 10px;">
                                        <span tabindex="0" onclick="window.open('{{$item->url}}');" style="cursor: pointer">
                                            <div class="card" style="box-shadow: none;border: none;background-color: transparent;">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                @if($item->img === null)
                                                            <div data-content="" class="image">
                                                        <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto" alt="noimg.jpg">
                                                        </div>
                                                        @else
                                                            <div data-content="" class="image">
                                                            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
                                                        </div>
                                                        @endif
                                                        <div class="col-12 changefontcolor">
                                                        <div class="card-block content">
                                                        <span class="card-title font-size-change" style="font-weight: bold">
                                                        {{ str_limit($item->name,50) }}</span>
                                                        <span class="font-size-change linebreak">
                                                                {{ $item->details }}
                                                            </span>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach($bestm as $item)
                                            @if(strpos($item->subgroup,strval($j-1)) !== false)
                                                <div class="col-4" style="padding: 10px;">
                                    <span tabindex="0" onclick="window.open('{{$item->url}}');" style="cursor: pointer">
                                        <div class="card" style="box-shadow: none;border: none;background-color: transparent;">
                                            <div class="row no-gutters">
                                                <div class="col-12">
                                            @if($item->img === null)
                                                        <div data-content="" class="image">
                                                    <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto" alt="noimg.jpg">
                                                </div>
                                                    @else
                                                        <div data-content="" class="image">
                                                    <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
                                                </div>
                                                    @endif
                                                    <div class="col-12 changefontcolor">
                                                <div class="card-block content">
                                                <span class="card-title font-size-change" style="font-weight: bold">
                                                {{ str_limit($item->name,50) }}</span>
                                                <span class="font-size-change linebreak">
                                                                {{ $item->details }}
                                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </span>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            @endfor
        </div>

    </div>
@endsection
