@extends('layouts.appeng')
<style>
    #formobile{
        display: none;
    }
    #forpc{
        display: block;
    }
    @media screen and (max-width : 767px)
    {
        #formobile{
            display: block;
        }
        #forpc{
            display: none;
        }
    }
    .forengimg {
        position: relative;
    }
    .forengimg img {
        vertical-align:middle;
    }
</style>
<?php
    $data = \App\eng::where('active','1')->get();
    $datacat = \App\eng::all()->groupBy('category');
    $arrimg = [
        'eng1',
        'eng2',
        'eng3',
        'eng4'
    ];
    $arreng = [
        'Strategic plan',
        'Committee',
        'M&E',
        'Best practice'
    ];
    $img = \App\img::whereIn('idfk',$arrimg)->get();
    $indeximg = 0;
?>
@section('content')
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <img style="width: 44px;
                height: 44px;
                padding-bottom: 5px;" src="{{$site}}/Defaultimg/3.svg"/>
                <span style="font-weight: bold;vertical-align: bottom;font-size: 20px">  Department of Disease Control</span>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
        <div class="row">
            <div class="col-12">
                <h1>Highlights</h1>
            </div>
            <div class="col-12">
                <hr style="border: 1px solid #3D3D3D;">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 row" style="margin-bottom: 100px;margin-top: 30px;display: flex;align-items: center;">
                @foreach($data as $item)
                    @if($item->highlights == 1)

                        <div class="col-2">
                            <img src="{{$site}}/uploads/{{json_decode($item->fileuniq)[1]}}" style="width: 100%;height: auto;margin: 5px">
                        </div>

                        <div class="col-7">
                            <span>{{$item->name}}</span>
                        </div>

                        <div class="col-3">
                            <button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" tabindex="0" onclick="window.open('{{ json_decode($item->fileuniq)[0] }}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">Download</button>
                        </div>

                        <div class="col-10 offset-2">
                            <hr style="border: 1px solid #B6C3C6;">
                        </div>

                    @endif
                @endforeach
            </div>
        </div>
    <br/>
    <br/>
    </div>
@endsection
@section('content2')
<div class="container-lg">
    <div class="row">
        <br/>
        <br/>
        @for($i=0;$i<4;$i++)
            <div class="col-6">
                <div class="view view-first">
                    @foreach($img as $imgs)
                        @if($imgs->idfk === $arrimg[$i])
                            <div class="forengimg">
                                <img class="card-img-top" src="{{$site}}/uploads/{{$imgs->path}}" height="auto" width="100%" alt="{{$imgs->name}}">
                            </div>
                            <?php
                                $indeximg = 1;
                            ?>
                        @endif
                    @endforeach
                    @if($indeximg != 1)
                        <div class="forengimg">
                            <img class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" alt="end-image" height="auto" width="100%">
                        </div>
                    @endif
                    <?php
                    $indeximg = 0;
                    ?>
                    <span style="margin-top: 10px;word-wrap: break-word;font-weight: bold;margin-bottom:20px;font-size: 1.25rem">{{ $arreng[$i] }}</span>
                    <hr style="border-bottom: 1px solid black">
                    @foreach($datacat as $keys => $items)
                        @if($arreng[$i] == $keys)
                            @foreach($items as $item)
                                @if($item->active == 1)
                                    <div class="row">
                                        <div class="col-2">
                                            <button tabindex="0" onclick="window.open('{{ json_decode($item->fileuniq)[0] }}','_blank');" style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: left;padding-right: 0"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px"></button>
                                        </div>
                                        <div class="col-10">
                                            <span style="word-wrap: break-word;font-size: 0.9rem">{{$item->name}}</span>
                                            <hr>
                                        </div>
                                    </div>
                                @endif
                                @if($loop->last)
                                    <br/>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </div>
            </div>
        @endfor้
    </div>
    @include('link-home')
    <br/>
    <br/>
</div>
@endsection
