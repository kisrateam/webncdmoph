@extends('layouts.app')
<style>
	html { height: 100%; }
	body { height: 100%; }

	#btn-custom{
		border: none;
		border-radius: 10px;
		color: #B6C3C6;
		background-color: transparent;
		box-shadow: none
	}

	#btn-custom.active{
		border: 1px solid #508EBF;
		color: #508EBF;
	}

	.result-table th:first-child{
		border-radius:20px 0 0 0;
	}
	.result-table th:last-child{
		border-radius:0 20px 0 0 ;
	}
	.result-table td[colspan="8"]{
		padding-top: 15px;
		height: 30px;
	}
	.result-table td[colspan="5"]{
		padding-top: 15px;
		height: 30px;
	}
    .result-table th{
        border: 1px;
    }
	.result-table{
		border: none;
    }
	.global:hover{
		background-color: #c7edfa;
		/*color: white;*/
		border-radius: 10px;
  	}
	.global label:hover{
		opacity: 0.5;
  	}
	div#forpc ul li a:hover{
		background-color: #c3e9f6;
		border-radius: 10px;
		color: white;
	}
	div.font-size-change p{
		margin: 0;
	}
</style>
@include('layouts.onlyDatatable')
@section('content')
	<?php
    $listof9global = array("9global1","9global2","9global3",
        "9global4","9global5","9global6",
        "9global7","9global8","9global9"
    );
	$tableresult = \App\result::where('name','resulttable')->first();
	$global = \App\result::whereIn('name',$listof9global)->with('img')->get();
	$data = \App\result::with('files')->get();
	?>
<div class="container-lg" id="forpc">
	<div class="row">
		<div class="col">
				<img style="
				width: 44px;
				height: 44px;
				" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
				<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  ผลการดำเนินงาน</span>
		</div>
	</div>
	<br/>
	<ul class="nav nav-pills">
		<li class="nav-item">
			<a onclick="showtable();" id="btn-custom" class="nav-link btn active font-size-change" data-toggle="pill" href="#menu1">ผลการประเมิน<br/>9 global targets</a>
		</li>
		<li class="nav-item">
			<a onclick="hidetable();" id="btn-custom" class="nav-link btn btn-custom font-size-change" data-toggle="pill" href="#menu2">ผลการประเมิน NCD <br/>progress monitor</a>
		</li>
		<li class="nav-item">
			<a onclick="hidetable();" id="btn-custom" class="nav-link btn btn-custom font-size-change" data-toggle="pill" href="#menu3">ผลการ <br/>ประเมินแผน</a>
		</li>
		<li class="nav-item">
			<a onclick="hidetable();" id="btn-custom" class="nav-link btn btn-custom font-size-change" data-toggle="pill" href="#menu4">ผลการดำเนินงาน<br/>ภายใต้แผน</a>
		</li>
	</ul>

	<div class="tab-content">
		<div id="menu1" class="container-fluid tab-pane active"><br>
            <div class="row">
				<div class="col-12">
					<p class="font-size-change" style="color: #508EBF;">สถานการณ์โรคไม่ติดต่อตาม 9 เป้าหมายระดับโลกของประเทศไทย</p>
					<hr style="border: 1px solid #B6C3C6;">
				</div>

                <?php $j = 0 ?>
                @for($i = 1;$i <= 9;$i++)
                    @foreach($global as $row)
                        @if($row->name == '9global'.$i)
							<div class="col-4 global">
								<div class="text-center" style="height: 100px;margin: 10px 0;padding: 10px 0;cursor: pointer" onclick="window.open('{{($row->imguniqicon != null?$row->imguniqicon:null)}}','_blank');">
									<label style="width: 100px;height: 100px;background-image:url('{{ $row->img!=null?asset('uploads/'.$row->img->path):asset('Defaultimg/noimg.jpg') }}');background-size:100% 100%;cursor: pointer"></label>
								</div>
								<div class="font-size-change" style="line-break: anywhere;">
									{!! $row->details !!}
								</div>
							</div>
						<?php $j++ ?>
                        @endif
                    @endforeach
                    @if($j == 0)
                        <div class="col-4 text-center" style="height: 100px;margin: 10px 0;">
                            <label style="width: 100px;height: 100px;background-image:url('{{ asset('Defaultimg/noimg.jpg') }}');background-size:100% 100%;cursor: pointer"></label>
						</div>
						<p>ไม่มีข้อมูล</p>
						@else
                        <?php $j = 0 ?>
                    @endif
                @endfor
            </div>
		</div>
		<div id="menu2" class="container-fluid tab-pane fade"><br>
			@foreach($data as $item)
				@if($item->category == 'ผลการประเมิน NCD progress monitor')
					<div class="row">
						<div class="col-12" style="margin-bottom: 15px">
							<span class="font-size-change" style="color: #508EBF;font-size: 1rem">{{$item->name}}</span>
						</div>
                        @if($item->img != null)
                            <div class="col-12 text-center">
                                <img style="max-width: 100%" src="{{$site}}/uploads/{{$item->img->path}}" width="auto" height="auto" alt="{{$item->img->name}}">
                            </div>
                        @endif
						<div class="col-12" style="margin-top: 15px">
							<span class="font-size-change">{!! $item->details !!}</span>
						</div>
						@if($item->files != null)
							@foreach($item->files->name as $key => $file)
								<div class="col-12 row" style="box-shadow:0 0 10px 3px #dadada;padding: 0 15px 20px 15px;border-radius: 10px;margin:15px 0 0 0;">
									<div class="col-9">
										<br/>
										<span>{{$file}}</span>
									</div>
									<div class="col-3">
										<br/>
										<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->files->files[$key]}}','_blank');">ดาวน์โหลด</button>
									</div>
								</div>
							@endforeach
						@endif
						<div class="col-12" style="padding: 0">
							<hr style="border: 1px solid #3D3D3D;">
						</div>
					</div>
					<br/>
				@endif
			@endforeach
		</div>
		<div id="menu3" class="container-fluid tab-pane fade"><br>
			@foreach($data as $item)
				@if($item->category == 'ผลการ ประเมินแผน')
					<div class="row">
						<div class="col-12" style="margin-bottom: 15px">
							<span class="font-size-change" style="color: #508EBF;font-size: 1rem">{{$item->name}}</span>
						</div>
                        @if($item->img != null)
                            <div class="col-12 text-center">
                                <img style="max-width: 100%" src="{{$site}}/uploads/{{$item->img->path}}" width="auto" height="auto" alt="{{$item->img->name}}">
                            </div>
                        @endif
						<div class="col-12" style="margin-top: 15px">
							<span class="font-size-change">{!! $item->details !!}</span>
						</div>
						@if($item->files != null)
							@foreach($item->files->name as $key => $file)
								<div class="col-12 row" style="box-shadow:0 0 10px 3px #dadada;padding: 0 15px 20px 15px;border-radius: 10px;margin:15px 0 0 0;">
									<div class="col-9">
										<br/>
										<span>{{$file}}</span>
									</div>
									<div class="col-3">
										<br/>
										<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->files->files[$key]}}','_blank');">ดาวน์โหลด</button>
									</div>
								</div>
							@endforeach
						@endif
						<div class="col-12" style="padding: 0">
							<hr style="border: 1px solid #3D3D3D;">
						</div>
					</div>
					<br/>
				@endif
			@endforeach
		</div>
		<div id="menu4" class="container-fluid tab-pane fade"><br>
			@foreach($data as $item)
				@if($item->category == 'ผลการดำเนินงานภายใต้แผน')
					<div class="row">
						<div class="col-12" style="margin-bottom: 15px">
							<span class="font-size-change" style="color: #508EBF;font-size: 1rem">{{$item->name}}</span>
						</div>
						@if($item->img != null)
                            <div class="col-12 text-center">
							    <img src="{{$site}}/uploads/{{$item->img->path}}" style="max-width: 100%" width="auto" height="auto" alt="{{$item->img->name}}">
                            </div>
						@endif
						<div class="col-12" style="margin-top: 15px">
							<span class="font-size-change">{!! $item->details !!}</span>
						</div>
						@if($item->files != null)
							@foreach($item->files->name as $key => $file)
								<div class="col-12 row" style="box-shadow:0 0 10px 3px #dadada;padding: 0 15px 20px 15px;border-radius: 10px;margin:15px 0 0 0;">
									<div class="col-9">
										<br/>
										<span>{{$file}}</span>
									</div>
									<div class="col-3">
										<br/>
										<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->files->files[$key]}}','_blank');">ดาวน์โหลด</button>
									</div>
								</div>
							@endforeach
						@endif
						<div class="col-12" style="padding: 0">
							<hr style="border: 1px solid #3D3D3D;">
						</div>
					</div>
					<br/>
				@endif
			@endforeach
		</div>
	</div>
</div>
@endsection
<script>
    $(document).ready(function(){
        showtable();

        $("td").each(function(){
            $(this).addClass("font-size-change");
        });
        $("th").each(function(){
            $(this).addClass("font-size-change");
        });
    });
    function showtable(){
        document.getElementById('resulttable').style.display = 'block';
    };
    function hidetable(){
		document.getElementById('resulttable').style.display = 'none';
    };
	function chktable() {
	    if($("select#Select option:selected").text() == "ผลการประเมิน 9 global targets"){
            showtable();
		}else{
            hidetable();
		}
    }
</script>
@section('content2')
	<?php
    $datacate = \App\result::all()->groupBy('category');
	?>
	<div class="container-fluid" id="formobile">
		<div class="col-12">
			<img style="
				width: 44px;
				height: 44px;
				" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
			<span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> ผลการดำเนินการ</span>
		</div>
		<br/>
		<ul class="nav" role="tablist">
			<select id='Select' class="form-control" onchange="chktable();">
				@foreach($datacate as $key => $item)
					<option value="{{ $loop->index }}">{{ $key }}</option>
				@endforeach
			</select>
		</ul>
		<div class="tab-content">
			<br/>
			@foreach($datacate as $keys => $items)
				<div class="tab-pane custom-result {{$loop->first ? 'active':''}}" id="{{ $loop->index }}">
					@if($keys == 'ผลการประเมิน 9 global targets')
						@foreach($global as $row)
							@for($i = 1;$i <= 9;$i++)
								@if($row->name == '9global'.$i)
									<div class="row" onclick="window.open('{{($row->imguniqicon != null?$row->imguniqicon:null)}}','_blank');" style="cursor: pointer;border-bottom: 1px solid #a8a8a8;{{$loop->last ? 'margin-bottom:40px':''}}">
										<div class="col-4 text-center" style="margin: 10px 0">
											<img src="{{ $row->img!=null?asset('uploads/'.$row->img->path):asset('Defaultimg/noimg.jpg') }}" style="cursor: pointer;height: auto;width: 100%">
										</div>
										<div class="col-8" style="margin: auto;width: 50%;padding: 10px;font-size: 20px">
											<p>{!! $row->details !!}</p>
										</div>
									</div>
								@endif
							@endfor
						@endforeach
					@else
						@foreach($items as $item)
						<div class="view view-first">
							<div class="col-12" style="margin-bottom: 15px">
								<span class="font-size-change" style="color: #508EBF;font-size: 1rem">{{$item->name}}</span>
							</div>
							@if($item->img != null)
								<div class="col-12 text-center">
									<img src="{{$site}}/uploads/{{$item->img->path}}" style="max-width: 100%" width="auto" height="auto" alt="{{$item->img->name}}">
								</div>
							@endif
							<div class="col-12" style="margin-top: 15px">
								<span class="font-size-change">{!! $item->details !!}</span>
							</div>
							@if($item->files != null)
								<div class="row" style="padding: 0 25px">
									@foreach($item->files->name as $key => $file)
										<div class="col-12" style="box-shadow:0 0 10px 3px #dadada;padding: 0 15px 20px 15px;border-radius: 10px;margin:15px 0 0 0;">
											<br/>
											<span>{{$file}}</span>
											<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->files->files[$key]}}','_blank');">ดาวน์โหลด</button>
										</div>
									@endforeach
								</div>
							@endif
							<div class="col-12" style="padding: 0">
								<hr style="border: 1px solid #3D3D3D;">
							</div>
						</div>
						@endforeach
					@endif
				</div>
			@endforeach
		</div>
	</div>
	@if($tableresult)
		<div class="container-lg" id="resulttable">
			<br/>
			<br/>

			<div class="col">
				<img style="
				width: 44px;
				height: 44px;
				" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
				<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">สถานการณ์โรคไม่ติดต่อตาม 9 เป้าหมายระดับโลกของประเทศไทย</span>
			</div>
			<br/>
			<span class="font-size-change">
				{!! $tableresult->details !!}
			</span>
		</div>
	@endif
	<script type="text/javascript">
        $('#Select').on('change', function (e) {
            $('.custom-result').hide();
            $('.custom-result').eq($(this).val()).show();
        });
	</script>
@endsection
