@extends('layouts.app')
@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
<style>
	html { height: 100%;}
	body { height: 100%;}
</style>
<?php
	$img = \App\img::where('idfk','content1')->first();
	$data = \App\content::where('category','1')->where('active','1')->get();
	$dataur = \App\content::where('category','2')->where('active','1')->get();
	$dall = \App\content::where('name','downloadall2')->first();
?>
@section('content')
	<div class="container-lg" id="forpc">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;
							height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
				<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564) ฉบับปรับปรุง ระยะปี พ.ศ. 2563 - 2565</span>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="col-12">
				@if($img == null)
					<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto"/>
				@else
					<div class="text-center">
						<img style="max-width: 100%;height: auto" src="{{$site}}/uploads/{{$img->path}}" alt="{{$img->name}}"/>
					</div>
					<div>
						<br/>
						<span class="font-size-change">{!! $img->name!=null ? $img->name:'' !!}</span>
					</div>
				@endif
			</div>

			<div class="col-12">
				<br/>
				<br/>
				<div class="row" style="box-shadow:0 0 10px 3px #dadada;padding: 20px 15px;border-radius: 10px;margin:0;">
					@foreach($data as $item)
						<div class="col-9">
							<span>{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button class="download_hover" style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" tabindex="0" onclick="gotofile('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
						@if(!$loop->last)
							<div class="col-12">
								<hr style="border: 1px solid #B6C3C6;width: 100%">
							</div>
						@endif
					@endforeach
				</div>
				@if($dall)
					<div class="col">
						<div style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 18px;padding: 10px">
							<button class="download_hover" style="border-radius: 20px;border: 1px solid #508EBF;background-color: transparent;color: #508EBF;outline: none;height: 40px;" tabindex="0" onclick="window.open('{{$dall->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="margin: 0 10px">ดาวน์โหลดทั้งหมด</button>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
@section('content2')
	<div class="container-fluid" id="formobile">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;
							height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
				<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (ฉบับปรับปรุง)</span>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="col-12">
				@if($img == null)
					<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto"/>
				@else
					<img src="{{$site}}/uploads/{{$img->path}}" width="100%" height="auto" alt="{{$img->name}}"/>
					<div class="text-center">
						<br/>
						<span class="font-size-change">{!! $img->name!=null ? $img->name:'' !!}</span>
					</div>
				@endif
				<br/>
				<br/>
			</div>

			<div class="col-12" style="box-shadow:0 0 10px 3px #dadada;padding: 20px 15px;border-radius: 10px;margin:0;">
				@foreach($data as $item)
					<div class="row">
						<div class="col-9">
							<span>{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button class="download_hover" style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" tabindex="0" onclick="gotofile('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
						@if(!$loop->last)
							<div class="col-12">
								<hr style="border: 1px solid #B6C3C6;width: 100%">
							</div>
						@endif
					</div>
				@endforeach
			</div>
			@if($dall)
				<div class="col">
					<div style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 18px;padding: 10px">
						<button class="download_hover" style="border-radius: 20px;border: 1px solid #508EBF;background-color: transparent;color: #508EBF;outline: none;height: 40px;" tabindex="0" onclick="window.open('{{$dall->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="margin: 0 10px">ดาวน์โหลดทั้งหมด</button>
					</div>
				</div>
			@endif
		</div>
	</div>
	<br/>

@endsection
<script>
function gotofile(file) {
window.open(file,'_blank');
}
</script>
