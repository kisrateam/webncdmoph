@extends('layouts.app')
<style>
	html { height: 100%; }
	body { height: 100%;min-height: 1000px; }

	.content div {
		width: 100%;
		display: -webkit-box;
		overflow: hidden;
		text-overflow: ellipsis;
		-webkit-box-orient: vertical;
		-webkit-line-clamp: 5;
		box-orient: vertical;
		line-clamp: 5;
		font-size: 16px;
	}

	.content{
		width:100%;
	}
	.card{
		font-weight: 200; !important;
		background-color: transparent !important;
	}
	.text {
		background-color: transparent;
		color: white;
		font-size: 18px;
		padding: 16px 32px;
	}
	li.nav-item a.nav-link{
		border: none;
		border-radius: 10px;
		color: #B6C3C6;
		background-color: transparent;
		box-shadow: none;
		outline: none;
	}
	li.nav-item a.nav-link.active{
		border: 1px solid #508EBF;
		color: #508EBF;
	}
	#subtabs li.nav-item a.nav-link{
		border: none;
		border-radius: 10px;
		color: #B6C3C6;
		background-color: transparent;
		box-shadow: none;
		outline: none;
	}
	#subtabs li.nav-item a.nav-link.active{
		color: #508EBF;
	}
	.image {
		position:relative;
		cursor: pointer;
	}
    .image img {
        vertical-align:top;
        max-height: 250px;
        max-width: 250px;
        width: 100%;
        height: 100%;
    }
	.image:after, .image:before {
		position:absolute;
		opacity:0;
		height: 0%;
		transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}
	.image:after {
		content:'\A';
		width:100%; height:100%;
		top:0; left:0;
		background:rgba(0,0,0,0.8);
	}
	.image:before {
		content: attr(data-content);
		width:100%;
		color:#fff;
		z-index:1;
		bottom:0;
		padding:4px 10px;
		text-align:center;
		background: url('{{ asset('/Defaultimg/readmore.svg') }}') no-repeat center;
		box-sizing:border-box;
		-moz-box-sizing:border-box;
	}
	.image:hover:after, .image:hover:before {
		opacity: 0.8;
		height: 100%;
		animation-name: fadeInOpacity;
		animation-iteration-count: 1;
		animation-timing-function: ease-in;
		animation-duration: 0.5s;
	}

	@keyframes fadeInOpacity {
		0% {
			opacity: 0;
			height: 0;
		}
		100% {
			opacity: 1;
			height: 100%;
		}
	}
	.card-text{
		color: #717171;
		line-height: 1rem;
		background-color: transparent;
	}
	div#forpc ul li a:hover{
		background-color: #c3e9f6;
		border-radius: 10px;
		color: white;
	}
	div.tabbable ul#subtabs li a:hover{
		color: #c3e9f6 !important;
	}
</style>
<?php
$arrayfornew = array(
    "ข่าวกิจกรรม",
    "ข่าวประชาสัมพันธ์");
$data = new \App\contentactivity();
?>
@section('content')
	<div id="forpc" class="container-lg">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;height: 44px;" src="{{$site}}/Defaultimg/ข่าวสาร.png"/>
				<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  ข่าวสาร</span>
			</div>
		</div>
		<br/>
		<ul class="nav nav-tabs" id="Tab" role="tablist" style="border: none;">
			<li class="nav-item" style="padding: 0 30px">
				<a class="nav-link active font-size-change" id="first-tab" data-target="#panel_a" data-toggle="tab" href="#a" role="tab" aria-controls="first" aria-selected="false">ข่าวประชาสัมพันธ์</a>
			</li>
			<li class="nav-item" style="padding: 0 30px">
				<a class="nav-link font-size-change" id="second-tab" data-target="#panel_b" data-toggle="tab" href="#b" role="tab" aria-controls="second" aria-selected="true">ข่าวกิจกรรม</a>
			</li>
		</ul>
		<div class="col-12"><hr style="border: 1px solid #B6C3C6;"></div>

		<div class="tab-content" id="TabA">
			<div class="tab-pane fade show active" id="panel_a" role="tabpanel" aria-labelledby="a-tab">
				<div class="tabbable">

					<ul class="nav nav-tabs" role="tablist" style="border: none" id="subtabs">
						<li class="nav-item"><a class="nav-link btn-sm active font-size-change" data-target="#panel_a_1" data-toggle="tab" href="#1" role="tab">ประชุมคณะกรรมการขับเคลื่อนแผน</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_a_2" data-toggle="tab" href="#2" role="tab">อบรม/สัมนา</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_a_3" data-toggle="tab" href="#3" role="tab">รณรงค์</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_a_4" data-toggle="tab" href="#4" role="tab">ประกวด/ประกาศ</a></li>
					</ul>
					<br/>

					<div class="tab-content" id="TabC">
						<div class="tab-pane fade show active" id="panel_a_1" role="tabpanel" aria-labelledby="1-tab">
							<div class="row">
                                <?php
                                $dataa1 = new $data;
                                $dataa1 = $dataa1->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','ประชุมคณะกรรมการ')->where('active','1')->get();
                                ?>
									@foreach($dataa1 as $key => $item)
										<div class="col-4" style="padding: 10px;">
										<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
											@if($item->img === null)
												<div data-content="" class="image">
													<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
												</div>
											@else
												<div data-content="" class="image">
													<img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
												</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        	<div style="padding: 8px;">
								                    <div class="card-text">
														<span id="n1{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
																var date_f = moment(date).add(543, 'year').format('LL');
																document.getElementById('n1{{$key}}').innerHTML = date_f;
															</script>
														</span>
														<span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
									</div>
									</div>
									</span>
								</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_a_2" role="tabpanel" aria-labelledby="2-tab">
							<div class="row">
                                <?php
                                $dataa2 = new $data;
                                $dataa2 = $dataa2->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','อบรม/สัมนา')->where('active','1')->get();
                                ?>
								@foreach($dataa2 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            	<div class="col-12">
											@if($item->img === null)
												<div data-content="" class="image">
													<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
												</div>
											@else
												<div data-content="" class="image">
													<img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
												</div>
											@endif
							           	 	</div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
														<span id="n2{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n2{{$key}}').innerHTML = date_f;
															</script>
														</span>
														<span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
										</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_a_3" role="tabpanel" aria-labelledby="3-tab">
							<div class="row">
                                <?php
                                $dataa3 = new $data;
                                $dataa3 = $dataa3->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','รณรงค์')->where('active','1')->get();
                                ?>
								@foreach($dataa3 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n3{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n3{{$key}}').innerHTML = date_f;
															</script>
														</span>
														<span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_a_4" role="tabpanel" aria-labelledby="4-tab">
                            <?php
                            $dataa4 = new $data;
                            $dataa4 = $dataa4->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','ข่าวประกวด/ประกาศ')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($dataa4 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n4{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n4{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="tab-pane fade" id="panel_b" role="tabpanel" aria-labelledby="b-tab">
				<div class="tabbable">

					<ul class="nav nav-tabs" role="tablist" style="border: none"  id="subtabs">
						<li class="nav-item"><a class="nav-link btn-sm active font-size-change" data-target="#panel_b_1" data-toggle="tab" href="#1" role="tab">ประชุมคณะกรรมการขับเคลื่อนแผน</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_b_2" data-toggle="tab" href="#2" role="tab">อบรม/สัมนา</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_b_3" data-toggle="tab" href="#3" role="tab">รณรงค์</a></li>
					</ul>
					<br/>

					<div class="tab-content" id="TabD">
						<div class="tab-pane fade show active" id="panel_b_1" role="tabpanel" aria-labelledby="a-tab">
                            <?php
                            $datab1 = new $data;
                            $datab1 = $datab1->where('category','ข่าวกิจกรรม')->where('subcategory','ประชุมคณะกรรมการ')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab1 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n5{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n5{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_b_2" role="tabpanel" aria-labelledby="1-tab">
                            <?php
                            $datab2 = new $data;
                            $datab2 = $datab2->where('category','ข่าวกิจกรรม')->where('subcategory','อบรม/สัมนา')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab2 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">
											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">
											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n6{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n6{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_b_3" role="tabpanel" aria-labelledby="2-tab">
                            <?php
                            $datab3 = new $data;
                            $datab3 = $datab3->where('category','ข่าวกิจกรรม')->where('subcategory','รณรงค์')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab3 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n7{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n7{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection
@section('content2')
	<div id="formobile" class="container-fluid">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;height: 44px;" src="{{$site}}/Defaultimg/ข่าวสาร.png"/>
				<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  ข่าวสาร</span>
			</div>
		</div>
		<br/>
		<ul class="nav nav-tabs" id="Tab" role="tablist" style="border: none;">
			<li class="nav-item" style="padding: 0 30px">
				<a class="nav-link active font-size-change" id="first-tab" data-target="#panel_am" data-toggle="tab" href="#a" role="tab" aria-controls="first" aria-selected="false">ข่าวประชาสัมพันธ์</a>
			</li>
			<li class="nav-item" style="padding: 0 30px">
				<a class="nav-link font-size-change" id="second-tab" data-target="#panel_bm" data-toggle="tab" href="#b" role="tab" aria-controls="second" aria-selected="true">ข่าวกิจกรรม</a>
			</li>
		</ul>
		<div class="col-12"><hr style="border: 1px solid #B6C3C6;"></div>

		<div class="tab-content" id="TabA">
			<div class="tab-pane fade show active" id="panel_am" role="tabpanel" aria-labelledby="a-tab">
				<div class="tabbable">

					<ul class="nav nav-tabs" role="tablist" style="border: none" id="subtabs">
						<li class="nav-item"><a class="nav-link btn-sm active font-size-change" data-target="#panel_am_1" data-toggle="tab" href="#1" role="tab">ประชุมคณะกรรมการขับเคลื่อนแผน</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_am_2" data-toggle="tab" href="#2" role="tab">อบรม/สัมนา</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_am_3" data-toggle="tab" href="#3" role="tab">รณรงค์</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_am_4" data-toggle="tab" href="#4" role="tab">ประกวด/ประกาศ</a></li>
					</ul>
					<br/>

					<div class="tab-content" id="TabC">
						<div class="tab-pane fade show active" id="panel_am_1" role="tabpanel" aria-labelledby="1-tab">
							<div class="row">
                                <?php
                                $dataa1 = new $data;
                                $dataa1 = $dataa1->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','ประชุมคณะกรรมการ')->where('active','1')->get();
                                ?>
									@foreach($dataa1 as $key => $item)
										<div class="col-4" style="padding: 10px;">
										<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
											@if($item->img === null)
												<div data-content="" class="image">
													<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
												</div>
											@else
												<div data-content="" class="image">
													<img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
												</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        	<div style="padding: 8px;">
								                    <div class="card-text">
														<span id="n1{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
																var date_f = moment(date).add(543, 'year').format('LL');
																document.getElementById('n1{{$key}}').innerHTML = date_f;
															</script>
														</span>
														<span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
									</div>
									</div>
									</span>
								</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_am_2" role="tabpanel" aria-labelledby="2-tab">
							<div class="row">
                                <?php
                                $dataa2 = new $data;
                                $dataa2 = $dataa2->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','อบรม/สัมนา')->where('active','1')->get();
                                ?>
								@foreach($dataa2 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            	<div class="col-12">
											@if($item->img === null)
												<div data-content="" class="image">
													<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
												</div>
											@else
												<div data-content="" class="image">
													<img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
												</div>
											@endif
							           	 	</div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
														<span id="n2{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n2{{$key}}').innerHTML = date_f;
															</script>
														</span>
														<span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
										</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_am_3" role="tabpanel" aria-labelledby="3-tab">
							<div class="row">
                                <?php
                                $dataa3 = new $data;
                                $dataa3 = $dataa3->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','รณรงค์')->where('active','1')->get();
                                ?>
								@foreach($dataa3 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n3{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n3{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_am_4" role="tabpanel" aria-labelledby="4-tab">
                            <?php
                            $dataa4 = new $data;
                            $dataa4 = $dataa4->where('category','ข่าวประชาสัมพันธ์')->where('subcategory','ข่าวประกวด/ประกาศ')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($dataa4 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n4{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n4{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="tab-pane fade" id="panel_bm" role="tabpanel" aria-labelledby="b-tab">
				<div class="tabbable">

					<ul class="nav nav-tabs" role="tablist" style="border: none"  id="subtabs">
						<li class="nav-item"><a class="nav-link btn-sm active font-size-change" data-target="#panel_bm_1" data-toggle="tab" href="#1" role="tab">ประชุมคณะกรรมการขับเคลื่อนแผน</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_bm_2" data-toggle="tab" href="#2" role="tab">อบรม/สัมนา</a></li>
						<li style="font-size: 4px;">
							<i role="presentation"  class="fas fa-circle" style="position: relative;top:45%;color: #B6C3C6;"></i>
						</li>
						<li class="nav-item"><a class="nav-link btn-sm font-size-change" data-target="#panel_bm_3" data-toggle="tab" href="#3" role="tab">รณรงค์</a></li>
					</ul>
					<br/>

					<div class="tab-content" id="TabD">
						<div class="tab-pane fade show active" id="panel_bm_1" role="tabpanel" aria-labelledby="a-tab">
                            <?php
                            $datab1 = new $data;
                            $datab1 = $datab1->where('category','ข่าวกิจกรรม')->where('subcategory','ประชุมคณะกรรมการ')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab1 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n5{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n5{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_bm_2" role="tabpanel" aria-labelledby="1-tab">
                            <?php
                            $datab2 = new $data;
                            $datab2 = $datab2->where('category','ข่าวกิจกรรม')->where('subcategory','อบรม/สัมนา')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab2 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">
											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">
											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n6{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n6{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane fade" id="panel_bm_3" role="tabpanel" aria-labelledby="2-tab">
                            <?php
                            $datab3 = new $data;
                            $datab3 = $datab3->where('category','ข่าวกิจกรรม')->where('subcategory','รณรงค์')->where('active','1')->get();
                            ?>
							<div class="row">
								@foreach($datab3 as $key => $item)
									<div class="col-4" style="padding: 10px;">
									<span onclick="gotomedia('{{$item->id}}');" style="cursor: pointer">
										<div class="thumbnail">
											<div class="card" style="box-shadow: none;border: none">
							        		<div class="row no-gutters">
							            <div class="col-12">
														@if($item->img === null)
												<div data-content="" class="image">

											            <img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
																</div>
											@else
												<div data-content="" class="image">

											            <img src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
																</div>
											@endif
							           	 </div>
							            <div class="col-12 changefontcolor">
							                <div class="card-block content">
							                    <h5 class="card-title font-size-change" style="font-weight: bold">
								                    {{ str_limit($item->name,50) }}</h5>
					                        <div style="padding: 8px;">
								                    <div class="card-text">
									                    <span id="n7{{$key}}">
															<script>
																var date = '{{ $item->created_at }}';
                                                                var date_f = moment(date).add(543, 'year').format('LL');
                                                                document.getElementById('n7{{$key}}').innerHTML = date_f;
															</script>
														</span>
									                    <span>{!! $item->details !!}</span>
								                    </div>
							                    </div>
							                </div>
							            </div>
							        </div>
								    		</div>
										</div>
									</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection

<script>
    function gotomedia(id) {
        window.open( 'media/'+id,'_blank');
    }
</script>
