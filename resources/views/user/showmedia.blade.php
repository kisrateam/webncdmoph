@extends('layouts.app')
<style>
	html { height: 100%; }
	body { height: 100%; }
</style>
<?php
	$arrnew = ['ข่าวกิจกรรม','ข่าวประชาสัมพันธ์'];
?>
@section('content')
	<div class="container-lg" id="forpc">
		<br/>
		<div class="card" style="width: 100%;box-shadow: none;border: none;background-color: transparent">
			<div class="card-header" style="background-color: transparent">
				<h3>{{$data->name}}</h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<span class="font-size-change" style="float: left;padding-top: 2px;color: #B6C3C6;">
							อัพเดทล่าสุด <span id="media">
								<script>
									var date = '{{ $data->created_at }}';
									var date_f = moment(date).add(543, 'year').format('LLL');
									document.getElementById('media').innerHTML = date_f;
								</script>
							</span>
						</span>
						<div class="float-right">
							@if(!in_array($data->category, $arrnew))
								<span style="color: #B6C3C6;">จำนวนดาวน์โหลด <span style="color: dodgerblue;font-size: 20px">{{$data->downloaded}}</span></span>
							@endif
							<span style="margin-left: 10px;color: #B6C3C6;">จำนวนคนอ่าน <span style="color: dodgerblue;font-size: 20px">{{$data->hitcount}}</span></span>
						</div>
					</div>
				</div>
				<br/>
				<div>
					@if($data->fileuniq != 'news')
						@if($data->img === null)
							<div class="image view view-first">
								<img src="{{$site}}/Defaultimg/noimg.jpg">
							</div>
						@else
							<div class="image view view-first">
								<img style="margin-left: auto;margin-right: auto;display: block;" src="{{$site}}/uploads/{{$data->img->path}}" max-width="100%" height="auto" alt="{{$data->img->name}}">
							</div>
						@endif
					@endif
					<br/>
					<span class="font-size-change font-size-change">
						{!! $data->details !!}
					</span>
				</div>
				<br/>
				@if($data->fileuniq != 'news')
					<div class="card" style="width: 100%;box-shadow: none;border: none;background-color:transparent !important;">
						<div class="card-header" style="background-color:transparent !important;border: none;">
							<img style="width: 33px;
							  height: 33px;
							  padding-bottom: 5px;" src="{{$site}}/Defaultimg/7.svg"/>
							ไฟล์แนบ
						</div>
						<div class="card-body" style="box-shadow:0 0 10px 3px #dadada;padding: 20px 15px;border-radius: 10px;margin:0;">
							<div class="row" style="margin-left: 20px;">
								<div class="col-12">
									<span class="font-size-change">{{$data->namefile}}</span>
									<div class="float-right">
										<a style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="gotofile('{{$data->id}}','{{$data->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
				<br/>
			</div>
		</div>
	</div>
	<br/>
	</div>
@endsection
@section('content2')
	<div class="container-fluid" id="formobile">
		<br/>
		<div class="card" style="width: 100%;box-shadow: none;border: none">
			<div class="card-header">
				<h3>{{$data->name}}</h3>
				<span style="float: left;padding-top: 2px">อัพเดทล่าสุด {{$data->updated_at}}</span>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<span style="padding-top: 2px;">จำนวนคนอ่าน <span style="color: dodgerblue;font-size: 20px">{{$data->hitcount}}</span></span>
						<div class="float-right">
							@if(!in_array($data->category, $arrnew))
								<span>จำนวนดาวน์โหลด <span style="color: dodgerblue;font-size: 20px">{{$data->downloaded}}</span></span>
							@endif
						</div>
					</div>
				</div>
				<br/>
				<div>
					@if($data->fileuniq != 'news')
						@if($data->img === null)
							<div class="image view view-first">
								<img src="{{$site}}/Defaultimg/noimg.jpg">
							</div>
						@else
							<div class="image view view-first">
								<img src="{{$site}}/uploads/{{$data->img->path}}" width="100%" height="auto" alt="{{$data->img->name}}">
							</div>
						@endif
					@endif
					<br/>
					{!! $data->details !!}
				</div>
				<br/>
				@if($data->fileuniq !== null)
					<div class="card" style="width: 100%;box-shadow: none;border: none">
						<div class="card-header">
							<img style="width: 33px;
							  height: 33px;
							  padding-bottom: 5px;" src="{{$site}}/Defaultimg/7.svg"/>
							ไฟล์แนบ
						</div>
						<div class="card-body">
							<div class="row" style="margin-left: 20px">
								<div class="col-12">
									<span>{{$data->namefile}}</span>
									<div class="float-right">
										<a style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="gotofile('{{$data->id}}','{{$data->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
				<br/>
			</div>
		</div>
	</div>
	<br/>
	</div>
@endsection

<script type="text/javascript">
	function gotofile(id,file) {
    var _token = $('input[name="_token"]').val();
		$.ajax({
			url : "{{$site}}/showcontent/"+id,
			  data:{ _token:_token  },
			  success: function () {
				window.open(file,'_blank');
			  },
			  error: function (data) {
				console.log(data);
			  }
		});
  }
</script>