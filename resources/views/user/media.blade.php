@extends('layouts.app')
<style>
    html { height: 100%; }
    body { height: 100%; }
    .image {
        position:relative;
        cursor: pointer;
    }
    .image img {
        vertical-align:top;
        max-height: 250px;
        max-width: 250px;
        width: 100%;
        height: 100%;
    }
    .image:after, .image:before {
        position:absolute;
        opacity:0;
        height: 0%;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
    }
    .image:after {
        content:'\A';
        width:100%; height:100%;
        top:0; left:0;
        background:rgba(0,0,0,0.8);
    }
    .image:before {
        content: attr(data-content);
        width:100%;
        color:#fff;
        z-index:1;
        bottom:0;
        padding:4px 10px;
        text-align:center;
        background: url('{{ asset('/Defaultimg/readmore.svg') }}') no-repeat center;
        box-sizing:border-box;
        -moz-box-sizing:border-box;
    }
    .image:hover:after, .image:hover:before {
        opacity: 0.8;
        height: 100%;
        animation-name: fadeInOpacity;
        animation-iteration-count: 1;
        animation-timing-function: ease-in;
        animation-duration: 0.5s;
    }

    @keyframes fadeInOpacity {
        0% {
            opacity: 0;
            height: 0%;
        }
        100% {
            opacity: 1;
            height: 100%;
        }
    }
    #btn-custom{
        border: none;
        border-radius: 10px;
        color: #B6C3C6;
        background-color: transparent;
        box-shadow: none;
        outline: none;
    }
    #btn-custom.active{
        border: 1px solid #508EBF;
        color: #508EBF;
    }

    .content div {
        width: 100%;
        display: -webkit-box;
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 4;
        font-size: 14px;
    }

    .content{
        width:100%;
    }
    .card-text{
        color: #717171;
    }
    div#forpc ul li button:hover{
        background-color: #c3e9f6;
        border-radius: 10px;
        color: white;
    }
    div.card-text span p{
        margin-bottom:0 !important;
    }
</style>
<?php
$arrayforact = array(
    "แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)",
    "สถานการณ์ / ข้อมูล",
    "ผลการดำเนินงาน/Best practice",
    "คู่มือ แนวทาง สื่อ");
$data = new \App\contentactivity();
$dataact = $data->all()->whereIn('category',$arrayforact)->sortByDesc('updated_at')->groupBy('category');
?>
@section('content')
    <div class="container-lg" id="forpc">
        <div class="row">
            <div class="col-12">
                <img style="width: 44px;height: 44px;" src="{{$site}}/Defaultimg/สื่อเผยแพร่.png"/>
                <span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">สื่อเผยแพร่</span>
            </div>
            <div class="col-12"><hr style="border: 1px solid #B6C3C6;"></div>
        </div>
        <ul class="nav" role="tablist">
            @foreach($dataact as $key => $item)
                <li style="padding: 0 5px;word-wrap: break-word;text-align:center; vertical-align:middle" role="presentation" class="col-md-3 col-sm-6">
                    <button class="text-center nowrap {{$loop->first ? 'active' : ''}} font-size-change" id="btn-custom" href="#tab-{{ $loop->index }}" aria-controls="#tab-{{ $loop->index }}" role="tab" data-toggle="tab" style="font-size: 12px;">
                        {{$key}}
                    </button>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" style="padding-top: 20px">
            <?php $i=0 ?>
            @foreach($dataact as $items)
                <div id="tab-{{ $loop->index }}" class="tab-pane {{$loop->first ? 'active':''}}">
                    <div class="panel-body">
                        <div class="row">
                            @foreach($items as $key => $item)
                                <div class="col-md-4">
									<span onclick="gotomedia('{{$item->id}}');">
										<div class="card" style="box-shadow: none;border: none;background-color: transparent;">
											@if($item->img === null)
                                                <div data-content="" class="image">
													<img style="border-bottom: 1px solid black" class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" name="imaged">
												</div>
                                            @else
                                                <div data-content="" class="image">
													<img class="card-img-top" src="{{$site}}/uploads/{{$item->img->path}}" name="imaged" alt="{{$item->img->name}}">
												</div>
                                            @endif
											<div class="content changefontcolor" style="cursor: pointer;padding: 0;">
												<span class="card-title font-size-change">
													{{ str_limit($item->name,30) }}
												</span>
												<div class="card-text font-size-change">
													<span>
                           <?php
                                                        $item->details = preg_replace("/<img[^>]+\>/i", "", $item->details);
                                                        ?>
                                                        {!! $item->details !!}
													</span>
		                    					</div>
												<span id="m{{$i.$key}}" style="color: #B6C3C6;font-size: 10px">
													<script>
														var date = '{{ $item->created_at }}';
                                                        var date_f = moment(date).add(543, 'year').format('LL');
                                                        document.getElementById('m{{$i.$key}}').innerHTML = date_f;
													</script>
												</span>
											</div>
										</div>
									</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <?php $i++ ?>
            @endforeach
        </div>
    </div>
@endsection
@section('content2')
    <div class="container-fluid" id="formobile">
        <div class="row">
            <div class="col-12">
                <img style="width: 44px;height: 44px;" src="{{$site}}/Defaultimg/สื่อเผยแพร่.png"/>
                <span style="font-weight: bold;vertical-align: bottom;font-size: 20px">สื่อเผยแพร่</span>
            </div>
            <div class="col-12"><hr style="border: 1px solid #3D3D3D;"></div>
        </div>
        <ul class="nav" role="tablist">
            <select id='Selectmedia' class="form-control">
                @foreach($dataact as $key => $item)
                    <option value="{{ $loop->index }}">{{ $key }}</option>
                @endforeach
            </select>
        </ul>
        <div class="tab-content" style="padding-top: 20px">
            @foreach($dataact as $items)
                <div id="{{ $loop->index }}" role="tabpanel" class="tab-pane custom-media {{$loop->first ? 'active':''}}">
                    @foreach($items->take(4) as $item)
                        <div onclick="gotomedia('{{$item->id}}');" class="{{$loop->first ? 'active':''}}" style="margin-bottom: 20px">
                            <div class="image view view-first">
                                @if($item->img === null)
                                    <img style="border-bottom: 1px solid black" class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
                                @else
                                    <img class="card-img-top" src="{{$site}}/uploads/{{$item->img->path}}" width="100%" height="auto" alt="{{$item->img->name}}">
                                @endif
                                <div class="content changefontcolor" style="cursor: pointer;padding: 0;">
									<span class="card-title font-size-change">
										{{ str_limit($item->name,30) }}
									</span>
                                    <div class="card-text font-size-change">
										<span>
                          <?php
                                            $item->details = preg_replace("/<img[^>]+\>/i", "", $item->details);
                                            ?>
                                            {!! $item->details !!}
										</span>
                                    </div>
                                    <span id="m{{$i.$i.$key}}" style="color: #B6C3C6;font-size: 10px">
										<script>
											var date = '{{ $item->created_at }}';
                                            var date_f = moment(date).add(543, 'year').format('LL');
                                            document.getElementById('m{{$i.$i.$key}}').innerHTML = date_f;
										</script>
									</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <?php $i++ ?>
            @endforeach
        </div>
        <br/>
    </div>
    <script>
        $('#Selectmedia').on('change', function (e) {
            $('.custom-media').hide();
            $('.custom-media').eq($(this).val()).show();
        });
    </script>
@endsection
@include('layouts.onlyDatatable')
<script>
    function gotomedia(id) {
        window.open( 'media/'+id,'_blank');
    }
</script>
