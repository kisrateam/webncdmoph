@extends('layouts.app')
@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
<style>
	html { height: 100%;}
	body { height: 100%;}
	.content_hover:hover{
		background-color: #4fc3f7;
		cursor: pointer;
	}
</style>
<?php
	$img = \App\img::where('idfk','content')->first();
	$data = \App\content::where('category','0')->where('active','1')->get();
	$dall = \App\content::where('name','downloadall1')->first();
?>
@section('content')
	<div class="container-lg" id="forpc">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;
							height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
				<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</span>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="col-12">
				@if($img == null)
					<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto"/>
				@else
					<div class="text-center">
						<img style="max-width: 100%;height: auto" src="{{$site}}/uploads/{{$img->path}}" alt="{{$img->name}}"/>
					</div>
					<div>
						<br/>
						<span class="font-size-change">{!! $img->name!=null ? $img->name:'' !!}</span>
					</div>
				@endif
			</div>

			<div class="col-12">
				<br/>
				<br/>
				<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
					<div class="font-size-change text-center" style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
						<a href="{{route('contentre')}}" style="color: #508EBF;padding: 0">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)<br/>ฉบับปรับปรุงระยะปี พ.ศ. 2563 - 2565</a>
					</div>
				</div>
				<div class="row" style="box-shadow:0 0 10px 3px #dadada;padding: 20px 15px;border-radius: 10px;margin:0;">
					@foreach($data as $item)
						<div class="col-9">
							<span class="font-size-change">{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button class="download_hover" style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
						@if(!$loop->last)
							<div class="col-12">
								<hr style="border: 1px solid #B6C3C6;width: 100%">
							</div>
						@endif
				@endforeach
				</div>

				{{--@if($dall)--}}
					{{--<div class="col">--}}
						{{--<div style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 18px;padding: 10px">--}}
							{{--<button style="border-radius: 20px;border: 1px solid #508EBF;background-color: transparent;color: #508EBF;outline: none;height: 40px;" onclick="window.open('{{$dall->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="margin: 0 10px">ดาวน์โหลดทั้งหมด</button>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--@endif--}}
				<br/>
			</div>
		</div>
		<div class="col-12" style="padding: 0;margin-bottom: 50px">
			<img style="width: 30px;
						height: 30px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
			<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  นโยบายเร่งรัด</span>
			<hr style="border: 1px solid #B6C3C6;width: 100%">
			<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
				<div class="font-size-change text-center" style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
					<a href="{{route('contentf')}}" style="color: #508EBF;padding: 0">นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูงและเบาหวาน ปี พ.ศ. 2563 - 2564</a>
				</div>
			</div>
			{{--<div class="row content_hover" style=";padding:15px 0;box-shadow:0 0 10px 3px #dadada;border-radius: 10px;margin: 0;">--}}
				{{--<div class="col text-center" onclick="window.open('{{route('contentf')}}','_self')">--}}
					{{--<span class="font-size-change">นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูงและเบาหวาน ปี พ.ศ. 2563 - 2564</span>--}}
				{{--</div>--}}
			{{--</div>--}}
		</div>
		<div class="col-12" style="padding: 0;margin-bottom: 50px">
			<img style="width: 30px;
						height: 30px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
			<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนปฏิรูปด้าน NCDs</span>
			<hr style="border: 1px solid #B6C3C6;width: 100%">
			<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
				<div class="font-size-change text-center" style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
					<a href="{{route('contentn')}}" style="color: #508EBF;padding: 0">แผนปฏิรูปด้าน NCDs</a>
				</div>
			</div>
			{{--<div class="row content_hover" style=";padding:15px 0;box-shadow:0 0 10px 3px #dadada;border-radius: 10px;margin: 0;">--}}
				{{--<div class="col text-center" onclick="window.open('{{route('contentn')}}','_self')">--}}
					{{--<span class="font-size-change">แผนปฏิรูปด้าน NCDs</span>--}}
				{{--</div>--}}
			{{--</div>--}}
		</div>
	</div>
@endsection
@section('content2')
	<div class="container-fluid" id="formobile">
		<div class="row">
			<div class="col-12">
				<img style="width: 44px;
							height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
				<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</span>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="col-12">
				@if($img == null)
					<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto"/>
				@else
					<img src="{{$site}}/uploads/{{$img->path}}" width="100%" height="auto" alt="{{$img->name}}"/>
					<div class="text-center">
						<br/>
						<span class="font-size-change">{!! $img->name!=null ? $img->name:'' !!}</span>
					</div>
				@endif
			</div>

			<div class="col-12">
				<br/>
				<br/>
				<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
					<div style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
						<a href="{{route('contentre')}}" style="color: #508EBF;padding: 0">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (ฉบับปรับปรุง)</a>
					</div>
				</div>
				<div class="row" style="box-shadow:0 0 10px 3px #dadada;padding: 20px 15px;border-radius: 10px;margin:0;">
					@foreach($data as $item)
						<div class="col-9">
							<span>{{$item->name}}</span>
						</div>

						<div class="col-3">
							<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" onclick="window.open('{{$item->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
						</div>
						@if(!$loop->last)
							<div class="col-12">
								<hr style="border: 1px solid #B6C3C6;width: 100%">
							</div>
						@endif
					@endforeach
				</div>
				<br/>
				{{--@if($dall)--}}
					{{--<div class="col">--}}
						{{--<div style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 18px;padding: 10px">--}}
							{{--<button style="border-radius: 20px;border: 1px solid #508EBF;background-color: transparent;color: #508EBF;outline: none;height: 40px;" onclick="window.open('{{$dall->fileuniq}}','_blank');"><img src="{{$site}}/Defaultimg/clouddownload.svg" style="margin: 0 10px">ดาวน์โหลดทั้งหมด</button>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--@endif--}}
			</div>
		</div>
		<div class="col-12" style="padding: 0;margin-bottom: 50px">
			<img style="width: 30px;
						height: 30px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
			<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  นโยบายเร่งรัด</span>
			<hr style="border: 1px solid #B6C3C6;width: 100%">
			<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
				<div class="font-size-change text-center" style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
					<a href="{{route('contentf')}}" style="color: #508EBF;padding: 0">นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูงและเบาหวาน ปี พ.ศ. 2563 - 2564</a>
				</div>
			</div>
		</div>
		<div class="col-12" style="padding: 0;margin-bottom: 50px">
			<img style="width: 30px;
						height: 30px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
			<span class="font-size-change" style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนปฏิรูปด้าน NCDs</span>
			<hr style="border: 1px solid #B6C3C6;width: 100%">
			<div class="col" style="background-color: #E3EDF7;border-radius: 20px;margin-bottom: 30px">
				<div class="font-size-change text-center" style="height: 100px;display: flex;justify-content: center;align-items: center;font-size: 20px">
					<a href="{{route('contentn')}}" style="color: #508EBF;padding: 0">แผนปฏิรูปด้าน NCDs</a>
				</div>
			</div>
		</div>
	</div>
@endsection