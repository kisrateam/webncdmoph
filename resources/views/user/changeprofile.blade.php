@extends('layouts.app')
@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
@if(Session::has('error'))
	<div class="alert alert-error">
		<strong>error: </strong>{{ Session::get('error') }}
	</div>
@endif
<style>
	html { height: 100%; }
	body { height: 100%; }
</style>
@section('content')
	<div class="container-lg">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">{{ __('เปลี่ยนชื่อผู้ใช้งาน') }}</div>
					<div class="card-body">
						<form method="POST" action="{{ route('changeprofile') }}">
							@csrf
							<div class="form-group row">
								<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อผู้ใช้') }}</label>
								<div class="col-md-6">
									<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" minlength="4" maxlength='12' value="{{ $data->name }}" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="current-password" class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่านปัจจุบัน') }}</label>
								<div class="col-md-6">
									<input id="current-password" type="password" class="form-control" name="current-password">
								</div>
							</div>
							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">
										{{ __('เปลี่ยนข้อมูล') }}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<div class="card-header">{{ __('เปลี่ยนรหัสผ่าน') }}</div>
					<div class="card-body">
						<form method="POST" action="{{ route('changeprofile') }}">
							@csrf
							<div class="form-group row">
								<label for="current-password" class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่านปัจจุบัน') }}</label>
								<div class="col-md-6">
									<input id="current-password" type="password" class="form-control" name="current-password">
								</div>
							</div>
							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่านใหม่') }}</label>
								<div class="col-md-6">
									<input id="password" autocomplete="new-password" type="password" class="form-control" name="password" minlength="7">
								</div>
							</div>
							<div class="form-group row">
								<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่านใหม่-อีกครั้ง') }}</label>
								<div class="col-md-6">
									<input id="password-confirm" autocomplete="new-password" type="password" class="form-control" name="password_confirmation" minlength="7">
								</div>
							</div>
							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">
										{{ __('เปลี่ยนข้อมูล') }}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
