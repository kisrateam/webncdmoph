@extends('layouts.appadmin')
@section('content')
	<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
	@include('layouts.onlyDatatable')
	<style>
		.ck-editor__editable {
			min-height: 200px;
		}
	</style>
	<form method="POST" action="{{ route('contentact.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลข่าวสาร</h3>
							<div class="col-md-12">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
									   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
							</div>
							<!-- Name -->
							<div class="md-form">
								<i role="presentation"  class="fas fa-heading prefix"></i>
								<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<!-- Message -->
							<textarea class="details" name="details">{{ old('details') }}
							</textarea>
							<script>
								var CSRFToken = $('meta[name="csrf-token"]').attr('content');
								var options = {
								  filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
								  filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
								  filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
								  filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
								};
							</script>
							<script>
                				CKEDITOR.replace('details', options);
							</script>
							<div class="md-form">
								<div align="center">
									ตัวอย่างรูป
									<div id="previewImg"></div>
								</div>
								<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
								</div>
							</div>
							<script type="text/javascript">
                window.preview = function (input) {
                  if (input.files && input.files[0]) {
                    $("#previewImg").html("");
                    $(input.files).each(function () {
                      var reader = new FileReader();
                      reader.readAsDataURL(this);
                      reader.onload = function (e) {
                        $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                      }
                    });
                  }
                }
							</script>
							<div class="md-form">
								<select type="text" id="category" name="category" class="form-control col-md-12" autocomplete="off" onchange="getsub(this.value);">
									<option disabled selected hidden>ประเภท</option>
										<option>ข่าวกิจกรรม</option>
										<option>ข่าวประชาสัมพันธ์</option>
									</select>
							</div>
							<div class="md-form">
								<div>
									<select class="form-control" id="sub_category" name="subcategory">
										<option disabled selected hidden>เลือกประเภทก่อน</option>
										<option></option>
									</select>
								</div>
							</div>
								<!-- Send button -->
								<button class="btn btn-info btn-block" type="submit">บันทึก</button>
							<!-- Basic dropdown -->
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection
<script>
    function getsub(data){
        let list = {0:['ประชุมคณะกรรมการ','อบรม/สัมนา','รณรงค์'],
			1:['ประชุมคณะกรรมการ','อบรม/สัมนา','รณรงค์','ข่าวประกวด/ประกาศ']};
        let name=data;
        switch(name){
			case 'ข่าวกิจกรรม' : {
                $("#sub_category").empty();
                for(let i=0; i<list[0].length; i++){
                    let name = list[0][i];
                    let option = "<option value='"+name+"'>"+name+"</option>";
                    $("#sub_category").append(option);
                }
			    break;
			}
			case 'ข่าวประชาสัมพันธ์' :{
                $("#sub_category").empty();
                for(let i=0; i<list[1].length; i++){
                    let name = list[1][i];
                    let option = "<option value='"+name+"'>"+name+"</option>";
                    $("#sub_category").append(option);
                }
                break;
            }
		}
    }
</script>
