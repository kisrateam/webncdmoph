@extends('layouts.app')
<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
@include('layouts.onlyDatatable')
<style>
	html { height: 100%; }
	body { height: 100%; }

	.ck-editor__editable {
		min-height: 200px;
	}
</style>
@section('content')
	<div class="container" id="forpc">
		<div class="row">
			<div class="col-12">
				<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px;cursor:pointer;" tabindex="0" onclick="window.open('{{route('topic')}}','_self')"> กระดานสนทนา
				</span>
				<i role="presentation"  style="font-size: 16px" class="fas fa-caret-left"></i>
				<span style="font-size: 14px;font-weight: normal">แก้ไขกระดานสนทนา</span>
				<div class="float-right">
					@if(Auth::check())
						<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">
							<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> บทสนทนา</a>
					@endif
				</div>
			</div>
			<div class="col-12">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
						<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
							กระดานสนทนาของฉัน</a>
					</div>
				@endif
				<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
				<hr style="border: 1px solid #B6C3C6;">
			</div>
		</div>
		<p style="font-size: 24px;font-weight: bold">แก้ไขกระดานสนทน</p>
			<div class="row justify-content-center">
				<form method="POST" action=" {{route('edittopic')}}">
					@csrf
					<div class="card-body">
						<input type="text" class="form-control" id="id" name="id" value="{{$data->id}}" hidden>
						<div class="form-group row">
							<label style="text-align: right" for="privacy" class="col-sm-12 col-md-2 col-form-label">กลุ่มเป้าหมาย</label>
							<div class="col-sm-12 col-md-8">
								<select class="browser-default custom-select" name="privacy">
									<option {{(\Auth::user()->role == 'user' ? 'hidden':'selected disabled hidden')}}>เลือกกลุ่มเป้าหมาย</option>
									<option {{$data->privacy == 1?'selected':''}} value="1">สาธารณะ</option>
									<option {{$data->privacy == 2?'selected':''}} value="2">สมาชิก</option>
									<option {{$data->privacy == 3?'selected':''}} {{(\Auth::user()->role == 'user' ? 'hidden disabled':'')}} value="3">คณะทำงาน</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label style="text-align: right" for="category" class="col-sm-12 col-md-2 col-form-label">หมวดหมู่</label>
							<div class="col-sm-12 col-md-8">
								<select class="browser-default custom-select" name="category">
									<option selected disabled hidden>เลือกยุทธศาสตร์</option>
									<option {{$data->category == 1?'selected':''}} value="1">ยุทธศาสตร์ที่ 1</option>
									<option {{$data->category == 2?'selected':''}} value="2">ยุทธศาสตร์ที่ 2</option>
									<option {{$data->category == 3?'selected':''}} value="3">ยุทธศาสตร์ที่ 3</option>
									<option {{$data->category == 4?'selected':''}} value="4">ยุทธศาสตร์ที่ 4</option>
									<option {{$data->category == 5?'selected':''}} value="5">ยุทธศาสตร์ที่ 5</option>
									<option {{$data->category == 6?'selected':''}} value="6">ยุทธศาสตร์ที่ 6</option>
								</select>
							</div>
							{{--<div class="col-sm-6 col-md-4" style="{{(\Auth::user()->role == 'user' ? 'display:none;':'')}}">--}}
								{{--<select class="browser-default custom-select" name="subcategory">--}}
									{{--<option selected disabled hidden>เลือกแผน</option>--}}
									{{--<option {{$data->subcategory == 1?'selected':''}} value="1">แผนและการดำเนินการ</option>--}}
									{{--<option {{$data->subcategory == 2?'selected':''}} value="2">รายงานผลดำเนินการ</option>--}}
									{{--<option {{$data->subcategory == 3?'selected':''}} value="3">การประสานงาน</option>--}}
								{{--</select>--}}
							{{--</div>--}}
						</div>
						<div class="form-group row">
							<label style="text-align: right" for="topic" class="col-sm-12 col-md-2 col-form-label">หัวข้อ</label>
							<div class="col-sm-12 col-md-8">
								<input type="text" class="form-control" id="topic" name="topic" placeholder="หัวข้อ" value="{{$data->name}}" required autocomplete="off">
							</div>
						</div>
						<div class="form-group row">
							<label style="text-align: right" for="text" class="col-sm-12 col-md-2 col-form-label">รายละเอียด</label>
							<div class="col-sm-12 col-md-8">
							<textarea placeholder="รายละเอียด" class="details" name="details">
								{{$data->details}}
								</textarea>
								<script>
									  let CSRFToken = $('meta[name="csrf-token"]').attr('content');
									  let options = {
										filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
										filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
										filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
										filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
									  };
								</script>
								<script>
                  CKEDITOR.replace('details', options);
								</script>
							</div>
						</div>
						<div class="row" style="float: right;padding-right: 18%">
							<button type="submit" style="color: white;background: #508EBF;border-radius: 20px;" class="btn btn-sm">ยืนยัน</button>
							<a href="{{route('topic')}}" style="color: #B6C3C6;background: white;border-radius: 20px;" class="btn btn-sm">ยกเลิก</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
@section('content2')
	<div class="container-fluid" id="formobile">
		<div class="row">
			<div class="col-12">
				<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px;cursor:pointer;" tabindex="0" onclick="window.open('{{route('topic')}}','_self')"> กระดานสนทนา
				</span>
				<i role="presentation"  style="font-size: 16px" class="fas fa-caret-left"></i>
				<span style="font-size: 14px;font-weight: normal">แก้ไขกระดานสนทนา</span>
				<div class="float-right">
					@if(Auth::check())
						<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">
							<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> บทสนทนา</a>
					@endif
				</div>
			</div>
			<div class="col-12">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
						<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
							กระดานสนทนาของฉัน</a>
					</div>
				@endif
				<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
				<hr style="border: 1px solid #B6C3C6;">
			</div>
		</div>
		<p style="font-size: 24px;font-weight: bold">แก้ไขกระดานสนทน</p>
		<div class="row justify-content-center">
			<form method="POST" action=" {{route('edittopic')}}">
				@csrf
				<div class="card-body">
					<input type="text" class="form-control" id="id" name="id" value="{{$data->id}}" hidden>
					<div class="form-group row">
						<label for="privacy" class="col-sm-12 col-md-2 col-form-label">กลุ่มเป้าหมาย</label>
						<div class="col-sm-12 col-md-8">
							<select class="browser-default custom-select" name="privacy">
								<option {{(\Auth::user()->role == 'user' ? 'hidden':'selected disabled hidden')}}>เลือกกลุ่มเป้าหมาย</option>
								<option {{$data->privacy == 1?'selected':''}} value="1">สาธารณะ</option>
								<option {{$data->privacy == 2?'selected':''}} value="2">สมาชิก</option>
								<option {{$data->privacy == 3?'selected':''}} {{(\Auth::user()->role == 'user' ? 'hidden disabled':'')}} value="3">คณะทำงาน</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
					<label style="text-align: right" for="category" class="col-sm-12 col-md-2 col-form-label">หมวดหมู่</label>
						<div class="col-sm-12 col-md-8">
							<select class="browser-default custom-select" name="category">
							<option selected disabled hidden>เลือกยุทธศาสตร์</option>
							<option {{$data->category == 1?'selected':''}} value="1">ยุทธศาสตร์ที่ 1</option>
							<option {{$data->category == 2?'selected':''}} value="2">ยุทธศาสตร์ที่ 2</option>
							<option {{$data->category == 3?'selected':''}} value="3">ยุทธศาสตร์ที่ 3</option>
							<option {{$data->category == 4?'selected':''}} value="4">ยุทธศาสตร์ที่ 4</option>
							<option {{$data->category == 5?'selected':''}} value="5">ยุทธศาสตร์ที่ 5</option>
							<option {{$data->category == 6?'selected':''}} value="6">ยุทธศาสตร์ที่ 6</option>
							</select>
						</div>
					{{--<div class="col-sm-6 col-md-4" style="{{(\Auth::user()->role == 'user' ? 'display:none;':'')}}">--}}
					{{--<select class="browser-default custom-select" name="subcategory">--}}
					{{--<option selected disabled hidden>เลือกแผน</option>--}}
					{{--<option {{$data->subcategory == 1?'selected':''}} value="1">แผนและการดำเนินการ</option>--}}
					{{--<option {{$data->subcategory == 2?'selected':''}} value="2">รายงานผลดำเนินการ</option>--}}
					{{--<option {{$data->subcategory == 3?'selected':''}} value="3">การประสานงาน</option>--}}
					{{--</select>--}}
					{{--</div>--}}
					</div>
					<div class="form-group row">
						<label for="topic2" class="col-sm-12 col-md-2 col-form-label">หัวข้อ</label>
						<div class="col-sm-12 col-md-8">
							<input type="text" class="form-control" id="topic2" name="topic" placeholder="หัวข้อ" value="{{$data->name}}" required autocomplete="off">
						</div>
					</div>
					<div class="form-group row">
						<label for="text" class="col-sm-12 col-md-2 col-form-label">รายละเอียด</label>
						<div class="col-sm-12 col-md-8">
							<textarea placeholder="รายละเอียด" class="details" name="details" id="details2">
								{{$data->details}}
								</textarea>
							<script>
                                CKEDITOR.replace('details2', options);
							</script>
						</div>
					</div>
					<div class="row" style="float: right;">
						<button type="submit" style="color: white;background: #508EBF;border-radius: 20px;" class="btn btn-sm">ยืนยัน</button>
						<a href="{{route('topic')}}" style="color: #B6C3C6;background: white;border-radius: 20px;" class="btn btn-sm">ยกเลิก</a>
					</div>
				</div>
			</form>
		</div>
	</div>
	</div>
@endsection
