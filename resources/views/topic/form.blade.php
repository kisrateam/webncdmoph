<?php
$listofgroup = ["หน้าฟืด","ยุทธศาสตร์ที่ 1","ยุทธศาสตร์ที่ 2","ยุทธศาสตร์ที่ 3","ยุทธศาสตร์ที่ 4","ยุทธศาสตร์ที่ 5","ยุทธศาสตร์ที่ 6"];
$listoftooltip = ["หน้าฟืด","ยุทธศาสตร์ที่ 1","ยุทธศาสตร์ที่ 2","ยุทธศาสตร์ที่ 3","ยุทธศาสตร์ที่ 4","ยุทธศาสตร์ที่ 5","ยุทธศาสตร์ที่ 6"];
$listnum = ['a',1,2,3,4,5,6];
?>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<ul class="nav nav-tabs" id="Tab" role="tablist" style="border: none">
    @for($i = 0;$i < count($listofgroup);$i++)
        <li class="nav-item" data-toggle="tooltip" title="{{$listoftooltip[$i]}}">
            <a class="nav-link {{$i == 0 ? 'active':''}}" data-target="{{'#panel_d_'.$listnum[$i]}}" data-secondary="{{'#panel_c_'.$listnum[$i]}}" data-toggle="tab" href="#" role="tab" aria-selected="false">{{$listofgroup[$i]}}</a>
        </li>
    @endfor
</ul>
<br/>

<div class="tab-content" id="TabA" style="padding:0 20px;">
    <div class="tab-pane fade show active" id="panel_c_a" role="tabpanel" aria-labelledby="a-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่
                    <img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $dataa = new $data;
                $dataa = $dataa->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($dataa->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;"
                           href="{{$site}}/home/topic/{{$item->id}}"
                           target="_blank">
                        <h5>{{$item->name()}}</h5>
                        <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($dataa->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                <span>
                <h5>ไม่พบบทสนทนา</h5>
                </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_1" role="tabpanel" aria-labelledby="1-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data1 = new $data;
                $data1 = $data1->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data1->where('category','1')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
                        <span>
                        @if($item->status == 0)
                                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                            @endif
                            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                        <h5>{{$item->name()}}</h5>
                        <h6>โพสโดย {{$item->users->name}}</h6>
                        </a>
                        </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                        </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data1->where('category','1')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_2" role="tabpanel" aria-labelledby="2-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data2 = new $data;
                $data2 = $data2->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data2->where('category','2')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
                        <span>
                        @if($item->status == 0)
                                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                            @endif
                            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                        <h5>{{$item->name()}}</h5>
                        <h6>โพสโดย {{$item->users->name}}</h6>
                        </a>
                        </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                        </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data2->where('category','2')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_3" role="tabpanel" aria-labelledby="3-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data3 = new $data;
                $data3 = $data3->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data3->where('category','3')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
        <span>
        @if($item->status == 0)
                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
            @endif
            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
        <h5>{{$item->name()}}</h5>
        <h6>โพสโดย {{$item->users->name}}</h6>
        </a>
        </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
        </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data3->where('category','3')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
        <span>
        <h5>ไม่พบบทสนทนา</h5>
        </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_4" role="tabpanel" aria-labelledby="4-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data4 = new $data;
                $data4 = $data4->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data4->where('category','4')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
        <span>
        @if($item->status == 0)
                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
            @endif
            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
        <h5>{{$item->name()}}</h5>
        <h6>โพสโดย {{$item->users->name}}</h6>
        </a>
        </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
        </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data4->where('category','4')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
        <span>
        <h5>ไม่พบบทสนทนา</h5>
        </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_5" role="tabpanel" aria-labelledby="5-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data5 = new $data;
                $data5 = $data5->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data5->where('category','5')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
        <span>
        @if($item->status == 0)
                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
            @endif
            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id.'a5'}}" target="_blank">
        <h5>{{$item->name()}}</h5>
        <h6>โพสโดย {{$item->users->name}}</h6>
        </a>
        </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
        </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data5->where('category','5')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
        <span>
        <h5>ไม่พบบทสนทนา</h5>
        </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_c_6" role="tabpanel" aria-labelledby="6-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่<img style="margin-left: 10px" src="{{$site}}/Defaultimg/topic-new.svg"></span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $data6 = new $data;
                $data6 = $data6->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('created_at','DESC');
                ?>
                @foreach($data6->where('category','6')->get()->take(4) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($data6->where('category','6')->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
    </div>
</div>
