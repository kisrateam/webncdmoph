@extends('layouts.app')

@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
@if(Session::has('error'))
	<div class="alert alert-error">
		<strong>Error: </strong>{{ Session::get('error') }}
	</div>
@endif
<style>
	html { height: 100%;}
	body { height: 100%;}

	/*main { background-color: #ececec }*/

	#dtBasicExample thead {
		display: none;
	}
	table.dataTable {
		/*border-spacing: 5px !important;*/
	}

	input[name="autoname"]{
		width: 100%;
		border: 1px solid #ccc;
		border-radius: 40px;
		padding: 12px 20px 12px 30px;
		outline: none;
		float: right;
	}

	li.nav-item a.nav-link{
		font-size: 85%;
		border: none;
		box-shadow: none;
		margin: 0;
		color: grey;
	}
	li.nav-item a.nav-link.active{
		border-radius: 10px;
		border: 1px solid #508EBF;
		color: #508EBF;
	}
	.center-block{
		position: relative;
		left: 40%;
	}
</style>
@section('content')
	<div class="container-lg" id="forpc">
		<div class="row" style="width:100%;">
			<div style="width: 200px;">
				<img class="imgcolorblue" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px"> กระดานสนทนา
				<p style="font-size: 18px;font-weight: 400;padding-left: 22%">{{$counttopic}} บทสนทนา</p>
				</span>
			</div>
			<div style="width: calc(100% - 200px - 120px);">
				<input type="search" id="autoname" name="autoname" autocomplete="off" placeholder="ค้นหาบทสนทนา">
				<div class="md-form" style="position: absolute;width: calc(100% - 380px);">
					<div id="List"></div>
				</div>
			</div>
			<div style="width: 120px;">
				@if(Auth::check())
					<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px;width: 100%;font-size: 16px" class="btn">
						<i role="presentation"  class="fas fa-plus"></i> บทสนทนา</a>
				@else
					<a tabindex="0" onclick="switchlabel(1);" data-toggle="modal" data-target="#loginModal" style="background-color: #2EA1A0;color:white;border-radius: 20px;width: 100%;font-size: 16px" class="btn">
						<i role="presentation"  class="fas fa-plus"></i> บทสนทนา</a>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-12" style="padding: 0;">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
							<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
								กระดานสนทนาของฉัน</a>
					</div>
					<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
				@endif
				<hr style="border: 1px solid #B6C3C6;">
			</div>
		</div>
        <?php
        $timetocount = '-12 hours';
        ?>
		{{--@if(!Auth::check() || \Auth::user()->role == 'user')--}}
			@include('topic.forpc')
		{{--@else--}}
			{{--@include('topic.formember')--}}
		{{--@endif--}}
	</div>

@endsection
@section('content2')
<div id="formobile" class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-5 row">
			<div class="col-6 col-sm-12" style="padding: 0">
				<img class="imgcolorblue" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 22px;font-weight: bold;padding-left: 10px"> กระดานสนทนา</span>
			</div>
			<div class="col-6 col-sm-12" style="padding: 0">
				<p style="font-size: 18px;font-weight: 400;padding-left: 22%">{{$counttopic}} บทสนทนา</p>
			</div>
		</div>
		<div class="col-xs-12 col-sm-7">
			<input type="search" id="autonamem" name="autoname" autocomplete="off" placeholder="ค้นหาบทสนทนา">
			<div class="md-form" style="position: absolute;width: 100%;">
				<div id="Listm"></div>
			</div>
		</div>
		<div class="col-12">
			@if(Auth::check())
				<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px;width: 100%;font-size: 16px" class="btn">
					<i role="presentation"  class="fas fa-plus"></i> บทสนทนา</a>
			@else
				<a tabindex="0" onclick="switchlabel(1);" data-toggle="modal" data-target="#loginModal" style="background-color: #2EA1A0;color:white;border-radius: 20px;width: 100%;font-size: 16px" class="btn">
					<i role="presentation"  class="fas fa-plus"></i> บทสนทนา</a>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-12" style="padding: 0;">
			<br/>
			@if(Auth::check())
				<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
				<div class="float-right">
					<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
						กระดานสนทนาของฉัน</a>
				</div>
				<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
			@endif
			<hr style="border: 1px solid #B6C3C6;">
		</div>
	</div>
    <?php
    $timetocount = '-12 hours';
    ?>
	{{--@if(!Auth::check() || \Auth::user()->role == 'user')--}}
		@include('topic.form')
		@include('topic.form2')
	{{--@else--}}
		{{--@include('topic.formember')--}}
	{{--@endif--}}
</div>
{{--@if(!Auth::check() || \Auth::user()->role == 'user')--}}
<div class="container-lg" id="forpc">
	@include('topic.forpc2')
</div>
{{--@else--}}
	{{--@include('topic.formember2')--}}
{{--@endif--}}


{{--<div id="formobile">--}}
	{{--<div class="row">--}}
		{{--<div class="col-12">--}}
			{{--<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">--}}
			{{--<span style="font-size: 20px;font-weight: bold;padding-left: 10px"> กระดานสนทนา--}}
		{{--<span style="font-size: 18px;font-weight: 400;padding-left: 10px">{{$counttopic}} คำถาม</span>--}}
		{{--</span>--}}
		{{--</div>--}}
		{{--<div class="col-12">--}}
			{{--<br/>--}}
			{{--@if(Auth::check())--}}
				{{--<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">--}}
					{{--<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> คำถาม</a>--}}
				{{--<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;--}}
							{{--box-sizing: border-box;border-radius: 20px;" class="btn">--}}
					{{--คำถามของฉัน</a>--}}
			{{--@endif--}}
		{{--</div>--}}
		{{--<div class="col-12">--}}
			{{--<br/>--}}
			{{--@if(Auth::check())--}}
				{{--<span>รหัสสมาชิก : {{\Auth::user()->name}}</span>--}}
				{{--<p>สถานะ : {{\Auth::user()->role}}</p>--}}
			{{--@endif--}}
		{{--</div>--}}
		{{--<div class="col-12" style="width: 100%;">--}}
			{{--<input type="text" name="searchtopic" autocomplete="off">--}}
		{{--</div>--}}
	{{--</div>--}}
	{{--<hr style="border: 1px solid #B6C3C6;">--}}

	{{--<br/>--}}

	{{--<div class="row" style="padding: 0 20px;">--}}
		{{--<div class="col-12">--}}
			{{--<span style="font-size: 20px;font-weight: bold;">บทสนทนาใหม่</span>--}}
			{{--<br/>--}}
		{{--</div>--}}
		{{--<div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px">--}}
			{{--@foreach($data->take(4) as $item)--}}
				{{--<div class="col-12" style="padding: 10px">--}}
			{{--<span>--}}
					{{--@if($item->status == 0)--}}
						{{--<img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">--}}
					{{--@endif--}}
					{{--<a style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">--}}
					{{--<h3>{{$item->name}}</h3>--}}
					{{--<h6>โพสโดย {{$item->users->name}}</h6>--}}
				{{--</a>--}}
			{{--</span>--}}
			{{--<span style="float:right;color: black;" disabled>--}}
				{{--<span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>--}}
				{{--<img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}--}}
			{{--</span>--}}
					{{--<h6 style="position: absolute">{{$item->timeago()}}</h6>--}}
				{{--</div>--}}
				{{--<hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">--}}
			{{--@endforeach--}}
		{{--</div>--}}
	{{--</div>--}}
{{--</div>--}}
@endsection

@include('layouts.onlyDatatable')
<script type="text/javascript">
    $(document).on('click', '#Tab a', function(e) {
        let otherTabs = $(this).attr('data-secondary').split(',');
        for(let i= 0; i<otherTabs.length;i++) {
            let nav = $('<ul class="nav d-none" id="tmpNav"></ul>');
            nav.append('<li class="nav-item"><a href="#" data-toggle="tab" data-target="' + otherTabs[i] + '">nav</a></li>"');
            nav.find('a').tab('show');
        }
    });
</script>
<script>
    $(document).ready(function(){
        $('#autoname').keyup(function(){
            var query = $(this).val();
            if(query != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ $site.'/autocomplete' }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#List').fadeIn();
                        $('#List').html(data);
                    }
                });
            }
        });
        $(document).on('click', '#autocom', function(){
            $('#List').fadeOut();
        });
        document.getElementById("autoname").addEventListener("focus", valuetonull);
        document.getElementById("autoname").addEventListener("focusout", hideauto);

        $('#autonamem').keyup(function(){
            console.log('t');
            var query = $(this).val();
            if(query != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ $site.'/autocomplete' }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#Listm').fadeIn();
                        $('#Listm').html(data);
                    }
                });
            }
        });
        $(document).on('click', '#autocomm', function(){
            $('#Listm').fadeOut();
        });
        document.getElementById("autonamem").addEventListener("focus", valuetonull);
        document.getElementById("autonamem").addEventListener("focusout", hideauto);
    });

    function hideauto(){
        setTimeout(function(){ $('#List').fadeOut(); }, 100);
        setTimeout(function(){ $('#Listm').fadeOut(); }, 100);
    }

    function valuetonull(){
        document.getElementById('autoname').value='';
        document.getElementById('autonamem').value='';
    }
</script>
