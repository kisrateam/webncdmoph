@extends('layouts.app')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/th.min.js"></script>
@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
@include('layouts.onlyDatatable')
<style>
	html { height: 100%; }
	body { height: 100%; }

	.ck-editor__editable {
		min-height: 200px;
	}
</style>
@section('content')
	<div class="container" id="forpc">
		<div class="row">
			<div class="col-12">
				<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px;cursor:pointer;" tabindex="0" onclick="window.open('{{route('topic')}}','_self')"> กระดานสนทนา
				</span>
				<i role="presentation"  style="font-size: 16px" class="fas fa-caret-left"></i>
				<span style="font-size: 14px;font-weight: normal">{{ $data->name() }}</span>
				<div class="float-right">
					@if(Auth::check())
						<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">
							<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> บทสนทนา</a>
					@endif
				</div>
			</div>
			<div class="col-12">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
						<a href="{{ route('mytopic') }}" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
							กระดานสนทนาของฉัน</a>
					</div>
					<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
				@endif
				<hr style="border: 1px solid #B6C3C6;">
			</div>
		</div>
		<br/>
		<div class="card" style="width: 100%">
				<div class="card-header changefontcolor" style="padding-bottom: 0">
					<span style="font-size: 18px;font-weight: bold">โพสโดย : {{ $data->username() }}</span>
					@if(Auth::check()&&Auth::user()->role == 'admin')
						{{--Admin control topic--}}
						@if($data->status == 0)
						<a tabindex="0" onclick="goopen()" style="float:right;background-color: white;border-radius: 3rem;color: black;border: 1px solid #DB97A4;margin: 0 3px;" class="btn-sm"><img width="15px" height="15px" style="margin-right: 6px" src="{{$site}}/Defaultimg/close.svg">เปิดบทสนทนา</a>
						@else
						<a tabindex="0" onclick="goclose()" style="float:right;background-color: white;border-radius: 3rem;color: black;border: 1px solid #DB97A4;margin: 0 3px;" class="btn-sm"><img width="15px" height="15px" style="margin-right: 6px" src="{{$site}}/Defaultimg/topic-close.svg">ปิดบทสนทนา</a>
						@endif
					@endif
					@if(Auth::check())
						@if(Auth::user()->id == $data->userid || Auth::user()->role == 'admin')
							<a tabindex="0" onclick="godelete()" style="float:right;background-color: white;border-radius: 3rem;color: black;border: 1px solid #DB97A4;margin: 0 3px;" class="btn-sm"><img width="15px" height="15px" style="margin-right: 6px" src="{{$site}}/Defaultimg/topic-delete.svg">ลบ</a>
						@endif
					@endif
					@if(Auth::check() && Auth::user()->id == $data->userid)
						<a tabindex="0" onclick="goedit()" style="float:right;background-color: white;border-radius: 3rem;color: black;border: 1px solid #DB97A4;margin: 0 3px;" class="btn-sm"><img width="15px" height="15px" style="margin-right: 6px" src="{{$site}}/Defaultimg/topic-edit.svg">แก้ไข</a>
					@endif
					<span><a class="open-modaltopic" style="float:right;color: #DB97A4;margin-right: 10px" data-toggle="modal" data-target="#reportTopic" data-id="{{ $data->id }}">รายงานปัญหา</a></span>
					<p style="margin-bottom: 5px;color: #B6C3C6" id="date">
						<script>
                            var date = '{{ $data->created_at }}';
                            var date_f = moment(date).add(543, 'year').format('LLL');
                            document.getElementById('date').innerHTML = date_f;
						</script>
					</p>
				</div>
				<div class="card-body changefontcolor" style="padding-top: 5px">
					<p style="margin-top: 10px;font-weight: bold">{{ $data->name() }}</p>
					<p style="margin-top: 10px">{!! $data->details !!}</p>
					<br/>
				</div>
			</div>
		<br/>
	<p style="font-size: 18px;font-weight: bold">ความคิดเห็นทั้งหมด&nbsp;&nbsp;&nbsp;<img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($comments)}}</p>
	<div class="card" style="width: 100%">
		<div class="card-body changefontcolor">
			แสดงความคิดเห็น
			<form method="POST" action="{{ route('addcomment') }}">
				@csrf
				<div class="form-group row">
					<div class="col-sm-12 col-md-12">
						<input type="text" value="{{$data->id}}" name="topicid" hidden="true">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-12 col-md-12">
						<textarea placeholder="ข้อความ" class="details" name="message" id="message">
							@if($data->status == 0)
								<p style="text-align:center;"><span class="text-huge">กระทู้ถูกปิดการแสดงความคิดเห็นโดยผู้ดูแลระบบ</span></p>
							@elseif(!Auth::check())
								<p style="text-align:center;"><span class="text-huge">กรุณาเข้าสู่ระบบเพื่อตอบกระทู้</span></p>
							@endif
						</textarea>
						<script>
						  let CSRFToken = $('meta[name="csrf-token"]').attr('content');
						  let options = {
							filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
							filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
							filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
							filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
							customConfig: '{{$site}}/CKEditor/configfortopic.js'
						  };
						</script>
						<script>
              CKEDITOR.replace('message', options);
							@if(!Auth::check() || $data->status == 0)
                CKEDITOR.config.readOnly = true;
							@endif
						</script>
					</div>
				</div>
				<div class="row" style="float: right;">
					@if(Auth::check() || $data->status != 0)
						<button style="background: #508EBF;border-radius: 20px;color: white" type="submit" class="btn btn-sm">แสดงความคิดเห็น</button>
					@endif
				</div>
			</form>
		</div>
	</div>
	<br/>
	@foreach($comments as $key => $item)
		<div class="card" style="width: 100%">
			<div class="card-body changefontcolor">
				<div>
				<p style="color: #508EBF;">ความคิดเห็นที่ {{count($comments)-$loop->index}}</p>
					{!! $item->message !!}
					<span>ชื่อผู้ใช้งาน : {{$item->username}}</span>
				</div>
				<span id="comment{{$key}}">
					<script>
						var date = '{{ $item->created_at }}';
						var date_f = moment(date).add(543, 'year').format('LLL');
						document.getElementById('comment{{$key}}').innerHTML = date_f;
					</script>
				</span>
				<div class="float-right"><span><a class="open-modalcomment" style="color: #DB97A4" data-toggle="modal" data-target="#reportComment" data-id="{{ $item->id }}">รายงานปัญหา</a></span></div>
			</div>
		</div>
		<br/>
	@endforeach
</div>
@endsection
{{--modal report--}}
<script>
    $(document).ready(function() {
        $(document).on("click", ".open-modalcomment", function () {
            var ele = document.getElementsByName('reportdetails');
            for(var i=0;i<ele.length;i++)ele[i].checked = false;
            $('.continue').prop('disabled', true);
            // let url_string = "http://www.example.com/t.html?a=1&b=3&c=m2-m3-m4-m5";
            // // let url_string = window.location.href;
            // let url = new URL(url_string);
            // let a = url.searchParams.get("a");
            // let b = url.searchParams.get("b");
            // let c = url.searchParams.get("c");
            document.getElementById('forid').value = $(this).data('id');
            // document.getElementById('test').value = b;
        });
    });

    function checkProgress1() {
        if ($("input:radio[name*='reportdetails']:checked").length != 0) {
            $('.continue').prop('disabled', false);
        } else {
            $('.continue').prop('disabled', true);
        }
    }

    $(function () {
        checkProgress1();
        $("input:radio[name*='reportdetails']").on("click change", checkProgress1);
    });

    $(document).ready(function() {
        $(document).on("click", ".open-modaltopic", function () {
            var ele = document.getElementsByName('reportdetails2');
            for(var i=0;i<ele.length;i++)ele[i].checked = false;
            $('.continue2').prop('disabled', true);
            document.getElementById('forid2').value = $(this).data('id');
        });
    });

    function checkProgress2() {
        if ($("input:radio[name*='reportdetails2']:checked").length != 0) {
            $('.continue2').prop('disabled', false);
        } else {
            $('.continue2').prop('disabled', true);
        }
    }

    $(function () {
        checkProgress2();
        $("input:radio[name*='reportdetails2']").on("click change", checkProgress2);
    });
</script>
<!-- Modal comment-->
<div class="modal fade" id="reportComment" tabindex="-1" role="dialog" aria-labelledby="reportComment"
	 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form method="POST" action="{{ route('report') }}">
			@csrf
			<div class="modal-content">
				<div class="row text-center">
					<div class="col-12" style="margin-top: 20px">
						<span><h5 style="font-weight: bold">รายงานปัญหา</h5></span>
						<span><h6>การรายงานโพสต์นี้ จะไม่ระบุตัวตนของคุณ</h6></span>
					</div>
					<div class="col-12" style="padding: 0 7%">
						<hr style="border: 1px solid #B6C3C6;">
					</div>
				</div>
				<div class="row text-center">
					<input type="text" id="forid" name="id" style="display: none;">
					<input type="text" name="type" value="comments" style="display: none;">
					<div class="col-12">
						<span><h4>ทำไมคุณจึงรายงานโพสต์นี้ ?</h4></span>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report1" value="เป็นสแปม" name="reportdetails">
							<label class="custom-control-label" for="report1">เป็นสแปม</label>
						</div>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report2" value="มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด" name="reportdetails">
							<label class="custom-control-label" for="report2">มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด</label>
						</div>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report3" value="มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ" name="reportdetails">
							<label class="custom-control-label" for="report3">มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row justify-content-center">
						<button style="width: 200px" type="submit" class="btn btn-primary continue">ยืนยัน</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="reportTopic" tabindex="-1" role="dialog" aria-labelledby="reportTopic"
	 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form method="POST" action="{{ route('report') }}">
			@csrf
			<div class="modal-content">
				<div class="row text-center">
					<div class="col-12" style="margin-top: 20px">
						<span><h5 style="font-weight: bold">รายงานปัญหา</h5></span>
						<span><h6>การรายงานโพสต์นี้ จะไม่ระบุตัวตนของคุณ</h6></span>
					</div>
					<div class="col-12" style="padding: 0 7%">
						<hr style="border: 1px solid #B6C3C6;">
					</div>
				</div>
				<div class="row text-center">
					<input type="text" id="forid2" name="id" style="display: none;">
					<input type="text" name="type" value="topics" style="display: none;">
					<div class="col-12">
						<span><h4>ทำไมคุณจึงรายงานโพสต์นี้ ?</h4></span>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report4" value="เป็นสแปม" name="reportdetails2">
							<label class="custom-control-label" for="report4">เป็นสแปม</label>
						</div>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report5" value="มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด" name="reportdetails2">
							<label class="custom-control-label" for="report5">มีเนื้อหาไม่ถูกต้อง หรือมีความผิดพลาด</label>
						</div>
					</div>
					<div class=" col-11 offset-1">
						<div class="custom-control custom-radio float-left" style="margin: 10px;">
							<input type="radio" class="custom-control-input" id="report6" value="มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ" name="reportdetails2">
							<label class="custom-control-label" for="report6">มีเนื้อหาไม่สุภาพ หรือ มีคำหยาบ</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row justify-content-center">
						<button style="width: 200px" type="submit" class="btn btn-primary continue2">ยืนยัน</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@section('content2')
	<div class="container-fluid" id="formobile">
		<br/>
		<div class="card" style="width: 100%">
				<div class="card-header" style="padding-bottom: 0">
					<span style="font-size: 18px;font-weight: bold">{{$data->name()}}</span>
					@if(Auth::check()&&Auth::user()->role == 'admin')
						{{--Admin control topic--}}
						@if($data->status == 0)
						<a tabindex="0" onclick="goopen()" style="float:right;background-color: white;border-radius: 3rem;color: #01673F;" class="btn-sm">เปิดกระทู้</a>
						@else
						<a tabindex="0" onclick="goclose()" style="float:right;background-color: white;border-radius: 3rem;color: #01673F;" class="btn-sm">ปิดกระทู้</a>
						@endif
						<a style="float:right;color: white;">&nbsp;&nbsp;</a>
						<a tabindex="0" onclick="godelete()" style="float:right;background-color: white;border-radius: 3rem;color: #01673F;" class="btn-sm">ลบกระทู้</a>
					@endif
					@if(Auth::check())
						@if(Auth::user()->id == $data->userid)
							<a tabindex="0" onclick="goedit()" style="float:right;background-color: white;border-radius: 3rem;color: #01673F;" class="btn-sm">แก้ไขกระทู้</a>
						@endif
					@endif
					<p style="margin-bottom: 5px">
						โพสโดย {{$data->username}}
					</p>
				</div>
				<div class="card-body" style="padding-top: 5px">
					<div class="float-right">
						<span>โพสเมื่อ {{$data->created_at}}</span>
					</div>
					<p style="margin-top: 25px">{!! $data->details !!}</p>
					<br/>

				</div>
			</div>
		<br/>
	<p style="font-size: 18px;font-weight: bold">ความคิดเห็นทั้งหมด&nbsp;&nbsp;&nbsp;<img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($comments)}}</p>
	<div class="card" style="width: 100%">
		<div class="card-body">
			แสดงความคิดเห็น
			<form method="POST" action="{{ route('addcomment') }}">
				@csrf
				<div class="form-group row">
					<div class="col-sm-12 col-md-12">
						<input type="text" value="{{$data->id}}" name="topicid" hidden="true">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-12 col-md-12">
						<textarea placeholder="ข้อความ" class="details" name="message" id="message2">
							@if($data->status == 0)
								<p style="text-align:center;"><span class="text-huge">กระทู้ถูกปิดการแสดงความคิดเห็นโดยผู้ดูแลระบบ</span></p>
							@elseif(!Auth::check())
								<p style="text-align:center;"><span class="text-huge">กรุณาเข้าสู่ระบบเพื่อตอบกระทู้</span></p>
							@endif
						</textarea>
						<script>
              				CKEDITOR.replace('message2', options);
							@if(!Auth::check() || $data->status == 0)
                			CKEDITOR.config.readOnly = true;
							@endif
						</script>
					</div>
				</div>
				<div class="row" style="float: right;">
					@if(Auth::check() || $data->status != 0)
						<button style="background: #508EBF;border-radius: 20px;color: white" type="submit" class="btn btn-sm">ยืนยัน</button>
					@endif
				</div>
			</form>
		</div>
	</div>
	<br/>
	@foreach($comments as $item)
		<div class="card" style="width: 100%">
			<div class="card-body">
				<p style="color: #508EBF;">ความคิดเห็นที่ {{count($comments)-$loop->index}}</p>
				{!! $item->message !!}
				<span>ชื่อผู้ใช้งาน : {{$item->username}}</span>
				@if($item->role == 'admin')
					<button style="background-color: white;border-radius: 3rem;color: #01673F;" disabled>Admin</button>
				@endif
				<p>ตอบกลับเมื่อ {{$item->created_at}}</p>
			</div>
		</div>
		<br/>
	@endforeach
</div>
@endsection
<script>
  function goclose() {
    var _token = $('input[name="_token"]').val();
    var _id = $('input[name="topicid"]').val();
    Swal.fire({
	    title: 'คุณแน่ใจใช่ไหม ?',
	    text: "กระทู้จะถูกปิดแสดงความคิดเห็น!",
	    icon: 'warning',
	    showCancelButton: true,
	    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'ยกเลิก',
	    confirmButtonText: 'ใช่ ปิดเลย!'
	  }).then((result) => {
	    if (result.value) {
        $.ajax(
          {
            type: "GET",
            url: "{{$site}}/home/topic/closetopic/" + _id,
            data:{ _token:_token,id:_id },
            success: function (data) {
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'กระทู้ถูกปิดความคิดเห็นแล้ว',
                showConfirmButton: false,
                timer: 3000
              });
              location.reload();
            },
            error: function (data) {
              console.log(data);
              Swal.fire({
                icon: 'error',
                title: 'อุ๊บบ...',
                text: 'มีบางอย่างผิดพลาด !'
              })
            }
          });
	    }
	  })
  }

  function goopen() {
    var _token = $('input[name="_token"]').val();
    var _id = $('input[name="topicid"]').val();
    Swal.fire({
      title: 'คุณแน่ใจใช่ไหม ?',
      text: "เปิดแสดงความคิดเห็น!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'ยกเลิก',
      confirmButtonText: 'ใช่ เปิดเลย!'
    }).then((result) => {
      if (result.value) {
        $.ajax(
          {
            type: "GET",
            url: "{{$site}}/home/topic/opentopic/" + _id,
            data:{ _token:_token,id:_id },
            success: function (data) {
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'กระทู้ถูกเปิดความคิดเห็นแล้ว',
                showConfirmButton: false,
                timer: 3000
              });
              location.reload();
            },
            error: function (data) {
              console.log(data);
              Swal.fire({
                icon: 'error',
                title: 'อุ๊บบ...',
                text: 'มีบางอย่างผิดพลาด !'
              })
            }
          });
      }
    })
  }

  function godelete() {
    var _token = $('input[name="_token"]').val();
    var _id = $('input[name="topicid"]').val();
    Swal.fire({
      title: 'คุณแน่ใจใช่ไหม ?',
      text: "ลบกระทู้!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'ยกเลิก',
      confirmButtonText: 'ใช่ ลบเลย!'
    }).then((result) => {
      if (result.value) {
        $.ajax(
          {
            type: "GET",
            url: "{{$site}}/home/topic/deletetopic/" + _id,
            data:{ _token:_token,id:_id },
            success: function (data) {
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'กระทู้ถูกลบแล้ว',
                showConfirmButton: false,
                timer: 3000
              });
              location.href = "{{URL::to('/home/topic')}}";
            },
            error: function (data) {
              Swal.fire({
                icon: 'error',
                title: 'อุ๊บบ...',
                text: 'มีบางอย่างผิดพลาด !'
              })
            }
          });
      }
    })
  }

  function goedit() {
    var _id = $('input[name="topicid"]').val();
    location.href = '{{$site}}/home/topic/'+_id+'/edit';
  }
</script>
