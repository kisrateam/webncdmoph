<div class="tab-content container-lg" id="TabB">
    <div class="tab-pane fade show active" id="panel_d_a" role="tabpanel" aria-labelledby="a-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost = new $data;
                $datamost = $datamost->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite">
                    <?php
                    $dataall = new $data;
                    $dataall = $dataall->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->orderByDesc('topics.created_at')->paginate(10, ['*'], 'a');
                    ?>
                    @foreach($dataall as $item)
                        <div class="col-12" style="padding: 10px">
                        <span>
                        @if($item->status == 0)
                                <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                            @endif
                            <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                        <h5>{{$item->name()}}</h5>
                        <h6>โพสโดย {{$item->users->name}}</h6>
                        </a>
                        </span>
                            <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                        <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                        <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                        </span>
                            <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @endforeach
                    @if($dataall->hasPages())
                        <div class="text-center">
                            <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore" id="loadmore">โหลดเพิ่ม</button>
                        </div>
                    @endif
                    <div id="spinner" style="display: none">
                        <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                    </div>
                    <div id="dataall">
                        {{ $dataall->links() }}
                    </div>
                    <script>
                        $(function() {
                            let $posts = $(".infinite");
                            let $ul = $("#dataall ul.pagination");
                            let $link = $ul.find("a[rel='next']").attr("href");
                            $ul.hide();
                            $(".loadmore").click(function() {
                                if(typeof $link !== 'undefined'){
                                    $ul.remove();
                                    document.getElementById('loadmore').remove();
                                    document.getElementById('spinner').style.display = "block";
                                    setTimeout(function() {
                                        document.getElementById('spinner').remove();
                                        $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                            $posts.append(
                                                $(response).find(".infinite").html()
                                            );
                                        });
                                    }, 1000);
                                    setTimeout(() => {
                                        checkcolor();
                                    }, 1500);
                                }else{
                                    document.getElementById('loadmore').remove();
                                    document.getElementById('spinner').style.display = "block";
                                    setTimeout(function() {
                                        document.getElementById('spinner').remove();
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'ข้อมูลหมดแล้ว'
                                        });
                                    }, 1000);
                                }
                            });
                        });
                    </script>
                    @if($dataall->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_1" role="tabpanel" aria-labelledby="1-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost1 = new $data;
                $datamost1 = $datamost1->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',1)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost1->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':''}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost1->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row" >
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite1">
                    <?php
                    $dataall1 = new $data;
                    $dataall1 = $dataall1->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',1)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a1');
                    ?>
                    @if($dataall1->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall1 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall1->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore1" id="loadmore1">โหลดเพิ่ม</button>
                            </div>
                        @endif
                        <div id="spinner1" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall1">
                            {{ $dataall1->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite1");
                                let $ul = $("#dataall1 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore1").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore1').remove();
                                        document.getElementById('spinner1').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner1').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite1").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore1').remove();
                                        document.getElementById('spinner1').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner1').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_2" role="tabpanel" aria-labelledby="2-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost2 = new $data;
                $datamost2 = $datamost2->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',2)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost2->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost2->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">            <div class="infinite2">
                    <?php
                    $dataall2 = new $data;
                    $dataall2 = $dataall2->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',2)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a2');
                    ?>
                    @if($dataall2->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall2 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall2->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore2" id="loadmore2">โหลดเพิ่ม</button>
                            </div>
                        @endif

                        <div id="spinner2" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall2">
                            {{ $dataall2->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite2");
                                let $ul = $("#dataall2 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore2").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore2').remove();
                                        document.getElementById('spinner2').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner2').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite2").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore2').remove();
                                        document.getElementById('spinner2').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner2').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_3" role="tabpanel" aria-labelledby="3-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost3 = new $data;
                $datamost3 = $datamost3->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',3)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost3->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost3->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite3">
                    <?php
                    $dataall3 = new $data;
                    $dataall3 = $dataall3->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',3)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a3');
                    ?>
                    @if($dataall3->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall3 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall3->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore3" id="loadmore3">โหลดเพิ่ม</button>
                            </div>
                        @endif

                        <div id="spinner3" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall3">
                            {{ $dataall3->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite3");
                                let $ul = $("#dataall3 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore3").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore3').remove();
                                        document.getElementById('spinner3').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner3').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite3").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore3').remove();
                                        document.getElementById('spinner3').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner3').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_4" role="tabpanel" aria-labelledby="4-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost4 = new $data;
                $datamost4 = $datamost4->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',4)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost4->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost4->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite4">
                    <?php
                    $dataall4 = new $data;
                    $dataall4 = $dataall4->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',4)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a4');
                    ?>
                    @if($dataall4->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall4 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall4->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore4" id="loadmore4">โหลดเพิ่ม</button>
                            </div>
                        @endif

                        <div id="spinner4" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall4">
                            {{ $dataall4->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite4");
                                let $ul = $("#dataall4 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore4").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore4').remove();
                                        document.getElementById('spinner4').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner4').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite4").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore4').remove();
                                        document.getElementById('spinner4').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner4').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_5" role="tabpanel" aria-labelledby="5-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost5 = new $data;
                $datamost5 = $datamost5->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',5)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost5->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost5->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite5">
                    <?php
                    $dataall5 = new $data;
                    $dataall5 = $dataall5->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',5)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a5');
                    ?>
                    @if($dataall5->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall5 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src={{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall5->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore5" id="loadmore5">โหลดเพิ่ม</button>
                            </div>
                        @endif

                        <div id="spinner5" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall5">
                            {{ $dataall5->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite5");
                                let $ul = $("#dataall5 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore5").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore5').remove();
                                        document.getElementById('spinner5').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner5').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite5").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore5').remove();
                                        document.getElementById('spinner5').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner5').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="panel_d_6" role="tabpanel" aria-labelledby="6-tab">
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนายอดนิยม</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <?php
                $datamost6 = new $data;
                $datamost6 = $datamost6->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',6)->orderBy('hitcount','DESC');
                ?>
                @foreach($datamost6->get()->take(6) as $item)
                    <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                        <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                        <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endforeach
                @if($datamost6->get()->count() == 0)
                    <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                    </div>
                    <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="margin-bottom: 20px">
                <span style="font-size: 20px;font-weight: bold;">บทสนทนาทั้งหมด</span>
            </div>
            <div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff;">
                <div class="infinite6">
                    <?php
                    $dataall6 = new $data;
                    $dataall6 = $dataall6->whereIn('privacy',(Auth::check()&&(Auth::User()->role=='member'||Auth::User()->role=='admin')?['1','2','3']:['1']))->where('category',6)->orderByDesc('topics.created_at')->paginate(5, ['*'], 'a6');
                    ?>
                    @if($dataall6->count() == 0)
                        <div class="col-12 text-center" style="padding: 10px">
                    <span>
                    <h5>ไม่พบบทสนทนา</h5>
                    </span>
                        </div>
                        <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                    @else
                        @foreach($dataall6 as $item)
                            <div class="col-12" style="padding: 10px">
                    <span>
                    @if($item->status == 0)
                            <img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
                        @endif
                        <a class="changefontcolortopic" style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
                    <h5>{{$item->name()}}</h5>
                    <h6>โพสโดย {{$item->users->name}}</h6>
                    </a>
                    </span>
                                <span class="changefontcolortopic" style="float:right;color: black;" disabled>
                    <span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
                    <img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
                    </span>
                                <h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':'#c3c3c3'}};">{{$item->timeago()}}</h6>
                            </div>
                            <hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
                        @endforeach
                        @if($dataall6->hasPages())
                            <div class="text-center">
                                <button style="border-radius: 3rem;" class="btn btn-outline-dark-green btn-sm loadmore6" id="loadmore6">โหลดเพิ่ม</button>
                            </div>
                        @endif

                        <div id="spinner6" style="display: none">
                            <img class="center-block" src="{{$site}}/Defaultimg/loading.gif" alt="กำลังโหลด..." />
                        </div>
                        <div id="dataall6">
                            {{ $dataall6->links() }}
                        </div>
                        <script>
                            $(function() {
                                let $posts = $(".infinite6");
                                let $ul = $("#dataall6 ul.pagination");
                                let $link = $ul.find("a[rel='next']").attr("href");
                                $ul.hide();
                                $(".loadmore6").click(function() {
                                    if(typeof $link !== 'undefined'){
                                        $ul.remove();
                                        document.getElementById('loadmore6').remove();
                                        document.getElementById('spinner6').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner6').remove();
                                            $.get($ul.find("a[rel='next']").attr("href"), function(response) {
                                                $posts.append(
                                                    $(response).find(".infinite6").html()
                                                );
                                            });
                                        }, 1000);
                                        setTimeout(() => {
                                            checkcolor();
                                        }, 1500);
                                    }else{
                                        document.getElementById('loadmore6').remove();
                                        document.getElementById('spinner6').style.display = "block";
                                        setTimeout(function() {
                                            document.getElementById('spinner6').remove();
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'ข้อมูลหมดแล้ว'
                                            });
                                        }, 1000);
                                    }
                                });
                            });
                        </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.onlyDatatable')
<script>
    function checkcolor() {
        const stay = localStorage.getItem('checktheme');
        if(typeof(stay) != "undefined" && stay == '0'){
            changetoblack();
        }else if(typeof(stay) != "undefined" && stay == '2'){
            changetoblind();
        }else{
            changetodefault();
        }
    }
</script>
