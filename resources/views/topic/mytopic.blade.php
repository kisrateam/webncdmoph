@extends('layouts.app')

@if(Session::has('success'))
	<div class="alert alert-success">
		<strong>Success: </strong>{{ Session::get('success') }}
	</div>
@endif
@if(Session::has('error'))
	<div class="alert alert-error">
		<strong>Error: </strong>{{ Session::get('error') }}
	</div>
@endif
<style>
	html { height: 100%;}
	body { height: 100%;}

	#dtBasicExample thead {
		display: none;
	}
	table.dataTable {
		/*border-spacing: 5px !important;*/
	}
	#logoquestion{
		filter: invert(29%) sepia(23%) saturate(2325%) hue-rotate(114deg) brightness(94%) contrast(99%);
	}
</style>
<?php
$timetocount = '-12 hours';
?>
@section('content')
	<div class="container" id="forpc">
		<div class="row">
			<div class="col-12">
				<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px;cursor:pointer;" tabindex="0" onclick="window.open('{{route('topic')}}','_self')"> กระดานสนทนา
				</span>
				<i role="presentation"  style="font-size: 16px" class="fas fa-caret-left"></i>
				<span style="font-size: 14px;font-weight: normal">กระดานสนทนาของฉัน</span>
				<div class="float-right">
					@if(Auth::check())
						<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">
							<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> บทสนทนา</a>
					@endif
				</div>
			</div>
			<div class="col-12">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
						<a href="" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
							กระดานสนทนาของฉัน</a>
					</div>
				@endif
				<p style="color: #c3c3c3;">สถานะ : {{\Auth::user()->role}}</p>
			</div>
		</div>
		<hr style="border: 1px solid #B6C3C6;">
		<br/>

		<div class="row">
			<div class="col-12">
				<span style="font-size: 24px;font-weight: bold;">กระดานสนทนาของฉัน {{$counttopic}} ข้อ</span>
			</div>
			<div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px;background-color: #ffffff">
				@foreach($data as $item)
					<div class="col-12" style="padding: 10px">
				<span>
					@if($item->status == 0)
						<img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
					@endif
					<a style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
					<h3>{{$item->name}}</h3>
				</a>
			</span>
						<span style="float:right;color: black;" disabled>
					<span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
					<img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
				</span>
						<h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':''}};">{{$item->timeago()}}</h6>
					</div>
					<hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
				@endforeach
				@if($data->count() == 0)
				<div class="col-12 text-center" style="padding: 10px">
				<span>
				<h3>ไม่พบบทสนทนา</h3>
				</span>
				</div>
				<hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
				@endif
			</div>
		</div>
	</div>
@endsection
@section('content2')
	<div class="container-fluid" id="formobile">
		<div class="row">
			<div class="col-12">
				<img id="logoquestion" src="{{$site}}/Defaultimg/question.svg">
				<span style="font-size: 24px;font-weight: bold;padding-left: 10px"> กระดานสนทนา
			</span>
				<div class="float-right">
					@if(Auth::check())
						<a href="{{ route('showaddtopic') }}" style="background-color: #2EA1A0;color:white;border-radius: 20px" class="btn">
							<i role="presentation"  class="fas fa-plus" style="size: 20px 20px"></i> บทสนทนา</a>
					@endif
				</div>
			</div>
			<div class="col-12">
				<br/>
				@if(Auth::check())
					<span>ชื่อผู้ใช้งาน : {{\Auth::user()->name}}</span>
					<div class="float-right">
						<a href="" style="border: 1px solid #DB97A4;
									box-sizing: border-box;border-radius: 20px;" class="btn">
							คำถามของฉัน</a>
					</div>
				@endif
				<p>สถานะ : {{\Auth::user()->role}}</p>
			</div>
		</div>
		<hr style="border: 1px solid #B6C3C6;">
		<br/>

		<div class="row">
			<div class="col-12">
				<span style="font-size: 24px;font-weight: bold;">คำถามของฉัน {{$counttopic}} คำถาม</span>
			</div>
			<div class="col-12" style="border: 1px solid #B6C3C6;padding-bottom: 20px;margin-bottom: 20px;border-radius: 10px">
				@foreach($data as $item)
					<div class="col-12" style="padding: 10px">
				<span>
				@if($item->status == 0)
						<img src="{{$site}}/Defaultimg/close.svg" style="float: right;border-radius: 3rem">
					@endif
					<a style="color: black;" href="{{$site}}/home/topic/{{$item->id}}" target="_blank">
					<h3>{{$item->name}}</h3>
				</a>
			</span>
						<span style="float:right;color: black;" disabled>
					<span>จำนวนผู้เข้าชม {{$item->hitcount}}</span>
					<img src="{{$site}}/Defaultimg/commentcount.svg"> {{count($item->comments)}}
				</span>
						<h6 style="position: absolute;color: {{$item->created_at > \Carbon\Carbon::parse($timetocount)?'#508EBF':''}};">{{$item->timeago()}}</h6>
					</div>
					<hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
				@endforeach
				@if($data->count() == 0)
					<div class="col-12 text-center" style="padding: 10px">
				<span>
				<h3>ไม่พบบทสนทนา</h3>
				</span>
					</div>
					<hr style="border: 1px solid #B6C3C6;margin: 20px 5px 0 5px">
				@endif
			</div>
		</div>
	</div>
@endsection
