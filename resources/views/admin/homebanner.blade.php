@extends('layouts.appadmin')
<style>
	html { height: 100%; width: 100%;}
	body { height: 100%; width: 100%;}
</style>
<?php
$banner1 = \DB::table('homebanner')->where('url','1')->first();
if($banner1)$path1 = json_decode($banner1->path);
else $path1 = null;
$banner2 = \DB::table('homebanner')->where('url','2')->first();
if($banner2)$path2 = json_decode($banner2->path);
else $path2 = null;
?>
@section('content')
<div class="col">
	<span class="font-weight-bold">การจัดการแบนเนอร์</span>
	<div class="float-right">
		<a style="border-radius: 40px;padding: 5px 30px;background-color: #203F54" class="btn" tabindex="0" onclick="confirmdelete('{{$banner1 != null ? $banner1->id:null}}');">ลบ</a>
		<a style="border-radius: 40px;padding: 5px 30px;background-color: #203F54" class="btn" data-toggle="modal" data-target="#createModal">เปลี่ยนข้อมูล</a>
	</div>
</div>
<br>
<img style="cursor: pointer;" tabindex="0" onclick="window.open('{{$path1 != null ? $path1[1]:''}}');" src="{{$path1 != null && $path1[0] != null ? $site.'/uploads/'.$path1[0]:$site.'/Defaultimg/noimg.jpg'}}" width="100%" height="200px" alt="banner1">
<div class="col">
	<br>
	<div class="float-right" style="width: 70%;font-size: 16px">
		<label style="float: left;" for="url">Link Url: </label>
		<span style="overflow: hidden;display: block;padding: 0 4px 0 10px;"><input style="width: 100%;" type="text" value="{{$path1 != null ? $path1[1]:''}}" readonly></span>
	</div>
	<span style="font-size: 14px">ภาพแบนเนอร์*</span>
	<p style="color: #01673F;font-size: 12px;margin: 0">*รองรับ JPEG, PNG</p>
	<p style="color: #01673F;font-size: 12px">*ขนาดไฟล์ไม่เกิน 5335 * 680 px(19:2.4)</p>
</div>

{{--modal-banner--}}
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" id="createModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="createModalLabel">เปลี่ยนข้อมูล</h4>
				<button type="button" name="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form method="POST" action="{{ route('banner') }}" enctype="multipart/form-data">
					@csrf
					<input value="{{$banner1 != null ? $banner1->id:''}}" name="id" hidden>
					<input value="1" name="category" hidden>
					<div class="form-group row justify-content-center">
						<div class="col-md-10 text-center">
							<label for="url">Link Url</label>
							<div class="input-group mb-3 input-icons">
								<input style="border-radius: 3rem;border-color: #01673F" type="text" class="form-control input-field" placeholder="Link Url" aria-label="Link Url" id="url" name="url" autocomplete="off" value="{{$path1 != null ? $path1[1]:''}}">
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-center">
						<div class="col-md-10 text-center">
							<label for="img">รูปแบนเนอร์</label>
							<div class="input-group mb-3 input-icons">
								<label for="img" class="form-control" style="border-radius: 3rem;border-color: #01673F">เพิ่มรูปภาพ</label>
								<input style="border-radius: 3rem;border-color: #01673F;display: none" type="file" class="form-control input-field" placeholder="รูปภาพ" aria-label="รูปภาพ" id="img" name="img" autocomplete="off">
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-center">
						<div class="col-md-8">
							<button style="border-radius: 3rem;background-color:#01673F ;color: white;" type="submit" class="btn btn-block">
								{{ __('เปลี่ยนข้อมูล') }}
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

<div class="col">
	<span class="font-weight-bold">การจัดการแบนเนอร์ (ภาษาอังกฤษ)</span>
	<div class="float-right">
		<a style="border-radius: 40px;padding: 5px 30px;background-color: #203F54" class="btn" tabindex="0" onclick="confirmdelete('{{$banner2 != null ? $banner2->id:null}}');">ลบ</a>
		<a style="border-radius: 40px;padding: 5px 30px;background-color: #203F54" class="btn" data-toggle="modal" data-target="#createModal1">เปลี่ยนข้อมูล</a>
	</div>
</div>
<br>
<img style="cursor: pointer;" tabindex="0" onclick="window.open('{{$path2 != null ? $path2[1]:''}}');" src="{{$path2 != null && $path2[0] != null ? $site.'/uploads/'.$path2[0]:$site.'/Defaultimg/noimg.jpg'}}" width="100%" height="200px">
<div class="col">
	<br>
	<div class="float-right" style="width: 70%;font-size: 16px">
		<label style="float: left;" for="url">Link Url: </label>
		<span style="overflow: hidden;display: block;padding: 0 4px 0 10px;"><input style="width: 100%;" type="text" value="{{$path2 != null ? $path2[1]:''}}" readonly></span>
	</div>
	<span style="font-size: 14px">ภาพแบนเนอร์*</span>
	<p style="color: #01673F;font-size: 12px;margin: 0">*รองรับ JPEG, PNG</p>
	<p style="color: #01673F;font-size: 12px">*ขนาดไฟล์ไม่เกิน 5335 * 680 px(19:2.4)</p>
</div>

{{--modal-banner--}}
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createModal1Label" id="createModal1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="createModal1Label">เปลี่ยนข้อมูล</h4>
				<button type="button" name="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form method="POST" action="{{ route('banner') }}" enctype="multipart/form-data">
					@csrf
					<input value="{{$banner2 != null ? $banner2->id:''}}" name="id" hidden>
					<input value="2" name="category" hidden>
					<div class="form-group row justify-content-center">
						<div class="col-md-10 text-center">
							<label for="url2">Link Url</label>
							<div class="input-group mb-3 input-icons">
								<input style="border-radius: 3rem;border-color: #01673F" type="text" class="form-control input-field" placeholder="Link Url" aria-label="Link Url" id="url2" name="url" autocomplete="off" value="{{$path2 != null ? $path2[1]:''}}">
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-center">
						<div class="col-md-10 text-center">
							<label for="img">รูปแบนเนอร์</label>
							<div class="input-group mb-3 input-icons">
								<label for="img2" class="form-control" style="border-radius: 3rem;border-color: #01673F">เพิ่มรูปภาพ</label>
								<input style="border-radius: 3rem;border-color: #01673F;display: none" type="file" class="form-control input-field" placeholder="รูปภาพ" aria-label="รูปภาพ" id="img2" name="img" autocomplete="off">
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-center">
						<div class="col-md-8">
							<button style="border-radius: 3rem;background-color:#01673F ;color: white;" type="submit" class="btn btn-block">
								{{ __('เปลี่ยนข้อมูล') }}
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    function confirmdelete(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "รูปและลิ้งจะหายไป !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleteimg(id);
            }
        })
    };

    function deleteimg(id) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
			{
				type: "DELETE",
				url: "{{$site}}/admin/banner/"+id,
				data:{ _token:_token  },
				success: function (data) {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'ข้อมูลถูกลบแล้ว',
						showConfirmButton: false,
						timer: 3000
					});
					setTimeout(function(){ window.location.reload(); }, 1000);
				},
				error: function (data) {
					Swal.fire({
						icon: 'error',
						title: data.error,
						text: 'มีบางอย่างผิดพลาด !'
					})
				}
			});
        }
    };
</script>
@endsection
