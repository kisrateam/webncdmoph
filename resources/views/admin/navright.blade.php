<style>
	.topnav {
		background-color: #508EBF;
	}
	#logoimg{
		position: absolute;
		left: 100px;
		top: 5px;
	}
	#bg{
		position: absolute;
		left: 90px;
		width: 78px;
		height: 150px;
		background-color: white;
		background-size: 300px 300px;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
		border-radius: 0px 0px 40px 40px;
	}
	.dropdown-content {
		display: none;
		position: absolute;
		background-color: white;
		min-width: 160px;
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
		right: -5px;
		top: 70px;
		z-index: 1;
		border: 1px solid #508EBF;
		border-radius: 5px;
	}
	.dropdown-content:before {
		content: "";
		position: absolute;
		top: -30px;
		right: 10%;
		border: 15px solid transparent;
		border-bottom-color: #508EBF;
	}
	.dropdown-content a {
		padding: 20px 16px;
		text-decoration: none;
	}
	.dropdown-content a:hover {
		color: white;
		background-color: #508EBF;
	}
</style>
<nav class="navbar navbar-expand-lg topnav">
	<div class="collapse navbar-collapse" id="navbarSupportedContent" style="z-index: 1;">
		<div id="bg"></div>
		<ul>
			<img id="logoimg" src="{{$site}}/Defaultimg/Logo.png">
		</ul>
		<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
			<li class="nav-item dropdown">
				<a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					{{--{{ Auth::user()->name }} <span class="caret"></span>--}}
					<img src="{{$site}}/Defaultimg/avatar-login.svg" style="margin: 0;">
				</a>
				<div class="dropdown-menu dropdown-content dropdown-menu-right" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="{{$site}}">
						{{ __('หน้าผู้ใช้งาน') }}
					</a>
					<a class="dropdown-item" href="{{ route('admin')}}">
						{{ __('หน้าแอดมิน') }}
					</a>
					<a class="dropdown-item" href="{{ route('logout') }}"
					   tabindex="0" onclick="event.preventDefault();
						   document.getElementById('logout-form').submit();">
						{{ __('ออกจากระบบ') }}
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</li>
		</ul>
	</div>
</nav>
