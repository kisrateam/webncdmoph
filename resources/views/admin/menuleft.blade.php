<style>
	.nav-right{
		background: white;
		box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.25);
	}
	.nav-link.active {
		color:#01673F;
		background-color: #E5E5E5;
	}
	a img{
		margin: 4px 10px 4px 0;
		width: 36px;
		height: 30px;
	}
	img.active{
		filter: invert(29%) sepia(23%) saturate(2325%) hue-rotate(114deg) brightness(94%) contrast(99%);
	}
	.submenu{
		padding: 10px 0 10px 63px;
	}

	[data-toggle="collapse"]:after {
		display: inline-block;
		font-family: "Font Awesome 5 Free", serif;
		font-weight: 900;
		font-size: 25px;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		content: "\f0d7";
		transform: rotate(180deg) ;
		transition: all linear 0.25s;
		float: right;
		position: relative;
		top: 3px;
	}
	[data-toggle="collapse"].collapsed:after {
		transform: rotate(0deg) ;
	}
</style>
<div class="list-group nav-right" id="accordion">
	<a data-toggle="collapse" data-target="#homeadmin" aria-expanded="true" class="nav-link collapsed" aria-controls="homeadmin"><img src="{{$site}}/Defaultimg/1.svg" alt="หน้าหลัก">หน้าหลัก</a>
	<p class="sub-menu collapse {{ Route::current()->getName() == 'homebanner' || Route::current()->getName() == 'imgslide.index'
	? "show" : "" }}" id="homeadmin" data-parent="#accordion">
		<a href="{{ route('homebanner') }}" class="nav-link submenu {{ Route::current()->getName() == 'homebanner' ? "active" : "" }}">แบนเนอร์</a>
		<a href="{{ route('imgslide.index') }}" class="nav-link submenu {{ Route::current()->getName() == 'imgslide.index' ? "active" : "" }}">พื้นที่นำเสนอ</a>
	</p>
	<a href="{{ route('team.index') }}" class="nav-link {{ Route::current()->getName() == 'team.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/2.svg" alt="หน้าหลัก">คณะกรรมการ</a>
	<a href="{{ route('contents.index') }}" class="nav-link {{ Route::current()->getName() == 'contents.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/3.svg" alt="แผนการป้องกันและควบคุมโรคไม่ติดต่อ">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</a>
	<a href="{{ route('result.index') }}" class="nav-link {{ Route::current()->getName() == 'result.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/4.svg" alt="ผลการดำเนินงาน">ผลการดำเนินงาน</a>
	<a href="{{ route('best.index') }}" class="nav-link {{ Route::current()->getName() == 'best.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/best.svg" alt="Best practice">Best practice</a>
	<a href="{{ route('contentnew.index') }}" class="nav-link {{ Route::current()->getName() == 'contentnew.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/6.svg" alt="ข่าวสาร">ข่าวสาร</a>
	<a href="{{ route('contentact.index') }}" class="nav-link {{ Route::current()->getName() == 'contentact.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/7.svg" alt="สื่อเผยแพร่">สื่อเผยแพร่</a>
	<a href="{{ route('topic.index') }}" class="nav-link {{ Route::current()->getName() == 'topic.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/question.svg" alt="กระดานสนทนา">กระดานสนทนา</a>
	<a href="{{ route('link.index') }}" class="nav-link {{ Route::current()->getName() == 'link.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/link.svg" alt="หน่วยงานที่เกี่ยวข้อง">หน่วยงานที่เกี่ยวข้อง</a>
	<a data-toggle="collapse" data-target="#eng" aria-expanded="true" class="nav-link collapsed" aria-controls="eng"><img src="{{$site}}/Defaultimg/8.svg" alt="ภาษาอังกฤษ">ภาษาอังกฤษ</a>
	<p class="sub-menu collapse {{ Route::current()->getName() == 'eng1' || Route::current()->getName() == 'eng2' || Route::current()->getName() == 'eng3' || Route::current()->getName() == 'eng4'
	? "show" : "" }}" id="eng" data-parent="#accordion">
		<a href="{{ route('eng1') }}" class="nav-link submenu {{ Route::current()->getName() == 'eng1' ? "active" : "" }}">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ.2560-2564)</a>
		<a href="{{ route('eng2') }}" class="nav-link submenu {{ Route::current()->getName() == 'eng2' ? "active" : "" }}">คณะกรรมการ</a>
		<a href="{{ route('eng3') }}" class="nav-link submenu {{ Route::current()->getName() == 'eng3' ? "active" : "" }}">ผลการดำเนินงาน</a>
		<a href="{{ route('eng4') }}" class="nav-link submenu {{ Route::current()->getName() == 'eng4' ? "active" : "" }}">best pratice</a>
	</p>
	<a href="{{ route('userrole') }}" class="nav-link {{ Route::current()->getName() == 'userrole' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/login-email.svg" alt="สิทธิ์การเข้าถึง">สิทธิ์การเข้าถึง</a>
	{{--<a href="{{ route('menuheader.index') }}" class="nav-link {{ Route::current()->getName() == 'menuheader.index' ? "active" : "" }}"><img src="{{$site}}/Defaultimg/link.svg">เมนู</a>--}}
</div>
