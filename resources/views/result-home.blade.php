<?php
$resultdata = \App\result::where('active','1')->first();
?>
@if($resultdata != null)
<div id="forpc" class="row">
	<div class="col-12" style="padding: 0">
		<img style="width: 50px;
	    height: 50px;" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
		<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px">  ผลการดำเนินงาน</span>
		<div class="float-right">
			<button class="seeallbutton" onclick="gotoresult();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
			</button>
		</div>
		<hr style="border: 1px solid #E65093;">
	</div>
	<div class="col-12">
		<p role="heading" class="font-size-change" style="color: #508EBF;font-weight: bold;font-size: 24px">{{$resultdata->name}}</p>
	</div>
	<div class="col-12" style="width: 100%;height: auto;">
		<img src="{{$site}}/uploads/{{$resultdata->img->path}}" width="100%" height="auto" alt="{{$resultdata->img->name}}">
	</div>
</div>
<div id="formobile">
	<div class="row text-center">
		<div class="col-12">
			<img style="width: 50px;
	    height: 50px;" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
		</div>
		<div class="col-12">
			<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">ผลการดำเนินงาน</span>
			<hr style="border: 1px solid #E65093;margin-top: 0">
		</div>
		<div class="col-12">
			<p style="color: #508EBF;font-weight: bold;font-size: 24px">{{$resultdata->name}}</p>
		</div>
	<div class="col-12" style="width: 100%;height: auto;">
		<img src="{{$site}}/uploads/{{$resultdata->img->path}}" width="100%" height="auto" alt="{{$resultdata->img->name}}">
	</div>
		<div class="col-12">
		<br/>
			<button class="seeallbutton" onclick="gotoresult();" style="
					background: #B6C3C6;
					border-radius: 20px;
					border: none;
					padding: 5px 15px;
					color: white;">ดูทั้งหมด
			</button>
		</div>
	</div>
</div>
@else
<div id="forpc" class="row">
	<div class="col-12" style="padding: 0">
		<img style="width: 50px;
	    height: 50px;" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
		<span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px">  ผลการดำเนินงาน</span>
		<div class="float-right">
			<button class="seeallbutton" onclick="gotoresult();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
			</button>
		</div>
		<hr style="border: 1px solid #E65093;">
	</div>
	<div class="col-12">
		<p style="color: #508EBF;font-weight: bold;font-size: 24px">ไม่พบข้อมูล</p>
	</div>
	<div class="col-12" style="width: 100%;height: auto;">
		<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="60%">
	</div>
</div>
<div id="formobile">
	<div class="row text-center">
		<div class="col-12">
			<img style="width: 50px;
 		 height: 50px;" src="{{$site}}/Defaultimg/ผลดำเนินงาน.png"/>
		</div>
		<div class="col-12">
			<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">ผลการดำเนินงาน</span>
			<hr style="border: 1px solid #E65093;margin-top: 0">
		</div>
		<div class="col-12">
			<p style="color: #508EBF;font-weight: bold;font-size: 24px">ไม่พบข้อมูล</p>
		</div>
	<div class="col-12" style="width: 100%;height: auto;">
		<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
	</div>
		<div class="col-12">
		<br/>
			<button class="seeallbutton" onclick="gotoresult();" style="
					background: #B6C3C6;
					border-radius: 20px;
					border: none;
					padding: 5px 15px;
					color: white;">ดูทั้งหมด
			</button>
		</div>
	</div>
</div>
@endif
<br>
<script type="text/javascript">
	function gotoresult() {
		window.location.href = "{{URL::to(route('result'))}}";
  }
</script>
