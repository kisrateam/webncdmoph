<style>
	#btn-custom{
		border: none;
		border-radius: 10px;
		color: #B6C3C6;
		background-color: transparent;
		box-shadow: none;
		outline: none;
	}

	#btn-custom.active{
		border: 1px solid #508EBF;
		color: #508EBF;
	}

	.content div {
		max-width: 300px;
		display: -webkit-box;
		overflow: hidden;
		text-overflow: ellipsis;
		-webkit-box-orient: vertical;
		-webkit-line-clamp: 3;
		box-orient: vertical;
		line-clamp: 3;
		font-size: 13px;
	}

	@media screen and (max-width : 766px)
	{
		.content div{
			-webkit-box-orient: vertical;
			-webkit-line-clamp: 4;
			box-orient: vertical;
			line-clamp: 3;
			font-weight: bold;
			font-size: 20px;
		}
	}
	.content{
		width:100%;
		height: auto;
		border-radius:10px;
	}
	.image {
		position:relative;
		cursor: pointer;
	}
	.image img {
		vertical-align:middle;
	}
	.image:after, .image:before {
		position:absolute;
		opacity:0;
		height: 0%;
		transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}
	.image:after {
		content:'\A';
		width:100%; height:100%;
		top:0; left:0;
		background:rgba(0,0,0,0.8);
	}
	.image:before {
		content: attr(data-content);
		width:100%;
		color:#fff;
		z-index:1;
		bottom:0;
		padding:4px 10px;
		text-align:center;
		background: url('{{ asset('/Defaultimg/readmore.svg') }}') no-repeat center;
		box-sizing:border-box;
		-moz-box-sizing:border-box;
	}
	.date {
		position:relative;
		cursor: pointer;
	}
	.date img {
		vertical-align:top;
	}
	.date:after, .date:before {
		position:absolute;
		opacity:0;
		height: 0;
		transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}

	.date:after {
		content:'\A';
		width:100%;
		height:25%;
		top:0;
		left:0;
		background:rgba(0,0,0,0.7);
	}
	.date:before {
		content: attr(data-content);
		white-space: pre;
		font-size: 40px;
		width:100%;
		color:#fff;
		z-index:1;
		bottom:0;
		padding:4px 10px;
		text-align:center;
		background:rgba(0,0,0,0.8);
		box-sizing:border-box;
		-moz-box-sizing:border-box;
		overflow: hidden;
	}

	.image:hover:after, .image:hover:before {
		opacity: 0.7;
		height: 100%;
		animation-name: fadeInOpacity;
		animation-iteration-count: 1;
		animation-timing-function: ease-in;
		animation-duration: 0.3s;
	}

	.date:hover:after{
		opacity: 0.9;
		height: 20%;
	}

	.date:hover:before{
		opacity: 0.8;
		height: 80%;
	}

	@keyframes fadeInOpacity {
		0% {
			opacity: 0;
			height: 0;
		}
		100% {
			opacity: 1;
			height: 100%;
		}
	}
	.card-title{
		font-size: 16px;
		font-weight: bold;
	}
	.card-text{
		font-size: 12px;
	}
	.changefontcolor{
		background-color: rgba(0,0,0,0.01);
	}
	div#forpc ul li button:hover{
		background-color: #c3e9f6;
		border-radius: 10px;
		color: white;
	}
</style>
<?php
$arrayforact = array(
	"แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)",
	"ผลการดำเนินงาน/Best practice",
    "สถานการณ์ / ข้อมูล",
    "คู่มือ แนวทาง สื่อ");
$arrayfornew = array(
	"ข่าวกิจกรรม",
	"ข่าวประชาสัมพันธ์");
$data = new \App\contentactivity();
$dataact = $data->all()->whereIn('category',$arrayforact)->sortByDesc('updated_at')->groupBy('category');
$datanew = $data->all()->whereIn('category',$arrayfornew)->sortByDesc('updated_at')->groupBy('category');
?>
<title>หน้าแรก</title>
{{--contentnew--}}
<div id="forpc">
	<div class="row">
		<div class="col-12">
			<img alt="ข่าวสาร" style="width: 50px;
	    	height: 50px;" src="{{$site}}/Defaultimg/ข่าวสาร.png"/>
			<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px">  ข่าวสาร</span>
			<div class="float-right">
				<button class="seeallbutton" onclick="gotonew();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
				</button>
			</div>
			<hr style="border: 1px solid #E65093;">
		</div>
	</div>
	<div>
	<ul class="nav text-center" role="tablist">
		@foreach($datanew as $key => $item)
			<li style="padding: 0;" role="presentation" class="col-2 font-size-change">
				<button style="white-space:nowrap;" class="{{$loop->first ? 'active' : ''}}" id="btn-custom" href="#tab-{{ $key }}" aria-controls="#tab-{{ $key }}" role="tab" data-toggle="tab">
					{{$key}}
				</button>
			</li>
		@endforeach
	</ul>
	</div>
	<div class="tab-content" style="padding-top: 20px">
        <?php $i = 0 ?>
		@foreach($datanew as $key => $items)
			<div id="tab-{{ $key }}" class="tab-pane {{$loop->first ? 'active':''}}">
				<div class="panel-body">
					<div class="row">
						@foreach($items->take(4) as $index => $item)
							<div class="col-md-6">
								<div onclick="gotomedia('{{$item->id}}');" class="card" style="margin: 20px;box-shadow: none;border: none;background-color: transparent">
									<div class="row no-gutters">
										<div class="col-5" role="button">
											@if($item->img === null)
												<div class="image">
													<img alt="ข่าวสาร" src="{{$site}}/Defaultimg/noimg.jpg" alt="no-image" width="100%" height="100%">
												</div>
											@else
												<div class="image">
													<img alt="ข่าวสาร" src="{{$site}}/uploads/{{$item->img->path}}" alt="uploads" height="auto" width="100%">
												</div>
											@endif
										</div>
										<div class="col-7 changefontcolor" style="padding: 10px 0 0 10px;background-color: #f1f3f5">
											<div class="card-block content">
												<div class="card-title font-size-change">{{ str_limit($item->name,65) }}</div>
												<div class="card-text font-size-change">
													<span>{!! $item->details !!}</span>
												</div>
												<p id="new{{$i.$index}}" style="color: #bac7ca;">
													<script>
                                                        var date = '{{ $item->created_at }}';
                                                        var date_f = moment(date).add(543, 'year').format('LL');
                                                        document.getElementById('new{{$i.$index}}').innerHTML = date_f;
													</script>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<?php $i++ ?>
		@endforeach
	</div>
	<br/>
</div>
<div id="formobile">
	<div class="row text-center">
		<div class="col-12">
			<img alt="ข่าวสาร" style="width: 50px;
	    	height: 50px;" src="{{$site}}/Defaultimg/ข่าวสาร.png"/>
		</div>
		<div class="col-12">
			<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">ข่าวสาร</span>
		</div>
		<div class="col-12"><hr style="border: 1px solid #E65093;"></div>
	</div>
	<ul class="nav text-center" role="tablist">
		@foreach($datanew as $key => $item)
			<li style="padding: 0" role="presentation" class="col-6">
				<button style="white-space:nowrap;" class="text-center {{$loop->first ? 'active' : ''}}" id="btn-custom" href="#tab-{{ $key.'1' }}" aria-controls="#tab-{{ $key.'1' }}" role="tab" data-toggle="tab">
					{{$key}}
				</button>
			</li>
		@endforeach
	</ul>
	<div class="tab-content" style="padding-top: 20px">
		@foreach($datanew as $key => $items)
			<div id="tab-{{ $key.'1' }}" class="tab-pane {{$loop->first ? 'active':''}}">
				<div class="panel-body">
					<div class="row">
						@foreach($items->take(4) as $index => $item)
							<div class="col-md-6">
								<div onclick="gotomedia('{{$item->id}}');" class="card" style="margin: 20px;box-shadow: none;border: none">
									<div class="row no-gutters">
										<div class="col-6" style="padding: 0 10px 0 0">
														@if($item->img === null)
												<div class="image view view-first">
													<img alt="ข่าวสาร" src="{{$site}}/Defaultimg/noimg.jpg" class="img-fluid" alt="no-image">
															</div>
											@else
												<div class="image view view-first">
													<img alt="ข่าวสาร" src="{{$site}}/uploads/{{$item->img->path}}" class="img-fluid" alt="{{$item->img->name}}" width="100%" height="auto">
												</div>
											@endif
										</div>
										<div class="col-6" style="padding: 20px 0 0 0">
											<div class="card-block content">
												<div style="font-size: 12px;" class="font-size-change">{{ str_limit($item->name,65) }}</div>
												<p id="new{{$i.$index}}" style="color: #bac7ca;">
													<script>
                                                        var date = '{{ $item->created_at }}';
														var date_f = moment(date).add(543, 'year').format('LL');
														document.getElementById('new{{$i.$index}}').innerHTML = date_f;
													</script>
												</p>
											</div>
										</div>
									</div>
							    </div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
        <?php $i++ ?>
		@endforeach
	</div>
	<br/>
	<div class="col-12 text-center">
		<button class="seeallbutton" onclick="gotonew();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
		</button>
	</div>
	<br/>
</div>
<br>
{{--contentact--}}
<div id="forpc">
	<div class="row">
		<div class="col-12">
			<img alt="สื่อเผยแพร่" style="width: 50px;
	    	height: 50px;" src="{{$site}}/Defaultimg/สื่อเผยแพร่.png"/>
			<span style="margin-left: 10px;font-weight: bold;vertical-align: bottom;font-size: 20px">  สื่อเผยแพร่</span>
			<div class="float-right">
				<button class="seeallbutton" onclick="gotomedias();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
				</button>
			</div>
			<hr style="border: 1px solid #E65093;">
		</div>
	</div>
	<ul class="nav" role="tablist">
		@foreach($dataact as $key => $item)
			<li style="padding: 0;word-wrap: break-word;text-align:center; vertical-align:middle" role="presentation" class="col-3 font-size-change">
				<button class="text-center nowrap {{$loop->first ? 'active' : ''}}" id="btn-custom" href="#tab-{{ $loop->index }}" aria-controls="#tab-{{ $loop->index }}" role="tab" data-toggle="tab">
				{{$key}}
				</button>
			</li>
		@endforeach
	</ul>
	<div class="tab-content" style="padding-top: 20px">
<?php $j=0 ?>
		@foreach($dataact as $items)
			<div id="tab-{{ $loop->index }}" class="tab-pane {{$loop->first ? 'active':''}}">
				<div class="panel-body">
					<div class="row">
						@foreach($items->take(4) as $key => $item)
							<div onclick="gotomedia('{{$item->id}}');" class="col-3">
								@if($item->img === null)
									<div data-content="" class="date" id="media{{$j.$key}}" data-html="true">
										<img alt="สื่อเผยแพร่" style="border-bottom: 1px solid black;object-fit: cover;" class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" name="imaged">
									</div>
									<script>
                                        var date = '{{ $item->updated_at }}';
                                        var date_d = moment(date).add(543, 'year').format('Do');
                                        var date_m = moment(date).add(543, 'year').format('MMM');
                                        var fulldate = date_d+"\r\n"+date_m;
                                        $('#media{{$j.$key}}').attr('data-content',fulldate);
									</script>
								@else
									<div data-content="" class="date" id="media{{$j.$key}}" data-html="true">
										<img alt="สื่อเผยแพร่" class="card-img-top" src="{{$site}}/uploads/{{$item->img->path}}" name="imaged">
									</div>
									<script>
                                        var date = '{{ $item->updated_at }}';
                                        var date_d = moment(date).add(543, 'year').format('Do');
                                        var date_m = moment(date).add(543, 'year').format('MMM');
                                        var fulldate = date_d+"\r\n"+date_m;
                                        $('#media{{$j.$key}}').attr('data-content',fulldate);
									</script>
								@endif
								<h5 class="changefontcolor font-size-change" style="word-wrap: break-word;">{{ str_limit($item->name,50) }}</h5>
							</div>
						@endforeach
					</div>
				</div>
			</div>
        <?php $j++ ?>
		@endforeach
	</div>
<br/>
</div>
<div id="formobile">
	<div class="row text-center">
		<div class="col-12">
			<img alt="สื่อเผยแพร่" style="width: 50px;
	    	height: 50px;" src="{{$site}}/Defaultimg/สื่อเผยแพร่.png"/>
		</div>
		<div class="col-12">
			<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">สื่อเผยแพร่</span>
		</div>
		<div class="col-12"><hr style="border: 1px solid #E65093;"></div>
	</div>
	<ul class="nav" role="tablist">
		<select id="Selected" class="form-control">
			@foreach($dataact as $key => $item)
				<option value="{{ $loop->index.'1' }}">{{ $key }}</option>
			@endforeach
		</select>
	</ul>
	<div class="tab-content" style="padding-top: 20px">
		@foreach($dataact as $items)
			<div id="{{ $loop->index.'1' }}" role="tabpanel" class="tab-pane custom-selected {{$loop->first ? 'active':''}}">
				<div class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						@foreach($items->take(4) as $item)
							<div style="height: auto;width: 100%" class="carousel-item {{$loop->first ? 'active':''}}">
								<div class="image view view-first" onclick="gotomedia('{{$item->id}}');" style="height: auto;width: 100%">
								@if($item->img === null)
								<img alt="สื่อเผยแพร่" style="border-bottom: 1px solid black" src="{{$site}}/Defaultimg/noimg.jpg">
								@else
								<img alt="สื่อเผยแพร่" src="{{$site}}/uploads/{{$item->img->path}}">
								@endif
								<h3 style="margin-bottom:20px;word-wrap: break-word;">{{ str_limit($item->name,60) }}</h3>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		@endforeach
	</div>
	<br/>
	<div class="col-12 text-center">
		<button class="seeallbutton" onclick="gotomedias();" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
		</button>
	</div>
</div>
<br>
@include('layouts.onlyDatatable')
<script>
  $('#Selected').on('change', function(e) {
    $('.custom-selected').removeClass('active in');
    $('#' + $(e.currentTarget).val()).addClass("active in");
  });

  function gotomedia(id) {
	window.open('home/media/'+id,'_blank');
  }

  function gotonew() {
    window.location.href = "{{URL::to(route('new'))}}";
  }

  function gotomedias() {
    window.location.href = "{{URL::to(route('media'))}}";
  }
</script>
