<style>
	#team-border{
		border:none;
		border-radius: 20px;
	}
</style>
<?php
$data1 = \App\img::where('idfk','team1')->first();
?>
<div id="forpc" class="col-12" style="padding: 0">
	<img style="width: 50px;
    height: 50px;" src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
	<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> คณะกรรมการ</span>
	<div class="float-right">
		<button class="seeallbutton" tabindex="0" onclick="window.open('{{route('team')}}','_self');" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
		</button>
	</div>
	<hr style="border: 1px solid #E65093;">
</div>
<div id="formobile" class="text-center">
	<div class="col-12">
		<img style="width: 50px;
	    height: 50px;" src="{{$site}}/Defaultimg/คณะกรรมการ.png"/>
	</div>
	<div class="col-12">
		<span style="font-weight: bold;vertical-align: bottom;font-size: 20px"> คณะกรรมการ</span>
		<hr style="border: 1px solid #E65093;">
	</div>
</div>
<div id="team-border">
	@if($data1 == null)
		<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
	@else
		<img src="{{$site}}/uploads/{{$data1->path}}" width="100%" height="auto" style="padding: 20px" alt="{{$data1->name}}">
	@endif
</div>
<div id="formobile" class="text-center">
	<button class="seeallbutton" tabindex="0" onclick="window.open('{{route('team')}}','_self');" style="
			background: #B6C3C6;
			border-radius: 20px;
			border: none;
			padding: 5px 15px;
			color: white;">ดูทั้งหมด
	</button>
</div>
<br>
