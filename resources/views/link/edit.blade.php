@extends('layouts.appadmin')
@section('content')
@include('layouts.onlyDatatable')

<form method="POST" action=" {{route('link.update',$model->id)}} " enctype="multipart/form-data">
		@csrf
		<div class="container py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">แก้ไขข้อมูลเว็บไซต์</h3>
							<!-- Name -->
							<div class="md-form">
								<input type="text" id="name" name="name" class="form-control col-md-12" autocomplete="off" value="{{$model->name}}">
								<label for="name">ชื่อเว็บไซต์</label>
							</div>
							<div class="md-form">
								<input type="text" id="url" name="url" class="form-control col-md-12" autocomplete="off" value="{{$model->url}}">
								<label for="url">ยูอาร์แอล</label>
							</div>
							<div class="md-form">
								@if($model->img !== null)
									<div align="center">
										ตัวอย่างรูปเก่า
									</div>
									<div align="center">
										<img class='thumb' style="max-height: 100px;max-width: 100px;" src="{{$site}}/uploads/{{$model->img->path}}">
									</div>
								@endif
								<div align="center">
									ตัวอย่างรูปที่เปลี่ยน
								</div>
								<div align="center">
									<div id="previewImg"></div>
								</div>
									<div class="custom-file">
										<i role="presentation"  class="far fa-images prefix"></i>
										<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
										<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(4:1, 3:1, 2:1, 1:1)</label>
									</div>
								</div>
								<script type="text/javascript">
                  window.preview = function (input) {
                    if (input.files && input.files[0]) {
                      $("#previewImg").html("");
                      $(input.files).each(function () {
                        var reader = new FileReader();
                        reader.readAsDataURL(this);
                        reader.onload = function (e) {
                          $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                        }
                      });
                    }
                  }
								</script>
							<!-- Send button -->
							{{ method_field('PUT') }}
							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection
