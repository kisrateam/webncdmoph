@extends('layouts.appadmin')
@section('content')
@include('layouts.onlyDatatable')

<form method="POST" action="{{ route('link.store') }}" enctype="multipart/form-data">
	@csrf
<div class="container py-1">
	<!--Section: Content-->
	<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
		<!--Grid row-->
		<div class="row d-flex justify-content-center">
			<!--Grid column-->
			<div class="col-md-6">
				<!-- Default form contact -->
				<form class="text-center" action="#!">
						<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลเว็บไซต์</h3>
						<!-- Name -->
						<div class="md-form">
							<i role="presentation"  class="fas fa-globe-americas prefix"></i>
							<input type="text" id="name" name="name" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">ชื่อเว็บไซต์</label>
						</div>
						<div class="md-form">
							<i role="presentation"  class="fas fa-link prefix"></i>
							<input type="text" id="url" name="url" class="form-control col-md-12" autocomplete="off" required>
							<label for="url">ยูอาร์แอล</label>
						</div>
						<div class="md-form">
							<div align="center">
								ตัวอย่างรูป
								<div id="previewImg"></div>
							</div>
							<div class="custom-file">
								<i role="presentation"  class="far fa-images prefix"></i>
								<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
								<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(4:1, 3:1, 2:1, 1:1)</label>
							</div>
						</div>
						<script type="text/javascript">
							window.preview = function (input) {
							  if (input.files && input.files[0]) {
								$("#previewImg").html("");
								$(input.files).each(function () {
								  var reader = new FileReader();
								  reader.readAsDataURL(this);
								  reader.onload = function (e) {
									$("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
								  }
								});
							  }
							}
						</script>
						<button class="btn btn-info btn-block" type="submit">บันทึก</button>
				</form>
				<!-- Default form contact -->
			</div>
			<!--Grid column-->
		</div>
		<!--Grid row-->
	</section>
	<!--Section: Content-->
</div>
</form>
@endsection
