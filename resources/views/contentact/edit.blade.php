@extends('layouts.appadmin')
@section('content')
	<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
	@include('layouts.onlyDatatable')
	<form method="POST" action=" {{route('contentact.update',$contentmodel->id)}} " enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">แก้ไขข้อมูลสื่อเผยแพร่</h3>
							<div class="col-4">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
									   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow" {{$contentmodel->active == '1' ? 'checked':''}}/>
							</div>
							<!-- Name -->
							<div class="md-form">
								<i role="presentation"  class="fas fa-heading prefix"></i>
								<input type="text" id="name" name="name" value="{{$contentmodel->name}}" class="form-control col-md-12" autocomplete="off">
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<!-- Message -->
							<textarea class="details" name="details">{!! $contentmodel->details !!}
					</textarea>
							<script>
								var CSRFToken = $('meta[name="csrf-token"]').attr('content');
								var options = {
								  filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
								  filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
								  filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
								  filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
								};
							</script>
							<script>
                CKEDITOR.replace('details', options);
							</script>

							{{--<div class="md-form">--}}
								{{--@if($contentmodel->file !== null)--}}
									{{--<div class="row">--}}
										{{--<div class="col-1">--}}
											{{--ลบ--}}
										{{--</div>--}}
										{{--<div class="col-6">--}}
											{{--เปลื่ยนชื่อไฟล์--}}
										{{--</div>--}}
										{{--<div class="col-5">--}}
											{{--ใส่ไฟล์ใหม่--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--@foreach($contentmodel->file['name'] as $key => $file)--}}
										{{--<div class="row">--}}
											{{--<div class="col-1">--}}
												{{--<a onclick="deletesome('{{$contentmodel->file['id']}}','{{$contentmodel->file['files'][$loop->index]}}');" class="btn-outline-danger btn-sm">ลบ</a>--}}
											{{--</div>--}}
											{{--<div class="col-6">--}}
												{{--<input type="text" id="name" name="namefile[]" value="{{$file}}" class="form-control col-md-12" autocomplete="off">--}}
												{{--<label for="name">ชื่อไฟล์</label>--}}
											{{--</div>--}}
											{{--<div class="col-5">--}}
												{{--<input type="file" id="file" name="file[]" value="{{ old('file') }}" accept=".pdf,.doc,.xls,.ppt">--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--@endforeach--}}
								{{--@else--}}
									{{--<div class="row">--}}
										{{--<div class="col-12 text-center">--}}
											{{--ไม่พบไฟล์เก่า--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--@endif--}}
							{{--</div>--}}
							<div class="md-form">
								@if($contentmodel->img !== null)
									<div align="center">
										ตัวอย่างรูปเก่า
									</div>
									<div align="center">
											<img class='thumb' style="max-height: 100px;max-width: 100px;" src="{{$site}}/uploads/{{$contentmodel->img->path}}">
									</div>
								@endif
									<div align="center">
										ตัวอย่างรูปที่เปลี่ยน
									</div>
									<div align="center">
										<div id="previewImg"></div>
									</div>
									<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
								</div>
									<script type="text/javascript">
                    window.preview = function (input) {
                      if (input.files && input.files[0]) {
                        $("#previewImg").html("");
                        $(input.files).each(function () {
                          var reader = new FileReader();
                          reader.readAsDataURL(this);
                          reader.onload = function (e) {
                            $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                          }
                        });
                      }
                    }
									</script>
								<div class="md-form">
									<input type="text" id="namefile" name="namefile" value="{{ $contentmodel->namefile }}" class="form-control col-md-12" autocomplete="off">
									<label for="name">ชื่อไฟล์</label>
								</div>
								<div class="md-form">
									<input type="text" id="file" name="file" value="{{ $contentmodel->fileuniq }}" class="form-control col-md-12" autocomplete="off">
									<label for="name">ลิ้งไฟล์</label>
								</div>
								<div class="md-form">
									<select type="text" id="category" name="category" class="form-control col-md-12" autocomplete="off">
										<option disabled selected hidden>ประเภท</option>
										<option {{$contentmodel->category == 'แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)' ? 'selected' : ''}}>แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)</option>
										<option {{$contentmodel->category == 'สถานการณ์ / ข้อมูล' ? 'selected' : ''}}>สถานการณ์ / ข้อมูล</option>
										<option {{$contentmodel->category == 'ผลการดำเนินงาน/Best practice' ? 'selected' : ''}}>ผลการดำเนินงาน/Best practice</option>
										<option {{$contentmodel->category == 'คู่มือ แนวทาง สื่อ' ? 'selected' : ''}}>คู่มือ แนวทาง สื่อ</option>
									</select>
								</div>
								{{ method_field('PUT') }}
								<button class="btn btn-info btn-block" type="submit">บันทึก</button>
							</div>
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection

<script>
	function deletesome(id,filename) {
    var _token = $('input[name="_token"]').val();
    Swal.fire({
      title: 'คุณแน่ใจใช่ไหม ?',
      text: "ลบไฟล์ !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'ยกเลิก',
      confirmButtonText: 'ใช่ ลบเลย!'
    }).then((result) => {
      if (result.value) {
        $.ajax(
          {
            type: "GET",
            url: '/contentact/deletesome/'+id+'/'+filename,
            data:{ _token:_token},
            success: function (data) {
              console.log(data);
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'ไฟล์ถูกลบแล้ว',
                showConfirmButton: false,
                timer: 3000
              });
              location.reload();
            },
            error: function (data) {
              console.log(data);
              Swal.fire({
                icon: 'error',
                title: 'อุ๊บบ...',
                text: 'มีบางอย่างผิดพลาด !'
              })
            }
          });
      }
    })
  }
</script>
