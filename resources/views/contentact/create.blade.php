@extends('layouts.appadmin')
@section('content')
	<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
	@include('layouts.onlyDatatable')
	<form method="POST" action="{{ route('contentact.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลสื่อเผยแพร่</h3>
							<div class="col-md-12">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
									   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
							</div>
							<!-- Name -->
							<div class="md-form">
								<i role="presentation"  class="fas fa-heading prefix"></i>
								<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<!-- Message -->
							<textarea class="details" name="details">{{ old('details') }}
							</textarea>
							<script>
								var CSRFToken = $('meta[name="csrf-token"]').attr('content');
								var options = {
								  filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
								  filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
								  filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
								  filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
								};
							</script>
							<script>
                				CKEDITOR.replace('details', options);
							</script>
							{{--<div class="md-form">--}}
									{{--<a class="btn btn-outline-primary" name="addDom">เพิ่มไฟล์ข้อมูล</a>--}}
							{{--</div>--}}
							{{--<div class="md-form">--}}
							{{--<div class="row" id="addDom">--}}
								{{--</div>--}}
							{{--</div>--}}
							<div class="md-form">
								<div align="center">
									ตัวอย่างรูป
									<div id="previewImg"></div>
								</div>
								<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
								</div>
							</div>
							<script type="text/javascript">
                window.preview = function (input) {
                  if (input.files && input.files[0]) {
                    $("#previewImg").html("");
                    $(input.files).each(function () {
                      var reader = new FileReader();
                      reader.readAsDataURL(this);
                      reader.onload = function (e) {
                        $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                      }
                    });
                  }
                }
							</script>
							<div class="md-form">
								<input type="text" id="namefile" name="namefile" value="{{ old('file') }}" class="form-control col-md-12" autocomplete="off">
								<label for="name">ชื่อไฟล์</label>
							</div>
							<div class="md-form">
								<input type="text" id="file" name="file" value="{{ old('file') }}" class="form-control col-md-12" autocomplete="off">
								<label for="name">ลิ้งไฟล์</label>
							</div>
							<div class="md-form">
								<select type="text" id="category" name="category" class="form-control col-md-12" autocomplete="off">
									<option disabled selected hidden>ประเภท</option>
										<option>แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)</option>
										<option>สถานการณ์ / ข้อมูล</option>
										<option>ผลการดำเนินงาน/Best practice</option>
										<option>คู่มือ แนวทาง สื่อ</option>
									</select>
							</div>
								<!-- Send button -->
								<button class="btn btn-info btn-block" type="submit">บันทึก</button>
							<!-- Basic dropdown -->
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection
@include('layouts.onlyDatatable')
<script>
  $(document).ready(function() {
    let i = 1;
    $("a[name='addDom']").click(function() {
      $('<p class="col-1" style="cursor: pointer" id="'+i+'">'+'X'+'</p>')
      .appendTo('#addDom');
      $('<div class="col-5" id="namefile'+i+'">')
      .append($("<input />", { type: "text",name:"namefile[]", id:"namefile"+i,class: "form-control",autoComplete:"off",placeholder:"ชื่อไฟล์",required:"true" }))
	    .appendTo('#addDom');
      $('<div class="col-6" id="file'+i+'">')
	    .append($("<input />", { type: "file",name:"file[]", id:"file"+i,accept:".pdf,.doc,.xls,.ppt" }))
      .appendTo('#addDom');
      i++;
    });

    $("#addDom").on('click', 'p', function() {
      let id = $(this).attr('id');
      const namefile = 'div[id='+'\'namefile'+id+'\']';
      const file = 'div[id='+'\'file'+id+'\']';
      $($(this)).remove();
      $(namefile).remove();
      $(file).remove();
    });
  });
</script>
