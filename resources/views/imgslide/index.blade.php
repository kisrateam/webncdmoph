@extends('layouts.appadmin')
<style>
	.carousel {
		height: 400px;
	}
	.carousel-item,
	.carousel-inner,
	.carousel-inner img {
		height: 100%;
		width: 100%;
	}
	.carousel-item {
		text-align: center;
	}
</style>
@section('content')
	<div class="container-fluid">
		<div class="row">
			@if(count($data) == 0)
				<div class="col-md-3" style="padding: 20px">
					<div class="thumbnail">
						<img onclick="window.location.href = '{{URL::route('imgslide.create')}}'" style="cursor: pointer" src="{{$site}}/Defaultimg/imgupload.svg" width="100%" height="280px">
					</div>
				</div>
			@endif
		@foreach($data as $item)
			@if($loop->first && count($data) < 5)
					<div class="col-md-3" style="padding: 20px">
						<div class="thumbnail">
							<img onclick="window.location.href = '{{URL::route('imgslide.create')}}'" style="cursor: pointer" src="{{$site}}/Defaultimg/imgupload.svg" width="100%" height="280px">
						</div>
					</div>
				@endif
				<div class="col-md-3" style="padding: 20px;" onclick="window.open('{{$item->url}}');">
						<div class="thumbnail" style="cursor: pointer;">
							<div class="card">
								@if($item->path === null)
									<div class="image view view-first">
										<span><img style="border-bottom: 1px solid black;cursor: pointer" class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" name="imaged" width="100%" height="250"></span>
									</div>
								@else
									<div class="image view view-first">
										<span><img class="card-img-top" src="{{$site}}/uploads/{{$item->path}}" name="imaged" style="cursor: pointer" width="100%" height="250"></span>
									</div>
								@endif
								<div class="card-body">
									<h5 class="card-title" style="color: black">{{ str_limit($item->name,30) }}</h5>
									<p class="card-text">
										{!! mb_strimwidth($item->details, 0, 300, "...");!!}
									</p>
									<p class="card-text">
										{{ $item->created_at }}
									</p>
									<a style="border-radius: 20px;font-size: 16px;background-color: #203F54;color: white" class="btn btn-sm" onclick="window.location.href = '{{ route('imgslide.edit',['imgslide' => $item->id]) }}';">แก้ไข</a>
								</div>
							</div>
						</div>
					</div>
		@endforeach
		</div>
	</div>
@endsection