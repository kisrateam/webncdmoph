@extends('layouts.appadmin')
@section('content')
<style>

</style>
<form method="POST" action=" {{route('imgslide.update',$data->id)}} " enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<div class="row d-flex justify-content-center">

					<div class="col-12">
						<div class="row">
							<div class="col-4 offset-8">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
								       data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow" {{$data->active == '1' ? 'checked':''}}/>
							</div>
						</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" value="{{$data->name}}" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
									<label for="name">ชื่อเรื่อง*</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" value="{{$data->details}}" id="details" name="details" value="{{ old('details') }}" class="form-control col-md-12" autocomplete="off">
									<label for="details">คำอธิบาย</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="md-form">
								<div class="custom-file" style="display:none;">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
								</div>
							</div>
							<script type="text/javascript">
								window.preview = function (input) {
								  if (input.files && input.files[0]) {
									$("#previewImg").html("");
									$(input.files).each(function () {
									  var reader = new FileReader();
									  reader.readAsDataURL(this);
									  reader.onload = function (e) {
										$("#previewImg").append("<img class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
									  }
									});
									document.getElementById("uploadimg").style.backgroundImage = 'none';
								  }
								}
							</script>
							<div class="col-2" align="right">
								<label style="font-size: 16px">อัพโหลดภาพ*</label>
								<p style="font-size: 12px;color: #01673F;">*รองรับ JPEG, PNG</p>
								<p style="font-size: 12px;color: #01673F;">*ขนาดไฟล์ไม่เกิน</p>
								<p style="font-size: 12px;color: #01673F;">420*298 px(4:3)</p>
							</div>
							<div class="col-8" align="center">
								@if($data->path == null)
									<label id="uploadimg" for="img" style="
											min-width: 100%;
											min-height: 320px;
											background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');
											background-size:100% 100%;">
										<span id="previewImg"></span>
									</label>
								@else
									<label id="uploadimg" for="img" style="
											min-width: 100%;
											min-height: 320px;
											height: auto;
											background-image:url('{{ asset('/uploads/'.$data->path) }}');
											background-size:100% 100%;
											cursor: pointer">
										<span id="previewImg"></span>
									</label>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" value="{{$data->url}}" id="url" name="url" value="{{ old('url') }}" class="form-control col-md-12" autocomplete="off">
									<label for="url">Link Url</label>
								</div>
							</div>
						</div>
						<div class="row">
							{{ method_field('PUT') }}
							<div class="col-8 offset-2">
								<div class="md-form">
									<div class="col-12">
										<button class="btn btn-info" type="submit" style="width: 75%">บันทึก</button>
										<div class="float-right" style="width: 20%">
											<a onclick="confrimdelete('{{$data->id}}');" class="btn btn-danger" style="width: 100%">ลบ</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
</form>
@endsection
<script>
    function confrimdelete(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleted(id);
            }
        })
    }

    function deleted(id) {
        var _token = $('input[name="_token"]').val();
        $.ajax(
            {
                type: "DELETE",
                url: "{{ route('imgslide.store') }}"+'/'+id,
                data:{ _token:_token  },
                success: function (data) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'ข้อมูลถูกลบแล้ว',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    location.href = '{{route('imgslide.index')}}';
                },
                error: function (data) {
                    console.log(data);
                    Swal.fire({
                        icon: 'error',
                        title: 'อุ๊บบ...',
                        text: 'มีบางอย่างผิดพลาด !'
                    })
                }
            });
    }
</script>
