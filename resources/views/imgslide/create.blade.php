@extends('layouts.appadmin')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
@include('layouts.onlyDatatable')
<style>

</style>
<form method="POST" action="{{ route('imgslide.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="container-fluid py-1">
		<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
			<div class="row d-flex justify-content-center">

				<div class="col-12">
					<div class="row">
							<div class="col-4 offset-8">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
								       data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
							</div>
					</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
									<label for="name">ชื่อเรื่อง*</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" id="details" name="details" value="{{ old('details') }}" class="form-control col-md-12" autocomplete="off">
									<label for="details">คำอธิบาย</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="md-form">
								<div class="custom-file" style="display:none;">
									<i class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
								</div>
							</div>
							<script type="text/javascript">
								window.preview = function (input) {
								  if (input.files && input.files[0]) {
									$("#previewImg").html("");
									$(input.files).each(function () {
									  var reader = new FileReader();
									  reader.readAsDataURL(this);
									  reader.onload = function (e) {
										$("#previewImg").append("<img class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
									  }
									});
									document.getElementById("uploadimg").style.backgroundImage = 'none';
								  }
								}
							</script>
							<div class="col-2" align="right">
								<label style="font-size: 16px">อัพโหลดภาพ*</label>
								<p style="font-size: 12px;color: #01673F;">*รองรับ JPEG, PNG</p>
								<p style="font-size: 12px;color: #01673F;">*ขนาดไฟล์ไม่เกิน</p>
								<p style="font-size: 12px;color: #01673F;">420*298 px(4:3)</p>
							</div>
							<div class="col-8" align="center">
								<label id="uploadimg" for="img" style="min-width: 100%;
										min-height: 320px;
										background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');
										background-size:100% 100%;
										cursor: pointer">
									<span id="previewImg"></span>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col-8 offset-2">
								<div class="md-form">
									<input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control col-md-12" autocomplete="off">
									<label for="url">Link Url</label>
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-8 offset-2">
							<div class="md-form">
								<button class="btn btn-info btn-block col-md-12" type="submit">บันทึก</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</form>
@endsection
@include('layouts.onlyDatatable')
