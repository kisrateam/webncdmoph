<style>
    .carousel-inner .carousel-item.active,
    .carousel-inner .carousel-item-next,
    .carousel-inner .carousel-item-prev {
        display: flex;
        transition: 0.6s;
    }
    .link-custom{
        /*width: 100%;*/
        height: 100px;
        /*display: flex;*/
        /*justify-content: center;*/
        /*align-items: center;*/
        font-weight: bold;
        border: 1px solid #B6C3C6;
        box-sizing: border-box;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
        cursor:pointer;
    }
    .link-custom p{
        font-size: 0.7rem !important;
        /*padding:48px 5% 0 5%;*/
    }

    .link-custom .img-custom{
        height: 50px;
        margin-top: 5px;
        margin-bottom: 5px;
        width: auto;
        padding-left: 5%;
        padding-right: 5%;
        /*padding:48px 5% 0 5%;*/
    }
</style>
<?php
$datalink = \App\link::with('img')->get();
?>

<div class="col" style="height: 200px;margin-top: 50px">
    <h2>{{(\Request::is('home/eng')?'Epidemiological Update':'หน่วยงานที่เกี่ยวข้อง')}}</h2>
    <div class="container text-center my-3">
        <div class="row mx-auto my-auto">
            <div id="Carousel" class="carousel slide w-100" data-interval="false" data-ride="carousel" style="height: 200px">
                <div class="carousel-inner" role="listbox">
                    @foreach($datalink as $key => $items)
                        <div id="link-carousel" class="carousel-item {{$loop->first ? 'active':''}}" style="height: 125px" slide-to="{{$key}}">
                            <div class="{{ $datalink->count() > 5 ? "col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2":"col-12" }}" style="padding: 0 5px">
                                <div class="link-custom" tabindex="0" onclick="window.open('{{ $items->url }}');">
                                    <img class="img-custom" id="image{{$key}}" style="max-width: 100%" src="{{$site}}/uploads/{{$items->img->path}}" alt="{{$items->img->name}}">
                                    <p style="color: black;">{!! $items->name !!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" style="width:50px;height: 125px; position: absolute;left: -6%;" href="#Carousel" role="button" data-slide="prev">
                    <span style="border-radius: 50%;" class="carousel-control-prev-icon grey" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" style="width:50px;height: 125px; position: absolute;right: -6%;" href="#Carousel" role="button" data-slide="next">
                    <span style="border-radius: 50%;"  class="carousel-control-next-icon grey" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    $(document).ready(function() {

        let c = parseInt("{{ $datalink->count() }}");

        $(".carousel-control-next").click(function(){
            let width = $(window).width();
            if(width >= 1200)cwidth = 6;
            else if(width >= 992)cwidth = 4;
            else if(width >= 768)cwidth = 3;
            else if(width >= 576)cwidth = 2;
            else cwidth = 1;
            if(cindex + cwidth >= c){
                cindex = cindex + cwidth - c;
            }else{
                cindex += cwidth;
            }
            $("#Carousel").carousel(cindex);
        });

        $(".carousel-control-prev").click(function(){
            let width = $(window).width();
            if(width >= 1200)cwidth = 6;
            else if(width >= 992)cwidth = 4;
            else if(width >= 768)cwidth = 3;
            else if(width >= 576)cwidth = 2;
            else cwidth = 1;
            if(cindex < cwidth){
                cindex = c + cindex - cwidth;
            }else{
                cindex -= cwidth;
            }
            $("#Carousel").carousel(cindex);
        });

        if (c > 5){
            $('.carousel #link-carousel').each(function(){
                let next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));
                for (let i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }
                    next.children(':first-child').clone().appendTo($(this));
                }
            });
        }
    });


</script>
