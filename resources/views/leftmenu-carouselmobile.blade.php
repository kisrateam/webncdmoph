<div id="formobile">
<?php
    $data = \App\imgslide::where('active',1)->orderBy('updated_at','DESC')->get();
?>
	<div id="stopmodal" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			@foreach($data as $item)
				<div class="carousel-item {{$loop->first ? 'active':''}}">
					<div class="card" style="box-shadow: none;border-style: none">
						@if($item->path === null)
							<img style="border-bottom: 1px solid black;padding-top: 70px" class="card-img-top" src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto">
						@else
							<img class="card-img-top" src="{{$site}}/uploads/{{$item->path}}" width="100%" height="auto" alt="{{$item->name}}">
						@endif
						<div class="card-body" style="word-wrap: break-word;">
							<h4 class="card-title"><a>{{ str_limit($item->name,30) }}</a></h4>
							<p class="card-text">{{ str_limit($item->details,100) }}</p>
							<div style="text-align: center;">
								<button onclick="window.open('{{$item->url}}');" class="btn btn-sm" style="background-color: #508EBF;color: white;position: relative">อ่านต่อ</button>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>