<style>
  .card-carousel {
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    padding-left: 20%;
  }
  .card-carousel .my-card {
    height: auto;
    width: 300px;
    position: relative;
    z-index: 1;
    -webkit-transform: scale(0.6) translateY(-2rem);
    transform: scale(0.6) translateY(-2rem);
    opacity: 0;
    cursor: pointer;
    pointer-events: none;
    transition: 1s;
  }
  .card-carousel .my-card:after {
    content: '';
    position: absolute;
    height: 2px;
    width: 100%;
    border-radius: 100%;
    background-color: rgba(0,0,0,0.3);
    bottom: -5rem;
    -webkit-filter: blur(4px);
    filter: blur(4px);
  }
  .card-carousel .my-card.active {
    z-index: 3;
    -webkit-transform: scale(1) translateY(0) translateX(0);
    transform: scale(1) translateY(0) translateX(0);
    opacity: 1;
    pointer-events: auto;
    transition: 1s;
  }
  .card-carousel .my-card.prev, .card-carousel .my-card.next {
    z-index: 2;
    -webkit-transform: scale(0.8) translateY(-1rem) translateX(0);
    transform: scale(0.8) translateY(-1rem) translateX(0);
    opacity: 0.6;
    pointer-events: auto;
    transition: 1s;
  }
  .card-carousel .my-card:nth-child(0):before {
    content: '0';
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
    font-size: 3rem;
    font-weight: 300;
    color: #fff;
  }
  @media only screen and (max-width : 736px)
  {
    .card-carousel {
      padding-left: 76%;
    }
    .card-carousel .my-card > img {
      height: 160px;
      width: 300px;
    }
  }
</style>
<div class="card-carousel" style="">
  @foreach($imgslide as $item)
    <span class="my-card">
      <img src="uploads/{{$item->path}}" alt="{{$item->name}}">
    </span>
  @endforeach
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>
<script>
  $num = $('.my-card').length;
  $even = $num / 2;
  $odd = ($num + 1) / 2;
  if ($num % 2 == 0) {
    $('.my-card:nth-child(' + $even + ')').addClass('active');
    $('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $even + ')').next().addClass('next');
  } else {
    $('.my-card:nth-child(' + $odd + ')').addClass('active');
    $('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $odd + ')').next().addClass('next');
  }
  $('.my-card').click(function() {
    $slide = $('.active').width();
    if ($(this).hasClass('next')) {
      $('.card-carousel').stop(false, true).animate({left: '-=' + $slide});
    } else if ($(this).hasClass('prev')) {
      $('.card-carousel').stop(false, true).animate({left: '+=' + $slide});
    }
    $(this).removeClass('prev next');
    $(this).siblings().removeClass('prev active next');
    $(this).addClass('active');
    $(this).prev().addClass('prev');
    $(this).next().addClass('next');
  });
  $('html body').keydown(function(e) {
    if (e.keyCode == 37) { // left
      $('.active').prev().trigger('click');
    }
    else if (e.keyCode == 39) { // right
      $('.active').next().trigger('click');
    }
  });
</script>