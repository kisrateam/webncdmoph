@extends('layouts.appadmin')
@section('content')
@include('layouts.onlyDatatable')

<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
	@csrf
<div class="container py-1">
	<!--Section: Content-->
	<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
		<!--Grid row-->
		<div class="row d-flex justify-content-center">
			<!--Grid column-->
			<div class="col-md-6">
				<!-- Default form contact -->
				<form class="text-center" action="#!">
						<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลคณะกรรมการ</h3>
					<div class="md-form">
						<input type="text" id="title" name="title" class="form-control col-md-12"
						       list="titlelist" autocomplete="off" value="{{ old('title') }}" required>
						<label for="title">คำนำหน้าชื่อ</label>
						<datalist id="titlelist">
							@foreach($collectiontitle as $option)
								<option>{{$option->name}}</option>
							@endforeach
						</datalist>
					</div>
						<div class="md-form">
							<input type="text" id="name" name="name" class="form-control col-md-12" autocomplete="off" value="{{ old('name') }}" required>
							<label for="name">ชื่อ</label>
						</div>
					<div class="md-form">
						<input type="text" id="lname" name="lname" class="form-control col-md-12" autocomplete="off" value="{{ old('lname') }}" required>
						<label for="lname">นามสกุล</label>
					</div>
					<div class="md-form">
						<input type="text" id="position" name="position" class="form-control col-md-12" autocomplete="off" value="{{ old('position') }}" required>
						<label for="position">ชื่อตำแหน่ง</label>
					</div>
					<div class="md-form">
						<select id="rank" name="rank" class="browser-default custom-select col-md-12" required>
							<option selected disabled hidden>เลือกระดับที่จะแสดง(ระดับสูงสุด ใช้ได้แค่คนเดียว)</option>
							<option value="1">ระดับสูงสุด</option>
							<option value="2">ระดับสูง</option>
							<option value="3">ระดับปรกติ</option>
							<option value="4">คณะทำงานอื่น ๆ</option>
						</select>
					</div>
					<div class="md-form">
						<input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control col-md-12" autocomplete="off">
						<label for="email">อีเมลล์ติดต่อ</label>
					</div>
					<div class="md-form">
						<input type="tel" id="tel" name="tel" value="{{ old('tel') }}" class="form-control col-md-12" autocomplete="off">
						<label for="tel">เบอร์โทรติดต่อ</label>
					</div>
					<div class="custom-file">
						<i role="presentation"  class="far fa-images prefix"></i>
						<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" aria-describedby="inputGroupFileAddon01" name="img" value="{{ old('img') }}" required>
						<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
					</div>
					<br/>
							<!-- Send button -->
							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
				</form>
				<!-- Default form contact -->
			</div>
			<!--Grid column-->
		</div>
		<!--Grid row-->
	</section>
	<!--Section: Content-->
</div>
</form>
@endsection
