<?php
$datarank2 = \App\team::where('rank',2)->with('file')->first();
$datarank3 = \App\team::where('rank','like','3%')->with('file')->get();
?>
<style>
	#submit-custom{
		position: relative;
		top: 7px;
	}
</style>
<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
	@csrf
	<input value="2" name="rank" hidden>
	<div class="row">
		<div class="col-4">
			<div class="md-form">
				<input type="text" id="name" name="name" value="{{$datarank2 != null ? $datarank2->name:''}}" class="form-control col-md-12" autocomplete="off" required>
				<label for="name">ชื่อไฟล์</label>
			</div>
		</div>
		<div class="custom-file col-4">
			<div class="md-form">
				<input type="text" name="file" value="{{$datarank2 != null ? $datarank2->fileuniq:''}}" class="form-control col-md-12" autocomplete="off">
				<label for="file">ลิ้งไฟล์</label>
			</div>
			<a></a>
		</div>
		<div class="{{$datarank2 == null ? '':'col-1'}}" style="display: {{$datarank2 == null ? 'none':'block'}};">
			<div class="md-form">
				<a id="submit-custom" style="background: white;border-radius: 20px;color: black;box-shadow: none;outline: none;border-color: black;margin: 0" onclick="confirmsub3('{{$datarank2 != null ? $datarank2->id:null}}');" class="btn btn-block">ลบ</a>
			</div>
		</div>
		<div class="{{$datarank2 == null ? 'col-4':'col-3'}}">
			<div class="md-form">
				<button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" id="submit-custom" type="submit" class="btn btn-block">บันทึก</button>
			</div>
		</div>
	</div>
</form>
<hr style="border: 1px solid #B6C3C6;">
<?php $j = 0 ?>
@for($i = 1;$i <= 5;$i++)
	@foreach($datarank3 as $item)
		@if($item->rank == '3.'.$i)
		<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
			@csrf
			<input value="{{'3.'.$i}}" name="rank" hidden>
			<div class="row">
				<div class="col-4">
					<div class="md-form">
						<input type="text" id="{{'name3.'.$i}}" name="name" value="{{$item != null ? $item->name:''}}" class="form-control col-md-12" autocomplete="off" required>
						<label for="name">ชื่อไฟล์</label>
					</div>
				</div>
				<div class="custom-file col-4">
					<div class="md-form">
						<input type="text" id="{{'file.'.$i}}" name="file" value="{{$item != null ? $item->fileuniq:''}}" class="form-control col-md-12" autocomplete="off">
						<label for="{{'file.'.$i}}">ลิ้งไฟล์</label>
					</div>
					<a></a>
				</div>
				<div class="col-1">
					<div class="md-form">
						<a id="submit-custom" style="background: white;border-radius: 20px;color: black;box-shadow: none;outline: none;border-color: black;margin: 0" onclick="confirmsub3('{{$item != null ? $item->id:null}}');" class="btn btn-block">ลบ</a>
					</div>
				</div>
				<div class="col-3">
					<div class="md-form">
						<button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" id="submit-custom" type="submit" class="btn btn-block">บันทึก</button>
					</div>
				</div>
			</div>
		</form>
		<?php $j++ ?>
		@endif
	@endforeach
	@if($j == 0)
	<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
		@csrf
		<input value="{{'3.'.$i}}" name="rank" hidden>
		<div class="row">
			<div class="col-4">
				<div class="md-form">
					<input type="text" id="{{'name3.'.$i}}" name="name" class="form-control col-md-12" autocomplete="off" required>
					<label for="name">ชื่อไฟล์</label>
				</div>
			</div>
			<div class="custom-file col-4">
				<div class="md-form">
					<input type="text" id="{{'file.'.$i}}" name="file" class="form-control col-md-12" autocomplete="off">
					<label for="{{'file.'.$i}}">ลิ้งไฟล์</label>
				</div>
				<a></a>
			</div>
			<div class="col-4">
				<div class="md-form">
					<button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" id="submit-custom" type="submit" class="btn btn-block">บันทึก</button>
				</div>
			</div>
		</div>
	</form>
	@else
	<?php $j = 0 ?>
	@endif
@endfor

<script>
    function confirmsub3(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "โปรดดูให้แน่ใจว่าเลือกถูกอัน !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deletesub3(id);
            }
        })
    };

    function deletesub3(id) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{ route('team.store') }}"+'/'+id,
                    data:{ _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function(){ window.location.reload(); }, 1000);
                    },
                    error: function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'อุ๊บบ...',
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    };
</script>