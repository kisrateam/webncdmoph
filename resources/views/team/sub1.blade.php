<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="row">
		<div class="md-form">
			<div class="custom-file" style="display:none;">
				<i role="presentation"  class="far fa-images prefix"></i>
				<input type="file" class="custom-file-input @error('img1') is-invalid @enderror" id="img1" name="img1" onchange="preview1(this);" accept="image/png,image/jpeg">
				<label class="custom-file-label" for="img1">เลือกไฟล์รูปภาพ</label>
			</div>
		</div>
		<script type="text/javascript">
      window.preview1 = function (input) {
        if (input.files && input.files[0]) {
          $("#previewImg1").html("");
          $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) {
              $("#previewImg1").append("<img class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
            }
          });
          document.getElementById("uploadimg1").style.backgroundImage = 'none';
        }
      }
		</script>
		<div class="col-3" align="right">
			<label style="font-size: 16px">อัพโหลดภาพแสดงหน้าแรก*:</label>
			<p style="font-size: 12px;color: #01673F;">*รองรับ JPEG, PNG</p>
			<p style="font-size: 12px;color: #01673F;">1200*1350 px</p>
		</div>
		<div class="col-4" align="center">
			@if($data1 == null || $data1->path == null)
				<label id="uploadimg1" for="img1" style="min-width: 100%;
						min-height: 220px;
						background-image:url('{{ asset('/Defaultimg/imgupload.svg') }}');
						background-size:100% 100%;
						cursor: pointer;">
					<span id="previewImg1"></span>
				</label>
			@else
				<label id="uploadimg1" for="img1" style="min-width: 100%;
						min-height: 220px;
						background-image:url('{{ asset('/uploads/'.$data1->path) }}');
						background-size:100% 100%;
						cursor: pointer">
					<span id="previewImg1"></span>
				</label>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="md-form">
			<div class="custom-file" style="display:none;">
				<i role="presentation"  class="far fa-images prefix"></i>
				<input type="file" class="custom-file-input @error('img2') is-invalid @enderror" id="img2" name="img2" onchange="preview2(this);" accept="image/png,image/jpeg">
				<label class="custom-file-label" for="img2">เลือกไฟล์รูปภาพ</label>
			</div>
		</div>
		<script type="text/javascript">
      window.preview2 = function (input) {
        if (input.files && input.files[0]) {
          $("#previewImg2").html("");
          $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) {
              $("#previewImg2").append("<img class='thumb' style=\"max-height: 700px;max-width: 700px;\" src='" + e.target.result + "'>");
            }
          });
          document.getElementById("uploadimg2").style.backgroundImage = 'none';
        }
      }
		</script>
		<div class="col-3" align="right">
			<label style="font-size: 16px">อัพโหลดภาพแสดงหน้าหลัก*:</label>
			<p style="font-size: 12px;color: #01673F;">*รองรับ JPEG, PNG</p>
			<p style="font-size: 12px;color: #01673F;">1400*4050 px</p>
		</div>
		<div class="col-4" align="center">
			@if($data2 == null || $data2->path == null)
				<label id="uploadimg2" for="img2" style="min-width: 100%;
						min-height: 220px;
						background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');
						background-size:100% 100%;
						cursor: pointer">
					<span id="previewImg2"></span>
				</label>
			@else
				<label id="uploadimg2" for="img2" style="min-width: 100%;
						min-height: 220px;
						background-image:url('{{ asset('/uploads/'.$data2->path) }}');
						background-size:100% 100%;
						cursor: pointer">
					<span id="previewImg2"></span>
				</label>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-1 offset-9">
			<div class="md-form">
				<a style="background: white;border-radius: 20px;color: black;box-shadow: none;outline: none;border-color: black;margin: 0" onclick="deleteimg('{{($data1!=null?$data1->name:'null')}}','{{($data2!=null?$data2->name:'null')}}');" class="btn btn-block">ลบ</a>
			</div>
		</div>
		<div class="col-2">
			<div class="md-form">
				<button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" type="submit" class="btn btn-block">บันทึก</button>
			</div>
		</div>
	</div>

</form>

<script>
    function deleteimg(name1,name2) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "รูปจะหายไปทั้งสองรูป !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                godeleteimg(name1,name2);
            }
        })
    };

    function godeleteimg(name1,name2) {
        let _token = $('input[name="_token"]').val();
        if(name1){
            $.ajax(
				{
					type: "DELETE",
					url: "{{ route('team.store') }}"+'/'+name1,
					data:{ _token:_token  },
					success: function (data) {
                        Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'ข้อมูลถูกลบแล้ว',
							showConfirmButton: false,
							timer: 3000
						});
                    },
					error: function (data) {
						Swal.fire({
							icon: 'error',
							title: 'อุ๊บบ...',
							text: 'มีบางอย่างผิดพลาด !'
						});
					}
				});
			};
		if(name2){
			$.ajax(
				{
					type: "DELETE",
					url: "{{ route('team.store') }}"+'/'+name2,
					data:{ _token:_token  },
					success: function (data) {
                        Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'ข้อมูลถูกลบแล้ว',
							showConfirmButton: false,
							timer: 3000
						});
                    },
					error: function (data) {
						Swal.fire({
							icon: 'error',
							title: 'อุ๊บบ...',
							text: 'มีบางอย่างผิดพลาด !'
						})
					}
				});
			};
        setTimeout(function(){ window.location.reload(); }, 1000);
    };
</script>
