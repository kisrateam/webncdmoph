<?php
$datarank1 = \App\team::where('rank',1)->with('file')->first();
?>
<form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
	@csrf
	<input value="1" name="rank" hidden>
	<div class="row">
		<div class="col-4">
			<div class="md-form">
				<input type="text" id="name" name="name" value="{{$datarank1 != null ? $datarank1->name:''}}" class="form-control col-md-12" autocomplete="off" required>
				<label for="name">ชื่อไฟล์</label>
			</div>
		</div>
		<div class="custom-file col-4">
			<div class="md-form">
				<input type="text" name="file" value="{{$datarank1 != null ? $datarank1->fileuniq:''}}" class="form-control col-md-12" autocomplete="off">
				<label for="file">ลิ้งไฟล์</label>
			</div>
			<a></a>
		</div>
        <div class="{{$datarank1 == null ? '':'col-1'}}" style="display: {{$datarank1 == null ? 'none':'block'}};">
            <div class="md-form">
                <a id="submit-custom" style="background: white;border-radius: 20px;color: black;box-shadow: none;outline: none;border-color: black;margin: 0" onclick="confirmsub2('{{$datarank1 != null ? $datarank1->id:null}}');" class="btn btn-block">ลบ</a>
            </div>
        </div>
        <div class="{{$datarank1 == null ? 'col-4':'col-3'}}">
            <div class="md-form">
                <button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" id="submit-custom" type="submit" class="btn btn-block">บันทึก</button>
            </div>
        </div>
	</div>
</form>

<script>
    function confirmsub2(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "โปรดดูให้แน่ใจว่าเลือกถูกอัน !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deletesub2(id);
            }
        })
    };

    function deletesub2(id) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{ route('team.store') }}"+'/'+id,
                    data:{ _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function(){ window.location.reload(); }, 1000);
                    },
                    error: function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'อุ๊บบ...',
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    };
</script>