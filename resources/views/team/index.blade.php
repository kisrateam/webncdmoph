@extends('layouts.appadmin')
@if(Session::has('success'))
  <div class="alert alert-success">
    <strong>Success: </strong>{{ Session::get('success') }}
  </div>
@endif
@if(Session::has('error'))
  <div class="alert alert-error">
    <strong>error: </strong>{{ Session::get('error') }}
  </div>
@endif
<style>
  #btn-custom{
    border: 1px solid #508EBF;
    box-sizing: border-box;
    border-radius: 20px;
  }
  #btn-custom.active{
    border: 1px solid #508EBF;
    color: white;
    background-color: #508EBF;
  }
</style>
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <img style="width: 44px;
  height: 44px;
  padding-bottom: 5px;" src="{{$site}}/Defaultimg/team-new.png"/>
      <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการคณะกรรมการ</span>
    </div>
  </div>
  <hr style="border: 1px solid #B6C3C6;">
  <ul class="nav nav-pills">
    <li class="nav-item">
      <a id="btn-custom" class="nav-link btn active" data-toggle="pill" href="#menu1">รูปภาพ</a>
    </li>
    <li class="nav-item">
      <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu2">คณะกรรมการ การขับเคลื่อนแผน</a>
    </li>
    <li class="nav-item">
      <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu3">คณะอนุกรรมการ การขับเคลื่อนแผน</a>
    </li>
    <li class="nav-item">
      <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu4">คณะทำงานติดตาม และประเมินผลแผน</a>
    </li>
  </ul>

  <div class="tab-content">
    <div id="menu1" class="container-fluid tab-pane active"><br>
        @include('team.sub1')
    </div>
    <div id="menu2" class="container-fluid tab-pane fade"><br>
        @include('team.sub2')
    </div>
    <div id="menu3" class="container-fluid tab-pane fade"><br>
        @include('team.sub3')
    </div>
    <div id="menu4" class="container-fluid tab-pane fade"><br>
        @include('team.sub4')
    </div>
  </div>
</div>
@endsection

@include('layouts.onlyDatatable')
