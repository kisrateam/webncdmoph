<?php
$img = \App\img::where('idfk','content3')->first();
$data = \App\content::where('active','1')->where('category','!=','3')->get();
$dall = \App\content::where('name','downloadall1')->first();
?>
<div id="forpc" class="row">
	<div class="col-12" style="padding: 0">
		<img alt="แผนยุทธศาสตร์" style="width: 44px;
    height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png"/>
		<span class="font-size-change" style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</span>
		<div class="float-right">
			<button class="seeallbutton" tabindex="0" onclick="window.open('{{route('content')}}','_self');" style="
				background: #B6C3C6;
				border-radius: 20px;
				border: none;
				padding: 5px 15px;
				color: white;">ดูทั้งหมด
			</button>
		</div>
		<hr style="border: 1px solid #E65093;padding-left: 20px">
	</div>
	<div class="row">
		<div class="col-6">
			<p>
				@if($img == null)
					<img alt="แผนยุทธศาสตร์" src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto"/>
				@else
					<img alt="แผนยุทธศาสตร์" src="{{$site}}/uploads/{{$img->path}}" width="100%" height="auto"/>
				@endif
			</p>
		</div>
		<br>
		<br>
		<div class="col-6">
			@foreach($data->take(7) as $item)
				<div class="row">
					<div class="col-9 font-size-change {{$loop->index == 5 ? 'd-none d-lg-block' : ($loop->index > 5 ? 'd-none d-xl-block':'')}}">
						<span>{{$item->name}}</span>
					</div>

					<div class="col-3 {{$loop->index == 5 ? 'd-none d-lg-block' : ($loop->index > 5 ? 'd-none d-xl-block':'')}}">
						<button class="download_hover" style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" tabindex="0" onclick="gotofile('{{$item->fileuniq}}');"><img alt="แผนยุทธศาสตร์" src="{{$site}}/Defaultimg/clouddownload.svg" style="padding-right: 5px">ดาวน์โหลด</button>
					</div>
				</div>
				<div class="row">
					<div class="col-12 {{$loop->index == 5 ? 'd-none d-lg-block' : ($loop->index > 5 ? 'd-none d-xl-block':'')}}">
						<hr style="border: 1px solid #c4d1d4;width: 100%">
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<div id="formobile">
	<div class="row text-center">
		<div class="col-12">
			<img style="width: 44px;
	    height: 44px;" src="{{$site}}/Defaultimg/แผนยุทธศาสตร์.png" alt="แผนยุทธศาสตร์"/>
			</div>
			<div class="col-12">
				<span style="font-weight: bold;vertical-align: bottom;font-size: 20px">  แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (2560 - 2564)</span>
				<hr style="border: 1px solid #E65093;">
			</div>
		</div>
		<div class="col-12">
			@if($img == null)
				<img src="{{$site}}/Defaultimg/noimg.jpg" width="100%" height="auto" alt="แผนยุทธศาสตร์"/>
			@else
				<img src="{{$site}}/uploads/{{$img->path}}" width="100%" height="auto" alt="แผนยุทธศาสตร์"/>
			@endif
		</div>
		<br/>
		<br/>
		<div class="col-12">
		@foreach($data->take(7) as $item)
			<div class="row">
				<div class="col-9">
					<span>{{$item->name}}</span>
				</div>

				<div class="col-3">
						<button style="border-radius: 20px;border: none;background-color: transparent;color: #508EBF;outline: none;float: right;" tabindex="0" onclick="gotofile('{{$item->fileuniq}}');"><img src="{{$site}}/Defaultimg/clouddownload.svg" alt="แผนยุทธศาสตร์" style="padding-right: 5px">ดาวน์โหลด</button>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<hr style="border: 1px solid #B6C3C6;width: 100%">
				</div>
			</div>
			@if($loop->last)
					<div class="row">
						<div class="col text-center">
							<button class="seeallbutton" tabindex="0" onclick="gotocontent();" style="
									background: #B6C3C6;
									border-radius: 20px;
									border: none;
									padding: 5px 15px;
									color: white;">ดูทั้งหมด
							</button>
						</div>
					</div>
			@endif
		@endforeach
	</div>
</div>
<br>

<script type="text/javascript">
	function gotocontent() {
    window.location.href = "{{URL::to(route('content'))}}";
  }

  function gotofile(file) {
      window.open(file,'_blank');
  }
</script>
