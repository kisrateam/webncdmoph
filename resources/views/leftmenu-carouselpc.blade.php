<style>
	#box-carousel .slick-slider {
		margin-left: -12%;
		margin-right: -12%;
	}

	#box-carousel .slick-list {
		padding: 0 15% !important;
		height: 100%!important;
	}

	#box-carousel .slick-dots {
		position: absolute;
		right: 20%;
		top: 100%;
		list-style:none;
		transform: rotate(270deg);
		transform-origin: 100% 0;
		cursor: pointer;
	}

	#box-carousel .slick-track {
		max-width: 100%!important;
		transform: translate3d(0, 0, 0)!important;
		perspective: 100px;
	}

	#box-carousel .slick-slide {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		margin: auto;
		opacity: 0;
		width: 100%!important;
		height: 85%!important;
		transform: translate3d(0, 0, 0);
		transition: transform 0.7s, opacity 0.7s;
		background-color: transparent;
		outline: none;
	}

	#box-carousel{
		height: 350px!important;
	}

	#box-carousel .slick-snext,
	#box-carousel .slick-sprev{
		display: block;
	}

	#box-carousel .slick-current {
		opacity: 1;
		position: relative;
		display: block;
		z-index: 2;
		margin-top: 20px;
	}

	#box-carousel .slick-snext {
		opacity: 1;
		transform: translate3d(25%, 0, -10px);
		z-index: 1;
		perspective: 1000px;
		margin-top: 20px;
	}

	#box-carousel .slick-sprev {
		opacity: 1;
		transform: translate3d(-25%, 0, -10px);
		margin-top: 20px;
	}

	#box-carousel .content-slick {
		display: block;
		width: 50%;
		padding: 0;
		text-align: left;
		background-color: white;
	}

	#box-carousel ul li.slick-active{
		background-color: #B6C3C6;
		color: white;
		border-radius: 100%;
	}

	#box-carousel ul.slick-dots li{
		width: 24px;
		text-align: center;
		margin-bottom: 4px;
		transform: rotate(90deg);
	}
	#formobile{
		display: none;
	}
	@media screen and (max-width : 992px)
	{
		#formobile{
			display: block;
		}
		#forpc{
			display: none;
		}
	}
</style>
<div id="forpc">
    <?php
    $data = \App\imgslide::where('active',1)->orderBy('updated_at','DESC')->get();
    ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
	<div class="container">
		<div class="rev_slider" id="box-carousel">
			@foreach($data->take(5) as $key => $item)
				<div class="rev_slide">
					<div class="row" style="height: auto;width:100%;filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));">
						<div class="content-slick col-7" style="height: auto;width:100%">
							@if($item->path === null)
								<img alt="no-image" src="{{$site}}/Defaultimg/noimg.jpg">
							@else
								<img style="height: auto;width: 100%" src="{{$site}}/uploads/{{$item->path}}" alt="ข่าวสาร">
							@endif
						</div>
						<div class="content-slick col-5" style="padding: 0 3%;">
							<br/>
							<p id="countline{{$key}}" class="font-size-change" style="font-weight: bold;font-size: 18px;line-height: 22px;
								/*text-align: justify;text-justify: inter-word;*/
								">
								{{ $item->name }}</p>
							<p id="linehide{{$key}}" class="font-size-change" style="word-wrap: break-word;line-height: 18px
								/*text-align: justify;text-justify: inter-word;*/
								">
								{{ $item->details }}</p>
							<a tabindex="0" onclick="window.open('{{$item->url}}');" class="btn btn-block" style="background-color: #508EBF;color: white;width: 80%;position: absolute;bottom: 5%;left: 10%;">อ่านต่อ</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<script>
    let rev = $('.rev_slider');
    rev.on('init', function(event, slick, currentSlide) {
        let
            cur = $(slick.$slides[slick.currentSlide]),
            next = cur.next(),
            prev = cur.prev();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        cur.removeClass('slick-snext').removeClass('slick-sprev');
        slick.$prev = prev;
        slick.$next = next;
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        let cur = $(slick.$slides[nextSlide]);
        slick.$prev.removeClass('slick-sprev');
        slick.$next.removeClass('slick-snext');
        let next = cur.next(),
            prev = cur.prev();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        slick.$prev = prev;
        slick.$next = next;
        cur.removeClass('slick-next').removeClass('slick-sprev');
    });

    rev.slick({
        speed: 0,
        autoplay: false,
        arrows: true,
        dots: true,
        focusOnSelect: true,
        prevArrow: '<button hidden aria-label="prev"> prev</button>',
        nextArrow: '<button hidden aria-label="next"> next</button>',
        infinite: true,
        centerMode: true,
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerPadding: '0',
        swipe: true,
        customPaging: function(slider, i) {
            return (i+1);
        },
    });

    $( document ).ready(function() {
        countLines();
        $('.slide-title').each(function () {
            let $slide = $(this).parent();
            if ($slide.attr('aria-describedby') != undefined) { // ignore extra/cloned slides
                $(this).attr('id', $slide.attr('aria-describedby'));
            }
        });
    });

    function countLines() {
        let c = parseInt('{{$data->count()}}');
        for(let i=0;i<c;i++){
            let width = $(window).width();
            let el = document.getElementById('countline'+i);
            let divHeight = el.offsetHeight;
            let lineHeight = parseInt(el.style.lineHeight);
            let lines = divHeight / lineHeight;
            if(lines > 3 && width < 1200)document.getElementById('linehide'+i).style.display = 'none';
        }
    }
</script>
