@extends('layouts.appadmin')
@section('content')
    <h1 style="font-weight: bold">การจัดการกระดานสนทนา</h1>
    <hr style="border: 1px solid #B6C3C6;">

    <div class="row">
        <div class="col-5"><span style="float: right">ผู้สร้าง :</span></div>
        <div class="col-7"><span>{{$data->users->name}}</span></div>
        <div class="col-5"><span style="float: right">รหัสผู้ดูแล :</span></div>
        <div class="col-7"><span>{{$data->users->name}}</span></div>
        <div class="col-5"><span style="float: right">สถานะ :</span></div>
        <div class="col-7"><span>{{$data->users->role}}</span></div>
    </div>
    <hr style="border: 1px solid #B6C3C6;">

    <div class="row">
        <div class="col-5"><span style="float: right">หัวข้อคำถาม :</span></div>
        <div class="col-7"><span>{{$data->name}}</span></div>
        <div class="col-5"><span style="float: right">หมวดหมู่ :</span></div>
        <div class="col-7"><span>{{$data->getcategory()}}</span></div>
        <div class="col-5"><span style="float: right">หมวดหมู่ย่อย :</span></div>
        <div class="col-7"><span>{{($data->subcategory != null ? $data->getsubcategory():'-')}}</span></div>
        <div class="col-6 offset-2 text-center"><span>รายละเอียด</span></div>
        <div class="col-7 offset-2" style="border: 1px solid #B6C3C6;"><span>{!! $data->details !!}</span></div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <a class="btn" tabindex="0" onclick="window.open('{{{$site.'/home/topic/'.$data->id}}}');" style="border-radius: 40px;color: black;background-color: white;border: 1px solid #B6C3C6;box-shadow: none">ไปที่การสนทนานี้</a>
                <a class="btn" tabindex="0" onclick="confirmdelete('{{$data->id}}');" style="border-radius: 40px;color: black;background-color: white;border: 1px solid #B6C3C6;box-shadow: none">ยืนยันการลบ</a>
            </div>
        </div>
    </div>
@endsection
<script>
    function confirmdelete(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "กระดานสนทนาจะหายไป !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleted(id);
            }
        })
    };

    function deleted(id) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{ route('topic.store') }}"+'/'+id,
                    data:{ _token:_token  },
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function(){ window.location.href = '{{route('topic.index')}}'; }, 1000);
                    },
                    error: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: 'error',
                            title: data.error,
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    };
</script>
