@extends('layouts.appadmin')
@section('content')
<div class="container-fluid">

    {{--<h1 style="font-weight: bold">การจัดการรายงานปัญหา</h1>--}}
    {{--<hr style="border: 1px solid #B6C3C6;">--}}


    @foreach($data as $index => $row)
    @if($loop->first)
        <div class="row">
            <div class="col-5"><span style="float: right">ผู้สร้าง :</span></div>
            <div class="col-7"><span>
                {{$row->type == 'comments'?$row->reports->topic->users->name:$row->reports->users->name}}
            </span></div>
            <div class="col-5"><span style="float: right">รหัสผู้ดูแล :</span></div>
            <div class="col-7"><span>
                {{$row->type == 'comments'?$row->reports->topic->users->name:$row->reports->users->name}}
            </span></div>
            <div class="col-5"><span style="float: right">สถานะ :</span></div>
            <div class="col-7"><span>
                {{$row->type == 'comments'?$row->reports->topic->users->role:$row->reports->users->role}}
            </span></div>
            <div class="col-9 offset-1">
                <hr style="border: 1px solid #B6C3C6;">
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-5"><span style="float: right">หัวข้อที่ :</span></div>
        <div class="col-7"><span>{{$index+1}}</span></div>
        <div class="col-5"><span style="float: right">เหตุผลที่ถูกรายงาน :</span></div>
        <div class="col-7"><span>{{$row->details}}</span></div>
        @if($row->type == 'comments')
            <div class="col-5"><span style="float: right">หมวดหมู่ :</span></div>
            <div class="col-7"><span>การตอบกลับ</span></div>
        @else
            <div class="col-5"><span style="float: right">หัวข้อที่ถูกรายงาน :</span></div>
            <div class="col-7"><span>{!! $row->getname()->name !!}</span></div>
            <div class="col-5"><span style="float: right">หมวดหมู่ :</span></div>
            <div class="col-7"><span>{{$row->reports->getcategory()}}</span></div>
            <div class="col-5"><span style="float: right">หมวดหมู่ย่อย :</span></div>
            <div class="col-7"><span>{{$row->reports->getsubcategory()}}</span></div>
        @endif
        <div class="col-9 offset-1">
            <hr style="border: 1px solid #B6C3C6;">
        </div>
        @if($loop->last)
            <div class="col-6 offset-2 text-center"><span>รายละเอียด</span></div>
            <div class="col-7 offset-2" style="border: 1px solid #B6C3C6;"><span>{!!
            $row->type == 'comments'?$row->reports->message:$row->reports->details
        !!}</span></div>
            <div class="col-12">
                <div class="float-right">
                    <a class="btn" onclick="confirmcancle('{{$data[0]->idreport}}','ไม่สนใจ');" style="border-radius: 40px;color: black;border: 1px solid #B6C3C6;box-shadow: none;">ไม่สนใจ</a>
                    <a class="btn" onclick="confirmdelete('{{$data[0]->idreport}}','{{$data[0]->type}}');" style="border-radius: 40px;color: black;border: 1px solid #B6C3C6;box-shadow: none;">ยืนยันการลบ</a>
                </div>
            </div>
        @endif
    </div>

@endforeach
</div>

@endsection

<script>
    function confirmcancle(id,type) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะไม่สนใจ ?',
            text: "ข้อมูลจะยังคงอยู่ !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ยืนยัน !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleted(id,type);
            }
        })
    };

    function confirmdelete(id,type) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "ข้อมูลที่ถูกรายงานจะถูกลบ !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleted(id,type);
            }
        })
    };

    function deleted(id,type) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
                {
                    type: "get",
                    url: '{{$site.'/admin/topic/report/deleted/'}}'+id+'/'+type,
                    data:{ _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function(){ window.location.href = '{{route('topic.index')}}'; }, 1000);
                    },
                    error: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: 'error',
                            title: data.error,
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    };
</script>