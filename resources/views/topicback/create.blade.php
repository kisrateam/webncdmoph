@extends('layouts.appadmin')
@section('content')
<form method="POST" action="{{ route('result.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="container py-1">
		<!--Section: Content-->
		<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
			<!--Grid row-->
			<div class="row d-flex justify-content-center">
				<!--Grid column-->
				<div class="col-md-12">
					<!-- Default form contact -->
					<form class="text-center" action="#!">

						<!-- Send button -->
						<button class="btn btn-info btn-block" type="submit">บันทึก</button>
						<!-- Basic dropdown -->
					</form>
					<!-- Default form contact -->
				</div>
				<!--Grid column-->
			</div>
			<!--Grid row-->
		</section>
		<!--Section: Content-->
	</div>

</form>
@endsection
