@extends('layouts.appadmin')
<style>
	#btn-custom{
		border: 1px solid #508EBF;
		box-sizing: border-box;
		border-radius: 20px;
	}
	a#btn-custom.active{
		border: 1px solid #508EBF;
		color: white;
		background-color: #508EBF;
	}
</style>
@section('content')
<div class="container-fluid">
    <div class="col-12">
        <img style="width: 44px;
height: 44px;
padding-bottom: 5px;" src="{{$site}}/Defaultimg/question.svg"/>
        <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการหน้ากระดานสนทนา</span>
        <hr style="border: 1px solid #3D3D3D;">
    </div>
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a id="btn-custom" class="nav-link btn active" data-toggle="pill" href="#menu1">จัดการ กระดานสนทนา</a>
        </li>
        <li class="nav-item">
            <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu2">รายงานปัญหา</a>
        </li>
        <li class="nav-item">
            <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu3">แบนคำหยาบ</a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="menu1" class="container-fluid tab-pane active"><br>
            <?php
                $topic = \App\topics::get();
            ?>
            <table id="topic1" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ไอดี</th>
                        <th>คำถาม</th>
                        {{--<th>หมวดหมู่</th>--}}
                        {{--<th>หมวดหมู่ย่อย</th>--}}
                        <th>รหัสผู้โพส</th>
                        <th>ระดับผู้โพส</th>
                        <th>วันที่อัพเดทล่าสุด</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($topic as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->name}}</td>
                        {{--<td>{{$row->getcategory()}}</td>--}}
                        {{--<td>{{$row->getsubcategory()}}</td>--}}
                        <td>{{$row->users->name}}</td>
                        <td>{{$row->users->role}}</td>
                        <td>{{$row->updated_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="menu2" class="container-fluid tab-pane"><br>
            <?php
            $report = \App\reports::get();
            ?>
            <table id="topic2" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>ไอดี</th>
                    <th>ประเภทที่โดนรายงาน</th>
                    <th>สาเหตุรายงาน</th>
                    <th>เวลารายงาน</th>
                </tr>
                </thead>
                <tbody>
                @foreach($report as $row)
                    <tr>
                        <td>{{$row->idreport}}</td>
                        <td>{{$row->type=='comments'?'การแสดงความคิดเห็น':'กระดานสนทนา'}}</td>
                        <td>{{$row->details}}</td>
                        <td>{{$row->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="menu3" class="container-fluid tab-pane fade"><br>
            @include('topicback.badword')
        </div>
    </div>
</div>
@endsection
@include('layouts.onlyDatatable')

<script type="text/javascript">
    let id = 0;

    $(document).ready(function() {
        let table = $('#topic1').DataTable({
            lengthChange: false,
            "order": [[ 4, "desc" ]],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            buttons: [
            {{--{--}}
                    {{--text: 'เพิ่มข้อมูล',--}}
                    {{--action: function ( e, dt, node, config ) {--}}
                        {{--window.location.href = '{{url('/admin/topic/create')}}';--}}
                    {{--}--}}
                {{--},--}}
                {{--{--}}
                    {{--text: 'แก้ไขข้อมูล',--}}
                    {{--action: function ( e, dt, node, config) {--}}
                        {{--$('#topic1 .selected').each(function() {--}}
                            {{--window.location.href = '{{url('/admin/topic')}}'+'/'+id[0]+'/edit';--}}
                        {{--});--}}
                    {{--}--}}
                {{--},--}}
                {
                    text: 'ดูข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#topic1 .selected').each(function() {
                            window.location.href = '{{url('/admin/topic')}}'+'/'+id[0];
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
            // initComplete: function () {
            //     this.api().columns([2,3,4,5]).every( function () {
            //         var column = this;
            //         var select = $('<select><option value=""></option></select>')
            //             .appendTo( $(column.header() ) )
            //             .on( 'change', function () {
            //                 var val = $.fn.dataTable.util.escapeRegex(
            //                     $(this).val()
            //                 );
            //
            //                 column
            //                     .search( val ? '^'+val+'$' : '', true, false )
            //                     .draw();
            //             } );
            //
            //         column.data().unique().sort().each( function ( d, j ) {
            //             select.append( '<option value="'+d+'">'+d+'</option>' )
            //         } );
            //     } );
            // }
        });

        table.buttons().container().appendTo( '#topic1_wrapper .col-md-6:eq(0)' );

        $('#topic1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#topic1').on( 'click', 'tr', function () {
            id = table.row( this ).data();
        } );

    } );

    let id2 = 0;

    $(document).ready(function() {
        let table2 = $('#topic2').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'ดูข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#topic2 .selected').each(function() {
                            let url = '{{url('/admin/topic/report')}}'+'/'+id2[0];
                            window.open(url, '_blank');
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "ไม่พบข้อมูล",
                "sInfoFiltered":   "(ค้นหาข้อมูล _MAX_ แถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
            initComplete: function () {
                this.api().columns([1,2]).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.header() ) )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        table2.buttons().container().appendTo( '#topic2_wrapper .col-md-6:eq(0)' );

        $('#topic2 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table2.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        $('#topic2').on( 'click', 'tr', function () {
            id2 = table2.row( this ).data();
        } );

    } );
</script>