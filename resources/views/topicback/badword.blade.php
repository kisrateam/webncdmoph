<style>
    .flex-container {
        display: flex;
        flex-wrap: wrap;
        border: 1px solid grey;
        border-radius: 20px;
    }

    .flex-container > button {
        margin: 5px;
        min-width: 50px;
        text-align: center;
        line-height: 12px;
        font-size: 14px;
        border-radius: 20px;
        height: 30px;
        background-color: transparent;
        border: 1px solid black;
        box-shadow: none;
    }

    .flex-container > button:hover{
        background-color: red;
    }
</style>
<?php
$data = \App\badwords::all();
?>
<div class="container-fluid">
    <div class="row">
        <h2>แบนคำที่ไม่ต้องการให้แสดง(คลิกเพื่อลบ)</h2>
        <div class="col-9">
            <br/>
            <div class="flex-container">
                @foreach($data as $item)
                    <button tabindex="0" onclick="deletesome('{{$item->id}}');">{{$item->badword}}</button>
                @endforeach
            </div>
        </div>
        <div class="col-3">
            <form method="POST" action="{{ route('badwords.store') }}" enctype="multipart/form-data">
                @csrf
                <h5>เพิ่มคำแบน</h5>
                <div class="md-form">
                    <input type="text" id="badword" name="badword" class="form-control col-12"
                           list="badwordlist" autocomplete="off" value="{{ old('badwordlist') }}" required>
                    <label for="badword">คำที่ต้องการแบน</label>
                    <datalist id="badwordlist">
                        @foreach($data as $option)
                            <option>{{$option->badword}}</option>
                        @endforeach
                    </datalist>
                </div>
                <button class="btn col-12">เพิ่ม</button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function deletesome(id) {
        var _token = $('input[name="_token"]').val();
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $.ajax(
                    {
                        type: "DELETE",
                        url: "{{ route('badwords.store') }}"+'/'+id,
                        data:{ _token:_token  },
                        success: function (data) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'ข้อมูลถูกลบแล้ว',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            setTimeout(function(){ window.location.reload(); }, 1000);
                        },
                        error: function (data) {
                            console.log(data);
                            Swal.fire({
                                icon: 'error',
                                title: 'อุ๊บบ...',
                                text: 'มีบางอย่างผิดพลาด !'
                            })
                        }
                    });
            }
        })

    }
</script>
