@extends('layouts.appadmin')
@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Success: </strong>{{ Session::get('success') }}
    </div>
@endif
@if(Session::has('error'))
    <div class="alert alert-error">
        <strong>error: </strong>{{ Session::get('error') }}
    </div>
@endif
@section('content')
	<div class="container-fluid">
<form method="POST" action="{{ route('eng.store') }}" enctype="multipart/form-data">
    @csrf
    <input value="3" type="text" name="catindex" style="display: none">
    <div class="row">
        <div class="col-12">
            <h3>การจัดการภาษาอังกฤษผลการดำเนินงาน</h3>
        </div>
        <div class="col-12">
            <hr style="border: 1px solid #B6C3C6;">
        </div>
        <div class="md-form">
            <div class="custom-file" style="display:none;">
                <i role="presentation"  class="far fa-images prefix"></i>
                <input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
                <label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ</label>
            </div>
        </div>
        <script type="text/javascript">
            window.preview = function (input) {
                if (input.files && input.files[0]) {
                    $("#previewImg").html("");
                    $(input.files).each(function () {
                        var reader = new FileReader();
                        reader.readAsDataURL(this);
                        reader.onload = function (e) {
                            $("#previewImg").append("<img class='thumb' style=\"max-height: 400px;max-width: 400px;\" src='" + e.target.result + "'>");
                        }
                    });
                    document.getElementById("uploadimg").style.backgroundImage = 'none';
                }
            }
        </script>
        <div class="col-3" align="right">
            <label style="font-size: 16px">อัพโหลดภาพ*</label>
            <p style="font-size: 12px;color: #01673F;">*รองรับ JPEG, PNG</p>
            <p style="font-size: 12px;color: #01673F;">*ขนาดไฟล์ไม่เกิน 1000*750 px(4:3)</p>
        </div>
        <div class="col-6" align="center">
            @if($img == null)
                <label id="uploadimg" for="img" style="
                        min-width: 100%;
                        min-height: 320px;
                        background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');
                        background-size:100% 100%;
                        cursor: pointer">
                    <span id="previewImg"></span>
                </label>
            @else
                <label id="uploadimg" for="img" style="
                        min-width: 100%;
                        min-height: 320px;
                        height: auto;
                        background-image:url('{{ asset('/uploads/'.$img->path) }}');
                        background-size:100% 100%;
                        cursor: pointer">
                    <span id="previewImg"></span>
                </label>
            @endif
        </div>
        <div class="col-1 offset-9">
            <div class="md-form" style="margin: 0">
                <a style="background: white;border:1px solid #3D3D3D ;border-radius: 20px;color: black;box-shadow: none;outline: none" onclick="deleteimg('{{$img != null ? $img->idfk:''}}');" class="btn btn-block">ลบ</a>
            </div>
        </div>
        <div class="col-2">
            <div class="md-form" style="margin: 0">
                <button style="background: #01673F;border-radius: 20px;color: white;box-shadow: none;outline: none" type="submit" class="btn btn-block">บันทึก</button>
            </div>
        </div>
    </div>
</form>
<table id="example" class="table table-striped table-bordered" style="width:100%">
		<thead>
		<tr>
			<th>ไอดี</th>
			<th>ชื่อ</th>
			<th>แสดงในหน้าหลัก</th>
			<th>แสดงบนไฮไลท์</th>
			<th>วันที่อัพเดทล่าสุด</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{$row->id}}</td>
				<td>{{$row->name}}</td>
                <td>{!! $row->active == 1 ? '<i role="presentation"  class="fas fa-check"></i>':'<i role="presentation"  class="fas fa-times"></i>' !!}</td>
                <td>{!! $row->highlights == 1 ? '<i role="presentation"  class="fas fa-check"></i>':'<i role="presentation"  class="fas fa-times"></i>' !!}</td>
				<td>{{$row->updated_at}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
    </div>
@endsection

@include('layouts.onlyDatatable')

<script type="text/javascript">
  var id = 0;

  $(document).ready(function() {
    var table = $('#example').DataTable({
      lengthChange: false,
      columnDefs: [
        { width: 55, targets: 1 },
        {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
        },
      ],
      "pageLength": 5,
      "pagingType": "full_numbers",
      buttons: [
          {
          text: 'เพิ่มข้อมูล',
          action: function ( e, dt, node, config ) {
            window.location.href = './eng/create';
          }
        },
        {
          text: 'แก้ไขข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              window.location.href = './eng/'+id[0]+'/edit';
            });
          }
        },
        {
          text: 'ลบข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              Swal.fire({
                title: 'คุณแน่ใจที่จะลบ ?',
                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่ ลบเลย !',
                cancelButtonText : 'ยกเลิก'
              }).then((result) => {
                if (result.value) {
                  goDelete();
                  table.row('.selected').remove().draw( false );
                }
              })
            });
          }
        }
      ],
      responsive: true,
      "language": {
        "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
        "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
        "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
        "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "แสดง _MENU_ แถว",
        "sLoadingRecords": "กำลังโหลดข้อมูล...",
        "sProcessing":     "กำลังดำเนินการ...",
        "sSearch":         "ค้นหา: ",
        "sZeroRecords":    "ไม่พบข้อมูล",
        "oPaginate": {
          "sFirst":    "หน้าแรก",
          "sPrevious": "ก่อนหน้า",
          "sNext":     "ถัดไป",
          "sLast":     "หน้าสุดท้าย"
        },
        "oAria": {
          "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
          "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
        }
      },
    });

    table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );

    $('#example tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } );

    $('#example').on( 'click', 'tr', function () {
      id = table.row( this ).data();
    } );

  } );

  function goDelete() {
    var _token = $('input[name="_token"]').val();
    $.ajax(
      {
        type: "DELETE",
        url: "{{ route('eng.store') }}"+'/'+id[0],
        data:{ _token:_token  },
        success: function (data) {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'ข้อมูลถูกลบแล้ว',
            showConfirmButton: false,
            timer: 3000
          })
        },
        error: function (data) {
          console.log(data);
          Swal.fire({
            icon: 'error',
            title: 'อุ๊บบ...',
            text: 'มีบางอย่างผิดพลาด !'
          })
        }
      });
  }

  function deleteimg(id) {
      var _token = $('input[name="_token"]').val();
      $.ajax(
          {
              type: "DELETE",
              url: "{{ route('eng.store') }}"+'/'+id,
              data:{ _token:_token },
              success: function (data) {
                  console.log(data);
                  Swal.fire({
                      position: 'top-end',
                      icon: 'success',
                      title: 'ข้อมูลถูกลบแล้ว',
                      showConfirmButton: false,
                      timer: 3000
                  });
                  setTimeout(window.location.reload(),2000);
              },
              error: function (data) {
                  console.log(data);
                  Swal.fire({
                      icon: 'error',
                      title: 'อุ๊บบ...',
                      text: 'มีบางอย่างผิดพลาด !'
                  })
              }
          });
  }
</script>
