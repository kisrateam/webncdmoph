@extends('layouts.appadmin')
@section('content')
    <?php
        $arr = [
            'แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ.2560-2564)',
            'นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูง',
            'ผลการดำเนินงาน',
            'อื่นๆ หรือ ข่าวสารเผยแพร่'
        ];
        switch(substr (url()->previous(), -4)){
            case 'eng1' : {
                $name = $arr[0];
                $catindex = 1;
                break;
            }
            case 'eng2' : {
                $name = $arr[1];
                $catindex = 2;
                break;
            }
            case 'eng3' : {
                $name = $arr[2];
                $catindex = 3;
                break;
            }
            case 'eng4' : {
                $name = $arr[3];
                $catindex = 4;
                break;
            }
            default : {
                $name = '';
                $catindex = 5;
			}
        }
    ?>
	<form method="POST" action="{{ route('eng.store') }}" enctype="multipart/form-data">
		@csrf
		@if($catindex == 5)
			<script>
				window.location = "{{ route('admin') }}";
			</script>
		@endif
		<input type="text" name="catindex" value="{{ $catindex }}" style="display: none;">
		<div class="col-12">
			<h3>การจัดการภาษาอังกฤษ {{$name}}</h3>
		</div>
		<div class="col-12">
			<hr style="border: 1px solid #B6C3C6;">
		</div>
		<div class="container-fluid py-1">
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<div class="row d-flex justify-content-center">
					<div class="col-md-6">
						<div class="col-12">
							<span>แสดงในหน้าหลัก</span>
							<input id="active" name="active" type="checkbox" data-toggle="toggle"
								   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
						</div>
						<div class="col-12">
							<span>แสดงในไฮไลท์</span>
							<input id="highlights" name="highlights" type="checkbox" data-toggle="toggle"
								   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
						</div>
						<div class="md-form">
							<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">ชื่อไฟล์*</label>
						</div>
						<div class="md-form">
							<input type="text" id="fileuniq" name="fileuniq" value="{{ old('fileuniq') }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">Link Url</label>
						</div>
						<div class="md-form">
							<div align="center">
								ตัวอย่างรูป(สำหรับขึ้น Highlights)
								<div id="previewImg"></div>
							</div>
							<div class="custom-file">
								<i role="presentation"  class="far fa-images prefix"></i>
								<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="imghl" onchange="preview(this);">
								<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
							</div>
						</div>
						<script type="text/javascript">
                            window.preview = function (input) {
                                if (input.files && input.files[0]) {
                                    $("#previewImg").html("");
                                    $(input.files).each(function () {
                                        var reader = new FileReader();
                                        reader.readAsDataURL(this);
                                        reader.onload = function (e) {
                                            $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                                        }
                                    });
                                }
                            }
						</script>
						<button class="btn btn-info btn-block" type="submit">บันทึก</button>
					</div>
				</div>
			</section>
		</div>
	</form>
@endsection
