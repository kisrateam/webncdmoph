@extends('layouts.appadmin')
@section('content')
	<form method="POST" action=" {{route('eng.update',$data->id)}} " enctype="multipart/form-data">
		@csrf
		<div class="col-12">
			<h3>การจัดการภาษาอังกฤษ {{$data->category}}</h3>
		</div>
		<div class="col-12">
			<hr style="border: 1px solid #B6C3C6;">
		</div>
		<div class="container-fluid py-1">
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<div class="row d-flex justify-content-center">
					<div class="col-md-6">
						<div class="col-12">
							<span>แสดงในหน้าหลัก</span>
							<input id="active" name="active" type="checkbox" data-toggle="toggle" {{$data->active == 1 ? 'checked':''}}
								   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
						</div>
						<div class="col-12">
							<span>แสดงในไฮไลท์</span>
							<input id="highlights" name="highlights" type="checkbox" data-toggle="toggle" {{$data->highlights == 1 ? 'checked':''}}
								   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
						</div>
						<div class="md-form">
							<input type="text" id="name" name="name" value="{{ $data->name }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">ชื่อไฟล์*</label>
						</div>
						<div class="md-form">
							<input type="text" id="fileuniq" name="fileuniq" value="{{ json_decode($data->fileuniq)[0] }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">Link Url</label>
						</div>
						<div class="md-form">
							@if(json_decode($data->fileuniq)[1] !== '')
								<div align="center">
									ตัวอย่างรูปเก่า
								</div>
								<div align="center">
									<img class='thumb' style="max-height: 100px;max-width: 100px;" src="{{$site}}/uploads/{{json_decode($data->fileuniq)[1]}}">
								</div>
							@endif
							<div align="center">
								ตัวอย่างรูป(สำหรับขึ้น Highlights)
								<div id="previewImg"></div>
							</div>
							<div class="custom-file">
								<i role="presentation"  class="far fa-images prefix"></i>
								<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="imghl" onchange="preview(this);">
								<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
							</div>
						</div>
						<script type="text/javascript">
                            window.preview = function (input) {
                                if (input.files && input.files[0]) {
                                    $("#previewImg").html("");
                                    $(input.files).each(function () {
                                        var reader = new FileReader();
                                        reader.readAsDataURL(this);
                                        reader.onload = function (e) {
                                            $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                                        }
                                    });
                                }
                            }
						</script>
						<button class="btn btn-info btn-block" type="submit">บันทึก</button>
					</div>
				</div>
			</section>
			{{method_field('PUT')}}
		</div>
	</form>
@endsection
