@extends('layouts.appadmin')
@if(Session::has('success'))
  <div class="alert alert-success">
    <strong>Success: </strong>{{ Session::get('success') }}
  </div>
@endif
@if(Session::has('error'))
  <div class="alert alert-error">
    <strong>error: </strong>{{ Session::get('error') }}
  </div>
@endif
@section('content')
	<div class="container-fluid">
    <div class="col-12">
        <img style="width: 44px;
height: 44px;
padding-bottom: 5px;" src="{{$site}}/Defaultimg/login-email.svg"/>
        <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการผู้ใช้งาน</span>
        <hr style="border: 1px solid #3D3D3D;">
    </div>

	<form method="get">
		@csrf
	</form>
	<table id="example" class="table table-striped table-bordered" style="width:100%">
		<thead>
		<tr class="text-center">
			<th>ไอดี</th>
          <th>ชื่อผู้ใช้งาน</th>
          <th>สิทธิ์</th>
          <th>อีเมลล์</th>
          <th>สถานะ</th>
          <th>เปลี่ยนสิทธิ์</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data as $row)
			<tr class="text-center">
				<td>{{$row->id}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->role}}</td>
                <td>{{$row->email}}</td>
                <td style="color: green">ใช้งานได้</td>
                <td>
                <button tabindex="0" onclick="changerole()" class="btn btn-sm btn-outline-danger waves-effect">เปลี่ยนสิทธิ์</button>
                </td>
			</tr>
		@endforeach
        @foreach($datawithDelete as $row)
			<tr class="text-center">
				<td>{{$row->id}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->role}}</td>
                <td>{{$row->email}}</td>
                <td style="color: red">ถูกระงับ</td>
                <td></td>
			</tr>
		@endforeach
		</tbody>
	</table>
    </div>
@endsection

<style>
    #togglePassword{
        position: absolute;
        right: 12%;
        top: 50%;
        cursor: pointer;
        z-index: 5;
    }
</style>

@include('layouts.onlyDatatable')

<script type="text/javascript">
  var id = 0;
  var _token = $('input[name="_token"]').val();

  function changerole(){
    Swal.mixin({
      confirmButtonText: 'ยืนยัน &rarr;',
      showCancelButton: true,
      progressSteps: ['1','2'],
    }).queue([
      {
          html:'<input type="password" class="form-control input-field" placeholder="รหัสผ่าน" aria-label="รหัสผ่าน" id="password" minlength="7" name="password" required>\n' +
          '<i role="presentation"  class="far fa-eye" id="togglePassword"></i>',
          focusConfirm: false,
          preConfirm: () => {
              return document.getElementById('password').value
          },
        title: 'ยืนยันรหัสผ่าน'
      },
      {
        input: 'select',
          inputOptions: {
            'user': 'user',
            'member': 'member',
            'admin': 'admin'
          },
        title: 'เลือกสิทธิ์'
      },
    ]).then((result) => {
      if (result.value) {
          $.ajax(
            {
              type: "GET",
              url: "{{$site}}/userrole/changerole/"+id[0]+"/"+result.value[0]+"/"+result.value[1],
              data:{ _token:_token,id:id[0],password:result.value[0],role:result.value[1] },
              success: function (data) {
                Swal.fire({
                  title: 'ผู้ใช้งาน '+data.item.name,
                  text: 'ถูกเปลี่ยนสิทธิ์เป็น '+data.item.role,
                  showConfirmButton:true
                }).then((result) => {
                  if (result.value) {
                    location.reload();
                  }
                })
              },
              error: function() {
                Swal.fire({
                  icon: 'error',
                  title: 'อุ๊บ...',
                  text: 'รหัสผ่านไม่ถูกต้อง!',
                })
            }
        });
      }
    });

    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');
    togglePassword.addEventListener('click', function (e) {
      const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
      password.setAttribute('type', type);
      this.classList.toggle('fa-eye-slash');
    });
  }

  $(document).ready(function() {
    var table = $('#example').DataTable({
      lengthChange: false,
      columnDefs: [
        {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
        },
      ],
        "order": [[ 4, "desc" ]],
      "pageLength": 10,
      "pagingType": "full_numbers",
      buttons: [
        {
          text: 'ลบข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              Swal.fire({
                title: 'คุณแน่ใจที่จะลบ ?',
                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่ ลบเลย !',
                cancelButtonText : 'ยกเลิก'
              }).then((result) => {
                if (result.value) {
                  goDelete();
                  // table.row('.selected').remove().draw( false );
                }
              })
            });
          }
        },{
          text: 'กู้ข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              Swal.fire({
                title: 'คุณแน่ใจที่จะกู้ข้อมูลผู้ใช้งาน ?',
                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่ กู้เลย !',
                cancelButtonText : 'ยกเลิก'
              }).then((result) => {
                if (result.value) {
                    goRestore();
                    // table.row('.selected').remove().draw( false );
                }
              })
            });
          }
        }
      ],
      responsive: true,
      "language": {
        "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
        "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
        "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
        "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "แสดง _MENU_ แถว",
        "sLoadingRecords": "กำลังโหลดข้อมูล...",
        "sProcessing":     "กำลังดำเนินการ...",
        "sSearch":         "ค้นหา: ",
        "sZeroRecords":    "ไม่พบข้อมูล",
        "oPaginate": {
          "sFirst":    "หน้าแรก",
          "sPrevious": "ก่อนหน้า",
          "sNext":     "ถัดไป",
          "sLast":     "หน้าสุดท้าย"
        },
        "oAria": {
          "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
          "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
        }
      },
    });

    table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );

    $('#example tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } );

    $('#example').on( 'click', 'tr', function () {
      id = table.row( this ).data();
    } );

  } );

  function goDelete() {
    $.ajax(
      {
        type: "GET",
        url: "{{$site}}/userrole/delete/"+id[0],
        data:{ _token:_token,id:id[0] },
        success: function (data) {
          if(data['success'] !== undefined){
              Swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: 'ผู้ใช้งานถูกลบแล้ว',
                  showConfirmButton: false,
                  timer: 3000
              });
          }else if(data['error'] !== undefined){
              Swal.fire({
                  icon: 'error',
                  title: 'อุ๊บบ...',
                  text: 'มีบางอย่างผิดพลาด !',
                  timer: 3000
              })
          }
          location.reload();
        }
      });
  }

  function goRestore() {
    $.ajax(
      {
        type: "GET",
        url: "{{$site}}/userrole/restore/"+id[0],
        data:{ _token:_token,id:id[0] },
        success: function (data) {
          if(data['success'] !== undefined){
              Swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: 'ผู้ใช้งานสามารถใช้งานได้แล้ว',
                  showConfirmButton: false,
                  timer: 3000
              });
          }else if(data['error'] !== undefined){
              Swal.fire({
                  icon: 'error',
                  title: 'อุ๊บบ...',
                  text: 'มีบางอย่างผิดพลาด !',
                  timer: 3000
              })
          }
          location.reload();
        }
      });
  }
</script>
