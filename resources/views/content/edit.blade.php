@extends('layouts.appadmin')
@section('content')
@include('layouts.onlyDatatable')
<style>

</style>
	<form method="POST" action=" {{route('contents.update',$model->id)}}" enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-12">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">แก้ไขข้อมูลแผนการป้องกัน</h3>
							<div class="row">
								<div class="col-4">
									<span>แสดงในหน้าหลัก</span>
									<input id="active" name="active" type="checkbox" data-toggle="toggle"
									       data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow" {{$model->active == 1 ? 'checked':''}}/>
								</div>
							</div>
							<!-- Name -->
							<div class="md-form">
								<i role="presentation"  class="fas fa-heading prefix"></i>
								<input type="text" id="name" name="name" value="{{$model->name}}" class="form-control col-md-12" autocomplete="off" required>
								<label for="name">ชื่อ</label>
							</div>
							<select class="browser-default custom-select" name="category">
								<option selected disabled hidden>ประเภทข้อมูล</option>
								<option {{$model->category == 0 ? "selected":''}} value="0">2560 - 2564</option>
								<option {{$model->category == 1 ? "selected":''}} value="1">ฉบับปรับปรุง</option>
								<option {{$model->category == 2 ? "selected":''}} value="2">นโยบายเร่งรัด</option>
								<option {{$model->category == 6 ? "selected":''}} value="6">แผนปฏิรูปด้าน NCDs</option>
							</select>
							<div class="md-form">
								<div class="custom-file">
									<input type="text" id="file" name="file" value="{{$model->fileuniq}}" class="form-control col-md-12" autocomplete="off">
									<label for="file">ลิ้งไฟล์</label>
								</div>
								<script type="text/javascript">
                  function readURL(input) {
                    if (input.files && input.files[0]) {
                      var reader = new FileReader();
                      reader.onload = function (e) {
                        $('#profile-img-tag').attr('src', e.target.result);
                      };
                      reader.readAsDataURL(input.files[0]);
                    }
                  }
                  $("#img").change(function(){
                    readURL(this);
                  });
								</script>
								<!-- Send button -->
								{{ method_field('PUT') }}
							</div>
							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection
