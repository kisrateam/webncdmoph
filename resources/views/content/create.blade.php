@extends('layouts.appadmin')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
@include('layouts.onlyDatatable')
<style>

</style>
<form method="POST" action="{{ route('contents.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="container py-1">
	<!--Section: Content-->
	<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
		<!--Grid row-->
		<div class="row d-flex justify-content-center">
			<!--Grid column-->
			<div class="col-md-12">
				<!-- Default form contact -->
				<form class="text-center" action="#!">
					<h3 align="center" class="font-weight-bold">เพิ่มข้อมูลแผนการป้องกัน</h3>
						<div class="row">
							<div class="col-4">
								<span>แสดงในหน้าหลัก</span>
								<input id="active" name="active" type="checkbox" data-toggle="toggle"
									   data-on="แสดง" data-off="ไม่แสดง" data-onstyle="success" data-offstyle="danger" data-style="slow"/>
							</div>
						</div>
						<div class="md-form">
							<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
							<label for="name">ชื่อ</label>
						</div>
						<select class="browser-default custom-select" name="category">
							<option selected disabled hidden>ประเภทข้อมูล</option>
							<option value="0">2560 - 2564</option>
							<option value="1">ฉบับปรับปรุง</option>
							<option value="2">นโยบายเร่งรัด</option>
							<option value="6">แผนปฏิรูปด้าน NCDs</option>
						</select>
						<div class="md-form">
							<div class="custom-file">
								<input type="text" id="file" name="file" value="{{ old('file') }}" class="form-control col-md-12" autocomplete="off">
								<label for="file">ลิ้งไฟล์</label>
							</div>
						<!-- Send button -->
							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
						</div>
				</form>
				<!-- Default form contact -->
			</div>
			<!--Grid column-->
		</div>
		<!--Grid row-->
	</section>
	<!--Section: Content-->
</div>
</form>
@endsection
