<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
@include('layouts.onlyDatatable')
<form method="POST" action=" {{route('contents.store')}}" enctype="multipart/form-data">
    @csrf
    <input style="display: none" value="1" name="category">
    <div class="row">
        <div class="col-5 offset-1" align="center">
            @if($data['img1'] == null || $data['img1']->path == "null")
                <label id="uploadimg1" for="img1" style="
                        min-width: 100%;
                        min-height: 320px;
                        background-image:url('{{ asset('Defaultimg/imgupload.svg') }}');
                        background-size:100% 100%;
                        cursor: pointer">
                    <span id="previewImg1"></span>
                </label>
            @else
                <label id="uploadimg1" for="img1" style="
                        min-width: 100%;
                        min-height: 320px;
                        height: auto;
                        background-image:url('{{ asset('/uploads/'.$data['img1']->path) }}');
                        background-size:100% 100%;
                        cursor: pointer">
                    <span id="previewImg1"></span>
                </label>
            @endif
        </div>
        <div class="col-6">
            <h3>ลิ้งดาวน์โหลดทั้งหมด</h3>
            <div class="md-form">
                <input type="text" id="downloadall" name="downloadall" value="{{$data['dall2']!=null&&$data['dall2']->fileuniq!=null ? $data['dall2']->fileuniq:''}}" class="form-control col-12" autocomplete="off">
                <label for="downloadall">ลิ้ง URL</label>
            </div>
            <h3>คำอธิบาย*:</h3>
            <textarea id="details2" class="details" name="detailbanner">{{$data['img1']!=null&&$data['img1']->name!=null ? $data['img1']->name:''}}
                        </textarea>
            <script>
                var CSRFToken = $('meta[name="csrf-token"]').attr('content');
                var options = {
                    filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
                    filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
                };
            </script>
            <script>
                CKEDITOR.replace('details2', options);
            </script>
            <h4>อัพโหลดไฟล์*:
                <div class="float-right">
                    <span onclick="confirmdelete('{{$data['img1']!=null&&$data['img1']->id!=null?$data['img1']->id : null}}');" class="btn btn-outline-dark-green btn-sm" style="border-radius: 20px;">ลบ</span>
                    <button type="submit" class="btn btn-outline-dark-green btn-sm" style="border-radius: 20px;">อัพโหลด</button>
                </div>
            </h4>
            <h6 style="color:#203F54">*รองรับ JPEG, PNG</h6>
            <h6 style="color:#203F54">*ขนาดไฟล์ไม่เกิน 378*522 px(A4)</h6>
        </div>
    </div>
    <input onchange="preview1(this);" style="display: none" type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img1" name="img" accept="image/*">
    <script type="text/javascript">
        window.preview1 = function (input) {
            if (input.files && input.files[0]) {
                $("#previewImg1").html("");
                $(input.files).each(function () {
                    var reader = new FileReader();
                    reader.readAsDataURL(this);
                    reader.onload = function (e) {
                        $("#previewImg1").append("<img class='thumb' style=\"max-height: 330px;max-width: 100%;\" src='" + e.target.result + "'>");
                    }
                });
                document.getElementById("uploadimg1").style.backgroundImage = 'none';
            }
        };
    </script>
</form>
<br/>
<table id="contenttable2" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>ไอดี</th>
        <th>ชื่อหัวข้อ</th>
        <th>ไฟล์</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['data1'] as $row)
        <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->name}}</td>
            <td>
                @if($row->fileuniq === null)
                    ไม่พบลิ้ง
                @else
                    <a href="{{$row->fileuniq}}" target="_blank">{{$row->fileuniq}}</a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script type="text/javascript">
    let id1 = 0;
    $(document).ready(function() {
        let table = $('#contenttable2').DataTable({
            lengthChange: false,
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
            ],
            "pageLength": 5,
            "pagingType": "full_numbers",
            buttons: [
                {
                    text: 'เพิ่มข้อมูล',
                    action: function ( e, dt, node, config ) {
                        window.location.href = "{{route('contents.create')}}";
                    },
                },
                {
                    text: 'แก้ไขข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#contenttable2 .selected').each(function() {
                            window.location.href = './contents/'+id1[0]+'/edit';
                        });
                    }
                },
                {
                    text: 'ลบข้อมูล',
                    action: function ( e, dt, node, config) {
                        $('#contenttable2 .selected').each(function() {
                            Swal.fire({
                                title: 'คุณแน่ใจที่จะลบ ?',
                                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'ใช่ ลบเลย !',
                                cancelButtonText : 'ยกเลิก'
                            }).then((result) => {
                                if (result.value) {
                                    goDelete();
                                    table.row('.selected').remove().draw( false );
                                }
                            })
                        });
                    }
                }
            ],
            responsive: true,
            "language": {
                "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "แสดง _MENU_ แถว",
                "sLoadingRecords": "กำลังโหลดข้อมูล...",
                "sProcessing":     "กำลังดำเนินการ...",
                "sSearch":         "ค้นหา: ",
                "sZeroRecords":    "ไม่พบข้อมูล",
                "oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
                },
                "oAria": {
                    "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                    "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                }
            },
        });
        table.buttons().container().appendTo( '#contenttable2_wrapper .col-md-6:eq(0)' );
        $('#contenttable2 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
        $('#contenttable2').on( 'click', 'tr', function () {
            id1 = table.row( this ).data();
        } );

        function goDelete() {
            let _token = $('input[name="_token"]').val();
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{ route('contents.store') }}"+'/'+id1[0],
                    data:{ _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        })
                    },
                    error: function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'อุ๊บบ...',
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    } );



    function confirmdelete(id) {
        Swal.fire({
            title: 'คุณแน่ใจที่จะลบ ?',
            text: "รูปและคำอธิบายจะหายไป !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่ ลบเลย !',
            cancelButtonText : 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                deleteimg(id);
            }
        })
    };

    function deleteimg(id) {
        let _token = $('input[name="_token"]').val();
        if(id){
            $.ajax(
                {
                    type: "DELETE",
                    url: "{{$site}}/admin/contentsimg/"+id,
                    data:{ _token:_token  },
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'ข้อมูลถูกลบแล้ว',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function(){ window.location.reload(); }, 1000);
                    },
                    error: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: 'error',
                            title: data.error,
                            text: 'มีบางอย่างผิดพลาด !'
                        })
                    }
                });
        }
    };
</script>