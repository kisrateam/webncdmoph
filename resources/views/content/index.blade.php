@extends('layouts.appadmin')
@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Success: </strong>{{ Session::get('success') }}
    </div>
@endif
@if(Session::has('error'))
    <div class="alert alert-error">
        <strong>error: </strong>{{ Session::get('error') }}
    </div>
@endif
<style>
    #btn-custom{
        border: 1px solid #508EBF;
        box-sizing: border-box;
        border-radius: 20px;
    }
    #btn-custom.active{
        border: 1px solid #508EBF;
        color: white;
        background-color: #508EBF;
    }
    .green-border-focus .form-control:focus {
        border: 1px solid #8bc34a;
        box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
    }
</style>
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <img alt="การจัดการแผนการป้องกัน" style="width: 44px;
      height: 44px;
      padding-bottom: 5px;" src="{{$site}}/Defaultimg/3.svg"/>
        <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการแผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (2560 - 2564)</span>
      </div>
    </div>
    <hr style="border: 1px solid #B6C3C6;">
    <ul class="nav nav-pills" style="margin-bottom: 30px">
      <li class="nav-item">
        <a id="btn-custom" class="nav-link btn active" data-toggle="pill" href="#menu1">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (2560 - 2564)</a>
      </li>
      <li class="nav-item">
        <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu2">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (ฉบับปรับปรุง)</a>
      </li>
      <li class="nav-item">
        <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu3">นโยบายเร่งรัดการจัดการปัญหาความดันโลหิตสูง</a>
      </li>
        <li class="nav-item">
        <a id="btn-custom" class="nav-link btn" data-toggle="pill" href="#menu4">แผนปฏิรูปด้าน NCDs</a>
      </li>
    </ul>

    <div class="tab-content">
      <div id="menu1" class="container-fluid tab-pane active"><br>
        @include('content.sub1')
      </div>
      <div id="menu2" class="container-fluid tab-pane fade"><br>
        @include('content.sub2')
      </div>
      <div id="menu3" class="container-fluid tab-pane fade"><br>
        @include('content.sub3')
      </div>
        <div id="menu4" class="container-fluid tab-pane fade"><br>
        @include('content.sub4')
        </div>
    </div>
</div>
@endsection
