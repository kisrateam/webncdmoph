<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
	* {
		margin: 0;
	}
	.wrapper {
		min-height: 100%;
		height: auto !important;
		height: 100%;
		margin: 0 auto -30px;
	}
	.footer {
		height:30px;
	}

	footer{
		background-color: #f2f2f2;
	}

	footer2{
		background-color: #f2f2f2;
	}

	#policy:hover {
		color: #ffc107 !important;
	}

	ul.list-unstyled li a:hover{
		color: rgb(230, 80, 147) !important;
	}
</style>
<br>
<br>
<br>
<footer id="footer" class="page-footer bottom">
 <div class="container">
	<div style="padding: 0 1% 2% 3%" class="container-fluid text-center text-md-left">
	<div class="row">
		<div class="col-6 col-md-2 mb-md-0 mb-3">
			<ul class="list-unstyled">
				<li style="font-size:1.2rem;font-weight: bold;margin-bottom: 10px">กลุ่มโรค</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/disease.php" target="_blank">รายชื่อโรค</a>
				</li>
			</ul>
		</div>
		<div class="col-6 col-md-3 mb-md-0 mb-3">
			<ul class="list-unstyled">
				<li style="font-weight: bold;margin-bottom: 10px;font-size:1.2rem">ข่าวสาร</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/newsmass.php" target="_blank">ข่าวเพื่อสื่อมวลชน</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/newshr.php" target="_blank">ข่าวรับสมัครบัคลากร</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/newspic.php" target="_blank">ข่าวประชาสัมพันธ์</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/newsvdo.php" target="_blank">วิดีโอข่าวประชาสัมพันธ์</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/newsactivity.php" target="_blank">ข่าวกิจกรรม</a>
				</li>
			</ul>
		</div>
		<div class="col-6 col-md-3 mb-md-0 mb-3">
			<ul class="list-unstyled">
				<li style="font-size:1.2rem;font-weight: bold;margin-bottom: 10px">เกี่ยวกับกรม</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/history.php" target="_blank">ประวัติกรม</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/organization.php" target="_blank">โครงสร้างกรม</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/executive.php" target="_blank">ผู้บริการกรมควบคุมโรค</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/uploads/ckeditor2/ethg/file/ministerial_ddc.pdf" target="_blank">กฏกระทรวงการแบ่งส่วนราชการ</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/index.php" target="_blank">คำรับรอง</a>
				</li>
				<li style="color: black;">
					<a role="button" style="color: #5d5d5d;font-size:1rem" href="https://ddc.moph.go.th/contact.php" target="_blank">ติดต่อกรม</a>
				</li>
			</ul>
		</div>
		<div class="col-6 col-md-2 mb-md-0 mb-3">
			<ul class="list-unstyled">
				<li>
					<a role="button" style="font-size:1.2rem;font-weight: bold;color: #5d5d5d;" href="http://old.ddc.moph.go.th/complaint/index.php" target="_blank">รับเรื่องร้องเรียน</a>
				</li>
			</ul>
		</div>
		<div class="col-6 col-md-2 mb-md-0 mb-3">
			<ul class="list-unstyled">
				<li>
					<a role="button" style="font-size:1.2rem;font-weight: bold;color: #5d5d5d;" href="tel:1422">สายด่วน 1422</a>
				</li>
			</ul>
		</div>
	</div>
		<hr style="border: 1px solid rgba(0,0,0,.1)">
		<div class="row">
			<div class="col-12" style="margin-bottom: 10px">
				<span style="font-size: 1.2rem;font-weight: bold">กองโรคไม่ติดต่อ</span>
				<div class="float-right">
					<div class="col" style="margin: 2px">
						<a role="button" href="https://www.facebook.com/%E0%B8%81%E0%B8%A3%E0%B8%A1%E0%B8%84%E0%B8%A7%E0%B8%9A%E0%B8%84%E0%B8%B8%E0%B8%A1%E0%B9%82%E0%B8%A3%E0%B8%84-%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B8%97%E0%B8%A3%E0%B8%A7%E0%B8%87%E0%B8%AA%E0%B8%B2%E0%B8%98%E0%B8%B2%E0%B8%A3%E0%B8%93%E0%B8%AA%E0%B8%B8%E0%B8%82-470988516420706" target="_blank" title="Facebook" alt="Facebook" style="text-decoration: none">
							<img src="{{$site}}/Defaultimg/facebook.png" width="30px" height="30px" alt="facebook">
						</a>
						<a role="button" href="http://line.me/ti/p/%40bzg3674m" target="_blank" title="line" alt="line" style="text-decoration: none">
							<img src="{{$site}}/Defaultimg/line.png" width="30px" height="30px" alt="line">
						</a>
					</div>
					<div class="col" style="margin: 2px">
						<a role="button" href="https://www.youtube.com/channel/UCbewflo59uOdLGFUh71DPiQ" target="_blank" title="youtube" alt="youtube" style="text-decoration: none">
							<img src="{{$site}}/Defaultimg/youtube.png" width="30px" height="30px" alt="youtube">
						</a>
						<a role="button" href="https://twitter.com/ddc_riskcom" target="_blank" title="instagram" alt="instagram" style="text-decoration: none">
							<img src="{{$site}}/Defaultimg/twitter.png" width="30px" height="30px" alt="twitter">
						</a>
					</div>
					<div class="col" style="text-align: right">
						<img src="http://guestscounter.com/count.php?c_style=3&id=1603704975" border="0">
					</div>
				</div>
				<span style="font-size: 16px;"><br/>ที่อยู่ : 88/21 ถนน ติวานนท์ ตำบลตลาดขวัญ อำเภอเมือง จังหวัดนนทบุรี 11000<br/>โทรศัพท์ 0 2590 3893<br/>Email : ddc.moph@ddc.mail.go.th</span>
			</div>
		</div>

	</div>
</div>
		<div id="copyright" class="footer-copyright">
			<div class="container">
				<div class="row">
					<div class="col" style="padding: 10px 40px">
						<span style="color: white;font-size: 18px">© สงวนลิขสิทธิ์ 2562 : กรมควบคุมโรค.</span>
					</div>
					<div class="float-right" style="padding: 10px 40px">
						<a role="button" id="policy" style="font-size: 18px;" href="https://ddc.moph.go.th/policy.php">นโยบายเว็บไซต์</a>
					</div>
				</div>
			</div>
		</div>
</footer>
