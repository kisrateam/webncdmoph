<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('MDB/css/bootstrap.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('MDB/css/mdb.min.css') }}">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="{{ URL::asset('MDB/css/style.css') }}">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ URL::asset('MDB/css/simple-sidebar.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Datatable -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.3/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style>
    html { height: 100%; width: 100%;scroll-behavior: smooth;}
    body { height: 100%; width: 100%;}
	#container {
		position: relative;
		box-sizing: border-box;
	}
    #totop {
          display: none;
          position: fixed;
          bottom: 20px;
          right: 30px;
          z-index: 99;
          font-size: 14px;
          border: none;
          outline: none;
          background-color: seagreen;
          color: white;
          cursor: pointer;
          padding: 15px;
          border-radius: 30%;
    }
    #totop:hover {
        background-color: rgba(0, 124, 110, 0.8);
    }
    main{
        min-height: 80%;
    }
</style>
<body>
@include('layouts.navbareng')
    <main>
        <div class="container" id="container">
            @include('sweetalert::alert')
            <div class="row">
                <div class="col-3">
                    @include('layouts.navleft')
                </div>
                <div class="col-9">
                    @include('layouts.navbardown')
                    @yield('content')
                </div>
            </div>
        </div>
        <div id="container">
            @yield('content2')
        </div>
        <button onclick="topFunction()" id="totop" title="Go to top" class="btn-sm"><i role="presentation"  class="fas fa-angle-up"></i></button>
    </main>
<script>
  let mybutton = document.getElementById("totop");
  window.onscroll = function() {scrollFunction()};
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
</body>
@include('layouts.footereng')
@include('layouts.onlyDatatable')
<!-- MDB core JavaScript -->

<script type="text/javascript" src="{{ URL::asset('MDB/js/mdb.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('MDB/js/bootstrap.js') }}"></script>

<!-- Menu Toggle Script -->
<script type="text/javascript">

</script>
</html>
