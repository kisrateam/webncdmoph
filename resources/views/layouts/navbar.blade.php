<style>
	li span {
		padding-right: 31px;
		font-size: 30px;
		color: black
	}
	.carousel-inner img {
		height: 100%;
		width: 100%;
	}
	.nav-item{
		text-align: right;
	}
	.circle {
		width: 30px;
		height: 30px;
		line-height: 24px;
		border-radius: 50%;
		font-size: 24px;
		text-align: center;
	}
	.dropdown-menu {
		position: relative !important;
		top: 35px; !important;
		border-radius: 20px; !important;
		padding: 0 10px;
		min-width: 200px;
		background-color: inherit;
		/*left: auto !important;*/
		/*right: 0 !important;*/
	}
	.dropdown-menu a{
		padding: 0 10px;
		line-height: 30px;
		cursor: pointer;
	}

	.dropdown-menu a:hover{
		background-color: #d8d8d8;
		border-radius: 8px;
	}

	#img-logo{
		height:135px;
		width: auto;
	}

	#navbarCollapse{
		font-size: 1.1rem;
	}
	a[data-toggle="dropdown"]{
		font-size: 1.2rem;
	}

	#forcollapse{
		display: none;
	}
	@media screen and (max-width: 767px) {
		#hidephone {
			visibility: hidden;
			clear: both;
			float: left;
			margin: 10px auto 5px 20px;
			width: 28%;
			display: none;
		}
	}
	@media screen and (max-width : 991px)
	{
		#forcollapse{
			display: block;
		}
		#header2{
			display: none;
		}
	}
	@media screen and (max-width : 1111px)
	{
		#img-logo{
			height:90px !important;
			width: auto;
		}
		#navbarCollapse{
			font-size: 0.8rem !important;
		}
		a[data-toggle="dropdown"]{
			font-size: 0.7rem !important;
		}
		.homeicon{
			margin-top: 5px !important;
			padding: 0 !important;
		}
	}
	@media screen and (max-width : 1300px)
	{
		#img-logo{
			height:115px;
			width: auto;
		}
		#navbarCollapse{
			font-size: 0.9rem;
		}
		a[data-toggle="dropdown"]{
			font-size: 0.9rem;
		}
		.homeicon{
			margin-top: 3px;
			padding: 0;
		}
	}

	@media screen and (max-width : 1350px)
	{
		#img-logo{
			height:125px;
			width: auto;
		}
	}
	@media screen and (max-width : 750px)
	{
		.flag{
			display: none;
		}
	}

	#onlycollapse {
		height: 100%;
		width: 0;
		position: fixed;
		z-index: 5;
		top: 0;
		left: 0;
		background-color: rgba(0,0,0,0.9);
		overflow-x: hidden;
		transition: 1s;
		padding-top: 4%;
	}

	.navbar-nav li:hover>.dropdown-menu {
		display: block;
	}

	#itemtopnav{
		color: #333333;
	}

	#itemtopnav:hover{
		color: #E65093 !important;
	}

	#itemdownnav:after {
		content: '';
		display: none;
		height: 2px;
		width: 100%;
		background: transparent;
	}

	#itemdownnav:hover:after {
		background-color: #E65093 !important;
		color: #E65093 !important;
		display: block;
	}

	.my-dropdown{
		position: relative;
		display: inline-block;
	}

	.my-dropdown-content{
		display: none;
		position: absolute;
		right: 0;
		top: 35px;
		background-color: #25a09f;
		width: 100vw;
		box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		z-index: 1;
	}

	.my-dropdown-content div li {
		padding: 0 15px;
		display: block;
		font-size: 18px;
	}

	.my-dropdown-content li a{
		color: white;
		padding: 7px 7px;
		text-decoration: none;
	}

	.my-dropdown-content li a:hover{
		color: #25a09f;
		background-color: white;
		border-radius: 10px;
	}

	.my-dropdown:hover .my-dropdown-content {
		display: block;
	}
    a#dec:hover{
        opacity: 0.5 !important;
    }
    a#inc:hover{
        opacity: 0.5 !important;
    }
    .circle:hover{
        opacity: 0.5 !important;
    }
    img#telephone:hover{
        opacity: 0.5 !important;
    }
</style>
<?php
$top = \App\menuheader::where('level','up')->where('active',1)->get();
$down = \App\menuheader::where('level','down')->where('active',1)->with('children')->get();
$banner = \Illuminate\Support\Facades\DB::table('homebanner')->where('url','1')->first();
if($banner)$path = json_decode($banner->path);
else $path = null;
?>
{{--navone--}}
<img alt="กรมควบคุมโรคแบนเนอร์" id="img-logo" tabindex="0" tabindex="0" onclick="window.open('{{$site}}','_self');" src="{{$site}}/Defaultimg/logo-navbar.png" style="position: absolute;z-index: 3;top: 12px;padding-left: 25px;cursor: pointer">
<nav id="header" class="navbar navbar-expand-sm" style="background-color: #E65093;height: 50px;padding: 0;margin: 0">
	<div class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">
		</ul>
		<div class="md-form my-0" style="border-bottom: 1px solid grey;">
			<ul class="nav flex-row" style="list-style: none;margin: 0;padding: 0;">
				<li style="border-left: 1px solid white;border-right: 1px solid white;line-height:40px;">
					<span style="color: white;font-size: 18px;padding: 0 3px;margin: 0 0 0 15px;">
						ขนาด
					</span>
					<span style="margin: 0;padding: 0 3px;">
						<a id='dec' style="color: white;font-size: 16px;" role="button">
							<i role="presentation"  class="fa fa-minus"></i>
						</a>
					</span>
					<span style="color: white;font-size: 20px;padding: 0 3px;margin: 0">
						ก
					</span>
					<span style="padding: 0 3px;margin: 0 15px 0 0;">
						<a id='inc' style="color: white;font-size: 16px" role="button">
							<i role="presentation"  style="" class="fa fa-plus"></i>
						</a>
					</span>
				</li>
				@include('layouts.onlyDatatable')
				<script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer);
                            toast.addEventListener('mouseleave', Swal.resumeTimer);
                        }
                    });
					{{--font size func--}}
						let size = 100;
						let max = 200
							,min = 50;
						function setFontSize(s) {
						  size = s;
						  $('.font-size-change').css('font-size', '' + size + '%');
                        }
						function increaseFontSize() {
						  if(size < max)
							setFontSize(size + 25);
                            Toast.fire({
                                icon: 'success',
                                title: 'ขนาด '+size+'%'
                            })
                        }
						function decreaseFontSize() {
						  if(size > min)
							setFontSize(size - 25);
                            Toast.fire({
                                icon: 'success',
                                title: 'ขนาด '+size+'%'
                            })
                        }
						$('#inc').click(increaseFontSize);
						$('#dec').click(decreaseFontSize);
						setFontSize(size);
					{{--font size func--}}
				</script>
				<li style="border-left: 1px solid white;padding-top: 10px">
					<div class="circle" style="background-color: #3D3D3D;color: white;margin: 0 5px 0 15px;cursor: pointer" tabindex="1" tabindex="0" onclick="changetoblack();" role="button">c</div>
				</li>
				<script>
					function changetoblack() {
                        let color = document.getElementsByClassName("changefontcolor");
                        let colortopic = document.getElementsByClassName("changefontcolortopic");
                        let header = document.getElementById('header');
                        let header2 = document.getElementById('header2');
                        let leftmenu = document.getElementsByClassName('forcolor');
                        let footer = document.getElementById('footer');
                        let footer2 = document.getElementById('footer2');
                        let copyright = document.getElementById('copyright');
                        let a = header2.getElementsByTagName('a');
                        let afoot = footer.getElementsByTagName('a');
                        let menu = leftmenu[0].getElementsByTagName('a');
                        color.forEach(item => item.style.color = 'white');
                        color.forEach(item => item.style.backgroundColor = 'black');
                        colortopic.forEach(item => item.style.color = 'white');
                        colortopic.forEach(item => item.style.backgroundColor = 'black');
                        leftmenu[0].style.backgroundColor = 'black';
                        document.body.style.backgroundColor = 'black';
                        document.body.style.color = 'white';
                        header.style.backgroundColor = 'black';
                        a.forEach(item => item.style.color = 'white');
                        afoot.forEach(item => item.style.color = 'white');
                        menu.forEach(item => item.style.color = 'white');
						header2.style.backgroundColor = 'black';
                        footer.style.backgroundColor = 'black';
                        footer.style.color = 'white';
                        footer2.style.backgroundColor = 'black';
                        footer2.style.color = 'white';
                        copyright.style.backgroundColor = 'black';
                        header.style.color = 'white';
                        header2.style.color = 'white';
                        copyright.style.color = 'white';
                        localStorage.setItem('checktheme', '0');
                    }
				</script>
				<li style="padding-top: 10px">
					<div class="circle" style="background-color: white;color: black;margin: 0 5px;cursor: pointer" tabindex="2" tabindex="0" onclick="changetodefault();" role="button">c</div>
				</li>
				<script>
                    function changetodefault() {
                        let color = document.getElementsByClassName("changefontcolor");
                        let colortopic = document.getElementsByClassName("changefontcolortopic");
                        let header = document.getElementById('header');
                        let header2 = document.getElementById('header2');
                        let footer = document.getElementById('footer');
                        let footer2 = document.getElementById('footer2');
                        let copyright = document.getElementById('copyright');
                        let leftmenu = document.getElementsByClassName('forcolor');
                        let a = header2.getElementsByTagName('a');
                        let afoot = footer.getElementsByTagName('a');
                        let menu = leftmenu[0].getElementsByTagName('a');
                        color.forEach(item => item.style.color = '');
                        color.forEach(item => item.style.backgroundColor = '');
                        colortopic.forEach(item => item.style.color = 'black');
                        colortopic.forEach(item => item.style.backgroundColor = '');
                        leftmenu[0].style.backgroundColor = '';
                        document.body.style.backgroundColor = '';
                        document.body.style.color = '';
                        header.style.backgroundColor = '#E65093';
                        a.forEach(function (i,index,array){
                            if (index > 59) {
                                // i.style.color = '';
                            }else{
                                i.style.color = '#333333';
                            }
                        });
                        afoot.forEach(function (i,index,array){
                            if (index === array.length - 1) {
                                i.style.color = 'white';
                            }else{
                                i.style.color = '5d5d5d';
                            }
                        });
                        menu.forEach(item => item.style.color = '');
                        header2.style.backgroundColor = '';
                        footer.style.backgroundColor = '';
                        footer.style.color = 'black';
                        footer2.style.backgroundColor = '';
                        footer2.style.color = 'black';
                        copyright.style.backgroundColor = '#E65093';
                        header.style.color = '';
                        header2.style.color = '';
                        copyright.style.color = '';
                        localStorage.setItem('checktheme', '1');
                    }
				</script>
				<li style="border-right: 1px solid white;padding-top: 10px">
					<div class="circle" style="background-color: #203F54;color: #FFD873;margin: 0 15px 0 5px;cursor: pointer" tabindex="3" tabindex="0" onclick="changetoblind();" role="button">c</div>
				</li>
				<script>
                    function changetoblind() {
                        let colorblind = '#FFFF00';
                        let color = document.getElementsByClassName("changefontcolor");
                        let colortopic = document.getElementsByClassName("changefontcolortopic");
                        let header = document.getElementById('header');
                        let header2 = document.getElementById('header2');
                        let leftmenu = document.getElementsByClassName('forcolor');
                        let footer = document.getElementById('footer');
                        let footer2 = document.getElementById('footer2');
                        let copyright = document.getElementById('copyright');
                        let a = header2.getElementsByTagName('a');
                        let afoot = footer.getElementsByTagName('a');
                        let menu = leftmenu[0].getElementsByTagName('a');
                        colortopic.forEach(item => item.style.color = colorblind);
                        colortopic.forEach(item => item.style.backgroundColor = 'black');
                        color.forEach(item => item.style.color = colorblind);
                        color.forEach(item => item.style.backgroundColor = 'black');
                        leftmenu[0].style.backgroundColor = 'black';
                        document.body.style.backgroundColor = 'black';
                        document.body.style.color = colorblind;
                        header.style.backgroundColor = 'black';
                        a.forEach(item => item.style.color = colorblind);
                        afoot.forEach(item => item.style.color = colorblind);
                        menu.forEach(item => item.style.color = colorblind);
                        header2.style.backgroundColor = 'black';
                        footer.style.backgroundColor = 'black';
                        footer.style.color = colorblind;
                        footer2.style.backgroundColor = 'black';
                        footer2.style.color = colorblind;
                        copyright.style.backgroundColor = 'black';
                        header.style.color = colorblind;
                        header2.style.color = colorblind;
                        copyright.style.color = colorblind;
                        localStorage.setItem('checktheme', '2');
                    }
				</script>
				<li style="padding: 12px 0 0 0;margin: 0 10px 0 15px" class="flag">
					<span style="padding: 0;">
						<a role="button" href="https://ddc.moph.go.th/en/index.php">
							<img class="center" src="{{$site}}/Defaultimg/logo-eng.png" alt="ENG" width="30" height="25">
						</a>
					</span>
				</li>
				<li style="padding: 12px 0 0 0;margin: 0 15px 0 10px" class="flag">
					<span style="padding: 0;">
						<a role="button" href="https://ddc.moph.go.th/index.php">
							<img class="center" src="{{$site}}/Defaultimg/logo-thai.png" alt="TH" width="30" height="25">
						</a>
					</span>
				</li>
				<li style="border-left: 1px solid white;">
					<a role="button" href="tel:1422" style="text-decoration: none;margin-right: 20px">
						<span class="fa" style="padding: 0">
						<img id="telephone" style="margin: 0 0 9px 9px;" src="{{$site}}/Defaultimg/call.svg" alt="Hotline 1422" width="30" height="25">
						</span>
						<span style="color: white;padding: 0 5px">1422</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
{{--navtwo--}}
<nav id="header2" class="navbar navbar-expand-lg" style="padding: 0 20px;background-color: white;height: 115px;">
	<div class="collapse navbar-collapse flex-column align-items-start" id="navbarCollapse" style="z-index: 2;">
		{{--navtop--}}
		<ul class="navbar-nav mb-auto mt-0 ml-auto">
			@foreach($top as $item)
				@if($loop->last)
					<li class="nav-item" style="padding-top: 10px">
						<a role="button" id="itemtopnav" class="nav-link py-0" href="{{$item->link != null ? $item->link:'#'}}" style="padding: 0 10px" target="_blank">{{$item->name}}</a>
					</li>
					<li class="nav-item" style="padding-top: 10px">
						<a role="button" href="https://ddc.moph.go.th/search.php" target="_blank" style="padding: 0 10px">
							<i role="presentation"  style="font-size: 1.5rem;color: #e84c93" class="fa fa-search" aria-hidden="true"></i>
						</a>
					</li>
				@else
					<li class="nav-item" style="padding-top: 10px">
						<a role="button"id="itemtopnav" class="nav-link py-0" href="{{$item->link != null ? $item->link:'#'}}" style="padding: 0 10px;" target="_blank">{{$item->name}}</a>
					</li>
					<li class="nav_line" style="padding-top: 10px"><a role="button" class="nav-item">| </a></li>
				@endif
			@endforeach
		</ul>
		<ul class="navbar-nav mb-auto mt-0 ml-auto" style="padding-top: 0.5rem;height: 45px;border-top: 1px solid #e6e6e6;">
			<li id="itemdownnav" class="nav-item homeicon" style="color: black;">
				<a role="button" class="nav-link" href="https://ddc.moph.go.th/index.php"><i role="presentation"  class="fas fa-home"></i></a>
			</li>
		@foreach($down as $item)
			@if( $item->parent_id == null )
			<li class="dropdown" style="position: relative">
				@if($item->name=='รายชื่อโรค')
					<li class="my-dropdown" style="background-color: #179c9b;padding: 5px 10px 30px 20px;border-radius: 15px 15px 0 0;color: white"><a role="button" style="color: white;font-size: 18px;padding: 0;" href="#" class="dropdown-toggle">รายชื่อโรค</a>
						<ul class="my-dropdown-content">
							<div class="col-12">
								<ul class="container-fluid" style="padding: 20px 0 20px 20%">
									<div class="row">
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ก" role="button"> ก </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ข" role="button"> ข </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ค" role="button"> ค </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=จ" role="button"> จ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ช" role="button"> ช </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ซ" role="button"> ซ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ต" role="button"> ต </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ถ" role="button"> ถ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ท" role="button"> ท </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=บ" role="button"> บ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ป" role="button"> ป </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ฝ" role="button"> ฝ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=พ" role="button"> พ </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ม" role="button"> ม </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ย" role="button"> ย </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ร" role="button"> ร </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ล" role="button"> ล </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ว" role="button"> ว </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ส" role="button"> ส </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=ห" role="button"> ห </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=อ" role="button"> อ </a></li>
									</div>
									<div class="row">
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=A" role="button"> A </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=B" role="button"> B </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=C" role="button"> C </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=D" role="button"> D </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=E" role="button"> E </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=F" role="button"> F </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=G" role="button"> G </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=H" role="button"> H </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=I" role="button"> I </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=K" role="button"> K </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=L" role="button"> L </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=M" role="button"> M </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=N" role="button"> N </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=P" role="button"> P </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=R" role="button"> R </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=S" role="button"> S </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=T" role="button"> T </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=V" role="button"> V </a></li>
										<li><a href="https://ddc.moph.go.th/disease.php?e=th&char=W" role="button"> W </a></li>
									</div>
								</ul>
							</div>
						</ul>
					</li>
				@else
					<a role="button" id="itemdownnav" data-toggle="dropdown" style="padding:4px 1rem;margin-bottom: 10px" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">{{$item->name}}
						<span role="presentation" style="padding: 0 0 0 5px;font-size: 20px" class="dropdown-toggle"></span></a>
				@endif
				<ul style="list-style-type:none;opacity: 0.9" class="dropdown-menu mt-2 rounded-0 white border-0 z-depth-1">
					@foreach($item->children as $submenu)
						<li class="dropdown-item dropdown-submenu p-0">
							@if($submenu->type == 'link')
								<a role="button" href="{{$submenu->link}}" class="dropdown-item w-100" target="_blank">{{$submenu->name}}</a>
							@elseif($submenu->type == 'havelink')
								<a role="button" href="#" data-toggle="dropdown" class="dropdown-item w-100">{{$submenu->name}}
									<span style="float: right;padding: 0;opacity: 0.5"><i role="presentation"  style="font-size: 16px;padding-top: 6px" class="fa fa-chevron-right"></i></span>
								</a>
								<ul class="dropdown-menu rounded-0 white border-0 z-depth-1">
									@foreach($submenu->childrenin as $insubmenu)
										<li class="dropdown-item p-0">
											<a role="button" href="{{$insubmenu->link}}" class="dropdown-item w-100" target="_blank">{{ $insubmenu->name }}</a>
										</li>
									@endforeach
								</ul>
							@endif
						</li>
					@endforeach
				</ul>
			</li>
			@endif
		@endforeach
		</ul>
	</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light white scrolling-navbar" id="forcollapse">
	<div class="container-fluid">
		<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarContent"
				aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
			<span role="presentation" class="navbar-toggler-icon" tabindex="4" onclick="opencollapse()"></span>
		</button>
	</div>
</nav>

<div id="onlycollapse">
	<div class="row">
		@foreach($down as $item)
			<div class="col-12">
				@if($loop->first)
					<div class="float-right">
						<a role="button" style="color: white;font-size: 30px;padding: 20px;text-decoration: none;" tabindex="4" onclick="closecollapse();">X</a>
					</div>
				@endif
			@if( $item->parent_id == null && $item->level == "down" && $item->name != 'รายชื่อโรค')
				<p style="margin-left: 5%;color: white;font-size: 26px">
					<span>{{$item->name}}</span>
					<span>
						@foreach($item->children as $submenu)
							<span>
								<a role="button" style="color: white;font-size: 16px" href="{{$submenu->link}}" class="dropdown-item w-100">{{$submenu->name}}</a>
							</span>
						@endforeach
					</span>
				</p>
			@endif
			</div>
		@endforeach
	</div>
</div>
<div>
	@if($banner)
		<a role="button" href="{{$path != null ? $path[1]:''}}" style="height: 200px;" target="_blank">
			<img src="{{$path != null && $path[0] != null ? $site.'/uploads/'.$path[0]:$site.'/Defaultimg/noimg.jpg'}}" width="100%" height="auto" alt="กรมควบคุมโรคแบนเนอร์">
		</a>
	@else
		<div style="height: 200px;background-color: #DBE8EB;"></div>
	@endif
</div>

<script>
    const $menu = $('.dropdown-menu');
    $(document).mouseup(e => {
        if (!$menu.is(e.target) && $menu.has(e.target).length === 0)
        {
            $menu.removeClass('show');
        }
    });

    $(document).ready(function() {
        const stay = localStorage.getItem('checktheme');
        if(typeof(stay) != "undefined" && stay == '0'){
            changetoblack();
		}else if(typeof(stay) != "undefined" && stay == '2'){
            changetoblind();
		}else{
            changetodefault();
		}
    });

    function opencollapse() {
        document.getElementById("onlycollapse").style.width = "90%";
    }
    function closecollapse() {
        document.getElementById("onlycollapse").style.width = "0";
    }

    $(document).mouseup(function(e)
    {
        let container = $("#onlycollapse");

        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            document.getElementById("onlycollapse").style.width = "0";
        }
    });
</script>
