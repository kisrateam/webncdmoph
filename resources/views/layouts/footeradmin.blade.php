<style>
    * {
        margin: 0;
    }
</style>
<footer id="footer" style="background-color: white;color:black;" class="page-footer font-small bottom">
    <!-- Copyright -->
    <br/>
    <br/>
    <br/>
    <div id="copyright" class="footer-copyright py-3 text-center" style="border-top: 1px solid #B6C3C6;background-color: white">
        <span style="color: black;">ลิขสิทธิ์ © </span><span style="color: black;" id="spanYear"></span>
        <span style="color: black;">: สงวนลิขสิทธิ์.</span>
    </div>
    <!-- Copyright -->
</footer>
<script>
    window.onload = function()
    {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        var current = new Date(year + 543, month, day);
        document.getElementById("spanYear").innerHTML = current.getFullYear();
    }
</script>