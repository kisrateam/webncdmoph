<style>
	.dropbtn {
		min-width: 80px;
		background-color: white;
		color: #508EBF;
		font-size: 16px;
		border: 1px solid #508EBF;
		box-sizing: border-box;
		border-radius: 20px;
		cursor: pointer;
        padding: 3px 10px;
        height: 33px;
	}
	.dropbtn:hover, .dropbtn:focus {
		background-color: #508EBF;
        color: white;
	}
	.dropdown-content {
        display: none;
		position: absolute;
		background-color: white;
		min-width: 160px;
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
		right: 0;
        top: 70px;
		z-index: 1;
        border: 1px solid #508EBF;
        border-radius: 5px;
	}
    .dropdown-content:before {
      content: "";
      position: absolute;
      top: -30px;
      right: 10%;
      border: 15px solid transparent;
      border-bottom-color: #508EBF;
    }
	.dropdown-content a {
		padding: 20px 16px;
		text-decoration: none;
	}
	.dropdown-content a:hover {
      color: white;
      background-color: #508EBF;
    }

    .show {
      display: block;
    }

    div a {
        padding-bottom: 10px;
    }

    #frame-login{
      border-radius: 30px;
      border: none;
    }
    #searchbar{
      width: calc(100% - 300px);
      border: 1px solid #B6C3C6;
      box-sizing: border-box;
      border-radius: 20px;
      height: 33px;
      padding-left: 20px;
      outline: none;
    }
    #searchbareng{
      width: calc(100% - 300px);
      border: 1px solid #B6C3C6;
      box-sizing: border-box;
      border-radius: 20px;
      height: 33px;
      padding-left: 20px;
      outline: none;
    }
    #searchicon{
      float: right;position: relative;top: 8px;
    }
    #onlymobile {
      display:block;
    }
    @media screen and (max-width : 766px)
      {
      #onlymobile{
        display: none;
      }
      #searchbar{
        width: 100%;
      }
      #searchbareng{
        width: 100%;
      }
      #searchicon{
        position: absolute;
        right: 10%;
        top: 27px;
      }
      .custom-search{
        width: calc(100% - 30px) !important;
      }
    }
  .hlsearch{
    color: #E65093;
    text-decoration: underline;
  }
  .rssicon:hover{
    opacity: 0.8;
  }
  .custom-search{
    width: calc(100% - 330px);
  }
</style>
  <div style="margin-top: 20px;justify-content: space-between;display: flex;">
    @if(\Request::is('home/eng'))
      <input aria-label="ค้นหาแผนการป้องกัน" id="searchbareng" name="searchbareng" type="search" placeholder="Search ..." required>
      <div class="md-form custom-search" style="position: absolute;">
        <div id="Listsearcheng"></div>
      </div>
    @else
      <input aria-label="ค้นหาแผนการป้องกัน" id="searchbar" name="searchbar" type="search" placeholder="ค้นหาแผนการป้องกัน" required>
      <div class="md-form custom-search" style="position: absolute;">
        <div id="Listsearch"></div>
      </div>
    @endif
    <a role="button">
    <span>
    <img id="searchicon" src="{{$site}}/Defaultimg/search.svg" alt="search">
    </span>
    </a>
    <span role="presentation" id="onlymobile" style="border-left: 1px solid #000000;height: 33px;float: right;">
    </span>
    <a role="button" id="onlymobile" href="{{route('eng')}}">
    <span class="rssicon" id="onlymobile" style="{{\Request::is('home/eng') ? 'display:none':'display:block'}}">
    <img style="border-radius: 100%;float: right;" src="{{$site}}/Defaultimg/eng.svg" alt="eng">
    </span>
    </a>
    <a role="button" id="onlymobile" href="{{$site}}">
    <span class="rssicon" id="onlymobile" style="{{\Request::is('home/eng') ? 'display:block':'display:none'}}">
    <img style="border-radius: 100%;float: right;" src="{{$site}}/Defaultimg/th.svg" alt="th">
    </span>
    </a>
    <a role="button" id="onlymobile" href="{{route('rssfeed')}}">
    <span>
    <img class="rssicon" style="float: right;" src="{{$site}}/Defaultimg/rssicon.png" width="80" height="33" alt="rssicon">
    </span>
    </a>
    <div id="onlymobile">
    @guest
      <a role="button" href="#" onclick="switchlabel(0);" class="dropbtn" data-toggle="modal" data-target="#loginModal" style="position: relative;top: 5px;"><i role="presentation"  style="padding-right: 10px" class="far fa-user"></i>เข้าสู่ระบบ</a>
    @else
      <button onclick="dropdown()" class="dropbtn dropdown-toggle" style="outline: none">
        {{ Auth::user()->name }}</button>
      <div id="myDropdown" class="dropdown-content text-center">
        <a role="button" class="dropdown-item" href="{{ route('showchangeprofile',['id'=>Auth::user()->id]) }}">
          <img src="{{$site}}/Defaultimg/avatar-login.svg" alt="avatar-login" style="margin: 0;filter: invert(100%) sepia(15%) saturate(2448%) hue-rotate(347deg) brightness(83%) contrast(88%);float: left;margin: 0 5px;">
          {{ __('เปลี่ยนข้อมูลส่วนตัว') }}
        </a>
        @if(Auth::user()->role == 'admin')
          <a role="button" class="dropdown-item" href="{{ route('admin')}}">
            <img src="{{$site}}/Defaultimg/management.svg" alt="management" style="margin: 0;width: 34px;height: 34px;float: left;margin: 0 5px">
            {{ __('การจัดการเว็บไซต์') }}
          </a>
        @endif
        <a role="button" class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
          <img src="{{$site}}/Defaultimg/logout.svg" alt="logout" style="margin: 0;width: 34px;height: 34px;float: left;margin: 0 5px">
          {{ __('ออกจากระบบ') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    @endguest
    </div>
  </div>
<br/>

<style>
  #togglePassword{
    position: absolute;
    right: 10px;
    top: 11px;
    cursor: pointer;
    z-index: 5;
  }
</style>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" id="frame-login">
      <div class="modal-header justify-content-center" style="background-color: #508EBF;color: white;border-radius: 30px 30px 0 0;">
          <h5 class="modal-title">เข้าสู่ระบบ</h5>
      </div>
      <div class="modal-body">
          <div class="col-12">
            <div style="background-color: white;height: 125px;width: 80px;position: absolute;top: -77px;box-shadow: 0 0 8px rgba(0, 0, 0, 0.25);border-radius: 0 0 30px 30px;">
              <img src="{{$site}}/Defaultimg/Logo.png" alt="Logo" style="position: relative;top: 30px;left: 11px;">
            </div>
            <div class="row" id="labelfortopic">
              <div class="col-md-12 text-center" style="font-size: 20px">
                <p>กรุณาเข้าสู่ระบบ/สมัครสมาชิก</p>
                <p>ก่อนทำการสร้างบทสนทนา</p>
              </div>
            </div>
            <div id="labelfortopicbr">
              <br/>
              <br/>
              <br/>
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
              <div class="form-group row justify-content-center">
                  <div class="col-md-8">
                    <div class="input-group mb-3 input-icons">
                      <img src="{{$site}}/Defaultimg/login-email.svg" alt="login-email" style="position: relative;right: 15px;">
                      <input type="email" class="form-control input-field" placeholder="อีเมล" aria-label="อีเมล" id="email1" name="email" autocomplete="username" required autofocus="autofocus">
                    </div>
                  </div>
                </div>
                <div class="form-group row justify-content-center">
                  <div class="col-md-8">
                    <div class="input-group mb-3 input-icons">
                      <img src="{{$site}}/Defaultimg/login-password.svg" alt="login-password" style="position: relative;right: 15px;">
                      <input type="password" class="form-control input-field" autocomplete="current-password" placeholder="รหัสผ่าน" aria-label="รหัสผ่าน" id="password1" minlength="7" name="password" required>
                      <i role="presentation"  class="far fa-eye" id="togglePassword"></i>
                    </div>
                  </div>
                </div>
                <div class="form-group row mb-0 justify-content-center">
                  <div class="col-md-6">
                    <button style="border-radius: 3rem;background-color:#508EBF ;color: white;" type="submit" class="btn btn-block">
                      {{ __('เข้าสู่ระบบ') }}
                    </button>
                  </div>
                </div>
                <div class="form-group row mb-0 justify-content-center">
                  <div class="col-md-6" style="padding-top: 10px">
                    <a role="button" style="border: 1px solid #3D3D3D;border-radius: 3rem;background-color:white ;color: black;" href="#" data-toggle="modal" data-target="#policyModal" class="btn btn-block">
                      {{ __('ลงทะเบียน') }}
                    </a>
                  </div>
                </div>
                <div class="form-group row mb-0 justify-content-center">
                  @if (Route::has('password.request'))
                    <a role="button" style="color: black;" class="btn btn-link" href="{{ route('password.request') }}">
                      {{ __('ลืมรหัสผ่าน ?') }}
                    </a>
                  @endif
                </div>
                <br/>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="policyModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" id="frame-login">
      <div class="modal-header justify-content-center" style="background-color: #508EBF;color: white;border-radius: 30px 30px 0px 0px;">
        <h5 class="modal-title">ลงทะเบียน</h5>
      </div>
      <div class="modal-body">
        <div style="background-color: white;height: 125px;width: 80px;position: absolute;top: -61px;left: 30px;box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.25);border-radius: 0px 0px 30px 30px;">
          <img src="{{$site}}/Defaultimg/Logo.png" alt="Logo" style="position: relative;top: 30px;left: 11px;">
        </div>
        <br/>
        <br/>
        <div class="text-center">
          <label style="width: 100%;text-align: center">ยืนยันการลงทะเบียน
            <textarea class="form-control rounded-1" rows="8" style="resize: none;width: 80%;margin-left: 10%" readonly>
              นโยบาย การใช้งานระบกรมควบคุมโรค สำนักโรคไม่ติดต่อโดยการคลิกปุ่มด้านล่าง ข้าพเจ้ายอมรับให้เข้าถึงข้อมูลส่วนตัว ซึ่งประกอบไปด้วย อีเมลและสื่อโซเชี่ยลมีเดียอนุญาตให้ส่งข้อความแจ้งเตือนในกรณีมีการอัพเดทระบบ หรือ ตั้งข้อคำถามในหน้ากระดานสนทนาอนุญาตให้ตั้งรหัสผ่านใหม่ผ่านช่องทางอีเมลที่สมัครหากการลงทะเบียนได้รับการอนุมัติคุณจะสามารถทำการตั้งหัวข้อคำถาม และแสดงความคิดเห็นในหัวข้อคำถามได้
            </textarea>
          </label>
          <br/>
          <div class="custom-control row custom-checkbox justify-content-center">
            <div class="offset-md-1 col-md-10">
              <input onclick="changed(this);" type="checkbox" class="custom-control-input offset-1 col-10 offset-sm-1 col-sm-10" id="check" unchecked>
              <label class="custom-control-label" for="check">ข้าพเจ้าได้อ่านและยอมรับนโยบายการใช้งานระบบทุกข้อแล้ว?</label>
            </div>
          </div>
          <br/>
          <br/>
          <br/>
          <div class="form-group row mb-0 justify-content-center">
            <div class="col-md-10">
              <button id="gonext" onclick="hidemodal();" style="border: 1px solid #3D3D3D;border-radius: 3rem;background-color:#508EBF ;color: white;" data-toggle="modal" data-target="#registerModal" class="btn btn-block" disabled>
                {{ __('ยืนยัน') }}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" id="frame-login">
      <div class="modal-header justify-content-center" style="background-color: #508EBF;color: white;border-radius: 30px 30px 0px 0px;">
        <h5 class="modal-title">ลงทะเบียน</h5>
      </div>
      <div class="modal-body">
        <div style="background-color: white;height: 125px;width: 80px;position: absolute;top: -61px;left: 30px;box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.25);border-radius: 0px 0px 30px 30px;">
          <img src="{{$site}}/Defaultimg/Logo.png" alt="Logo" style="position: relative;top: 30px;left: 11px;">
        </div>
        <br/>
        <br/>
        <form method="POST" action="{{ route('register') }}">
          @csrf
          <div class="form-group row justify-content-center">
            <div class="col-md-8">
              <div class="input-group mb-3 input-icons">
                <img src="{{$site}}/Defaultimg/2.svg" alt="ชื่อผู้ใช้งาน" style="position: relative;right: 15px;">
                <input type="text" class="form-control input-field" placeholder="ชื่อผู้ใช้งาน" aria-label="ชื่อผู้ใช้งาน" id="name" name="name" autocomplete="off" minlength="4" maxlength='12' required>
              </div>
            </div>
          </div>

          <div class="form-group row justify-content-center">
            <div class="col-md-8">
              <div class="input-group mb-3 input-icons">
                <img src="{{$site}}/Defaultimg/login-email.svg" alt="อีเมล" style="position: relative;right: 15px;">
                <input type="email" class="form-control input-field" placeholder="อีเมล" aria-label="อีเมล" id="email2" name="email" autocomplete="username" required>
              </div>
            </div>
          </div>

          <div class="form-group row justify-content-center">
            <div class="col-md-8">
              <div class="input-group mb-3 input-icons">
                <img src="{{$site}}/Defaultimg/login-password.svg" alt="รหัสผ่าน" style="position: relative;right: 15px;">
                <input type="password" class="form-control input-field" autocomplete="new-password" placeholder="รหัสผ่าน" aria-label="รหัสผ่าน" minlength="7" id="password2" name="password" required>
              </div>
            </div>
          </div>

          <div class="form-group row justify-content-center">
            <div class="col-md-8">
              <div class="input-group mb-3 input-icons">
                <img src="{{$site}}/Defaultimg/login-password.svg" alt="รหัสผ่าน-อีกครั้ง" style="position: relative;right: 15px;">
                <input type="password" class="form-control input-field" placeholder="รหัสผ่าน-อีกครั้ง" aria-label="รหัสผ่าน-อีกครั้ง" minlength="7" id="password-confirm" autocomplete="new-password" name="password_confirmation" required>
              </div>
            </div>
          </div>

          <div class="form-group row justify-content-center">
            <div class="col-md-8">
              <button style="border-radius: 3rem;background-color:#508EBF ;color: white;" type="submit" class="btn btn-block">
                {{ __('สมัครสมาชิก') }}
              </button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>

<script>
  const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#password1');
  togglePassword.addEventListener('click', function (e) {
      const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
      password.setAttribute('type', type);
      this.classList.toggle('fa-eye-slash');
  });

  function switchlabel(val){
      if(val == 0){
          document.getElementById('labelfortopic').style.display = 'none';
          document.getElementById('labelfortopicbr').style.display = 'block';
      }else{
          document.getElementById('labelfortopic').style.display = 'block';
          document.getElementById('labelfortopicbr').style.display = 'none';
      }
  }

  $(function () {
    $('#loginModal').on('shown.bs.modal', function () {
        $('#stopmodal').carousel('pause');
    });
    $('#loginModal').on('hidden.bs.modal', function () {
      $('#stopmodal').carousel('cycle');
    });
  });

  function hidemodal() {
    $('#policyModal').modal('hide');
  }

  function changed(value) {
    document.getElementById("gonext").disabled = !value.checked;
  }

  function dropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      let dropdowns = document.getElementsByClassName("dropdown-content");
      for (let i = 0; i < dropdowns.length; i++) {
        let openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  };

  $(document).ready(function(){
      $('#searchbar').keyup(function(){
          var query = $(this).val();
          const regex = /[A-Z]/g;
          if(query != '')
          {
              var _token = $('input[name="_token"]').val();
              $.ajax({
                  url:"{{ $site.'/searchcontents' }}",
                  method:"POST",
                  data:{query:query, _token:_token},
                  success:function(data){
                      $('#Listsearch').fadeIn();
                      $('#Listsearch').html(data);
                  }
              });
          }
      });
      $(document).on('click', '#searchbar', function(){
          $('#Listsearch').fadeOut();
      });
      if ($('#searchbar').length){
          document.getElementById("searchbar").addEventListener("focusout", autoclear);
      }

      $('#searchbareng').keyup(function(){
          var query = $(this).val();
          if(query != '')
          {
              var _token = $('input[name="_token"]').val();
              $.ajax({
                  url:"{{ $site.'/searchcontentseng' }}",
                  method:"POST",
                  data:{query:query, _token:_token},
                  success:function(data){
                      $('#Listsearcheng').fadeIn();
                      $('#Listsearcheng').html(data);
                  }
              });
          }
      });
      $(document).on('click', '#searchbar', function(){
          $('#Listsearcheng').fadeOut();
      });
      if ($('#searchbareng').length){
          document.getElementById("searchbareng").addEventListener("focusout", autocleareng);
      }
  });

  function autoclear(){
      setTimeout(function(){ $('#Listsearch').fadeOut(); }, 100);
      document.getElementById('searchbar').value='';
  }

  function autocleareng(){
      setTimeout(function(){ $('#Listsearcheng').fadeOut(); }, 100);
      document.getElementById('searchbareng').value='';
  }
</script>
