<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<!-- Google Fonts Roboto -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/bootstrap.min.css') }}">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/mdb.min.css') }}">
	<!-- Your custom styles (optional) -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/style.css') }}">
	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<main class="py-4">
	@yield('content')
</main>
@stack('scripts')
</body>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('MDB/js/bootstrap.js') }}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ URL::asset('MDB/js/popper.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('MDB/js/mdb.min.js') }}"></script>
</html>
