<style>
	.topnav {
		border-bottom: 8px solid #01673F;
	}
	li span {
		padding-right: 31px;
		font-size: 30px;
		color: black
	}
	.loginbutton{
		border-radius: 30px;
		background-color: #01673F;
		color: white;
	}
	.carousel {
		height: 400px;
	}
	.carousel-item,
	.carousel-inner,
	.carousel-inner img {
		height: 100%;
		width: 100%;
	}
	.carousel-item {
		text-align: center;
	}
	#forcollapse{
		display: none;
	}
	@media screen and (max-width: 766px) {
		#hidephone {
			visibility: hidden;
			clear: both;
			float: left;
			margin: 10px auto 5px 20px;
			width: 28%;
			display: none;
		}
		#forcollapse{
			display: block;
		}
	}
	.nav-item{
		text-align: right;
	}
	.circle {
		width: 30px;
		height: 30px;
		line-height: 24px;
		border-radius: 50%;
		font-size: 24px;
		text-align: center;
	}
	.dropdown-menu {
		left: auto !important;
		right: 0 !important;
	}
	.dropdown-menu a{
		padding: 0 10px;
		line-height: 30px;
		cursor: pointer;
	}
	.dropdown-menu a:hover{
		background-color: #d8d8d8;
	}
	a#dec:hover{
		opacity: 0.5 !important;
	}
	a#inc:hover{
		opacity: 0.5 !important;
	}
	.circle:hover{
		opacity: 0.5 !important;
	}
	img#telephone:hover{
		opacity: 0.5 !important;
	}
	#itemdownnav:after {
		content: '';
		display: none;
		height: 2px;
		width: 100%;
		background: transparent;
	}

	#itemdownnav:hover:after {
		background-color: #E65093 !important;
		color: #E65093 !important;
		display: block;
	}
	#onlycollapse {
		height: 100%;
		width: 0;
		position: fixed;
		z-index: 5;
		top: 0;
		left: 0;
		background-color: rgba(0,0,0,0.9);
		overflow-x: hidden;
		transition: 1s;
		padding-top: 4%;
	}
	#onlycollapse div.row div.col-12 p:hover{
		background-color: white;
		cursor: pointer;
	}
	#onlycollapse div.row div.col-12 p a:hover{
		color: black;
	}
</style>
<img src="{{$site}}/Defaultimg/logo-navbar.png" style="position: absolute;z-index: 1;top: 12px;padding-left: 20px;max-height:100px">
{{--navone--}}
<nav class="navbar navbar-expand-sm" style="background-color: #E65093;height: 50px;padding: 0;margin: 0">
	<div class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">
		</ul>
	<div class="md-form my-0">
			<ul class="nav flex-row" style="list-style: none;margin: 0;padding: 0">
				<li style="color: white;font-size: 18px;padding: 0 3px;margin: 0 0 0 15px;">
					<span style="color: white;font-size: 20px;padding: 0;margin: 0 10px;">
						Font
					</span>
					<span style="margin: 0;padding: 0 3px;">
						<a id='dec' style="color: white;font-size: 16px;" role="button">
							<i role="presentation"  class="fa fa-minus"></i>
						</a>
					</span>
					<span style="color: white;font-size: 20px;padding: 0 3px;margin: 0">
						a
					</span>
					<span style="padding: 0 3px;margin: 0 15px 0 0;">
						<a id='inc' style="color: white;font-size: 16px" role="button">
							<i role="presentation"  style="" class="fa fa-plus"></i>
						</a>
					</span>
				</li>
				@include('layouts.onlyDatatable')
				<script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer);
                            toast.addEventListener('mouseleave', Swal.resumeTimer);
                        }
                    });
					{{--font size func--}}
						let size = 100;
						let max = 200
							,min = 50;
						function setFontSize(s) {
						  size = s;
						  $('body').css('font-size', '' + size + '%');
						  $('footer').css('font-size', '' + size + '%');
						}
						function increaseFontSize() {
						  if(size < max)
							setFontSize(size + 25);
                            Toast.fire({
                                icon: 'success',
                                title: 'size '+size+'%'
                            })
						}
						function decreaseFontSize() {
						  if(size > min)
							setFontSize(size - 25);
                            Toast.fire({
                                icon: 'success',
                                title: 'size '+size+'%'
                            })
						}
						$('#inc').click(increaseFontSize);
						$('#dec').click(decreaseFontSize);
						setFontSize(size);
					{{--font size func--}}
				</script>
				<li style="border-left: 1px solid white;padding-top: 10px">
					<div class="circle" style="background-color: #3D3D3D;color: white;margin: 0 5px" role="button">c</div>
				</li>
				<li style="padding-top: 10px">
					<div class="circle" style="background-color: white;color: black;margin: 0 5px" role="button">c</div>
				</li>
				<li style="border-right: 1px solid white;padding-top: 10px">
					<div class="circle" style="background-color: #203F54;color: #FFD873;margin: 0 5px" role="button">c</div>
				</li>
				<li style="padding: 12px 0 0 0;margin: 0 10px">
					<span style="padding: 0;">
						<a href="https://ddc.moph.go.th/en/index.php" role="button">
							<img class="center" src="{{$site}}/Defaultimg/logo-eng.png" width="30" height="20">
						</a>
					</span>
				</li>
				<li style="padding: 12px 0 0 0;margin: 0 10px">
					<span style="padding: 0;">
						<a href="https://ddc.moph.go.th/index.php" role="button">
							<img class="center" src="{{$site}}/Defaultimg/logo-thai.png" width="30" height="20">
						</a>
					</span>
				</li>
				<li style="border-left: 1px solid white;">
					<a href="tel:1422" style="text-decoration: none;margin-right: 20px" role="button">
						<span class="fa" style="padding: 0">
						<img id="telephone" style="margin: 0 0 9px 9px;" src="{{$site}}/Defaultimg/call.svg" alt="Hotline 1422" width="30" height="25">
						</span>
						<span style="color: white;padding: 0 5px">1422</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
{{--navtwo--}}

<div style="background-color: white;">
	<nav class="navbar navbar-expand-md" style=" padding: 0 20px;">
		<div class="collapse navbar-collapse flex-column align-items-start ml-lg-2 ml-0" id="navbarCollapse" style="background-color: transparent;z-index: 2;font-size: 16px">
			<ul class="navbar-nav mb-auto mt-0 ml-auto" style="padding: 8px 0;height: 60px;">
				<li class="nav-item" style="margin-top: 10px;" role="button">
					<a role="button" id="itemdownnav" href="https://ddc.moph.go.th/en/index.php" class="nav-link py-0" style="color: black;">Home</a>
				</li>
				<li class="nav-item dropdown" style="margin-top: 10px;">
					<a id="itemdownnav" style="padding: 0" style="text-decoration: none" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button">
						About Department </a>
					<div class="dropdown-menu">
						<ul style="list-style: none;padding: 0">
							<li>
								<a style="text-decoration: none;width: 100%;" href="http://ddc.moph.go.th/en/history.php" role="button">History</a>
							</li>
							<li>
								<a style="text-decoration: none;width: 100%;" href="http://ddc.moph.go.th/en/organization.php" role="button">Organization</a>
							</li>
							<li>
								<a style="text-decoration: none;width: 100%;" href="http://ddc.moph.go.th/en/contact.php" role="button">Contact</a>
							</li>
						</ul>
					</div>
				</li>

			</ul>
		</div>
	</nav>
</div>
<nav class="navbar navbar-expand-lg navbar-light white scrolling-navbar" id="forcollapse">
	<div class="container-fluid">
		<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarContent"
				aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
			<span role="presentation" class="navbar-toggler-icon" tabindex="5" onclick="opencollapse()"></span>
		</button>
	</div>
</nav>

<?php
$down = \App\menuheader::where('level','down')->where('active',1)->with('children')->get();
?>
<div id="onlycollapse">
	<div class="row">
		<div class="col-12">
			<div class="float-right">
				<a role="button" style="color: white;font-size: 30px;padding: 20px;text-decoration: none;" tabindex="6" onclick="closecollapse();">X</a>
			</div>
			<p style="margin-left: 10%;color: white;font-size: 26px;">
				<span>
					<a role="button" style="text-decoration: none;width: 100%;color: white;" href="https://ddc.moph.go.th/en/index.php" >Home</a>
				</span>
			</p>
		</div>
		<div class="col-12">
			<span style="margin-left: 5%;color: white;font-size: 26px">About Department</span>
		</div>
		<div class="col-12">
			<p style="margin-left: 10%;color: white;font-size: 16px">
				<span>
					<a role="button" style="text-decoration: none;width: 100%;color: white;" href="http://ddc.moph.go.th/en/history.php">History</a>
				</span>
			</p>
		</div>
		<div class="col-12">
			<p style="margin-left: 10%;color: white;font-size: 16px">
				<span>
					<a role="button" style="text-decoration: none;width: 100%;color: white;" href="http://ddc.moph.go.th/en/organization.php">Organization</a>
				</span>
			</p>
		</div>
		<div class="col-12">
			<p style="margin-left: 10%;color: white;font-size: 16px">
				<span>
					<a role="button" style="text-decoration: none;width: 100%;color: white;" href="http://ddc.moph.go.th/en/contact.php">Contact</a>
				</span>
			</p>
		</div>
	</div>
</div>
<?php
$banner = \Illuminate\Support\Facades\DB::table('homebanner')->where('url','2')->first();
if($banner)$path = json_decode($banner->path);
else $path = null;
?>
<div>
	@if($banner)
		<a role="button" href="{{$path != null ? $path[1]:''}}" style="height: 200px;" target="_blank">
			<img src="{{$path != null && $path[0] != null ? $site.'/uploads/'.$path[0]:$site.'/Defaultimg/noimg.jpg'}}" width="100%" height="auto">
		</a>
	@else
		<div style="height: 200px;background-color: #DBE8EB;"></div>
	@endif
</div>

<script>
	function goLogin() {
		location.href = "{{route('login')}}";
  	}

    function Dropdown(id) {
        document.getElementById("Dropdown"+id).classList.toggle("show");
    }

    const $menu = $('.dropdown-menu');
    $(document).mouseup(e => {
        if (!$menu.is(e.target) && $menu.has(e.target).length === 0)
        {
            $menu.removeClass('show');
        }
    });

    function opencollapse() {
        document.getElementById("onlycollapse").style.width = "90%";
    }
    function closecollapse() {
        document.getElementById("onlycollapse").style.width = "0";
    }

    $(document).mouseup(function(e)
    {
        let container = $("#onlycollapse");

        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            document.getElementById("onlycollapse").style.width = "0";
        }
    });
</script>
