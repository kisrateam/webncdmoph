<style type="text/css">
	#padding-small{
		padding: 5px 10% 5px 0;
	}
	#formobile{
		display: none;
	}
	#forpc{
		display: block;
	}
	@media screen and (max-width : 767px){
		#padding-small{
			padding: 5px 10% 5px 10%;
		}
		#formobile{
			display: block;
		}
		#forpc{
			display: none;
		}
		.sidebar {padding-top: 15px;}
		.sidebar a {font-size: 18px;}
	}
	.center {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}

	.sidebar {
		height: 100%;
		width: 0;
		position: fixed;
		z-index: 5;
		top: 0;
		left: 0;
		background-color: white;
		overflow-x: hidden;
		transition: 1s;
		padding-top: 10%;
	}

	.sidebar .closebtn {
		font-size: 36px;
		padding: 0;
		color: #508EBF;
		transition: 0.3s;
	}

	.openbtn {
		background-color: #508EBF;
		border-radius: 20px;
		color: white;
		outline: none;
		box-shadow: none;
		padding: 0;
	}

	#main {
		transition: margin-left 1s;
		margin-top: 16px;
	}

	.loginbtn {
		min-width: 50%;
		background-color: white;
		color: #508EBF;
		font-size: 14px;
		border: 1px solid #508EBF;
		box-sizing: border-box;
		border-radius: 20px;
		margin: 0;
		padding: 0 10px;
		height: 33px;
		box-shadow: none;
	}

	.dropdown-content2 {
		display: none;
		position: absolute;
		background-color: white;
		min-width: 160px;
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
		top: 50px;
		z-index: 1;
		border: 1px solid #508EBF;
		border-radius: 5px;
	}
	.dropdown-content2:before {
		content: "";
		position: absolute;
		top: -30px;
		left: 25%;
		border: 15px solid transparent;
		border-bottom-color: #508EBF;
	}
	.dropdown-content2 a {
		padding: 20px 16px;
		text-decoration: none;
	}
	.dropdown-content2 a:hover {
		color: white;
		background-color: #508EBF;
	}

	div#mySidebar.sidebar div.row div.col-9 a{
		font-size: 14px;
	}

	div#forpc.list-group div.row{
		cursor: pointer;
	}
	.colornavleft.active{
		color: #508EBF;
	}
	.colornavleft img.active{
		filter: invert(66%) sepia(8%) saturate(4090%) hue-rotate(176deg) brightness(79%) contrast(86%);
	}
	.colornavleft{
		margin: 0;
	}
	.colornavleft:hover{
		background-color: #c3e9f6;
		color: white;
		border-radius: 10px;
	}
</style>
<div class="list-group forcolor font-size-change" id="forpc" style="
                          box-shadow: 0 1px 7px 1px rgba(0, 0, 0, 0.1);
                          border-radius: 10px;
                          margin-top: 20px;
                          padding: 15px 0;
                          min-width: 100px;
                          line-height: 30px;
                          ">
	<div class="row colornavleft {{\Route::currentRouteName() == 'welcome'?'active':''}}" tabindex="0" onclick="gomenu('{{$site}}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0" role="button" tabindex="0">
			<img src="{{$site}}/Defaultimg/1.svg" class="center {{\Route::currentRouteName() == 'welcome'?'active':''}}" alt="welcome">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">หน้าแรก</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'team'?'active':''}}" tabindex="0" onclick="gomenu('{{ route('team') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0" role="button" tabindex="1">
			<img src="{{$site}}/Defaultimg/2.svg" class="center {{\Route::currentRouteName() == 'team'?'active':''}}" alt="team">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">คณะกรรมการ</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'content'?'active':''}}"  tabindex="0" onclick="gomenu('{{ route('content') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="2">
			<img src="{{$site}}/Defaultimg/3.svg" class="center {{\Route::currentRouteName() == 'content'?'active':''}}" alt="content">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'result'?'active':''}}"  onclick="gomenu('{{ route('result') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="3">
			<img src="{{$site}}/Defaultimg/4.svg" class="center {{\Route::currentRouteName() == 'result'?'active':''}}" alt="result">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">ผลการดำเนินงาน</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'best'?'active':''}}"  onclick="gomenu('{{ route('best') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="4">
			<img src="{{$site}}/Defaultimg/best.svg" class="center {{\Route::currentRouteName() == 'best'?'active':''}}" alt="best">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">Best Practice</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'new'?'active':''}}" onclick="gomenu('{{ route('new') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="5">
			<img src="{{$site}}/Defaultimg/6.svg" class="center {{\Route::currentRouteName() == 'new'?'active':''}}" alt="new">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">ข่าวสาร</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'media'?'active':''}}" onclick="gomenu('{{ route('media') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="6">
			<img src="{{$site}}/Defaultimg/7.svg" class="center {{\Route::currentRouteName() == 'media'?'active':''}}" alt="media">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">สื่อเผยแพร่</a>
		</div>
	</div>
	<div class="row colornavleft {{\Route::currentRouteName() == 'topic'?'active':''}}" onclick="gomenu('{{ route('topic') }}')">
		<div class="col-sm-12 col-md-4" style="padding: 5px 0;" role="button" tabindex="7">
			<img src="{{$site}}/Defaultimg/question.svg" class="center {{\Route::currentRouteName() == 'topic'?'active':''}}" alt="topic">
		</div>
		<div id="padding-small" class="text-left col-sm-12 col-md-8">
			<a role="button" style="color: black;">กระดานสนทนา</a>
		</div>
	</div>
</div>
<div id="formobile">
	<div id="mySidebar" class="sidebar">

		<div class="row">
			<div class="col-12" style="padding: 0 9%">
				<img alt="flag" onclick="window.open('{{\Request::is('home/eng') ? $site:route('eng')}}','_self')" style="border-radius: 100%;position: relative;cursor: pointer" src="{{$site}}/Defaultimg/{{\Request::is('home/eng') ? 'th.svg':'eng.svg'}}">
			</div>
			<div class="col-12" style="margin-top: 3%;padding: 0 9%">
				@guest
					<a role="button" href="#" onclick="switchlabel(0);" class="btn loginbtn" data-toggle="modal" data-target="#loginModal"><i role="presentation"  style="padding-right: 10px" class="far fa-user"></i>เข้าสู่ระบบ</a>
				@else
					<button onclick="dropdown1()" class="dropbtn dropdown-toggle">
						{{ Auth::user()->name }}</button>
					<div id="myDropdown1" class="dropdown-content2">
						<a role="button" class="dropdown-item" href="{{ route('showchangeprofile',['id'=>Auth::user()->id]) }}">
							{{ __('เปลี่ยนข้อมูลส่วนตัว') }}
						</a>
						@if(Auth::user()->role == 'admin')
							<a role="button" class="dropdown-item" href="{{ route('admin')}}">
								{{ __('หน้าแอดมิน') }}
							</a>
						@endif
						<a role="button" class="dropdown-item" href="{{ route('logout') }}"
						   onclick="event.preventDefault();
							document.getElementById('logout-form1').submit();">
							{{ __('ออกจากระบบ') }}
						</a>
						<form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</div>
				@endguest
				<div class="float-right">
					<a role="button" href="{{route('rssfeed')}}">
						<span>
						  <img alt="rss" src="{{$site}}/Defaultimg/rssicon.png" width="80" height="33">
						</span>
					</a>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top: 7%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/1.svg" class="center" alt="หน้าแรก">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{$site}}">หน้าแรก</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/2.svg" class="center" alt="คณะกรรมการ">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('team') }}">คณะกรรมการ</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/3.svg" class="center" alt="แผนการป้องกันและควบคุมโรคไม่ติดต่อ">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('content') }}">แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี (พ.ศ. 2560 - 2564)</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/4.svg" class="center" alt="ผลการดำเนินงาน">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('result') }}">ผลการดำเนินงาน</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/best.svg" class="center" alt="Best Practice">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('best') }}">Best Practice</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/6.svg" class="center" alt="ข่าวสาร">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('new') }}">ข่าวสาร</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/7.svg" class="center" alt="สื่อเผยแพร่">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('media') }}">สื่อเผยแพร่</a>
			</div>
		</div>
		<div class="row" style="margin-top: 5%;">
			<div class="col-3" style="padding: 5px 0">
				<img src="{{$site}}/Defaultimg/question.svg" class="center" alt="กระดานสนทนา">
			</div>
			<div class="col-9">
				<a role="button" style="color: black;" href="{{ route('topic') }}">กระดานสนทนา</a>
			</div>
		</div>

	</div>

	<div id="main">
		<button class="btn btn-block openbtn" id="openbtn" onclick="openNav()">
			<span style="font-size: 24px;">☰</span>
		</button>
	</div>
</div>
@include('layouts.onlyDatatable')
<script type="text/javascript">
	function gomenu(tourl) {
		window.location.href = tourl;
	}
  function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("openbtn").style.display = "none";
    $('#stopmodal').carousel('pause');
  }

  function dropdown1() {
    document.getElementById("myDropdown1").classList.toggle("show");
  }

  $(document).mouseup(function(e)
  {
    let container = $("#mySidebar");

    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
      document.getElementById("mySidebar").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
      document.getElementById("openbtn").style.display = "block";
      $('#stopmodal').carousel('cycle');
    }
  });

</script>
