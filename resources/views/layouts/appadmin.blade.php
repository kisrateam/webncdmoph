<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/bootstrap.min.css') }}">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/mdb.min.css') }}">
	<!-- Your custom styles (optional) -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/style.css') }}">
	<!-- Custom styles for this template -->
	<link rel="stylesheet" href="{{ URL::asset('MDB/css/simple-sidebar.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<!-- Datatable -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.3/css/buttons.bootstrap4.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style>
	.sidenav {
		width: 250px;
		position: absolute;
		top: 17%;
		z-index: 1;
		left: 15px;
		background: white;
		overflow-x: hidden;
		padding: 8px 0;
	}
	.main {
		margin-left: 270px;
		font-size: 28px;
		padding: 0 10px;
	}
	.sidenav a {
		font-size: 14px;
		color: black;
		display: block;
		vertical-align: bottom;
	}
	.sidenav a:hover {
		color: #064579;
	}
	@media screen and (max-height: 450px) {
		.sidenav {padding-top: 15px;}
		.sidenav a {font-size: 18px;}
	}
</style>
<div id="page-content-wrapper">
	@include('admin.navright')
	@include('sweetalert::alert')
	<body>
		<div style="min-height: 90%">
			<div class="sidenav">
				@include('admin.menuleft')
			</div>
			<div class="main"  style="padding-top: 5%;">
				@yield('content')
			</div>
		</div>
		@include('layouts.footeradmin')
	</body>
	@stack('scripts')
</div>


@include('layouts.onlyDatatable')
<script type="text/javascript" src="{{ URL::asset('MDB/js/mdb.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('MDB/js/bootstrap.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>
</html>
