@extends('layouts.appadmin')
@section('content')
<div class="container-fluid">
    <div class="col-12">
        <img style="width: 44px;
    height: 44px;
    padding-bottom: 5px;" src="{{$site}}/Defaultimg/best.svg"/>
        <span style="margin-left:10px;font-weight: bold;vertical-align: bottom;font-size: 20px"> การจัดการ Best practice</span>
        <hr style="border: 1px solid #3D3D3D;">
    </div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
		<thead>
		<tr>
			<th>ไอดี</th>
			<th>ชื่อหัวข้อ</th>
			<th>ยุทธศาสตร์</th>
			<th>ตัวชี้วัด</th>
			<th>รูป</th>
			<th>URL</th>
		</tr>
		</thead>
		<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{$row->id}}</td>
				<td>{{$row->name}}</td>
				<td>{{$row->getgroup()}}</td>
				<td>{{$row->getsubgroup()}}</td>
				<td>
					@if($row->img === null)
						ไม่พบรูป
					@else
						<a class="pop">
							<img class="pop" id="imageresource" src="{{$site}}/uploads/{{$row->img->path}}" style="max-height: 100px;max-width: 100px;"/>
						</a>
					@endif
				</td>
                <td><a href="{{$row->url}}" target="_blank">{{$row->url}}</a></td>
            </tr>
		@endforeach
		</tbody>
	</table>
</div>
@endsection

@include('layouts.onlyDatatable')

<script type="text/javascript">
  var id = 0;

  $(document).ready(function() {
    var table = $('#example').DataTable({
      lengthChange: false,
      columnDefs: [
        { width: 55, targets: 1 },
        {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
        },
      ],
      "pageLength": 5,
      "pagingType": "full_numbers",
      buttons: [
          {
          text: 'เพิ่มข้อมูล',
          action: function ( e, dt, node, config ) {
            window.location.href = '{{route('best.create')}}';
          }
        },
        {
          text: 'แก้ไขข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              window.location.href = '{{$site}}/admin/best/'+id[0]+'/edit';
            });
          }
        },
        {
          text: 'ลบข้อมูล',
          action: function ( e, dt, node, config) {
            $('#example .selected').each(function() {
              Swal.fire({
                title: 'คุณแน่ใจที่จะลบ ?',
                text: "โปรดดูให้แน่ใจว่าที่เลือกนั้นถูกต้อง !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่ ลบเลย !',
                cancelButtonText : 'ยกเลิก'
              }).then((result) => {
                if (result.value) {
                  goDelete();
                  table.row('.selected').remove().draw( false );
                }
              })
            });
          }
        }
      ],
      responsive: true,
      "language": {
        "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
        "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
        "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
        "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "แสดง _MENU_ แถว",
        "sLoadingRecords": "กำลังโหลดข้อมูล...",
        "sProcessing":     "กำลังดำเนินการ...",
        "sSearch":         "ค้นหา: ",
        "sZeroRecords":    "ไม่พบข้อมูล",
        "oPaginate": {
          "sFirst":    "หน้าแรก",
          "sPrevious": "ก่อนหน้า",
          "sNext":     "ถัดไป",
          "sLast":     "หน้าสุดท้าย"
        },
        "oAria": {
          "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
          "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
        }
      },
    });

    table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );

    $('#example tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } );

    $('#example').on( 'click', 'tr', function () {
      id = table.row( this ).data();
    } );

      function goDelete() {
          var _token = $('input[name="_token"]').val();
          $.ajax(
              {
                  type: "DELETE",
                  url: "{{ route('best.store') }}"+'/'+id[0],
                  data:{ _token:_token  },
                  success: function (data) {
                      Swal.fire({
                          position: 'top-end',
                          icon: 'success',
                          title: 'ข้อมูลถูกลบแล้ว',
                          showConfirmButton: false,
                          timer: 3000
                      })
                  },
                  error: function (data) {
                      Swal.fire({
                          icon: 'error',
                          title: 'อุ๊บบ...',
                          text: 'มีบางอย่างผิดพลาด !'
                      })
                  }
              });
      }

  } );


</script>