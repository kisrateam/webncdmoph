@extends('layouts.appadmin')
@section('content')
	<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
	@include('layouts.onlyDatatable')
	<style>
		.ck-editor__editable {
			min-height: 200px;
		}
	</style>
	<form method="POST" action=" {{route('best.update',$data->id)}} " enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">แก้ไขข้อมูล Best Practice</h3>
							<!-- Name -->
							<div class="md-form">
								<input type="text" id="name" name="name" value="{{$data->name}}" class="form-control col-md-12" autocomplete="off">
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<!-- Message -->
							<div class="form-group">
								<label for="Textarea2">รายละเอียด</label>
								<textarea name="details" class="form-control rounded-0" id="Textarea2" rows="3">{{ $data->details }}</textarea>
							</div>
							<div class="md-form">
								@if($data->img !== null)
									<div align="center">
										ตัวอย่างรูปเก่า
									</div>
									<div align="center">
											<img class='thumb' style="max-height: 100px;max-width: 100px;" src="{{$site}}/uploads/{{$data->img->path}}">
									</div>
								@endif
									<div align="center">
										ตัวอย่างรูปที่เปลี่ยน
									</div>
									<div align="center">
										<div id="previewImg"></div>
									</div>
									<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
								</div>
									<script type="text/javascript">
                    window.preview = function (input) {
                      if (input.files && input.files[0]) {
                        $("#previewImg").html("");
                        $(input.files).each(function () {
                          var reader = new FileReader();
                          reader.readAsDataURL(this);
                          reader.onload = function (e) {
                            $("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
                          }
                        });
                      }
                    }
									</script>
									<div class="md-form">
										<input type="text" id="url" name="url" value="{{ $data->url }}" class="form-control col-md-12" autocomplete="off" required>
										<label for="url">ลิ้ง URL</label>
									</div>
									<div class="md-form">
										<select type="text" id="group" name="group" class="form-control col-md-12" autocomplete="off">
											<option disabled selected hidden>ประเภท</option>
											<option {{($data->group == 1 ? 'selected':'')}} value="1">ยุทธศาสตร์ที่ 1</option>
											<option {{($data->group == 2 ? 'selected':'')}} value="2">ยุทธศาสตร์ที่ 2</option>
											<option {{($data->group == 3 ? 'selected':'')}} value="3">ยุทธศาสตร์ที่ 3</option>
											<option {{($data->group == 4 ? 'selected':'')}} value="4">ยุทธศาสตร์ที่ 4</option>
											<option {{($data->group == 5 ? 'selected':'')}} value="5">ยุทธศาสตร์ที่ 5</option>
											<option {{($data->group == 6 ? 'selected':'')}} value="6">ยุทธศาสตร์ที่ 6</option>
										</select>
									</div>
									<div class="row">
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox1" value="1" name="subgroup[]" {{(strpos($data->subgroup,strval('1')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox1">ตัวชี้วัดที่ 1</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox2" value="2" name="subgroup[]" {{(strpos($data->subgroup,strval('2')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox2">ตัวชี้วัดที่ 2</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox3" value="3" name="subgroup[]" {{(strpos($data->subgroup,strval('3')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox3">ตัวชี้วัดที่ 3</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox4" value="4" name="subgroup[]" {{(strpos($data->subgroup,strval('4')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox4">ตัวชี้วัดที่ 4</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox5" value="5" name="subgroup[]" {{(strpos($data->subgroup,strval('5')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox5">ตัวชี้วัดที่ 5</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox6" value="6" name="subgroup[]" {{(strpos($data->subgroup,strval('6')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox6">ตัวชี้วัดที่ 6</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox7" value="7" name="subgroup[]" {{(strpos($data->subgroup,strval('7')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox7">ตัวชี้วัดที่ 7</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox8" value="8" name="subgroup[]" {{(strpos($data->subgroup,strval('8')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox8">ตัวชี้วัดที่ 8</label>
											</div>
										</div>
										<div class="col-4">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox9" value="9" name="subgroup[]" {{(strpos($data->subgroup,strval('9')) !== false ? 'checked':'')}}>
												<label class="custom-control-label" for="checkbox9">ตัวชี้วัดที่ 9</label>
											</div>
										</div>
									</div>
								{{ method_field('PUT') }}
								<button class="btn btn-info btn-block" type="submit">บันทึก</button>
							</div>
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection

<script>
	function deletesome(id,filename) {
    var _token = $('input[name="_token"]').val();
    Swal.fire({
      title: 'คุณแน่ใจใช่ไหม ?',
      text: "ลบไฟล์ !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'ยกเลิก',
      confirmButtonText: 'ใช่ ลบเลย!'
    }).then((result) => {
      if (result.value) {
        $.ajax(
          {
            type: "GET",
            url: '/contentact/deletesome/'+id+'/'+filename,
            data:{ _token:_token},
            success: function (data) {
              console.log(data);
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'ไฟล์ถูกลบแล้ว',
                showConfirmButton: false,
                timer: 3000
              });
              location.reload();
            },
            error: function (data) {
              console.log(data);
              Swal.fire({
                icon: 'error',
                title: 'อุ๊บบ...',
                text: 'มีบางอย่างผิดพลาด !'
              })
            }
          });
      }
    })
  }
</script>
