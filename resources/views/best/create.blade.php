@extends('layouts.appadmin')
@section('content')
	<script type="text/javascript" src="{{ URL::asset('CKEditor/ckeditor.js') }}"></script>
	@include('layouts.onlyDatatable')
	<style>
		.ck-editor__editable {
			min-height: 200px;
		}
	</style>
	<form method="POST" action="{{ route('best.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="container-fluid py-1">
			<!--Section: Content-->
			<section class="px-md-6 mx-md-6 text-center text-lg-left dark-grey-text">
				<!--Grid row-->
				<div class="row d-flex justify-content-center">
					<!--Grid column-->
					<div class="col-md-6">
						<!-- Default form contact -->
						<form class="text-center" action="#!">
							<h3 align="center" class="font-weight-bold">เพิ่มข้อมูล Best Practice</h3>
							<div class="md-form">
								<input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control col-md-12" autocomplete="off" required>
								<label for="name">ชื่อหัวข้อ</label>
							</div>
							<!-- Message -->
							<div class="form-group">
								<label for="Textarea2">รายละเอียด</label>
								<textarea name="details" class="form-control rounded-0" id="Textarea2" rows="3">{{ old('details') }}</textarea>
							</div>
							{{--<textarea class="details" name="details">{{ old('details') }}--}}
							{{--</textarea>--}}
							{{--<script>--}}
                {{--var CSRFToken = $('meta[name="csrf-token"]').attr('content');--}}
                {{--var options = {--}}
                  {{--filebrowserImageBrowseUrl: '{{$site}}/laravel-filemanager?type=Images',--}}
                  {{--filebrowserImageUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Images&_token='+CSRFToken,--}}
                  {{--filebrowserBrowseUrl: '{{$site}}/laravel-filemanager?type=Files',--}}
                  {{--filebrowserUploadUrl: '{{$site}}/laravel-filemanager/upload?type=Files&_token='+CSRFToken,--}}
                {{--};--}}
							{{--</script>--}}
							{{--<script>--}}
                {{--CKEDITOR.replace('details', options);--}}
							{{--</script>--}}
							{{--<div class="md-form">--}}
									{{--<a class="btn btn-outline-primary" name="addDom">เพิ่มไฟล์ข้อมูล</a>--}}
							{{--</div>--}}
							{{--<div class="md-form">--}}
							{{--<div class="row" id="addDom">--}}
								{{--</div>--}}
							{{--</div>--}}
							<div class="md-form">
								<div align="center">
									ตัวอย่างรูป
									<div id="previewImg"></div>
								</div>
								<div class="custom-file">
									<i role="presentation"  class="far fa-images prefix"></i>
									<input type="file" class="custom-file-input @error('img') is-invalid @enderror" id="img" name="img" onchange="preview(this);">
									<label class="custom-file-label" for="img">เลือกไฟล์รูปภาพ(1:1)</label>
								</div>
							</div>
							<script type="text/javascript">
								window.preview = function (input) {
								  if (input.files && input.files[0]) {
									$("#previewImg").html("");
									$(input.files).each(function () {
									  var reader = new FileReader();
									  reader.readAsDataURL(this);
									  reader.onload = function (e) {
										$("#previewImg").append("<img class='thumb' style=\"max-height: 100px;max-width: 100px;\" src='" + e.target.result + "'>");
									  }
									});
								  }
								}
							</script>
							<div class="md-form">
								<input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control col-md-12" autocomplete="off" required>
								<label for="url">ลิ้ง URL</label>
							</div>
							<div class="md-form">
								<select type="text" id="group" name="group" class="form-control col-md-12" autocomplete="off">
									<option disabled selected hidden>ประเภท</option>
										<option value="1">ยุทธศาสตร์ที่ 1</option>
										<option value="2">ยุทธศาสตร์ที่ 2</option>
										<option value="3">ยุทธศาสตร์ที่ 3</option>
										<option value="4">ยุทธศาสตร์ที่ 4</option>
										<option value="5">ยุทธศาสตร์ที่ 5</option>
										<option value="6">ยุทธศาสตร์ที่ 6</option>
								</select>
							</div>
							<div class="row">
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox1" value="1" name="subgroup[]">
										<label class="custom-control-label" for="checkbox1">ตัวชี้วัดที่ 1</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox2" value="2" name="subgroup[]">
										<label class="custom-control-label" for="checkbox2">ตัวชี้วัดที่ 2</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox3" value="3" name="subgroup[]">
										<label class="custom-control-label" for="checkbox3">ตัวชี้วัดที่ 3</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox4" value="4" name="subgroup[]">
										<label class="custom-control-label" for="checkbox4">ตัวชี้วัดที่ 4</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox5" value="5" name="subgroup[]">
										<label class="custom-control-label" for="checkbox5">ตัวชี้วัดที่ 5</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox6" value="6" name="subgroup[]">
										<label class="custom-control-label" for="checkbox6">ตัวชี้วัดที่ 6</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox7" value="7" name="subgroup[]">
										<label class="custom-control-label" for="checkbox7">ตัวชี้วัดที่ 7</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox8" value="8" name="subgroup[]">
										<label class="custom-control-label" for="checkbox8">ตัวชี้วัดที่ 8</label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="checkbox9" value="9" name="subgroup[]">
										<label class="custom-control-label" for="checkbox9">ตัวชี้วัดที่ 9</label>
									</div>
								</div>
							</div>

							<button class="btn btn-info btn-block" type="submit">บันทึก</button>
						</form>
						<!-- Default form contact -->
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->
			</section>
			<!--Section: Content-->
		</div>
	</form>
@endsection
@include('layouts.onlyDatatable')
<script>

</script>
