<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class img extends Model
{
	protected $table='img';

	protected $casts = [
		'name' => 'array',
		'path' => 'array'
	];

	protected $fillable = [
		'idfk','name','path','edited_by'
	];
}
