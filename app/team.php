<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class team extends Model
{
		protected $table='team';

		protected $fillable = [
			'name','rank','fileuniq','edited_by'
		];

		public function img(){
			return $this->hasOne(img::class,'idfk','imguniq');
		}

		public function file(){
			return $this->hasOne(files::class,'idfk','fileuniq');
		}
}
