<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class best extends Model
{
    protected $table='best';

    protected $fillable = [
        'name','details','group','subgroup','url','imguniq'
    ];

    public function img(){
        return $this->hasOne(img::class,'idfk','imguniq');
    }

    public function getgroup()
    {
        switch($this->group){
            case 1:$group = 'ยุทธศาสตร์ที่ 1';
                break;
            case 2:$group = 'ยุทธศาสตร์ที่ 2';
                break;
            case 3:$group = 'ยุทธศาสตร์ที่ 3';
                break;
            case 4:$group = 'ยุทธศาสตร์ที่ 4';
                break;
            case 5:$group = 'ยุทธศาสตร์ที่ 5';
                break;
            case 6:$group = 'ยุทธศาสตร์ที่ 6';
                break;
            default : $group = 'ไม่พบ';
                break;
        };
        return $group;
    }

    public function getsubgroup()
    {
        $subgroup = '';
        $strlen = strlen($this->subgroup)-1;

        if(strpos($this->subgroup,'1') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 1';
            if(strpos($this->subgroup,'1') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'2') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 2';
            if(strpos($this->subgroup,'2') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'3') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 3';
            if(strpos($this->subgroup,'3') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'4') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 4';
            if(strpos($this->subgroup,'4') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'5') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 5';
            if(strpos($this->subgroup,'5') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'6') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 6';
            if(strpos($this->subgroup,'6') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->supgroup,'7') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 7';
            if(strpos($this->subgroup,'7') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'8') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 8';
            if(strpos($this->subgroup,'8') != $strlen){
                $subgroup .= ', ';
            }
        }
        if(strpos($this->subgroup,'9') !== false) {
            $subgroup .= 'ตัวชี้วัดที่ 9';
            if(strpos($this->subgroup,'9') != $strlen){
                $subgroup .= ', ';
            }
        }
        return $subgroup;
    }
}
