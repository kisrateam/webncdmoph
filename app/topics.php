<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class topics extends Model
{
	use SoftDeletes;

	protected $table='topics';

	protected $fillable = [
		'name','details','userid','hitcount','status','privacy','category','subcategory'
	];

	public function users(){
		return $this->hasOne(User::class,'id','userid')->withTrashed();
	}

	public function comments(){
		return $this->hasMany(comments::class,'topicid','id');
	}

    public function name()
    {
        $name = $this->autoban_badword($this->name);
        return $name;
    }

    public function username()
    {
        $username = $this->autoban_badword($this->users->name);
        return $username;
    }

    public function details()
    {
        $details = $this->autoban_badword($this->details);
        return $details;
    }

    public function getcategory()
    {
        switch($this->category){
            case 1:$category = 'ยุทธศาสตร์ที่ 1';
                break;
            case 2:$category = 'ยุทธศาสตร์ที่ 2';
                break;
            case 3:$category = 'ยุทธศาสตร์ที่ 3';
                break;
            case 4:$category = 'ยุทธศาสตร์ที่ 4';
                break;
            case 5:$category = 'ยุทธศาสตร์ที่ 5';
                break;
            case 6:$category = 'ยุทธศาสตร์ที่ 6';
                break;
            default : $category = 'ไม่พบ';
                break;
            };
        return $category;
    }

    public function getsubcategory()
    {
        switch($this->subcategory){
            case 1:$subcategory = 'แผนและการดำเนินงาน';
                break;
            case 2:$subcategory = 'รายงานผลดำเนินงาน';
                break;
            case 3:$subcategory = 'การประสานงาน';
                break;
            default : $subcategory = 'ไม่พบ';
                break;
        };
        return $subcategory;
    }

    public function timeago()
    {
        $timeago = $this->gettimeago($this->updated_at);
        return $timeago;
    }

    public function autoban_badword($string){
        $bad = badwords::select('badword')->get();
        $badword = [];
        foreach ($bad as $item){
            $badword[] = $item['badword'];
        }
        $text = str_replace($badword, '***', $string);
        return $text;
    }

    public function gettimeago($times){
        $seconds_ago = (time() - strtotime($times));
        if ($seconds_ago >= 31536000)$time = "อัพเดทเมื่อ " . intval($seconds_ago / 31536000) . " ปี ที่แล้ว";
        elseif ($seconds_ago >= 2419200)$time = "อัพเดทเมื่อ " . intval($seconds_ago / 2419200) . " เดือน ที่แล้ว";
        elseif ($seconds_ago >= 86400)$time = "อัพเดทเมื่อ " . intval($seconds_ago / 86400) . " วัน ที่แล้ว";
        elseif ($seconds_ago >= 3600)$time = "อัพเดทเมื่อ " . intval($seconds_ago / 3600) . " ชั่วโมง ที่แล้ว";
        elseif ($seconds_ago >= 60)$time = "อัพเดทเมื่อ " . intval($seconds_ago / 60) . " นาที ที่แล้ว";
        else $time = "อัพเดทเมื่อไม่ถึงนาทีที่ผ่านมา";
        return $time;
    }
}
