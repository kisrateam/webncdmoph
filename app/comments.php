<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class comments extends Model
{
    use SoftDeletes;

	protected $table='comments';

	protected $fillable = [
		'topicid','message','userid'
	];

    public function topic(){
        return $this->hasOne(topics::class,'id','topicid');
    }
}
