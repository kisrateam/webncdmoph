<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eng extends Model
{
    protected $table='eng';

    protected $fillable = [
        'name','category','fileuniq','active','highlights','edited_by'
    ];
}
