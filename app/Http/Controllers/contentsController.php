<?php

namespace App\Http\Controllers;

use App\content;
use App\files;
use App\img;
use App\eng;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use function Sodium\compare;

class contentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['img'] = img::where('idfk', 'content')->first();
        $data['img1'] = img::where('idfk', 'content1')->first();
        $data['img2'] = img::where('idfk', 'content2')->first();
        $data['img3'] = img::where('idfk', 'content3')->first();
        $data['img4'] = img::where('idfk', 'content4')->first();
        $data['data'] = content::where('category', 0)->get();
        $data['data1'] = content::where('category', 1)->get();
        $data['data2'] = content::where('category', 2)->get();
        $data['data3'] = content::where('category', 6)->get();
        $data['dall1'] = content::where('name', 'downloadall1')->first();
        $data['dall2'] = content::where('name', 'downloadall2')->first();
        $data['dall3'] = content::where('name', 'downloadall3')->first();
        $data['dall4'] = content::where('name', 'downloadall4')->first();
        return view('content.index', compact('data'));
    }

    public function indexuser()
    {
        return view('user.content');
    }

    public function searchcontents(Request $request)
    {
        $arrnot = ['downloadall1','downloadall2','downloadall3','downloadall4'];
        if($request->get('query')){
            $query  = $request->get('query');
            $pattern = preg_quote($query);
            $data = new content();
            $data = $data->select('id','name','fileuniq')->whereNotIn('name',$arrnot)->Where('name','LIKE',"%{$query}%")->paginate(10);
            if(count($data) > 0){
                $outout = '<div class="dropdown-menu" style="display:block;width: 100%;position: absolute;z-index:10;top:8px;background-color: white;opacity: 0.9">';
                $site = 'https://ddc.moph.go.th/dncd/ncdstrategy/public/';
                foreach ($data as $row) {
                    $outout .= '
                    <a class="dropdown-item" style="overflow:hidden" onclick="window.open(&quot;'.$site.$row->fileuniq.'&quot;);" target="_blank">'.
                        preg_replace("/($pattern)/i", '<span class="hlsearch">$1</span>', $row->name)
                        .'</a>
                    <div class="dropdown-divider"></div>
                    ';}
                $outout .= '</div>';
                echo $outout;
            }else{
                $outout = '<div class="dropdown-menu" style="display:block;width: 100%;position: absolute;z-index:10;background-color: white;opacity: 0.9;top:8px;">';
                $outout .= '
                <a class="dropdown-item" style="overflow:hidden">ไม่พบการค้นหา</a>
                <div class="dropdown-divider"></div>
                ';
                $outout .= '</div>';
                echo $outout;
            }
        }
    }

    public function searchcontentseng(Request $request)
    {
        $arrnot = ['downloadall1','downloadall2','downloadall3','downloadall4'];

        if($request->get('query')){
            $query  = $request->get('query');
            $pattern = preg_quote($query);
            $data = new eng();
            $data = $data->select('id','name','fileuniq')->whereNotIn('name',$arrnot)
                ->whereRaw('LOWER(`name`) LIKE ? ',['%'.trim(strtolower($query)).'%'])
                ->paginate(10);
            if(count($data) > 0){
                $outout = '<div class="dropdown-menu" style="display:block;width: 100%;position: absolute;z-index:10;top:8px;background-color: white;opacity: 0.9">';
                $site = 'https://ddc.moph.go.th/dncd/ncdstrategy/public/';
                foreach ($data as $row) {
                    $outout .= '
                    <a class="dropdown-item" style="overflow:hidden" onclick="window.open(&quot;'.$site.$row->fileuniq.'&quot;);" target="_blank">'.
                        preg_replace("/($pattern)/i", '<span class="hlsearch">$1</span>', $row->name)
                        .'</a>
                    <div class="dropdown-divider"></div>
                    ';}
                $outout .= '</div>';
                echo $outout;
            }else{
                $outout = '<div class="dropdown-menu" style="display:block;width: 100%;position: absolute;z-index:10;background-color: white;opacity: 0.9;top:8px">';
                $outout .= '
                <a class="dropdown-item" style="overflow:hidden">'.'ไม่พบการค้นหา'.'</a>
                <div class="dropdown-divider"></div>
                ';
                $outout .= '</div>';
                echo $outout;
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if($request['category'] == 3){
                if ($request->hasFile('img')) {
                    $oldimg = img::where('idfk', 'content3')->first();
                    $image = $request->file('img');
                    $pathname = date('mdYHis') . uniqid() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('uploads'), $pathname);
                    if ($oldimg) {
                        File::delete(public_path() . '/uploads/' . $oldimg->path);
                        $oldimg->update([
                            'idfk' => 'content3',
                            'path' => $pathname
                        ]);
                    } else {
                        img::create([
                            'idfk' => 'content3',
                            'path' => $pathname
                        ]);
                    }
                }

            }
            else if($request['category'] == 6){
                if($request->hasFile('img') || $request['detailbanner'] || $request['downloadall']){
                    if ($request->hasFile('img')) {
                        $oldimg = img::where('idfk', 'content4')->first();
                        $image = $request->file('img');
                        $pathname = date('mdYHis') . uniqid() . '.' . $image->getClientOriginalExtension();
                        $image->move(public_path('uploads'), $pathname);
                        if ($oldimg) {
                            File::delete(public_path() . '/uploads/' . $oldimg->path);
                            $oldimg->update([
                                'idfk' => 'content4',
                                'path' => $pathname
                            ]);
                        } else {
                            img::create([
                                'idfk' => 'content4',
                                'path' => $pathname
                            ]);
                        }
                    }
                    if ($request['detailbanner']) {
                        $img = img::where('idfk','content4')->first();
                        if ($img) {
                            $img->update([
                                'idfk' => 'content4',
                                'name' => $request['detailbanner']
                            ]);
                        } else {
                            img::create([
                                'idfk' => 'content4',
                                'name' => $request['detailbanner'],
                                'path' => "null"
                            ]);
                        }
                    }
                    if ($request['downloadall']) {
                        $oldurl = content::where('category','7')->first();
                        if ($oldurl) {
                            $oldurl->update([
                                'name' => 'downloadall4',
                                'fileuniq' => $request['downloadall'],
                                'active' => '1',
                                'category' => '7',
                                'edited_by' => \Auth::user()->id
                            ]);
                        } else {
                            content::create([
                                'name' => 'downloadall4',
                                'fileuniq' => $request['downloadall'],
                                'active' => '1',
                                'category' => '7',
                                'edited_by' => \Auth::user()->id
                            ]);
                        }
                    }
                }else{
                    content::create([
                        'name' => $request['name'],
                        'fileuniq' => $request['file'],
                        'active' => ($request['active'] != null ? '1' : '0'),
                        'category' => $request['category'],
                        'edited_by' => \Auth::user()->id
                    ]);
                }

            }
            //img banner store func
            else if ($request->hasFile('img') || $request['detailbanner'] || $request['downloadall']) {
                if ($request->hasFile('img')) {
                    $oldimg = img::where('idfk', $request['category'] == 0 ? 'content' : ($request['category'] == 1 ? 'content1' : 'content2'))->first();
                    $image = $request->file('img');
                    $pathname = date('mdYHis') . uniqid() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('uploads'), $pathname);
                    if ($oldimg) {
                        File::delete(public_path() . '/uploads/' . $oldimg->path);
                        $oldimg->update([
                            'idfk' => $request['category'] == 0 ? 'content' : ($request['category'] == 1 ? 'content1' : 'content2'),
                            'path' => $pathname
                        ]);
                    } else {
                        img::create([
                            'idfk' => $request['category'] == 0 ? 'content' :($request['category'] == 1 ? 'content1' : 'content2'),
                            'path' => $pathname
                        ]);
                    }
                }
                if ($request['detailbanner']) {
                    $img = img::where('idfk', $request['category'] == 0 ? 'content' : ($request['category'] == 1 ? 'content1' : 'content2'))->first();
                    if ($img) {
                        $img->update([
                            'idfk' => $request['category'] == 0 ? 'content' : ($request['category'] == 1 ? 'content1' : 'content2'),
                            'name' => $request['detailbanner']
                        ]);
                    } else {
                        img::create([
                            'idfk' => $request['category'] == 0 ? 'content' : ($request['category'] == 1 ? 'content1' : 'content2'),
                            'name' => $request['detailbanner'],
                            'path' => "null"
                        ]);
                    }
                }
                if ($request['downloadall']) {
                    $oldurl = content::where('category',
                        $request['category'] == 0 ? '3' : ($request['category'] == 1 ? '4' : '5')
                    )->first();
                    if ($oldurl) {
                        $oldurl->update([
                            'name' => $request['category'] == 0 ? 'downloadall1' : ($request['category'] == 1 ? 'downloadall2' : 'downloadall3'),
                            'fileuniq' => $request['downloadall'],
                            'active' => '1',
                            'category' => $request['category'] == 0 ? '3' : ($request['category'] == 1 ? '4' : '5'),
                            'edited_by' => \Auth::user()->id
                        ]);
                    } else {
                        content::create([
                            'name' => $request['category'] == 0 ? 'downloadall1' : ($request['category'] == 1 ? 'downloadall2' : 'downloadall3'),
                            'fileuniq' => $request['downloadall'],
                            'active' => '1',
                            'category' => $request['category'] == 0 ? '3' : ($request['category'] == 1 ? '4' : '5'),
                            'edited_by' => \Auth::user()->id
                        ]);
                    }
                }
            }
            else {
                //normal store
                content::create([
                    'name' => $request['name'],
                    'fileuniq' => $request['file'],
                    'active' => ($request['active'] != null ? '1' : '0'),
                    'category' => $request['category'],
                    'edited_by' => \Auth::user()->id
                ]);
            }
            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            return redirect()->route('contents.index');
        } catch (\Exception $e) {
            Alert::error('ล้มเหลว', $e->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = content::where('id', $id)->first();
        return view('content.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $model = content::where('id', $id)->with('file')->first();
            $model->update([
                'name' => $request['name'],
                'fileuniq' => $request['file'],
                'active' => ($request['active'] != null ? '1' : '0'),
                'category' => $request['category'],
                'edited_by' => \Auth::user()->id
            ]);
            Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
            return redirect()->route('contents.index');
        } catch (\Exception $e) {
            Alert::error('ล้มเหลว', 'เปลี่ยนข้อมูลไม่สำเร็จ');
            return view('content.edit', compact('modal'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            content::where('id', $id)->delete();
            return response()->json(['success' => 'ลบข้อมูลสำเร็จ']);
        } catch (\Exception $e) {
            return response()->json(['error' => $id]);
        }
    }

    public function deleteimg($id)
    {
        try {
            img::where('id', $id)->delete();
            return response()->json(['success' => 'ลบข้อมูลสำเร็จ']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
}
