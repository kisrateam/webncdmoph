<?php

namespace App\Http\Controllers;

use App\comments;
use App\reports;
use App\topics;
use function compact;
use function dd;
use Exception;
use Illuminate\Http\Request;
use function response;
use function view;

class topicBackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('topicback.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('t');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = topics::where('id',$id)->firstorfail();
        return view('topicback.details',compact('data'));
    }

    public function showreports($id)
    {
        $data = reports::where('idreport',$id)->get();
        return view('topicback.reports',compact('data'));
    }

    public function deletereports($id,$type)
    {
        try{
            switch ($type){
                case 'comments':{
                    comments::where('id',$id)->first()->delete();
                    reports::where('idreport',$id)->delete();
                    break;
                }
                case 'topics':{
                    topics::where('id',$id)->first()->delete();
                    comments::where('topicid',$id)->delete();
                    reports::where('idreport',$id)->delete();
                    break;
                }
                case 'ไม่สนใจ':{
                    reports::where('idreport',$id)->delete();
                    break;
                }
            }
            return response()->json(['success'=>'ลบสำเร็จ']);
        }catch (Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd('t');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            topics::where('id',$id)->first()->delete();
            return response()->json(['success'=>$id]);
        }catch (Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
}
