<?php

namespace App\Http\Controllers;

use App\img;
use App\files;
use App\result;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class resultController extends Controller
{
    protected $listof9global;

    public function __construct()
    {
        $this->listof9global = array(
            "9global1",
            "9global2",
            "9global3",
            "9global4",
            "9global5",
            "9global6",
            "9global7",
            "9global8",
            "9global9"
        );
    }

    public function indexuser(){
        return view('user.result');
    }

    public function resulttable(Request $request){
        $edit = result::where('name','resulttable')->first();
        if($edit){
            $edit->update([
                'details' => $request['resulttable'],
                'edited_by' => \Auth::user()->id
            ]);
        }else{
            result::create([
                'name' => 'resulttable',
                'details' => $request['resulttable'],
                'category' => 'ผลการประเมิน 9 global targets',
                'best' => '0',
                'imguniq' => date('mdYHis') . uniqid(),
                'active' => '0',
                'edited_by' => \Auth::user()->id
            ]);
        }
        Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
        return redirect()->route('result.index');
    }

    public function index(){
        $data = result::with('img')->with('icon')->get();
        $dataicon = result::whereIn('name',$this->listof9global)->with('img')->get();
        return view('result.index',compact('data'))->with(compact('dataicon'));
    }

    public function create()
    {
        return view('result.create');
    }

    public function store(Request $request)
    {
        try{
            if($request->has('9global') != null || $request->file('imgicon') !== null){
                $getkeyname = array_keys($request['9global']);
                if($request->file('imgicon') !== null && count($request->file('imgicon'))>0){
                    $getkeyicon = array_keys($request->file('imgicon'));
                }else{
                    $getkeyicon = array_keys(['0'=>0]);
                }
                foreach ($getkeyicon as $keyimg => $icon){
                    foreach ($getkeyname as $name){
                        if($icon == $name){
                            $iconname = $request['9global'][$icon];
                            $pathicon = date('mdYHis') . uniqid() . '.' . $request->file('imgicon')[$icon]->getClientOriginalExtension();
                            $request->file('imgicon')[$icon]->move(public_path('uploads'), $pathicon);
                            $olddata = result::where('name','9global'.$icon)->first();
                            if($olddata){
                                $olddata->update([
                                    'name' => '9global'.$icon,
                                    'details' => $iconname,
                                    'category' => 'ผลการประเมิน 9 global targets',
                                    'imguniq' => date('mdYHis') . uniqid(),
                                    'best' => '0',
                                    'active' => '0',
                                    'edited_by' => \Auth::user()->id
                                ]);
                                $img = img::where('idfk',$olddata->imguniq)->first();
                                if ($img){
                                    File::delete(public_path() . '/uploads/' . $img->path);
                                    $img->update([
                                        'idfk' => $olddata->imguniq,
                                        'name' => $pathicon,
                                        'path' => $pathicon
                                    ]);
                                }else{
                                    img::create([
                                        'idfk' => $olddata->imguniq,
                                        'name' => $pathicon,
                                        'path' => $pathicon
                                    ]);
                                }
                            }else{
                                $newdata = result::create([
                                    'name' => '9global'.$icon,
                                    'details' => $iconname,
                                    'category' => 'ผลการประเมิน 9 global targets',
                                    'best' => '0',
                                    'imguniq' => date('mdYHis') . uniqid(),
                                    'active' => '0',
                                    'edited_by' => \Auth::user()->id
                                ]);
                                $img = img::where('idfk',$newdata->imguniq)->first();
                                if ($img){
                                    File::delete(public_path() . '/uploads/' . $img->path);
                                    $img->update([
                                        'idfk' => $newdata->imguniq,
                                        'name' => $pathicon,
                                        'path' => $pathicon
                                    ]);
                                }else{
                                    img::create([
                                        'idfk' => $newdata->imguniq,
                                        'name' => $pathicon,
                                        'path' => $pathicon
                                    ]);
                                }
                            }
                        }
                    }
                    if($keyimg == 0){
                        foreach ($request['9global'] as $keyname => $item){
                            $olddata = result::where('name','9global'.$keyname)->first();
                            if($olddata){
                                $olddata->update([
                                    'name' => '9global'.$keyname,
                                    'details' => $item,
                                    'imguniqicon' => ($request['input'][$keyname] != null ? $request['input'][$keyname]:null),
                                    'edited_by' => \Auth::user()->id
                                ]);
                            }else if(!$olddata && $item !== null){
                                result::create([
                                    'name' => '9global'.$keyname,
                                    'details' => $item,
                                    'category' => 'ผลการประเมิน 9 global targets',
                                    'best' => '0',
                                    'imguniq' => date('mdYHis') . uniqid(),
                                    'imguniqicon' => ($request['input'][$keyname] != null ? $request['input'][$keyname]:null),
                                    'active' => '0',
                                    'edited_by' => \Auth::user()->id
                                ]);
                            }
                        }
                    }
                }
                Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
                return redirect()->route('result.index');
            }

            if ($request['category'] == null){
                Alert::error('ล้มเหลว', 'กรุณาใส่ประเภท');
                return redirect()->back()->withInput();
            }
            $data = result::create([
                'name' => $request['name'],
                'details' => $request['details'],
                'category' => $request['category'],
                'imguniq' => date('mdYHis') . uniqid(),
                'imguniqicon' => date('mdYHis') . uniqid(),
                'active' => ($request['active'] != null ? '1':'0'),
                'edited_by' => \Auth::user()->id
            ]);
            if($request->has('namefile')){
                $namefiles = [];
                $files = [];
                foreach ($request['namefile'] as $item){
                    array_push($namefiles,$item);
                }
                foreach ($request['url'] as $item){
                    array_push($files,$item);
                }
                files::create([
                    'idfk' => $data->imguniqicon,
                    'files' => $files,
                    'name' => $namefiles
                ]);
            }
            if ($request->hasFile('img')) {
                $name = date('mdYHis') . uniqid() . '.' . $request['img']->getClientOriginalExtension();
                $destinationPath = public_path('uploads');
                $request['img']->move($destinationPath, $name);
                img::create([
                    'idfk' => $data->imguniq,
                    'path' => $name,
                    'name' => $request['img']->getClientOriginalName()
                ]);
            }
            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            return redirect()->route('result.index');
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getmessage());
            return back();
        }
    }

    public function edit($id)
    {
        $data = result::where('id',$id)->with('img')->with('icon')->withcount('icon')->first();
        return view('result.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        try{
            $data = result::where('id',$id)->with('img')->first();
            $oldimg = img::where('idfk',$data->imguniq)->first();
            $oldfile= files::where('idfk',$data->imguniqicon)->first();
            $data->update([
                'name' => $request['name'],
                'details' => $request['details'],
                'category' => $request['category'],
                'active' => ($request['active'] != null ? '1':'0'),
                'edited_by' => \Auth::user()->id
            ]);
            if ($request->hasFile('img')) {
                $name = date('mdYHis') . uniqid() . '.' . $request['img']->getClientOriginalExtension();
                $destinationPath = public_path('uploads');
                $request['img']->move($destinationPath, $name);
                if ($oldimg){
                    File::delete(public_path() . '/uploads/' . $oldimg->path);
                    $oldimg->update([
                        'name' => $request['img']->getClientOriginalName(),
                        'path' => $name
                    ]);
                }else{
                    img::create([
                        'idfk' => $data->imguniq,
                        'name' => $request['img']->getClientOriginalName(),
                        'path' => $name
                    ]);
                }
            }
            if($request->has('namefile')){
                $namefiles = [];
                $files = [];
                foreach ($request['namefile'] as $item){
                    array_push($namefiles,$item);
                }
                foreach ($request['url'] as $item){
                    array_push($files,$item);
                }
                if($oldfile){
                    $oldfile->update([
                        'idfk' => $data->imguniqicon,
                        'files' => $files,
                        'name' => $namefiles
                    ]);
                }else{
                    files::create([
                        'idfk' => $data->imguniqicon,
                        'files' => $files,
                        'name' => $namefiles
                    ]);
                }

            }
            Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
            return redirect()->route('result.index');
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getmessage());
            return view('result.edit',compact('data'));
        }
    }

    public function destroy($id)
    {
        try{
            $data = result::where('id',$id)->first();
            $img = img::where('idfk',$data->imguniq)->first();
            if($img){
                if (file_exists( public_path() . '/uploads/' . $img->path)) {
                    File::delete(public_path() . '/uploads/' . $img->path);
                }
            }
            $icons = img::where('idfk',$data->imguniqicon)->first();
            if($icons){
                foreach ($icons->path as $path){
                    if (file_exists( public_path() . '/uploads/' . $path)) {
                        File::delete(public_path() . '/uploads/' . $path);
                    }
                }
            }
            img::where('idfk',$data->imguniqicon)->delete();
            img::where('idfk',$data->imguniq)->delete();
            result::where('id',$id)->delete();
            return response()->json(['success'=>'ลบข้อมูลสำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['error'=>'ลบไม่สำเร็จ']);
        }
    }
}
