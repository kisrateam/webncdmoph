<?php

namespace App\Http\Controllers;

use App\files;
use App\img;
use App\team;
use App\title;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class teamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$data1 = img::where('idfk','team1')->first();
    	$data2 = img::where('idfk','team2')->first();
	    return view('team.index',compact('data1'),compact('data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $collectiontitle = DB::table('title')->select('name')->get();
	    return view('team.create',compact('collectiontitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	try{
    	    if ($request->hasFile('img1')) {
			    $image = $request->file('img1');
			    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
			    $destinationPath = public_path('uploads');
			    $image->move($destinationPath, $name);
			    $olddata1 = img::where('idfk','team1')->first();
			    if ($olddata1){
				    File::delete(public_path() . '/uploads/' . $olddata1->path);
			      $olddata1->update([
					    'idfk' => 'team1',
					    'name' => $request['img1']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }else{
				    img::create([
					    'idfk' => 'team1',
					    'name' => $request['img1']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }
	      }
		    if ($request->hasFile('img2')) {
			    $image = $request->file('img2');
			    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
			    $destinationPath = public_path('uploads');
			    $image->move($destinationPath, $name);
			    $olddata2 = img::where('idfk','team2')->first();
			    if ($olddata2){
				    File::delete(public_path() . '/uploads/' . $olddata2->path);
				    $olddata2->update([
					    'idfk' => 'team2',
                        'name' => $request['img2']->getClientOriginalName(),
                        'path' => $name
                    ]);
			    }else{
				    img::create([
					    'idfk' => 'team2',
                        'name' => $request['img2']->getClientOriginalName(),
                        'path' => $name
                    ]);
			    }
		    }
		    if($request['rank']){
//	        $namefkfile = date('mdYHis') . uniqid();
	        $olddata = team::where('rank',$request['rank'])->first();
                if ($olddata){
                    $olddata->update([
                        'name' => $request['name'],
                        'fileuniq' => ($request['file']?$request['file']:null),
                        'edited_by' => \Auth::user()->id
                    ]);
                }else{
                    team::create([
                        'name' => $request['name'],
                        'rank' => $request['rank'],
                        'fileuniq' => ($request['file']?$request['file']:null),
                        'edited_by' => \Auth::user()->id
                    ]);
                }
//            if ($request->hasFile('file')) {
//	          $oldfiles = files::where('idfk',$olddata->fileuniq)->first();
//				    $name = date('mdYHis') . uniqid() . '.' .$request->file('file')->getClientOriginalExtension();
//				    $request->file('file')->move(public_path('uploads'), $name);
//				    if($oldfiles){
//					    File::delete(public_path() . '/uploads/' . $oldfiles->files);
//					    $oldfiles->update([
//						    'name' => $request->file('file')->getClientOriginalName(),
//						    'files' => $name
//					    ]);
//				    }else{
//					    files::create([
//						    'idfk' => $olddata->fileuniq,
//						    'name' => $request->file('file')->getClientOriginalName(),
//						    'files' => $name
//					    ]);
//				    }
//			    }
            }
		    Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
		    return redirect()->route('team.index');
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', $e->getMessage());
		    return redirect()->back()->withInput();
	    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $collectiontitle = DB::table('title')->select('name')->get();
	    $model = team::where('team.id',$id)
		    ->leftjoin('title','team.title','=','title.id')
		    ->select('team.*','title.name as title')
		    ->first();
	    return view('team.edit',compact('collectiontitle'),compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    try{
          if ($request->hasFile('img1')) {
				    $oldfile = img::where('idfk','team1')->first();
				    $image = $request->file('img1');
				    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
				    $image->move(public_path('uploads'), $name);
				    if($oldfile){
					    File::delete(public_path() . '/uploads/' . $oldfile->path);
					    $oldfile->update([
						    'idfk' => 'team1',
						    'name' => 'team1',
						    'path' => $name
					    ]);
				    }else{
					    img::create([
						    'idfk' => 'team1',
						    'name' => 'team1',
						    'path' => $name
					    ]);
				    }
			    }
//		    $collectiontitle = DB::table('title')->select('name')->get();
//		    $namefkimg = date('mdYHis') . uniqid();
//		    $findtitle = title::where('name',$request['title'])->first();
//		    if(!$findtitle){
//			    $findtitle = title::create([
//				    'name' => $request['title']
//			    ]);
//		    }
//		    $model = team::where('id',$id)->first();
//		    if($request->rank == 1 && $model->rank != 1){
//			    $ckrank = team::where('rank',1)->first();
//			    if($ckrank) {
//				    Alert::error('ล้มเหลว', 'ระดับผู้อำนวยการมีได้แค่คนเดียว');
//				    return redirect()->back()->withInput();
//			    }
//		    }
//		    $model->update([
//			    'title' => $findtitle->id,
//			    'name' => $request['name'],
//			    'lname' => $request['lname'],
//			    'jobpositions' => $request['position'],
//			    'rank' => $request['rank'],
//			    'email' => $request['email'],
//			    'tel' => $request['tel']
//		    ]);
//
//		    if ($request->hasFile('img')) {
//			    $oldfile = img::where('idfk',$model->imguniq)->first();
//			    $model->update([
//				    'imguniq' => $namefkimg
//			    ]);
//			    $image = $request->file('img');
//			    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
//			    $image->move(public_path('uploads'), $name);
//			    $pathname[] = $name;
//			    $names[] = $name;
//			    if($oldfile){
//				    foreach($oldfile->path as $item) {
//					    File::delete(public_path() . '/uploads/' . $item);
//				    }
//				    $oldfile->update([
//					    'idfk' => $model->imguniq,
//					    'name' => $names,
//					    'path' => $pathname
//				    ]);
//			    }else{
//				    img::create([
//					    'idfk' => $model->imguniq,
//					    'name' => $names,
//					    'path' => $pathname
//				    ]);
//			    }
//		    }
		    Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
		    return view('team.edit',compact('collectiontitle'),compact('model'));
	    }catch (\Exception $e){
		    $collectiontitle = DB::table('title')->select('name')->get();
		    Alert::error('ล้มเหลว', 'เปลี่ยนข้อมูลไม่สำเร็จ');
		    return view('team.edit',compact('collectiontitle'),compact('model'));
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    try{
	        if (ctype_digit(strval($id))){
                team::where('id',$id)->delete();
            }
	        else{
                img::where('idfk',$id)->delete();
            }
		    return response()->json(['success'=>'ลบสำเร็จ']);
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }
}
