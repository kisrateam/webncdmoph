<?php

namespace App\Http\Controllers;

use App\contentactivity;
use App\files;
use App\img;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class contentactController extends Controller
{
		protected $listofact;

		public function __construct()
		{
			$this->listofact = array(
				"แผนการป้องกันและควบคุมโรคไม้ติดต่อ 5 ปี(พ.ศ. 2560-2564)",
				"สถานการณ์ / ข้อมูล",
				"ผลการดำเนินงาน/Best practice",
				"คู่มือ แนวทาง สื่อ"
			);
		}

		public function indexuser()
		{
			$data = contentactivity::whereIn('category',$this->listofact)->with('img')->with('file')->get();
			return view('user.media',compact('data'));
		}

		public function showuser($id){
			$data = contentactivity::where('id',$id)->with('img')->with('file')->firstorfail();
			$data->timestamps = false;
			$data->increment('hitcount');
			return view('user.showmedia',compact('data'));
		}

		public function showcontent($id){
			$data = contentactivity::where('id',$id)->first();
			$data->timestamps = false;
			$data->increment('downloaded');
			return response()->json(['success'=>'สำเร็จ']);
		}

    public function index()
    {
	    $data = contentactivity::whereIn('category',$this->listofact)->with('img')->with('file')->get();
	    return view('contentact.index',compact('data'));
    }

    public function showpdf($id) {
        $destinationPath = public_path('uploads');
        $pathname = $destinationPath .'/'. $id;
        return response()->file($pathname);
    }

    public function create()
    {
	    return view('contentact.create');
    }

    public function deletesome($id,$filename)
    {
        try{
            $contentmodel = contentactivity::with('img')
                ->leftJoin('files','content_activity.fileuniq','=','files.idfk')
                ->where('files.id',$id)->first();
            $newfile = json_decode($contentmodel->files);
            $newname = json_decode($contentmodel->name);
            $pos = array_search($filename, $newfile);
            File::delete(public_path() . '/uploads/' . $newfile[$pos]);
            unset($newfile[$pos]);
            unset($newname[$pos]);
            $newname1 = [];
            $newfile1 = [];
            foreach ($newname as $item) {
                $newname1[] = $item;
            }
            foreach ($newfile as $item) {
                $newfile1[] = $item;
            }
            $data = files::where('id',$id)->first();
            $data->update([
                'name' => $newname1,
                'files' => $newfile1
            ]);
            return response()->json(['success'=>'สำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        try{
            if ($request['details'] == null){
                Alert::error('ล้มเหลว', 'กรุณาใส่ข้อความเนื้อหา');
                return redirect()->back()->withInput();
            }
            if ($request['category'] == null){
                Alert::error('ล้มเหลว', 'กรุณาใส่ประเภท');
                return redirect()->back()->withInput();
            }
            $namefkimg = date('mdYHis') . uniqid();
            $model = contentactivity::create([
                'name' => $request['name'],
                'details' => $request['details'],
                'category' => $request['category'],
                'subcategory' => ($request['subcategory'] != null ? $request['subcategory']:null),
                'active' => ($request['active'] != null ? '1' : '0'),
                'imguniq' => $namefkimg,
                'fileuniq' => ($request['file'] != null ? $request['file']:'news'),
                'namefile' => ($request['namefile'] != null ? $request['namefile']:null),
                'edited_by' => \Auth::user()->id
            ]);

            if ($request->hasFile('img')) {
                $name = date('mdYHis') . uniqid() . '.' . $request->file('img')->getClientOriginalExtension();
                $destinationPath = public_path('uploads');
                $request->file('img')->move($destinationPath, $name);
                img::create([
                    'idfk' => $model->imguniq,
                    'path' => $name,
                    'name' => $request['img']->getClientOriginalName()
                ]);
            }

            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            $switchroute = contentactivity::where('id',$model->id)->whereIn('category',$this->listofact)->first();
          if($switchroute)return redirect()->route('contentact.index');
          else return redirect()->route('contentnew.index');
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', $e->getMessage());
		    return back();
	    }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
	    $contentmodel = contentactivity::where('id',$id)->with('img')->with('file')->first();
	    return view('contentact.edit',compact('contentmodel'));
    }

    public function update(Request $request, $id)
    {
	    try{
            $contentmodel = contentactivity::where('id',$id)->with('img')->with('file')->first();
//		    $oldfile1 = files::where('idfk',$contentmodel->fileuniq)->first();
		    $oldimg = img::where('idfk',$contentmodel->imguniq)->first();
		    $contentmodel->update([
			    'name' => $request->name,
			    'details' => $request->details,
			    'category' => $request->category,
                'subcategory' => ($request['subcategory'] != null ? $request['subcategory']:null),
                'active' => ($request['active'] != null ? '1' : '0'),
                'fileuniq' => ($request['file'] != null ? $request['file']:'news'),
                'namefile' => ($request['namefile'] != null ? $request['namefile']:null),
                'edited_by' => \Auth::user()->id
            ]);
		    if ($request->hasFile('img')) {
				    $name = date('mdYHis') . uniqid() . '.' . $request->file('img')->getClientOriginalExtension();
				    $destinationPath = public_path('uploads');
			      $request->file('img')->move($destinationPath, $name);
			    if($oldimg){
				    File::delete(public_path() . '/uploads/' . $oldimg->path);
                    $oldimg->update([
					    'idfk' => $contentmodel->imguniq,
					    'name' => $request['img']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }else{
				    img::create([
					    'idfk' => $contentmodel->imguniq,
					    'name' => $request['img']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }
		    }
		    Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
		    $switchroute = contentactivity::where('id',$id)->whereIn('category',$this->listofact)->first();
		    if($switchroute)return redirect()->route('contentact.index');
				else return redirect()->route('contentnew.index');
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', 'เปลี่ยนข้อมูลไม่สำเร็จ');
		    return view('contentact.edit',compact('contentmodel'));
	    }
    }

    public function destroy($id)
    {
	    try{
		    $data = contentactivity::where('id',$id)->first();
		    $oldfileimg = img::where('idfk',$data->imguniq)->first();
		    if($oldfileimg){
                File::delete(public_path() . '/uploads/' . $oldfileimg->path);
			    img::where('id',$oldfileimg->id)->delete();
		    }
		    contentactivity::where('id',$id)->delete();
		    return response()->json(['success'=>'ลบสำเร็จ']);
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }
}
