<?php

namespace App\Http\Controllers;

use App\img;
use App\team;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use function redirect;
use function response;

class HomeController extends Controller
{

    public function __construct()
    {
        //        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        return view('welcome');
    }

    public function best()
    {
        return view('user.best');
    }

    public function teamview(){
        $data = img::where('idfk','team2')->first();
        $download = team::with('file')->orderBy('rank')->get();
        return view('user.team',compact('data'),compact('download'));
    }

    public function showchangeprofile($id){
        $data = User::where('id',$id)->first();
        return view('user.changeprofile',compact('data'));
    }

    public function banner(Request $request){
        $set = DB::table('homebanner')->where('url',$request['category'])->first();
        if($set){
            $arrpath = json_decode($set->path);
        }else{
            $arrpath = ['',''];
        }
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            if(File::exists(public_path() . '/uploads/' . $arrpath[0])){
                File::delete(public_path() . '/uploads/' . $arrpath[0]);
            }
            $arrpath[0] = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads');
            $image->move($destinationPath, $arrpath[0]);
            //delete_old_file
            if ($set) {
                DB::table('homebanner')
                    ->where('id', $set->id)
                    ->update([
                        'path' => $arrpath,
                        'edited_by' => \Auth::user()->id
                    ]);
            }else{
                DB::table('homebanner')
                    ->insert(
                        [
                            'url'=>$request['category'],
                            'path' => json_encode($arrpath),
                            'edited_by' => \Auth::user()->id
                        ]
                    );
            }
        }
        if($request->has('url')){
            $arrpath[1] = $request['url'];
            if($set){
                DB::table('homebanner')->where('url',$request['category'])->update([
                    'path' => $arrpath,
                    'edited_by' => \Auth::user()->id
                ]);
            }elseif(!$set && $arrpath[0] != ''){
                DB::table('homebanner')->where('url',$request['category'])->update([
                    'path' => $arrpath,
                    'edited_by' => \Auth::user()->id
                ]);
            }else{
                DB::table('homebanner')
                    ->insert(
                        [
                            'url'=>$request['category'],
                            'path' => json_encode($arrpath)
                        ]
                    );
            }
        }
        return redirect()->back();
    }

    public function deletebanner($id){
        try{
            $oldimg = DB::table('homebanner')->where('id', $id)->first();
            if ($oldimg && File::exists(public_path() . '/uploads/' . $oldimg->path)) {
                File::delete(public_path() . '/uploads/' . $oldimg->path);
            }
            DB::table('homebanner')->where('id', $id)->delete();
            return response()->json(['success'=>'ลบสำเร็จ']);
        }catch (Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }

    public function changeprofile(Request $request){
        if($request->has('name')){
            $request->validate([
                'name' => 'required',
                'current-password' => 'required',
            ]);
        }else{
            $request->validate([
                'current-password' => 'required',
                'password' => 'same:password',
                'password_confirmation' => 'same:password',
            ]);
        }

        $current_password = Auth::User()->password;
        if(Hash::check($request['current-password'], $current_password))
        {
            $id = Auth::User()->id;
            $obj_user = User::find($id);
            if($request['password'] != null){
                $obj_user->Password = Hash::make($request['password']);
                Auth::logout();
                $obj_user->save();
                Alert::success('สำเร็จ', 'เปลี่ยนรหัสผ่านสำเร็จ เข้าสู่ระบบด้วยรหัสผ่านใหม่');
                return redirect()->route('welcome');
            }
            if($request['name'] != null)$obj_user->name = $request['name'];
            $obj_user->save();
            Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
            return redirect()->back();
        }else{
            Alert::error('ล้มเหลว', 'รหัสผ่านปัจจุบันไม่ถูกต้อง');
            return redirect()->back();
        }
    }
}
