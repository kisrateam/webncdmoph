<?php

namespace App\Http\Controllers;

use App\badwords;
use function dd;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use function redirect;

class badwordController extends Controller
{
    public function index()
    {
        $data = badwords::all();
        return view('badword.index',compact('data'));
    }

    public function store(Request $request)
    {
        $dupicate = badwords::where('badword',$request['badword'])->first();
        if(!$dupicate){
            badwords::create([
                'badword' => $request['badword'],
                'edited_by' => \Auth::user()->id
            ]);
        }else{
            Alert::error('ล้มเหลว', 'มีคำนี้อยู่แล้ว');
            return redirect()->back();
        }
        Alert::success('สำเร็จ', 'เพิ่มสำเร็จ');
        return redirect()->back();
    }

    public function destroy($id)
    {
        try{
            badwords::where('id',$id)->delete();
            return response()->json(['success'=>'ลบสำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
}
