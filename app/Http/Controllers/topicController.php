<?php

namespace App\Http\Controllers;

use App\badwords;
use App\comments;
use App\reports;
use App\topics;
use function count;
use function dd;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use function redirect;
use function url;
use function with;
use Illuminate\Support\Facades\Mail;

class topicController extends Controller
{
    public function index(){
        $data = new topics();
        return view('topic.topic',compact('data'))->with(['counttopic' => count($data->get())]);
    }

    function autocomplete(Request $request)
    {
        if($request->get('query')){
            $query  = $request->get('query');
            $data = new topics();
            $pattern = preg_quote($query);
            if(Auth::check()){
                if(Auth::user()->role == 'user'){
                    $data = $data->select('id','name')->Where(function($que) use ($query) {
                        $que->orWhere('details','LIKE', "%{$query}%")
                            ->orWhere('name', 'LIKE',  "%{$query}%");
                    })->whereIn('privacy',['1','2']);
                }elseif(Auth::user()->role == 'member' || Auth::user()->role == 'admin'){
                    $data = $data->select('id','name')->Where(function($que) use ($query) {
                        $que->orWhere('details','LIKE', "%{$query}%")
                            ->orWhere('name', 'LIKE',  "%{$query}%");
                    });
                }
            }else{
                $data = $data->select('id','name')->Where(function($que) use ($query) {
                    $que->orWhere('details','LIKE', "%{$query}%")
                        ->orWhere('name', 'LIKE',  "%{$query}%");
                })->where('privacy','1');
            }
            $data = $data->orderBy('name')->paginate(10);
            if(count($data) > 0){
                $outout = '<div class="dropdown-menu" style="display:block;width: 100%;position: absolute;z-index:10;top: 23px;background-color: white;opacity: 0.9">';
                $site = 'https://ddc.moph.go.th/dncd/ncdstrategy/public';
                foreach ($data as $row) {
                    $outout .= '
                    <a class="dropdown-item" style="overflow:hidden" href="'.$site.'/home/topic/'.$row->id.'" target="_blank">'.
                        preg_replace("/($pattern)/i", '<span class="hlsearch">$1</span>', $row->name)
                        .'</a>
                    <div class="dropdown-divider"></div>
                    ';}
                $outout .= '</div>';
                echo $outout;
            }else{
                $outout = '<div class="dropdown-menu"style="display:block;width: 100%;position: absolute;z-index:10;top: 23px;background-color: white;opacity: 0.9">';
                $outout .= '
                <a class="dropdown-item" style="overflow:hidden">ไม่พบการค้นหา</a>
                <div class="dropdown-divider"></div>
                ';
                $outout .= '</div>';
                echo $outout;
            }
        }
    }

    public function mytopic(){
        $data = topics::where('userid',Auth::user()->id)
            ->groupby('topics.id')
            ->orderbyDesc('topics.updated_at')
            ->with('users')
            ->with('comments')
            ->get();
        $counttopic = count($data);
        return view('topic.mytopic',compact('data'))->with(['counttopic' => $counttopic]);
    }

    public function Showaddtopics(){
        return view('topic.addtopic');
    }

    public function addtopic(Request $request){
        try{
            if($request['privacy'] == null){
                Alert::error('ล้มเหลว', 'กรุณาเลือกประเภทการเข้าถึง');
                return redirect()->back()->withInput();
            }
            if($request['details'] == null){
                Alert::error('ล้มเหลว', 'กรุณากรอกข้อความ');
                return redirect()->back()->withInput();
            }
            $details = topics::create([
                'name' => $request['topic'],
                'details' => $request['details'],
                'userid' => Auth::user()->id,
                'hitcount' => 0,
                'status' => 1,
                'privacy' => $request['privacy'],
                'category' => $request['category'],
                'subcategory' => ($request['subcategory'] != null ? $request['subcategory']:null)
            ]);
            $this->sendEmailToAdmin($details);
            Alert::success('สำเร็จ', 'ตั้งกระดานสนทนาแล้ว');
            return redirect()->route('topic');
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getMessage());
            return redirect()->back();
        }
    }

    public function sendEmailToAdmin($details){
        switch ($details->privacy){
            case '1':{
                $level = 'เห็นทั้งหมด';
                break;
            }
            case '2':{
                $level = 'เห็นเฉพราะผู้เข้าสู่ระบบ';
                break;
            }
            case '3':{
                $level = 'เห็นเฉพราะคณะทำงาน';
                break;
            }
            default:{
                $level = '';
                break;
            }
        }
        $to_name = 'ผู้ดูแลระบบ';
        //อีเมล แอดมิน
        $to_email = 'kruautoemail@gmail.com';
        //เว็บหลัก
        $site = 'https://ddc.moph.go.th/dncd/ncdstrategy/public/';
        $link = $site.'home/topic/'.$details->id;
        $data = array('body' => "มีการตั้งกระดานสนทนาใหม่ คลิกเพื่อดู" ,'link' => $link, 'name' => 'ชื่อหัวข้อ '.$details->name,'post'=>'ตั้งโดย '.$details->users->name,'level'=>'ประเภทกระดานสนทนา '.$level);
        Mail::send('Mail.topictoadmin', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('มีการตั้งกระดานสนทนาใหม่');
        $message->from('kruautoemail@gmail.com','อีเมลอัตโนมัติ');
        });
    }

    public function intopic($id){
        $data = topics::where('topics.id',$id)
            ->leftjoin('users','topics.userid','=','users.id')
            ->select('topics.*','users.name as username')
            ->firstorfail();
        $data->timestamps = false;
        $data->increment('hitcount');
        $comments = comments::where('topicid',$id)
            ->leftjoin('users','comments.userid','=','users.id')
            ->orderByDesc('comments.created_at')
            ->select('comments.*','users.name as username','users.role as role')
            ->get();
        return view('topic.intopic',compact('data'),compact('comments'));
    }

    public function addcomment(Request $request){
        try{
            $data = topics::where('id',$request['topicid'])->first();
            if($data->status != 0){
                comments::create([
                    'topicid' => $request['topicid'],
                    'message' => $request['message'],
                    'userid' => Auth::user()->id
                ]);
                $data->touch();
                Alert::success('สำเร็จ', 'ตอบกลับกระดานสนทนาแล้ว');
            }else{
                Alert::error('ล้มเหลว', 'กระทู้ถูกปิดแสดงความคิดเห็น');
            }
            return redirect()->route('intopic',['id'=>$request['topicid']]);
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', 'กรุณาใส่ข้อความ');
            return redirect()->back();
        }
    }

    public function report(Request $request){
        try{
            reports::create([
                'idreport' => $request['id'],
                'type'=> $request['type'],
                'details' => ($request['type']=='comments'?$request['reportdetails']:$request['reportdetails2'])
            ]);
            //alert
            return redirect()->back();
        }catch (\Exception $e){
            //alert
            return redirect()->back();
        }
    }

    public function closetopic($id){
        try{
            $data = topics::where('id',$id)->first();
            $data->update([
                'status' => 0
            ]);
            return response()->json(['success'=>'สำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['success'=>'ล้มเหลว']);
        }
    }

    public function opentopic($id){
        try{
            $data = topics::where('id',$id)->first();
            $data->update([
                'status' => 1
            ]);
            return response()->json(['success'=>'สำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['success'=>'ล้มเหลว']);
        }
    }

    public function deletetopic($id){
        try{
            $data = topics::where('id',$id)->where('userid',\Auth::user()->id)->first();
            if($data)$data->delete();
            return response()->json(['success'=>'สำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['success'=>'ล้มเหลว']);
        }
    }

    public function showedittopic($id){
        try{
            $data = topics::find($id);
            return view('topic.edit',compact('data'));
        }catch (\Exception $e){
            return redirect()->back();
        }
    }

    public function edittopic(Request $request){
        try{
            if($request['details'] == null){
                Alert::error('ล้มเหลว', 'กรุณากรอกข้อความ');
                return redirect()->back()->withInput();
            }
            $data = topics::find($request['id']);
            $data->update([
                'name' => $request['topic'],
                'details' => $request['details'],
                'privacy' => $request['privacy'],
                'category' => $request['category'],
                'subcategory' => ($request['subcategory'] != null ? $request['subcategory']:null)
            ]);
            Alert::success('สำเร็จ', 'แก้ไขกระดานสนทนาแล้ว');
            return redirect()->back()->withInput();
        }catch (\Exception $e){
            return redirect()->back();
        }
    }

//	public function restore($id)
//	{
//		$note = Note::onlyTrashed()->find($id);
//		if (!is_null($note)) {
//			$note->restore();
//			$response = $this->successfulMessage(200, 'Successfully restored', true, $note->count(), $note);
//		} else {
//			return response($response);
//		}
//		return response($response);
//	}
}
