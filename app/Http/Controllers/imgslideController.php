<?php

namespace App\Http\Controllers;

use App\imgslide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class imgslideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = imgslide::all();
        return view('imgslide.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('imgslide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	try{
			    $data = imgslide::create([
				    'name' => $request['name'],
				    'details' => $request['details'],
				    'url' => $request['url'],
				    'active' => ($request['active'] != null ? '1':'0'),
                    'edited_by' => \Auth::user()->id
                ]);
		    if ($request->hasFile('img')) {
				    $image = $request->file('img');
				    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
				    $destinationPath = public_path('uploads');
				    $image->move($destinationPath, $name);
				    $data->path = $name;
			      $data->save();
			    }
			    Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
			    return redirect()->route('imgslide.index');
		    }catch (\Exception $e){
			    Alert::error('ล้มเหลว', 'เพิ่มข้อมูลไม่สำเร็จ');
			    return back();
		    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = imgslide::where('id',$id)->first();
        return view('imgslide.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    try{
		    $data = imgslide::where('id',$id)->first();
		    $data->update([
			    'name' => $request['name'],
			    'details' => $request['details'],
			    'url' => $request['url'],
			    'active' => ($request['active'] != null ? '1':'0'),
                'edited_by' => \Auth::user()->id
            ]);
		    if ($request->hasFile('img')) {
			    if($data->path)File::delete(public_path() . '/uploads/' . $data->path);
			    $image = $request->file('img');
			    $name = date('mdYHis') . uniqid() . '.' .$image->getClientOriginalExtension();
			    $destinationPath = public_path('uploads');
			    $image->move($destinationPath, $name);
			    $data->path = $name;
			    $data->save();
		    }
		    Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
		    return redirect()->route('imgslide.index');
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', 'เพิ่มข้อมูลไม่สำเร็จ');
		    return back();
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    try{
		    $data = imgslide::where('id',$id)->first();
		    if($data){
				    File::delete(public_path() . '/uploads/' . $data->path);
		    }
		    imgslide::where('id',$id)->delete();
		    return response()->json(['success'=>'ลบข้อมูลสำเร็จ']);
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }
}
