<?php

namespace App\Http\Controllers;

use App\img;
use App\link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class linkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $data = link::with('img')->get();
	    return view('link.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    try{
		    $namefkimg = date('mdYHis') . uniqid();
		    $model = link::create([
                'name' => $request['name'],
                'url' => $request['url'],
                'imguniq' => $namefkimg,
                'edited_by' => \Auth::user()->id
            ]);
		    if ($request->hasFile('img')) {
			    $name = date('mdYHis') . uniqid() . '.' . $request->file('img')->getClientOriginalExtension();
			    $destinationPath = public_path('uploads');
			    $request->file('img')->move($destinationPath, $name);
			    img::create([
				    'idfk' => $model->imguniq,
				    'path' => $name,
				    'name' => $request['img']->getClientOriginalName()
			    ]);
		    }
		    Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
		    return redirect()->route('link.index');
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', 'เพิ่มข้อมูลไม่สำเร็จ');
		    return back();
	    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $model = link::where('id',$id)->with('img')->first();
	    return view('link.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    try{
		    $model = link::where('id',$id)->first();
		    $oldfile = img::where('idfk',$model->imguniq)->first();
		    $model->update([
			    'name' => $request->name,
			    'url' => $request->url,
                'edited_by' => \Auth::user()->id
            ]);
		    if ($request->hasFile('img')) {
			    $name = date('mdYHis') . uniqid() . '.' . $request->file('img')->getClientOriginalExtension();
			    $destinationPath = public_path('uploads');
			    $request->file('img')->move($destinationPath, $name);
			    if($oldfile){
				    File::delete(public_path() . '/uploads/' . $oldfile->path);
				    $oldfile->update([
					    'idfk' => $model->imguniq,
					    'name' => $request['img']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }else{
				    img::create([
					    'idfk' => $model->imguniq,
					    'name' => $request['img']->getClientOriginalName(),
					    'path' => $name
				    ]);
			    }
		    }
		    Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
		    return view('link.edit',compact('model'));
	    }catch (\Exception $e){
		    Alert::error('ล้มเหลว', 'เปลี่ยนข้อมูลไม่สำเร็จ');
		    return view('link.edit',compact('model'));
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    try{
		    link::where('id',$id)->delete();
		    return response()->json(['success'=>'ลบข้อมูลสำเร็จ']);
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }
}
