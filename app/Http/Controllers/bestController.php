<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\File;
use App\best;
use App\img;

class bestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = best::with('img')->get();
        return view('best.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('best.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if ($request['group'] == null || $request['subgroup'] == null){
                Alert::error('ล้มเหลว', 'กรุณาใส่ประเภท');
                return redirect()->back()->withInput();
            }
            $subgroup = '';
            foreach ($request['subgroup'] as $sub){
                $subgroup .= $sub;
            }
            $data = best::create([
                'name' => $request['name'],
                'details' => $request['details'],
                'group' => $request['group'],
                'subgroup' => $subgroup,
                'url' => $request['url'],
                'imguniq' => date('mdYHis') . uniqid(),
            ]);
            if ($request->hasFile('img')) {
                $path = date('mdYHis') . uniqid() . '.' . $request['img']->getClientOriginalExtension();
                $name = $request['img']->getClientOriginalName();
                $destinationPath = public_path('uploads');
                $request['img']->move($destinationPath, $path);
                img::create([
                    'idfk' => $data->imguniq,
                    'path' => $path,
                    'name' => $name
                ]);
            }
            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            return redirect()->route('best.index');
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getmessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = best::where('id',$id)->with('img')->first();
        return view('best.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = best::where('id',$id)->first();
            if ($request['group'] == null || $request['subgroup'] == null){
                Alert::error('ล้มเหลว', 'กรุณาใส่ประเภท');
                return redirect()->back()->withInput();
            }
            $subgroup = '';
            foreach ($request['subgroup'] as $sub){
                $subgroup .= $sub;
            }
            $data->update([
                'name' => $request['name'],
                'details' => $request['details'],
                'group' => $request['group'],
                'subgroup' => $subgroup,
                'url' => $request['url'],
            ]);
            if ($request->hasFile('img')) {
                $path = date('mdYHis') . uniqid() . '.' . $request['img']->getClientOriginalExtension();
                $name = $request['img']->getClientOriginalName();
                $destinationPath = public_path('uploads');
                $request['img']->move($destinationPath, $path);
                $oldimg = img::where('idfk',$data->imguniq)->first();
                if ($oldimg){
                    File::delete(public_path() . '/uploads/' . $oldimg->path);
                    $oldimg->update([
                        'name' => $name,
                        'path' => $path
                    ]);
                }else{
                    img::create([
                        'idfk' => $data->imguniq,
                        'name' => $name,
                        'path' => $path
                    ]);
                }
            }
            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            return redirect()->route('best.index');
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getmessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $data = best::where('id',$id)->first();
            $img = img::where('idfk',$data->imguniq)->first();
            if($img){
                if (file_exists( public_path() . '/uploads/' . $img->path)) {
                    File::delete(public_path() . '/uploads/' . $img->path);
                }
            }
            img::where('idfk',$data->imguniq)->delete();
            best::where('id',$id)->delete();
            return response()->json(['success'=>'ลบข้อมูลสำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['error'=>'ลบไม่สำเร็จ']);
        }
    }
}
