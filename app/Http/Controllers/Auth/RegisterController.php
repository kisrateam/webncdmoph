<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
	    $rules = [
		    'name' => ['required', 'string', 'max:12', 'unique:users'],
		    'email' => ['required', 'email', 'max:255', 'unique:users'],
		    'password' => ['required', 'string', 'min:7', 'confirmed'],
	    ];
	    $errormessage = [
		    'password.confirmed' => 'รหัสผ่านไม่ตรงกัน',
		    'email.unique' => 'มีอีเมลล์นี้ในระบบแล้ว',
		    'name.unique' => 'มีชื่อผู้ใช้งานนี้ในระบบแล้ว'
	    ];
      $validate = Validator::make($data,$rules,$errormessage);
	    if ($validate->fails()) {
		    $fieldsWithErrorMessagesArray = $validate->messages()->get('*');
		    if (isset($fieldsWithErrorMessagesArray['name'])){
			    Alert::error('ล้มเหลว', $fieldsWithErrorMessagesArray['name'][0]);
			    return Validator::make($data,$rules,$errormessage);
		    }
		    if (isset($fieldsWithErrorMessagesArray['email'])){
			    Alert::error('ล้มเหลว', $fieldsWithErrorMessagesArray['email'][0]);
			    return Validator::make($data,$rules,$errormessage);
		    }
		    if (isset($fieldsWithErrorMessagesArray['password'])){
			    Alert::error('ล้มเหลว', $fieldsWithErrorMessagesArray['password'][0]);
			    return Validator::make($data,$rules,$errormessage);
		    }
	    }else{
            Alert::success('สำเร็จ', 'สมัครสมาชิกสำเร็จ');
        }
	    return Validator::make($data,$rules,$errormessage);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => 'user'
        ]);
    }
}
