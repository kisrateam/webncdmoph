<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    protected function redirectTo()
    {
        $urlPrevious = url()->previous();
        $time = Carbon::now();
        DB::table('userlogs')->insert([
            'log' => Auth::user()->id.' เข้าสู่ระบบ เวลา '.$time
        ]);
        return $urlPrevious;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $deleted_user = User::onlyTrashed()->where('email',$request['email'])->first();
        if($deleted_user)Alert::error('ล้มเหลว', 'บัญชีคุณถูกระงับโปรดติดต่อ กองโรคไม่ติดต่อ');
        else Alert::error('ล้มเหลว', 'อีเมล หรือ รหัสผ่านไม่ถูกต้อง');
        return redirect()->back();
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
