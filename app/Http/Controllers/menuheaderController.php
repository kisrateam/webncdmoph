<?php

namespace App\Http\Controllers;

use App\menuheader;
use function compact;
use function dd;
use function ddd;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use function redirect;

class menuheaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = menuheader::with('children')->with('getName')->get();
        return view('menuheader.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentid = menuheader::where('parent_id',null)->get();
        return view('menuheader.create',compact('parentid'));
    }

    public function getchild($name)
    {
        $Data['data'] = menuheader::where('level', $name)->where('type','menu')->select('id','name')->get();
        echo json_encode($Data);
        exit;
    }

    public function getchildinchild($name)
    {
        $Data['data'] = menuheader::where('level', $name)->whereIn('type',['link','havelink'])->where('parent_index','!=',null)->select('id','name')->get();
        echo json_encode($Data);
        exit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $oldindex = menuheader::where('level',$request['level'])->where('parent_id',$request['parent_id'])->latest('created_at')->select('parent_id','parent_index')->first();
//        if ($oldindex && $oldindex->parent_index !== null)$oldindex->parent_index += 1;
//        else if($oldindex && $oldindex->parent_index === null)$oldindex->parent_index = 1;
        if($request['type'] == "link"){
            menuheader::create([
                'level' => $request['level'],
                'name' => $request['name'],
                'type' => $request['type'],
                'link' => $request['link'],
                'parent_id' => $request['parent_id'],
                'parent_index' => $request['index'],
                'active' => 1,
                'css' => $request['css']
            ]);
        }else if ($request['type'] == "link" && $request['parent_id'] != null){
            menuheader::create([
                'level' => $request['level'],
                'name' => $request['name'],
                'type' => $request['type'],
                'link' => $request['link'],
                'parent_id' => $request['parent_id'],
                'parent_index' => 1,
                'active' => 1,
                'css' => $request['css']
            ]);
        }else if($request['type'] == "menu"){
            menuheader::create([
                'level' => $request['level'],
                'name' => $request['name'],
                'type' => $request['type'],
                'parent_index' => $request['index'],
                'active' => 1,
                'css' => $request['css']
            ]);
        }else if($request['type'] == "menulink"){
            menuheader::create([
                'level' => $request['level'],
                'name' => $request['name'],
                'type' => $request['type'],
                'link' => $request['link'],
                'parent_index' => $request['index'],
                'active' => 1,
                'css' => $request['css']
            ]);
        }else if($request['type'] == "linkinlink" && $request['parent_id'] != null){
            $parentid = menuheader::where('id',$request['parent_id'])->first();
            $parentid->type = 'havelink';
            $parentid->save();
            menuheader::create([
                'level' => $request['level'],
                'name' => $request['name'],
                'type' => 'linkinlink',
                'link' => $request['link'],
                'parent_id' => $request['parent_id'],
                'parent_index' => $request['index'],
                'active' => 1,
                'css' => $request['css']
            ]);
        }else{
            Alert::error('ล้มเหลว', 'เพิ่มข้อมูลไม่สำเร็จ');
            return redirect()->route('menuheader.index');
        }
        Alert::success('สำเร็จ', 'เพิ่มข้อมูลแล้ว');
        return redirect()->route('menuheader.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = menuheader::where('id',$id)->with('children')->with('getName')->first();
        return view('menuheader.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $data = menuheader::where('id',$id)->first();
            if($data->parent_id == null){
                $subs = menuheader::where('parent_id',$id)->get();
                foreach ($subs as $sub){
                    $sub->where('parent_id',$id)->delete();
                }
            }
            menuheader::where('id',$id)->delete();
            return response()->json(['success'=>'ลบสำเร็จ']);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
}
