<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class userroleController extends Controller
{
    public function index(){
    	$data = User::all();
        $datawithDelete = User::onlyTrashed()->get();
    	return view('userrole.index',compact('data'))->with(compact('datawithDelete'));
    }

    public function changerole($id,$password,$role){
	    $current_password = Auth::User()->password;
	    if(Hash::check($password, $current_password))
	    {
	        if($id == Auth::User()->id){
                return response()->json('',401);
            }else{
                $data = User::find($id);
                $data->update([
                    'role' => $role
                ]);
            }
		    return response()->json(['item'=>$data]);
	    }else{
		    return response()->json('',401);
	    }
    }

    public function delete($id){
	    try{
	        $idadmin = \Auth::user()->id;
	        if($idadmin == $id){
                return response()->json(['error'=>'ไม่สามารถลบตัวเองได้']);
            }else{
                User::where('id',$id)->delete();
                return response()->json(['success'=>'ลบสำเร็จ']);
            }
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }

    public function restore($id){
	    try{
            User::onlyTrashed()->where('id', $id)->restore();
            return response()->json(['success'=>'คืนค่าสำเร็จ']);
	    }catch (\Exception $e){
		    return response()->json(['error'=>$e->getMessage()]);
	    }
    }
}
