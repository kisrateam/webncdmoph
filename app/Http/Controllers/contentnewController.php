<?php

namespace App\Http\Controllers;

use App\contentactivity;
use App\contentnew;
use Illuminate\Http\Request;

class contentnewController extends Controller
{
		protected $listofnew;

		public function __construct()
		{
			$this->listofnew = array(
				"ข่าวกิจกรรม",
				"ข่าวประชาสัมพันธ์"
			);
		}

    public function index()
    {
	    $data = contentactivity::whereIn('category',$this->listofnew)->with('img')->with('file')->get();
	    return view('contentnew.index',compact('data'));
    }

		public function indexuser()
		{
			return view('user.new');
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('contentnew.create');
    }


    public function edit($id)
    {
	    $contentmodel = contentactivity::where('id',$id)->with('img')->with('file')->first();
	    return view('contentnew.edit',compact('contentmodel'));
    }
}
