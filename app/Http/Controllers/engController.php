<?php

namespace App\Http\Controllers;

use App\eng;
use App\img;
use function compact;
use function dd;
use function ddd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use function view;

class engController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexuser()
    {
        return view('user.eng');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eng.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $arr = [
                'Strategic plan',
                'Committee',
                'M&E',
                'Best practice'
            ];
            switch ($request['catindex']){
                case '1' : {
                    $category = $arr[0];
                    $url = 'eng1';
                    break;
                }
                case '2' : {
                    $category = $arr[1];
                    $url = 'eng2';
                    break;
                }
                case '3' : {
                    $category = $arr[2];
                    $url = 'eng3';
                    break;
                }
                case '4' : {
                    $category = $arr[3];
                    $url = 'eng4';
                    break;
                }
            }
            if ($request->hasFile('img')) {
                $name = date('mdYHis') . uniqid() . '.' . $request->file('img')->getClientOriginalExtension();
                $destinationPath = public_path('uploads');
                $request->file('img')->move($destinationPath, $name);
                $oldimg = img::where('idfk',$url)->first();
                if($oldimg){
                    File::delete(public_path() . '/uploads/' . $oldimg->path);
                    $oldimg->update([
                        'path' => $name,
                        'name' => $url
                    ]);
                }else{
                    img::create([
                        'idfk' => $url,
                        'path' => $name,
                        'name' => $url
                    ]);
                }
                Alert::success('สำเร็จ', 'เปลี่ยนรูปภาพสำเร็จ');
                return redirect()->route($url);
            }
            $arrimg = [];
            if($request['fileuniq'] != null){
                $arrimg[0] = $request['fileuniq'];
            }else{
                $arrimg[0] = '';
            }
            if ($request->hasFile('imghl')) {
                $namehl = date('mdYHis') . uniqid() . '.' . $request->file('imghl')->getClientOriginalExtension();
                $destinationPathhl = public_path('uploads');
                $request->file('imghl')->move($destinationPathhl, $namehl);
                $arrimg[1] = $namehl;
            }else{
                $arrimg[1] = '';
            }
            eng::create([
                'name' => $request['name'],
                'category' => $category,
                'fileuniq' => json_encode($arrimg),
                'active' => ($request['active'] != null ? '1':'0'),
                'highlights' => ($request['highlights'] != null ? '1':'0'),
                'edited_by' => \Auth::user()->id
            ]);
            Alert::success('สำเร็จ', 'เพิ่มข้อมูลสำเร็จ');
            return redirect()->route($url);
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', $e->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = eng::where('id',$id)->first();
        return view('eng.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = eng::where('id',$id)->first();
            $arrimg = [];
            if($request['fileuniq'] != null){
                $arrimg[0] = $request['fileuniq'];
            }else{
                $arrimg[0] = json_decode($data->fileuniq)[0];
            }
            if ($request->hasFile('imghl')) {
                $namehl = date('mdYHis') . uniqid() . '.' . $request->file('imghl')->getClientOriginalExtension();
                $destinationPathhl = public_path('uploads');
                $request->file('imghl')->move($destinationPathhl, $namehl);
                $arrimg[1] = $namehl;
                File::delete(public_path() . '/uploads/' . json_decode($data->fileuniq)[1]);
            }else{
                $arrimg[1] = json_decode($data->fileuniq)[1];
            }
            $data->update([
                'name' => $request['name'],
                'fileuniq' => json_encode($arrimg),
                'active' => ($request['active'] != null ? '1':'0'),
                'highlights' => ($request['highlights'] != null ? '1':'0'),
                'edited_by' => \Auth::user()->id
            ]);
            Alert::success('สำเร็จ', 'เปลี่ยนข้อมูลสำเร็จ');
            return redirect()->back();
        }catch (\Exception $e){
            Alert::error('ล้มเหลว', 'เปลี่ยนข้อมูลไม่สำเร็จ');
            return view('eng.edit',compact('data'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $chk = eng::where('id',$id)->delete();
            if (!$chk){
                $old = img::where('idfk',$id)->first();
                File::delete(public_path() . '/uploads/' . $old->path);
                $old->delete();
            }
            return response()->json(['success'=>$id]);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
}
