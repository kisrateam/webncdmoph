<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class menuheader extends Model
{
	protected $table='menu_header';

	protected $fillable = [
		'level','parent_id','parent_index','name','link','type','active','css'
	];

    public function children()
    {
        return $this->hasMany('App\menuheader','parent_id', 'id');
    }

    public function getName()
    {
        return $this->belongsTo('App\menuheader', 'parent_id');
    }

    public function childrenin()
    {
        return $this->hasMany('App\menuheader','parent_id', 'id');
    }
}
