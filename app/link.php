<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class link extends Model
{
	protected $table='link';

	protected $fillable = [
		'name','url','imguniq','edited_by'
	];

	public function img(){
		return $this->hasOne(img::class,'idfk','imguniq');
	}
}
