<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contentactivity extends Model
{
	protected $table='content_activity';

	protected $fillable = [
		'name','details','imguniq','namefile','fileuniq','category','subcategory','hitcount','active','edited_by'
	];

	public function img(){
		return $this->hasOne(img::class,'idfk','imguniq');
	}

	public function file(){
		return $this->hasOne(files::class,'idfk','fileuniq');
	}
}
