<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class content extends Model
{
		protected $table='content';

		protected $fillable = [
			'name','fileuniq','active','category','edited_by'
		];

		public function file(){
			return $this->hasOne(files::class,'idfk','fileuniq');
		}
}
