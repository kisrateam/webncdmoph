<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class result extends Model
{
	protected $table='result';

	protected $fillable = [
		'name','details','imguniq','imguniqicon','category','active','best','edited_by'
	];

	public function img(){
		return $this->hasOne(img::class,'idfk','imguniq');
	}

	public function icon(){
		return $this->hasOne(img::class,'idfk','imguniqicon');
	}

	public function files(){
		return $this->hasOne(files::class,'idfk','imguniqicon');
	}
}
