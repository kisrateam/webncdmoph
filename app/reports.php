<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class reports extends Model
{
    use SoftDeletes;

    protected $table='topicreport';

    protected $fillable = [
        'idreport','type','details'
    ];

    public function reports(){
        if($this->type == 'comments')return $this->hasOne(comments::class,'id','idreport');
        else return $this->hasOne(topics::class,'id','idreport');
    }

    public function getname()
    {
        if($this->type == 'comments')$name = comments::where('id',$this->idreport)->select('message')->first();
        else $name = topics::where('id',$this->idreport)->select('name','details')->first();
        return $name;
    }
}
