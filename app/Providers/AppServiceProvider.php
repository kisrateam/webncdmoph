<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //local site
        view()->share('site', 'http://localhost:8000');
        //server site
//        view()->share('site', 'https://ddc.moph.go.th/dncd/ncdstrategy/public');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
