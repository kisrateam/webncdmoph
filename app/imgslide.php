<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imgslide extends Model
{
	protected $table= 'imgslide';

	protected $fillable = [
		'name','path','details','url','active','edited_by'
	];
}
