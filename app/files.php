<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class files extends Model
{
	protected $table='files';

	protected $casts = [
		'name' => 'array',
		'files' => 'array'
	];

	protected $fillable = [
		'idfk','name','files','donwloaded'
	];
}
