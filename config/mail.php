<?php

return [
	'driver' => env('MAIL_DRIVER', 'smtp'),
	'host' => env('MAIL_HOST', 'smtp.gmail.com'),
	'port' => env('MAIL_PORT', 587),
	'from' => [
		'address' => env('MAIL_FROM_ADDRESS', 'kruautoemail@gmail.com'),
		'name' => env('MAIL_FROM_NAME', 'อีเมล์อัตโนมัติ'),
	],
	'encryption' => env('MAIL_ENCRYPTION', 'tls'),
	'username' => 'kruautoemail@gmail.com',
	'password' => 'tpvvajykbscydwvc',
	'sendmail' => '/usr/sbin/sendmail -bs',
	'markdown' => [
		'theme' => 'default',
		'paths' => [
			resource_path('views/vendor/mail'),
		],
	],
	'log_channel' => env('MAIL_LOG_CHANNEL'),
	'stream' => [
		'ssl' => [
			'allow_self_signed' => true,
			'verify_peer'       => false,
			'verify_peer_name'  => false,
		],
	]
];
