<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/rss', function () {
    return view('user.gl-rss');
})->name('rssfeed');


Route::get('/card-carousel', function () {
	return view('card-carousel')->with('imgslide', \App\imgslide::all());
})->name('card-carousel');


Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/team', 'HomeController@teamview')->name('team');

//Route::group(['middleware' => ['auth']], function () {
	Route::get('/home/topic', 'topicController@index')->name('topic');
	Route::get('/home/mytopic', 'topicController@mytopic')->name('mytopic');
	Route::get('/home/topic/addtopic', 'topicController@Showaddtopics')->name('showaddtopic');
	Route::post('/home/topic/addtopic', 'topicController@addtopic')->name('addtopic');
    Route::post('/home/topic/edittopic', 'topicController@edittopic')->name('edittopic');
    Route::post('/home/topic/addcomment', 'topicController@addcomment')->name('addcomment');
    Route::get('/home/topic/{id}', 'topicController@intopic')->name('intopic');
    Route::get('/home/topic/{id}/edit', 'topicController@showedittopic')->name('showedittopic');
    Route::post('/home/topic/report', 'topicController@report')->name('report');
    Route::post('/autocomplete','topicController@autocomplete');
    Route::post('/searchcontents','contentsController@searchcontents');
    Route::post('/searchcontentseng','contentsController@searchcontentseng');
//});

Route::get('/home/media', 'contentactController@indexuser')->name('media');
Route::get('/home/new', 'contentnewController@indexuser')->name('new');
Route::get('/home/media/{id}', 'contentactController@showuser')->name('showmedia');
Route::get('/home/result', 'resultController@indexuser')->name('result');
Route::get('/home/best', 'HomeController@best')->name('best');
Route::get('/home/content', 'contentsController@indexuser')->name('content');
Route::get("/home/contentre", function(){
    return View::make("user.contentre");
})->name('contentre');
Route::get("/home/contentf", function(){
    return View::make("user.contentf");
})->name('contentf');
Route::get("/home/contentn", function(){
    return View::make("user.contentn");
})->name('contentn');
Route::get('/home/eng', 'engController@indexuser')->name('eng');
Route::get('/showchangeprofile/{id}','HomeController@showchangeprofile')->name('showchangeprofile');
Route::get('/home/topic/deletetopic/{id}', 'topicController@deletetopic')->name('deletetopic');
Route::post('/changeprofile','HomeController@changeprofile')->name('changeprofile');

Route::group(['middleware' => ['admin']], function () {
	Route::get('/admin','adminController@index')->name('admin');
	Route::resource('/admin/contents','contentsController');
	Route::resource('/admin/contentact','contentactController');
	Route::resource('/admin/badwords','badwordController');
	Route::resource('/admin/menuheader','menuheaderController');
	Route::resource('/admin/best','bestController');
	Route::resource('/admin/contentnew','contentnewController');
	Route::get('/contentact/deletesome/{id}/{name}','contentactController@deletesome');
	Route::resource('/admin/link','linkController');
	Route::resource('/admin/eng','engController');
	Route::resource('/admin/result','resultController');
	Route::post('/admin/result/resulttable','resultController@resulttable')->name('resulttable');
	Route::resource('/admin/topic','topicBackController');
	Route::get('/admin/topic/report/{id}','topicBackController@showreports')->name('showreports');
	Route::get('/admin/topic/report/deleted/{id}/{type}','topicBackController@deletereports')->name('deletereports');
	Route::resource('/admin/team','teamController');
	Route::resource('/admin/imgslide','imgslideController');
	Route::post('/admin/banner','HomeController@banner')->name('banner');
	Route::delete('/admin/banner/{id}','HomeController@deletebanner')->name('deletebanner');
    Route::delete('/admin/contentsimg/{id}','contentsController@deleteimg');
    Route::get('/admin/homebanner', function () {
		return view('admin.homebanner');
	})->name('homebanner');
	Route::get('/admin/eng1', function () {
	    $data = \App\eng::where('category','Strategic plan')->get();
	    $img = \App\img::where('idfk','eng1')->first();
		return view('eng.index1',compact('data'),compact('img'));
	})->name('eng1');
	Route::get('/admin/eng2', function () {
        $data = \App\eng::where('category','Committee')->get();
        $img = \App\img::where('idfk','eng2')->first();
        return view('eng.index2',compact('data'),compact('img'));
    })->name('eng2');
	Route::get('/admin/eng3', function () {
        $data = \App\eng::where('category','M&E')->get();
        $img = \App\img::where('idfk','eng3')->first();
        return view('eng.index3',compact('data'),compact('img'));
    })->name('eng3');
	Route::get('/admin/eng4', function () {
        $data = \App\eng::where('category','Best practice')->get();
        $img = \App\img::where('idfk','eng4')->first();
        return view('eng.index4',compact('data'),compact('img'));
    })->name('eng4');
	Route::get('/userrole','userroleController@index')->name('userrole');
	Route::get('/userrole/delete/{id}','userroleController@delete');
	Route::get('/userrole/restore/{id}','userroleController@restore');
	Route::get('/userrole/changerole/{id}/{password}/{role}','userroleController@changerole');
	Route::get('/home/topic/closetopic/{id}', 'topicController@closetopic')->name('closetopic');
	Route::get('/home/topic/opentopic/{id}', 'topicController@opentopic')->name('opentopic');
});

Route::get('/files/{id}', 'contentactController@showpdf')->name('file.show');
Route::get('/showcontent/{id}', 'contentactController@showcontent')->name('showcontent');

Route::get('/admin/getchild/{name}','menuheaderController@getchild')->name('getchild');
Route::get('/admin/getchildinchild/{name}','menuheaderController@getchildinchild')->name('getchildinchild');

//Test page
Route::get("/test", function(){
    return View::make("test");
})->name('test');

//RSS feed
Route::get('feed/News', function() {
    $feedNews = App::make("feed");
    // cache the feed for 60 minutes with custom cache key "feedNewsKey"
    $feedNews->setCache(60, 'feedNewsKey1');
    // check if there is cached feed and build new only if is not
    if (!$feedNews->isCached()) {
    // creating rss feed with our most recent 20 records in news table
        $news = \DB::table('content_activity')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        //        $feedNews->logo = 'http://yoursite.tld/logo.jpg';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
        $feedNews->pubdate = $news[0]->updated_at; // date of latest news
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true); // true or false if true can use setTextLimit
//        $feedNews->setTextLimit(100); // maximum length of description text
        foreach ($news as $n) {
            // set item's title, author, url, pubdate, description and content
            $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
        }
    }
    $feedNews->ctype = "text/xml";
    return $feedNews->render('rss');
});
Route::get('feed/News_activity', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey2');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','ข่าวกิจกรรม')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
Route::get('feed/News_release', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey3');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','ข่าวประชาสัมพันธ์')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
Route::get('feed/News_plan', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey4');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','แผนการป้องกันและควบคุมโรคไม่ติดต่อ 5 ปี(พ.ศ. 2560-2564)')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
Route::get('feed/News_status', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey5');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','สถานการณ์ / ข้อมูล')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
Route::get('feed/News_result', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey6');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','ผลการดำเนินงาน ผลการติดตาม และประเมินผลแผน/ยุทธศาสตร์')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
Route::get('feed/News_media', function() {
    $feedNews = App::make("feed");
    $feedNews->setCache(60, 'feedNewsKey7');
    if (!$feedNews->isCached()) {
        $news = \DB::table('content_activity')->where('category','คู่มือแนวทาง สื่อ')->orderBy('updated_at', 'desc')->take(20)->get();
        $feedNews->title = 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข';
        $feedNews->description = 'ระบบเผยแพร่ข่าวสาร RSS';
        $feedNews->link = url('feedNews');
        $feedNews->setDateFormat('datetime');
        $feedNews->pubdate = $news[0]->updated_at;
        $feedNews->lang = 'th';
//        $feedNews->setShortening(true);
//        $feedNews->setTextLimit(100);
        if (count($news) > 0){
            foreach ($news as $n) {
                $feedNews->add($n->name, 'กองโรคไม่ติดต่อ กรมควบคุมโรค กระทรวงสาธารณสุข', url('/home/media').'/'.$n->id, $n->created_at, $n->details, '');
            }
        }
    }
    return $feedNews->render('rss');
});
